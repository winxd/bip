<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>▲TOP</a>
	</head>
	<body>

<div class='container'>
	<?php include 'header.php'; ?>

	<h1>Org. Setup</h1>
	<p class='ball'/>
	<h2 align='center'>CBETA 組織</h2>
	
	<div class='center'>
		<br>
		Chinese Buddhist Electronic Text Association<br>
		│<br>
		Committee─Executive member─Director─Exec. Advisor─Advisors group<br>
		┌─┴─┐<br>
		Deputy Director　　Deputy Director<br>
		└─┬─┘<br>
		Manager<br>
		│<br>
	</div>
	<div class='flex'>
		<div>
		┌Research & Development group<br>
		├Information services group<br>
		├Rare Characters group<br>
		├Input group<br>
		├Proofreading group<br>
		├Network group<br>
		├Distribution group<br>
		└Bookkeeping group<br>
		</div>
	</div>
		<br>
		<br>
<hr />
<h2>Scetch of the functions of these entities:</h2>

<ul>
	<li>Group of Advisors: Formed of Senior Monks and from Academic Institutions.</li>
	<li>Committee: This Association is setup as a committee.</li>
	<li>Ececutive Member: A representative of the committee to supervise the daily activities.</li>
	<li>Director: The committee appoints one member as director to direct all affairs of the Association.</li>
	<li>Deputy directors: Two deputy directors are appointed to assist the director.</li>
	<li>Manager: Coordination of the efforts of the different groups and public relations is the responsiblity of the Manager.</li>
	<li>Research & Development group: Propose the overall project structure, while taking in account latest developments in the information technology and academical world under a long-term perspective.</li>
	<li>Information services group: Development of programs for use on single computers and networks.</li>
	<li>Rare character group: Development of techniques to handle non-system characters.</li>
	<li>Input group: Responsible for the scheduling and implementation of input.</li>
	<li>Proofreading group: Onscreen and onpaper proofreading of the texts.</li>
	<li>Network group: Responsible for making the results of the work of different groups available on the internet.</li>
	<li>Distribution group: Responsible for marketing and distribution of the product.</li>
	<li>Bookkeeping: Management of the financial resources.</li>
</ul>

<p class='right'><span class='orange'>▶▶▶ </span>(Online) [<a href='http://www.cbeta.org/intro/organization.php' target='_blank'>List of attendants of the founding meeting (Feb 15th)</a>]</p>



<br/>
</div>
	</body>
</html>