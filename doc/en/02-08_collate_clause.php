<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>▲TOP</a>
	</head>
	<body>

<div class='container'>
<?php include 'header.php'; ?>

	<h1>The Star Mark (*) Used in the Critical Notes</h1> <!--佳瑜新寫-->
	<p class='ball'/>
	<h2 class='center'>大正藏校勘條目 * (星號)說明</h2>
	<p>The Star Mark (*) used in the text or critical notes means:</p>
	<div class='bibox'>
		<p>'So below,' 'So above,' or 'et passim' .
		<p>Ex：</p>
		<p>In the text：　世尊正[1]遊知</p>
		<p>In the Note shows： 遊三本俱作遍下同トスベキヲ　[1]遊＝遍【三】* (i.e. The Three Editions read 遍 for 遊, so also below)</p>
	</div>
	<br>
	<p class='bl center'>The Star Mark appeared in the CBReader as follows:</p>
	<p class='m2 bl'>1. CBETA Online 的呈現</p>
	<p class='m3'>例：</p>

	<p class='m3 navy'>《長阿含經》：「謂四[9]扼。欲[＊]扼．有[＊]扼．見[＊]扼．無明[＊]扼。復有四法。謂四無[＊]扼。無欲[＊]扼．無有[＊]扼．無見[＊]扼．無無明[＊]扼」(CBETA, T01, no.1, p. 51, a22-24)</p>

	<p align='center'><img border='0' src='pic/02-08-1.png'/>

	<p class='m3'>有 [＊] (星號) 的校勘在<b>校勘條目欄</b>裡可以呈現出全部 [＊] 的校勘註解，並快速找到其他星號，以及原來所屬的校勘條目項目。</p>
	<p class='m2 bl'>2. CBReader 的「引用複製」</p>
	<p class='m3'>如果選擇上面那一段文後，點選”引用複製” 功能，選擇校勘條目(連同 ＊ )一起複製，複製的結果如下：</p>
	<p align='center'><img border='0' src='pic/02-08-2.png'/>
<br><br>
</div>
	</body>
</html>
