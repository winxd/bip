<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>▲TOP</a>
	</head>
	<body>

<div class='container'>

	<?php include 'header.php'; ?>

	<h1>CBETA Chinese Electronic Tripiṭaka Collection</h1> 
	<p class='ball'/>
	<h2>Copyright</h2>
	<p>The "Chinese Buddhist Electronic Text Association" (CBETA) has been formally established on Feb. 15, 1998 with support from the Yinshun Foundation of North-America (http://www.yinshun.org) and Chung-Hwa Institute of Buddhist Studies (http://www.chibs.edu.tw) to create, maintain and distribute free of charge an electronic version of the Chinese Buddhist Triptitaka. Since February 2001, the Seeland Education Foundation administered the assistance to this project.</p>
	<br>
	<p>The CBETA Chinese Electronic Tripitaka is based on:</p>
	<ul>
		<li>Taishō Tripiṭaka 「大正新脩大藏經」( © Daizo Shuppansha 大藏出版株式會社) Vols. 1-55 & 85</li>
		<li>Shinsan Zokuzōkyō (Xuzangjing) 「卍新纂續藏經」(© Kokusho Kankokai 株式會社國書刊行會 ) Vols. 1-90</li>
		<li>Selected Chinese Buddhist Canon from other editions
			<ul>
				<li>Jiaxing Canon 「嘉興大藏經」(新文豐出版公司) Vols. 1-40</li>
				<li>Fangshan shijing 「房山石經」, Jin Edition of the Canon「趙城金藏」, Northern Yongle Edition of the Canon 「永樂北藏」, and others.</li>
			</ul>
		</li>
		<li>Selections from the Taipei National Central Library Buddhist Rare Book Collection 「國家圖書館善本佛典」 ( © 國家圖書館 )</li>
		<li>Chinese Translation of the Pāli Canon (Yuan Heng Temple edition) 「漢譯南傳大藏經」 ( © 元亨寺 ) Vols. 1-70</li>
		<li>Selected Buddhist materials not contained in the Tripiṭaka</li>
			<ul>
				<li>Buddhist Texts not contained in the Tripiṭaka 「藏外佛教文獻」 (© Fang Guangchang) Vols. 1-9</li>
				<li>Passages concerning Buddhism from the Official Histories 「正史佛教資料類編」 (© Du Doucheng )</li>
				<li>Selections of Buddhist Stone Rubbings from the Northern Dynasties 「北朝佛教石刻拓片百品」 (© Institute of History and Philology, Academia Sinica)</li>
				<li>Supplement to the Dazangjing 「大藏經補編」(© Lan Ji-fu) Vols. 1-36</li>
				<li>Zhongguo Fosi Shizhi Huikan 「中國佛寺史志彙刊」 (杜潔祥主編)</li>
				<li>Zhongguo fosizhi congkan 「中國佛寺志叢刊」 (張智等編輯)</li>
			</ul>
	</ul>
	<p>The right to input and distribute this database has been officially granted by the copyright holders Daizo Shuppansha, Kokusho Kankokai, Taipei National Central Library, Yuan Heng Temple, Mr. Du Doucheng, Mr. Fang Guangchang and Institute of History and Philology, Academia Sinica. We would like to express our gratitude for this.</p>
	<br>
	<p>Please observe the following while using this database:</p>
	<ol>
		<li>Permission to use this database is limited to individuals and organizations for non profit use. Any commercial use requires the prior written permission of both this Association and the copyright holders.</li>
		<li>Permission to distribute or otherwise use any material from this database on computer networks, CD-ROM or any other media is granted on condition that its content is not altered and the copyright notice as well as version information is kept intact and visible to the user.</li>
		<li>If you encounter any error in this material, please report it to this Association.</li>
		<li>This Association will not be responsible for any harm or damage that might result in the usage of this database.</li>
		<li>All rights for this database are reserved.</li>
	</ol>
	<br>
	<br>
	<p align='center'><b>Chinese Buddhist Electronic Text Association</b></p>
	<p align='center'>All Rights Reserved. Priceless, not to be sold. No Commercial Use Allowed.</p>
</div>
	</body>
</html>