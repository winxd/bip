<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
  <head>
  <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
  <link rel='stylesheet' type='text/css' href='style.css'>
  <a id='back2top' href='#' title='Back to Top'>▲TOP</a>
  </head>
  
  <body>

<div class='container'>
<?php include 'header.php'; ?>

<h1>Collection Notation</h1>
	<p class='ball'/>
<h2 align='center'>CBETA 電子佛典集成代碼</h2>
<p align='center'>（註：尚未使用的代碼為：E,O,V,Y）</p>

<table>
	<tr>
		<th>代碼</th>
		<th>典籍</th>
		<th>略稱<br>別稱</th>
		<th>略符</th>
		<th>紙本資訊</th>
	</tr>
	<tr>
		<td class='center'>A</td>
		<td>趙城金藏<br>Jin Edition of the Canon</td>
		<td class='center'>趙城藏</td>
		<td class='center'>【金藏】</td>
		<td>北京：北京圖書館出版社, 2008.</td>
	</tr>
	<tr>
		<td class='center'>B</td>
		<td>大藏經補編<br>Supplement to the Dazangjing</td>
		<td class='center'>補編</td>
		<td class='center'>【補編】</td>
		<td>藍吉富 主編 /<br>台北：華宇出版社, 1985.</td>
	</tr>
	<tr>
		<td class='center'>C</td>
		<td>中華大藏經（中華書局版）<br>Zhonghua Canon (Zhonghua shuju Edition)</td>
		<td class='center'>中華藏</td>
		<td class='center'>【中華】</td>
		<td>中華大藏經編輯局 編 /<br>北京：中華書局, 1984-1997.</td>
	</tr>
	<tr>
		<td class='center'>D</td>
		<td>國家圖書館善本佛典<br>Selections from the Taipei National Central Library Buddhist Rare Book Collectio</td>
		<td class='center'>國圖</td>
		<td class='center'>【國圖】</td>
		<td>國家圖書館特藏組 收藏</td>
	</tr>
	<tr>
		<td class='center'>F</td>
		<td>房山石經<br>Fangshan shijing</td>
		<td class='center'>&nbsp;</td>
		<td class='center'>【房山】</td>
		<td>中國佛教圖書文物館 編 /<br>北京：華夏出版社, 2000.</td>
	</tr>
	<tr>
		<td class='center'>G</td>
		<td>佛教大藏經<br>Fojiao Canon</td>
		<td class='center'>&nbsp;</td>
		<td class='center'>【佛教】</td>
		<td>佛教書局 編輯 /<br>台北：佛教書局, 1978.</td>
	</tr>
	<tr>
		<td class='center'>GA</td>
		<td>中國佛寺史志彙刊<br>Zhongguo Fosi Shizhi Huikan</td>
		<td class='center'>志彙</td>
		<td class='center'>【志彙】</td>
		<td>杜潔祥 主編 /<br>台北：宗青圖書出版公司, 1980-1994.</td>
	</tr>
	<tr>
		<td class='center'>GB</td>
		<td>中國佛寺志叢刊<br>Zhongguo fosizhi congkan</td>
		<td class='center'>志叢</td>
		<td class='center'>【志叢】</td>
		<td>白化文、張智 主編 /<br>揚州：廣陵書社, 2006.</td>
	</tr>
	<tr>
		<td class='center'>H(ZS)</td>
		<td>正史佛教資料類編<br>Passages concerning Buddhism from the Official Histories</td>
		<td class='center'>正史</td>
		<td class='center'>【正史】</td>
		<td>杜斗城 輯編 /<br>蘭州：甘肅文化出版社, 2006.</td>
	</tr>
	<tr>
		<td class='center'>I</td>
		<td>北朝佛教石刻拓片百品<br>Selections of Buddhist Stone Rubbings from the Northern Dynasties</td>
		<td class='center'>佛拓</td>
		<td class='center'>【佛拓】</td>
		<td>顏娟英 主編 /<br>台北：中央研究院歷史語言研究所, 2008.</td>
	</tr>
	<tr>
		<td class='center'>J</td>
		<td>嘉興大藏經（新文豐版）<br>Jiaxing Canon (Xinwenfeng Edition)</td>
		<td class='center'>嘉興藏</td>
		<td class='center'>【嘉興】</td>
		<td>徑山藏版版藏 /<br>台北：新文豐, 1987.</td>
	</tr>
	<tr>
		<td class='center'>K</td>
		<td>高麗大藏經（新文豐版）<br>Tripiṭaka Koreana (Xinwenfeng Edition)</td>
		<td class='center'>高麗藏</td>
		<td class='center'>【麗】</td>
		<td>高麗大藏經完刊推進委員會 原刊 /<br>台北：新文豐, 1982.</td>
	</tr>
	<tr>
		<td class='center'>L</td>
		<td>乾隆大藏經（新文豐版）<br>Qianlong Edition of the Canon (Xinwenfeng Edition)</td>
		<td class='center'>清藏、<br>龍藏、<br>乾隆藏</td>
		<td class='center'>【龍】</td>
		<td>台北：新文豐, 1991.</td>
	</tr>
	<tr>
		<td class='center'>M</td>
		<td>卍正藏經（新文豐版）<br>Manji Daizōkyō (Xinwenfeng Edition)</td>
		<td class='center'>卍正藏</td>
		<td class='center'>【卍正】</td>
		<td>京都．藏經書院 原刊 /<br>台北：新文豐, 1980.</td>
	</tr>
	<tr>
		<td class='center'>N</td>
		<td>漢譯南傳大藏經<br>Chinese Translation of the Pali Tipiṭaka</td>
		<td class='center'>巴利三藏</td>
		<td class='center'>【南傳】</td>
		<td>元亨寺漢譯南傳大藏經編譯委員會 / 高雄：元亨寺妙林出版社, 1995.</td>
	</tr>
	<tr>
		<td class='center'>P</td>
		<td>永樂北藏<br>Northern Yongle Edition of the Canon</td>
		<td class='center'>北藏</td>
		<td class='center'>【北藏】</td>
		<td>永樂北藏整理委員會 編 /<br>北京：線裝書局, 2000.</td>
	</tr>
	<tr>
		<td class='center'>Q</td>
		<td>磧砂大藏經（新文豐版）<br>Qisha Edition of the Canon (Xinwenfeng Edition)</td>
		<td class='center'>磧砂藏</td>
		<td class='center'>【磧砂】</td>
		<td>延聖院大藏經局 編輯 /<br>台北：新文豐, 1987.</td>
	</tr>
	<tr>
		<td class='center'>R</td>
		<td>卍續藏經（新文豐版）<br>Manji Zokuzōkyō (Xinwenfeng Edition)</td>
		<td class='center'>卍續藏</td>
		<td class='center'>&nbsp;</td>
		<td>京都．藏經書院 原刊 /<br>台北：新文豐, 1994.</td>
	</tr>
	<tr>
		<td class='center'>S</td>
		<td>宋藏遺珍（新文豐版）<br>Songzang yizhen (Xinwenfeng Edition)</td>
		<td nowrap='nowrap'>&nbsp;</td>
		<td class='center'>【宋遺】</td>
		<td>範成 輯補 /<br>台北：新文豐, 1978.</td>
	</tr>
	<tr>
		<td class='center'>T</td>
		<td>大正新脩大藏經<br>Taishō Tripiṭaka</td>
		<td class='center'>大正藏</td>
		<td class='center'>【大】</td>
		<td>大正新修大藏經刊行會 編 /<br>東京：大藏出版株式會社, Popular Edition in 1988.</td>
	</tr>
	<tr>
		<td class='center'>U</td>
		<td>洪武南藏<br>Southern Hongwu Edition of the Canon</td>
		<td class='center'>初刻南藏</td>
		<td class='center'>【洪武】</td>
		<td>四川省佛教協會 /<br>成都：四川省佛教協會, 1999.</td>
	</tr>
	<tr>
		<td class='center'>W(ZW)</td>
		<td>藏外佛教文獻<br>Buddhist Texts not contained in the Tripiṭaka</td>
		<td class='center'>藏外</td>
		<td class='center'>【藏外】</td>
		<td>方廣錩 主編 /<br>北京：宗教文化出版社, 1995-2003.</td>
	</tr>
	<tr>
		<td class='center'>X</td>
		<td>卍新纂大日本續藏經<br>Manji Shinsan Dainihon Zokuzōkyō</td>
		<td class='center'>新纂卍續藏</td>
		<td class='center'>【卍續】</td>
		<td>河村照孝 編集 /<br>東京：株式會社國書刊行會, 1975-1989.</td>
	</tr>
	<tr>
		<td class='center'>Z</td>
		<td>卍大日本續藏經<br>Manji Dainihon Zokuzōkyō</td>
		<td class='center'>&nbsp;</td>
		<td class='center'>&nbsp;</td>
		<td>京都：藏經書院, 1905-1912.</td>
	</tr>
</table>
<br><br>
</div>
  </body>
</html>