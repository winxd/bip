<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>▲TOP</a>
	</head>

	<body>

<div class='container'>
	<?php include 'header.php'; ?>

	<h1>Origin</h1>
	<p class='ball'/>
	<h2 class='center'>CBETA 緣起</h2>
	
<h2>Remote cause :</h2>
<p class='m2'>In the last years, through the contribution of many people, a large number of Buddhist sutras appeared on electronic networks, this was due to a feverish wave of input of electronic Buddhist scriptures. The goal of everybody was to make this available on the network for free use, so these files have been made available for download free of charge through FTP, or for convenient browsing through GOPHER or the World-Wide Web. More recently, it has even become possible to do simple searches of Buddhist texts on the Web. The next step was to put these files together to form electronic books and make the appearance on the screen more attractive. All those who put effort in this hoped to increase the availability of Buddhist scriptures on the net to let more people come in contact with the teachings of Buddhism and to use the means of computers and networks to open up a new way of reading the sutras.</p>

<h2>Immediate cause :</h2>
<ol>
	<li>As result of a message from Mr. Hsiao Chenkuo (蕭鎮國), who offered to donate computer files of 25 volumes of text in CCCII encoding, the group '25T' was formed on November 6th, 1997 and the Center of Buddhist Studies, National Taiwan University took the responsibility to further handle this case.</li>
	<li>Friends from the 'Online electronic Buddhist texts BBS' (buda-tech) already hat a plan to input the Buddhist scriptures and had started to proceed with input.</li>
	<li>The groupt '25T' already began to proceed with a large scale computerization of Buddhist texts.</li>
</ol>

<h2>Foundation :</h2>
<p class='m2'>With Professor Ven. Heng-Ching (釋恆清) from National Taiwan University's Center for Buddhist Studies taking the lead and providing initial funding and logistic, a grant provided by the Yin-Shun Foundation of North-America (北美印順導師基金會) and much help from the Chung-Hwa Institute of Buddhist Studies (中華佛學研究所), made it possible to officially form the 'Chinese Buddhist Electronic Text Association (CBETA)' on February 15th, 1998 at Fagu Shan's Anhe-Yuan.</p>

<h2>Additional causes :</h2>
<ol>
	<li>Buda-Tech Discussion Group at the Lion's roar BBS of NTU and Dear Garden BBS and the discussions and experiences made there with respect to the creation of Buddhist electronic resources, as well as the Mailing list set up by Mr. Tseng Guo-Feng (曾國豐). Many electronic versions of sutras as well as technology has developed out of this.</li>
	<li>Electronic Buddhist Texts Work Group (EBTWG, 電子佛典編輯小組): This group, funded by Mr. Hsu Yan-Hui (徐言輝) and his friends mostly used sanning and OCR of the Fojiao dazangjing (佛教大藏經) edition to systematically create files of Buddhist sutras.</li>
	<li>25T Group: Under the leadership of NTU's Center for Buddhist Studies, this group took the responsibility for making the 25 volumes of the Buddhist canon, which have been input on behalf of Mr. Hsiao Chen-kuo (蕭鎮國) available to the general public. This became a forerunner of CBETA.</li>
	<li>Rare characters group: This group has been specially concerned with harmonizing the different attempts to work with characters that can not be displayed in a system character set.</li>
</ol>

<p class='right'><span class='orange'>▶▶▶ </span>(Online) [<a href='http://homepage.ntu.edu.tw/~ntubudda/info/index.html' target='_blank'>Center for Buddhist Studies]</a></p>



<br><br/>
</div>
	</body>
</html>