<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>▲TOP</a>
	</head>
	<body>

<div class='container'>
	<?php include 'header.php'; ?>

	<h1>Donation Account</h1>
	<p class='ball'/>
	<h2>劃撥捐款帳號：</h2>
		<p class='m2'>郵政劃撥帳號：19538811</p>
		<p class='m2'>戶名：財團法人西蓮教育基金會</p>
		<p class='m2'>此為 CBETA 專戶，若欲指定特殊用途者，請特別註明，我們會專款專用。</p>
		<hr>
	<h2>Online Credit Card / PayPal donation</h2>
		<p class='m2'><a href='http://www.paypal.com' target='_blank'>PayPal</a>  is an online system of a global payment solution. CBETA uses its service to provide the uses to donate by using the credit cards or PayPal account to support the CBETA project.</p>
		<p class='m2'>Since the donation made is in US currency, hence all the receipts will be issued in the US dollars consequently. However for the domestic donators, a Chinese official receipt will also be made according to the foreign exchange rate for the purpose of tax deduction.</p>
		<p class='m2'><span class='orange'>&#9654 </span>For the first time users please visit the
			<a href='http://www.cbeta.org/donation/paypal.htm' target='_blank'>Instruction</a>(in Chinese)</p>
		<p class='m2'><span class='orange'>&#9654 </span>
			<a href='https://www.paypal.com/xclick/business=donation@cbeta.org&item_name=Chinese+Buddhist+Electronic+Text+Association+(CBETA)&page_style=Primary&no_shipping=2&return=http%3A//www.cbeta.org&cancel_return=http%3A//www.cbeta.org&tax=0&currency_code=USD' target='_blank'><b>For the online Credit Card / PayPal donation</b>&nbsp;
			<img src='http://www.cbeta.org/cbreader/help/images/paypal.gif' alt='劃撥帳號 / 信用卡 / PayPal / 支票捐款' width='62' height='31' border='0' align='top'>
			<img src='http://www.cbeta.org/cbreader/help/images/donation.gif' alt='劃撥帳號 / 信用卡 / PayPal / 支票捐款' width='73' height='44' border='0' align='top'></a></p>
		<hr>
		<a name='check' id='check'></a>
	<h2>支票捐款抬頭：</h2>
		<p class='m2'>支票捐款的大德，支票抬頭請填寫「財團法人西蓮教育基金會」。</p>
		<p class='m2'>For Donation:</p>
		<p class='m2'>CBETA is part of Seeland Educational projects, any donation please entitle to <b>The Seeland Education Foundation.</b></p>
<br><br/>
</div>
	</body>
</html>