<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>▲TOP</a>
	</head>
	<body>

<div class='container'>
<?php include 'header.php'; ?>

	<h1>Abbreviations of Tripitaka Editions Appeared in Taisho Notes</h1>
	<p class='ball'/>	
	<h2 align='center'>版本略符</h2>
	<p align='center'>T01-T55, T85</p>
	
	<p class='m2'>本表第一欄「大正藏略符」為《大正藏》第一冊至第五十五冊暨第八十五冊書末所附略符表整合而成，凡《大正藏》校勘條目中闕略版本者以「闕略」二字註記。第二欄「CBETA略符」除包含《大正藏》所有略符表中的略符外，也把各冊校勘中提到而《大正藏》略符表闕略的版本增訂在內，並於「版本註．例」欄舉其實例，於「說明」欄兼載出現冊別。</p>

<table>
	<tr>
		<th class='center'>大正藏<br>略符</th>
		<th class='center'>CBETA<br>略符</th>
		<th class='center'>版本<br>註．例</th>
		<th class='center' colspan='2'>說　　　　　明</th>
	</tr>
	<tr>
		<td class='center'><img border='0' height='46' src='pic/san.gif' width='46' /></td>
		<td class='center'>【三】</td>
		<td>宋，元，明三本</td>
		<td colspan='2' width='344'>(The &#39;Three Editions&#39; of the Sung, the Yuan and the Ming dynasties)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='44' src='pic/song.gif' width='48' /></td>
		<td class='center'>【宋】</td>
		<td>宋本</td>
		<td>南宋思溪藏 (The &#39;Sung Edition&#39; A. D. 1239)</td>
		<td rowspan='3' width='178'>These Dates are subject to re-examination.</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='40' src='pic/yuan.gif' width='43' /></td>
		<td class='center'>【元】</td>
		<td>元本</td>
		<td>元大普寧寺藏 (The &#39;Yuan Edition&#39; A. D. 1290)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='38' src='pic/ming.gif' width='43' /></td>
		<td class='center'>【明】</td>
		<td>明本</td>
		<td>明方冊藏 (The &#39;Ming Edition&#39; A. D. 1601)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='34' src='pic/li.gif' width='37' /></td>
		<td class='center'>【麗】</td>
		<td>麗本</td>
		<td colspan='2' width='344'>高麗海印寺本 (The &#39;Kao-Li Edition&#39; A. D. 1151)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='35' src='pic/li-b.gif' width='40' /></td>
		<td class='center'>【麗乙】</td>
		<td>麗本別刷</td>
		<td colspan='2' width='344'>(Another print of the Kao-Li Edition)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='43' src='pic/sheng.gif' width='40' /></td>
		<td class='center'>【聖】</td>
		<td>正倉院聖語藏本（天平寫經）</td>
		<td colspan='2' width='344'>(The Tempyō Mss. [A. D. 729-] and the Chinese Mss. of the Sui [A. D. 581-617] and Tang [A. D. 618-822] dynasties, belonging to the Imperial Treasure House Shōsō-in at Nara, specially called Shōgo-zō)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='34' src='pic/sheng-b.gif' width='42' /></td>
		<td class='center'>【聖乙】</td>
		<td>正倉院聖語藏本別寫</td>
		<td colspan='2' width='344'>(Another copy of the same)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='35' src='pic/gong.gif' width='37' /></td>
		<td class='center'>【宮】</td>
		<td>宮內省圖書寮本（舊宋本）</td>
		<td colspan='2' width='344'>(The Old Sung Edition [A. D. 1104-1148] belonging to the Library of the Imperial Household)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='36' src='pic/de.gif' width='38' /></td>
		<td class='center'>【德】</td>
		<td>大德寺本</td>
		<td colspan='2' width='344'>(The Tempyō Mss. of the monastery &#39;Daitoku-ji&#39;)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='37' src='pic/wan.gif' width='40' /></td>
		<td class='center'>【万】</td>
		<td>萬德寺本</td>
		<td colspan='2' width='344'>(The Tempyō Mss. of the monastery &#39;Mantoku-ji&#39;)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='33' src='pic/shi.gif' width='37' /></td>
		<td class='center'>【石】</td>
		<td>石山寺本</td>
		<td colspan='2' width='344'>(The Tempyō Mss. of the monastery &#39;Ishiyama-dera&#39;)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='35' src='pic/zhi.gif' width='38' /></td>
		<td class='center'>【知】</td>
		<td>知恩院本</td>
		<td colspan='2' width='344'>(The Tempyō Mss. of the monastery &#39;Chion-in&#39;)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='34' src='pic/ti.gif' width='37' /></td>
		<td class='center'>【醍】</td>
		<td>醍醐寺本</td>
		<td colspan='2' width='344'>(The Tempyō Mss. of the monastery &#39;Daigo-ji&#39;)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='32' src='pic/he.gif' width='35' /></td>
		<td class='center'>【和】</td>
		<td>仁和寺藏本</td>
		<td colspan='2' width='344'>(Ninnaji Mss. by Kōūkai and others. C. 800. A. D.)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='32' src='pic/dong.gif' width='34' /></td>
		<td class='center'>【東】</td>
		<td>東大寺本</td>
		<td colspan='2' width='344'>(The Tempyō Mss. of the monastery &#39;Tōdai-ji&#39;)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='30' src='pic/zhong.gif' width='32' /></td>
		<td class='center'>【中】</td>
		<td>中村不折氏藏本</td>
		<td colspan='2' width='344'>(Mr. Nakamura&#39;s Mss. from Tun-huang)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='35' src='pic/jiu.gif' width='35' /></td>
		<td class='center'>【久】</td>
		<td>久原文庫本</td>
		<td colspan='2' width='344'>(The Tempyō Mss. belonging to the Kuhara Library)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='29' src='pic/sen.gif' width='35' /></td>
		<td class='center'>【森】</td>
		<td>森田清太郎氏藏本</td>
		<td colspan='2' width='344'>(The Tempyō Mss. owned by Mr. Seitaro Morita)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='32' src='pic/dun.gif' width='31' /></td>
		<td class='center'>【敦】</td>
		<td rowspan='3'>敦煌本</td>
		<td colspan='2' rowspan='3' width='344'>敦煌出土藏經 (Stein Mss. from Tun-huang)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='31' src='pic/dun-b.gif' width='30' /></td>
		<td class='center'>【敦乙】</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='30' src='pic/dun-c.gif' width='32' /></td>
		<td class='center'>【敦丙】</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='33' src='pic/fu.gif' width='36' /></td>
		<td class='center'>【福】</td>
		<td rowspan='2'>西福寺本</td>
		<td colspan='2' rowspan='2' width='344'>(The Tempyō Mss. of the monastery &#39;Saifuku-ji&#39;)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='32' src='pic/fu-b.gif' width='33' /></td>
		<td class='center'>【福乙】</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='33' src='pic/bo.gif' width='32' /></td>
		<td class='center'>【博】</td>
		<td>東京帝室博物館本</td>
		<td colspan='2' width='344'>(The Chinese Mss. of the Tang dynasty belonging to the Imperial Museum of Tokyo)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='30' src='pic/suo.gif' width='30' /></td>
		<td class='center'>【縮】</td>
		<td>縮刷本</td>
		<td colspan='2' width='344'>縮刷大藏經 Tokyo edition (small typed)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='33' src='pic/jin.gif' width='36' /></td>
		<td class='center'>【金】</td>
		<td>金剛藏本</td>
		<td colspan='2' width='344'>(The Mss. preserved in the Kongō-zō Library, Tōji, Kyoto)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='32' src='pic/gao.gif' width='34' /></td>
		<td class='center'>【高】</td>
		<td>高野版本</td>
		<td colspan='2' width='344'>(The Edition of Kooya-san, C. 1250 A. D.)</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【南藏】</td>
		<td>例：T03p0110 [9]	明註曰夫南藏作王</td>
		<td colspan='2' width='344'>出現冊別：T03、T09、T11、T12、T15、T16、T17、T25、T26、T27、T28、T32、T50、T51、T52、T53、T54、T55</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【北藏】</td>
		<td>例：T09p0500 [1]	明註曰人北藏作及</td>
		<td colspan='2' width='344'>出現冊別：T09、T10、T11、T16、T17、T25、T28、T30、T32、T52、T53</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【獅谷】</td>
		<td>例：T11p0926 [1]	栗獅谷本作粟</td>
		<td colspan='2' width='344'>出現冊別：T11</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【流布本】</td>
		<td>例：T12p0265 [5]	果＝異【三】，流布本亦同</td>
		<td colspan='2' width='344'>出現冊別：T12、T29、T31</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【大曆】</td>
		<td>例：T16p0726 [1]	明註曰三毘陀大曆新譯作四吠陀</td>
		<td colspan='2' width='344'>出現冊別：T16</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【日光】</td>
		<td>例：T33p0854 [7]	經文廣解四字日光本作夾註</td>
		<td colspan='2' width='344'>出現冊別：T33</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【明異】</td>
		<td>例：T02p0353 [6]	敏＝敦【明異】</td>
		<td colspan='2' width='344'>出現冊別：T02、T05、T06、T13、T14、T15</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【內】</td>
		<td>參考「<a href='02-11_ver-e.html#nei' target='_blank' rel='nofollow'>版本略考</a>」</td><!--待更新連結為02-11_版本略考.html-->
		<td colspan='2' width='344'>出現冊別：T32、T48、T51</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【別】</td>
		<td>參考「<a href='02-11_ver-e.html#bie' target='_blank' rel='nofollow'>版本略考</a>」</td><!--待更新連結為02-11_版本略考.html-->
		<td colspan='2' width='344'>出現冊別：T13</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【南】</td>
		<td>參考「<a href='02-11_ver-e.html#nan' target='_blank' rel='nofollow'>版本略考</a>」</td><!--待更新連結為02-11_版本略考.html-->
		<td colspan='2' width='344'>出現冊別：T03</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【宮乙】</td>
		<td>參考「<a href='02-11_ver-e.html#gongyi' target='_blank' rel='nofollow'>版本略考</a>」</td><!--待更新連結為02-11_版本略考.html-->
		<td colspan='2' width='344'>出現冊別：T14</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【敦方】</td>
		<td>參考「<a href='02-11_ver-e.html#dunfang' target='_blank' rel='nofollow'>版本略考</a>」</td><!--待更新連結為02-11_版本略考.html-->
		<td colspan='2' width='344'>出現冊別：T09</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【敦內】</td>
		<td>參考「<a href='02-11_ver-e.html#dunnei' target='_blank' rel='nofollow'>版本略考</a>」</td><!--待更新連結為02-11_版本略考.html-->
		<td colspan='2' width='344'>出現冊別：T09</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【聖丙】</td>
		<td>參考「<a href='02-11_ver-e.html#shengbing' target='_blank' rel='nofollow'>版本略考</a>」</td><!--待更新連結為02-11_版本略考.html-->
		<td colspan='2' width='344'>出現冊別：T25、T26</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【西】</td>
		<td>參考「<a href='02-11_ver-e.html#xi' target='_blank' rel='nofollow'>版本略考</a>」</td><!--待更新連結為02-11_版本略考.html-->
		<td colspan='2' width='344'>出現冊別：T16、T24</td>
	</tr>
	<tr>
		<td class='center' rowspan='2'>闕略</td>
		<td class='center'>【原イ】</td>
		<td rowspan='2'>例：T40p0721, [45]	合＝念ぃ【甲】，＝含 ィ【甲】</td>
		<td colspan='2' rowspan='2' width='344'>校勘條目中的日文略符ィ，就是某對校本，CBETA校勘版以【某本ィ】表示；日文略符ぃ是另一對校本，CBETA校勘版以【某本ぃ】表示。</td>
	</tr>
	<tr>
		<td class='center'>【原ぃ】</td>
	</tr>
</table>

<p class='center'>註：本頁轉寫字使用 Lucida Sans Unicode 字型</p>
<p class='bl'>CBETA按：</p>
<p class='m2'>森田清太郎氏藏本，說明中原作「森田清太郎氏藏本 (The Tempyoo Ms. owned by Mr. Seitaro Morita)	」，依上下例判	Ms.當為	Mss.字母脫略，說明中逕改	Ms.作	Mss.。</p>

<br><br>
</div>
	</body>
</html>
