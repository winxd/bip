<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>▲TOP</a>
	</head>
	<body>

<div class='container'>
<?php include 'header.php'; ?>

	<h1>Copy with Citation Reference</h1>
	<p class='ball'/>
	<h2 class='center'>CBETA 引用複製</h2>

	<p>The Copy and Cite function in this CBReader provides the reference page numbers when citing from the CBETA Text.</p>
	<p>CBETA 的引用複製是在使用者複製一小段經文時，同時將經文的相關出處一併帶出來，讓日後讀到此段經文的人，可以很快知道經文的來源及出處。</p>
	<p>And in order to keep the reference records, the citing of the Electronic Taisho and Zokuzokyo Tripitaka are different, according to the versions:</p>
	<p>除了《卍新纂續藏經》略有不同之外，《大正新脩大藏經》及其他各藏經出處是依冊數、經號、頁數、欄數、行數 之順序 紀錄，例如： (T30, no. 1579, p. 517, b6~17) 。</p>
	<p>這表示此段經文出自於大正藏第 30 冊，1579 經，第 517 頁第二欄的第六至十七行。</p>
	<p>而《卍新纂續藏經》出處的記錄，採用</p>
	<p>《卍新纂大日本續藏經》（ X: Xuzangjing 卍新纂續藏。東京：國書刊行會）、<br>
			《卍大日本續藏經》（ Z: Zokuzokyo 卍續藏。京都：藏經書院）、<br>
			《卍續藏經．新文豐影印本》（ R: Reprint 。台北：新文豐）</p>
	<p>以上三種版本並列，例如： (CBETA, X78, no. 1553, p. 420, a4-5 // Z 2B:8, p. 298, a1-2 // R135, p. 595, a1-2) 。</p>
	<p>關於這三個版本更詳細的資料，請見底下「<a href='#zrx'>卍續藏三種版本及其對照說明</a>」。</p>
	<br>
	<p class='bl'>The following sample can be used as an citation abbreviation explanation in the reference:</p>
	<p>論文寫作引用 CBETA 經文時，可參考底下格式，直接放在論文&#65339;參考文獻&#65341;前 ，說明 CBETA 引用複製的格式：</p>
	<div class='bibox'>
		<p>《大正新脩大藏經》與《卍新纂續藏經》的資料引用是出自「中華電子佛典協會」 (Chinese Buddhist Electronic Text Association, 簡稱 CBETA) 的電子佛典系列光碟 (2016)。引用《大正新脩大藏經》出處是依冊數、經號、頁數、欄數、行數之順序紀錄，例如：(T30, no. 1579, p. 517, b6~17)。引用《卍新纂續藏經》出處的記錄，採用《卍新纂大日本續藏經》（X: Xuzangjing 卍新纂續藏。東京：國書刊行會）、《卍大日本續藏經》（Z: Zokuzokyo 卍續藏。京都：藏經書院）、《卍續藏經．新文豐影印本》（R: Reprint。台北：新文豐）三種版本並列，例如：(CBETA, X78, no. 1553, p. 420, a4-5 // Z 2B:8, p. 298, a1-2 // R135, p. 595, a1-2)。</p>
	</div>

	<p>若引用之藏經不同，請依實際情況調整。</p>
	<br>
	<br>
	<a name='zrx'><h1>How to Copy and Cite the CBETA Electronic Xuzangjing (Zokuzokyo)</h1></a>
	<p align='center' class='navy22'>──instruction for citing the three different editions of Xuzangjing in the CBReader──</p>
	<div class='bibox'>
		<p>This CBETA Electronic Tripitaka Collection contains the reference page numbers of the following three different editions of Xuzangjing:</p>
		<p class='m2 red'>Z:&nbsp;Zokuzokyo&nbsp;
			<span class='navy'>The Dainihon Zokuzokyo, Kyoto: Zokuzokyo Shoin.</span><br/>
			<span class='navy'>　　　　　　《卍大日本續藏經》卍續藏。京都：藏經書院。</p>
		<p class='m2 red'>R:&nbsp;Reprint&nbsp;&nbsp;&nbsp;</span>
			<span class='navy'> Xuzangjing (Xinwenfeng Reprint), Taipie: Hsin-wen-feng ch'u-pan-she.</span><br/>
			<span class='navy'>　　　　　　《卍續藏經．新文豐影印本》。台北：新文豐。</span></p>
		<p class='m2 red'>X:&nbsp;Xuzangjing</span>
			<span class='navy'>The Shinsan Dainihon Zokuzokyo, Tokyo: Kokusho Kankokai.</span><br/>
			<span class='navy'>　　　　　　《卍新纂大日本續藏經》(卍新纂續藏)。東京：國書刊行會。</span></p>
		<p><span style='color:#330066'>In this CBReader the Copy and Cite function of the Electronic Xuzangjing keeps the reference records of the above three different editions in the following order:</span></p>
		<p><span style='color:#330066'>The first is The Shinsan Dainihon Zokuzokyo (X), the second is The Dainihon Zokuzokyo (Z), and the last is the Xuzangjing (Xinwenfeng Reprint) (R), i.e.: </span><span class='orange'> (CBETA, X78, no. 1553, p. 420, a4-5 //	
			Z 2B:8, p. 298, a1-2 // R135, p. 595, a1-2)</span></p>
		<p><span style='color:#330066'>The copy and cite reference numbers of these three different references will be explained:</span></p>
	</div>
<br>
<br>
	<div class='bibox_y'>
		<h2 align='center'>《卍大日本續藏經》（《卍續藏》）</h2>
		<p>
			<span class='orange bl'>Z: Zokuzokyo</span><br>
			<span class='navy'><i>The Dainihon Zokuzokyo,</i><br>
			Kyoto: Zokuzokyo Shoin.</p>
	</div>
	<br/>
	<p>The original Kyoto edition is in a traditional stitched and boxed format. There are 3 series (編), 150 sets (套), and 750 fascicles (冊), totaling:</p>
	<p class='m2 navy'>The First Series: 1-95 sets  (第一編：1-95套 )<br>
										The Second Series: 1-32 sets  (第二編：1-32套 )<br>
										The addition to the Second Series: 1-23 sets  (第二編乙：1-23套 )</p>
	<p>The format of the Zokuzokyo is that every 5 fascicles (stitched volumes, 冊)  are packed in a case (函, as a set/套) , and every set has 500 to 550 leaves (葉, 丁in Japanese) , and each leaf has 4 panels (columns a~d, 欄).</p>
	<p>Therefore, 「Z 2B:8, p. 298, a1-2」 means:</p> 
		<p class='m2 navy'>The Dainihon Zokuzokyo (Zokuzokyo), the addition to the Second Series (or Part II B), set 8, leaf 298, column a, line 1-2.</p>

		<p class='m2 orange'>(《大日本續藏經》第二編乙．第8套．第298葉．第一欄．1-2行。)</p>
		<br>


		<div class='bibox_y'>
			<h2 align='center'>《卍續藏經》(台灣新文豐影印本)</h2>
			<p>
				<span class='orange bl'>R: Reprint</span><br>
				<span class='navy'><i>Xuzangjing (Xinwenfeng Reprint),</i><br>
					Taipie: Hsin-wen-feng ch'u-pan-she.</span></p>
		</div>

		<p>The Taiwan Xinwenfeng Reprint《卍續藏經》was based on the the original Kyoto edition, The Dainihon Zokuzokyo《 卍大日本續藏經》, but the format of the panels was changed to  2 columns per page comprising 150 volumes.
Every volume of the Reprint equals one set of the original Zokuzokyo:</p>
		<p class='m2 navy'>R001 ~ 095 ==> The First Series: 1-95 sets (第一編 1-95套, )<br>
												R096 ~ 127 ==> The Second Series: 1-32 sets (第二編 1-32套 )<br>
												R128 ~ 150 ==> The addition to the Second Series: 1-23 sets (第二編乙 1-23套 )</p>
		<p>Therefore,  R135, p. 595, a1-2 means:</p>
		<p class='m2'>Xuzangjing (Xinwenfeng Reprint), volume 135, page 595, column a, line 1-2 .</p>
		<p class='m2 orange'>《卍續藏經》（台灣新文豐影印本）第135冊．595頁．上欄．1-2行。</p>
		</p>
		<p class='navy'>This is the same as「Z 2B:8, p. 298, a1-2」: The Dainihon Zokuzokyo <span class='orange'>(Zokuzokyo), the addition to the Second Series (or Part II B), the 8th set, leaf 298, column a, line 1-2.</span></p>
		<br>

	<div class='bibox_y'>
		<h2 align='center'>《卍新纂續藏經》</h2>
		<p>
			<span class='orange bl'>X: Xuzangjing</span><br>
			<span class='navy'><i>The Shinsan Dainihon Zokuzokyo,</i><br>
			Tokyo: Kokusho Kankokai.</span></p>
	</div>
	<p><span class='navy'>The Shinsan zokuzokyo is the revised edition of the original Zokuzokyo published by Kokusho Kankokai</span> (國書刊行會, Taibei Reprint: Baima jingshe yinjing hui 白馬精舍印經會), printed in 3 columns per page, 90 volumes -- including an "index" (索引) volume and a "catalogue" (目綠) volume.</p>

	<p>Therefore, 「X78, no.1553, p. 420, a4-5」means:</p>

	<p class='m2 navy'>The Shinsan Dainihon Zokuzokyo, volume 78, sutra no. 1553, page 420, column a, line 4-5.</p>

	<p class='m2 orange'>《卍新纂大日本續藏經》第78冊．1553經．420頁．上欄．4-5行</p>
	<p>This is the same as the above mentioned:「 Z 2B:8, p. 298, a1-2」or「R135, p. 595, a1-2」.</p>

	<p>※Some additional sutras appear in this new edition only. Accordingly, the cited resource shows X edition.</p>
<br><br>
<p align='center'><span style='font-size:22px; font-weight:bold; color:#663300'>計算方法</span></p>

<hr color='#330066'>
		<div class='bibox_y'> 
			<h2 align='center'>X&nbsp;=&gt;&nbsp;R</h2>
		</div>

	<p>目前 CBETA 經文資料庫，對於卍續藏的引用出處，以 X: 《卍新纂大日本續藏經》為基礎來記錄。</p>
	<p>同一經文在 X 版本中為「每頁三欄（上、中、下）、每欄 24行、每行 20字」，在 R 版本中為「每頁兩欄（上、下）、每欄 18行、每行 20字」。兩者每行字數一致。所以同一經文中，只要是有文字的行，都可以找到 X 與 R 的對應行號。如：</p>
		<p class='m2'>
			<span class='orange'>X78n1553_p0420a10║R135_p0595a05<br/>
				X78n1553_p0420a11║R135_p0595a06<br/> 
				X78n1553_p0420a12║R135_p0595a07<br/> 
				X78n1553_p0420a13║R135_p0595a08<br/> 
				X78n1553_p0420a14║R135_p0595a09</span>
		</p> 
	<p>但在卷或標題交界處， X 比 R 多出一些空白行，這些 X 空白行就沒有對應的 R 頁數及行號。如：</p>
		<p class='m2'>
			<span style='color:#FF9900'>X78n1553_p0420a01║R135_pxxxxxxx<br/>
				X78n1553_p0420a02║R135_pxxxxxxx<br/> 
				X78n1553_p0420a03║R135_pxxxxxxx</span><br/>
			<span class='orange'>X78n1553_p0420a04║R135_p0595a01<br/>
				X78n1553_p0420a05║R135_p0595a02</span>
		</p>
	<p>上述的對照表是以程式計算，再加以人工除錯編輯而成。當中的格式，如「R135_p0595a01」 ，表示：卍續藏新文豐影印本第 135冊．595頁．上欄．1行。</p>
<br><hr/><br>
	<div class='bibox_y'>
		<h2 align='center'><a href='#index2-02' name='text2-02'>R&nbsp;=&gt;&nbsp;Z</a></h2>
	</div>
	<p>Z 分成三編，每一編含若干套。Z 的一套相當 R 的一冊，R 的冊數與 Z 的對照如下：</p>
		<p class='m2'>
		 R001~R095 => Z 第一編	 1-95套<br/> 
		 R096~R127 => Z 第二編	 1-32套<br/> 
		 R128~R150 => Z 第二編乙 1-23套<br/> 
		</p>
	<p>Z 的套中還分冊，但頁碼是以套為單位，所以冊可以省略。至於頁碼的對照，Z 是算 '張'(leaf)的，Z 的一張等於 R 的兩頁，所以 Z 的一張有四欄。也就是 R 的頁碼除以2，再加上餘數，就是 Z 的頁碼。R 的奇數頁是 Z 的 a,b 欄，而偶數頁是 Z 的 c,d 欄。行號，R 跟 Z 一樣。</p>
	<p>如此規則的對照邏輯，正可以透過程式精確運算出來。如：</p>
		<p class='m2'>
		<span class='orange'>R135_p0595a01║Z2B:8_p0029a01<br/>
			R135_p0595a02║Z2B:8_p0029a02<br/>
			R135_p0595a03║Z2B:8_p0029a03<br/>
			R135_p0595a04║Z2B:8_p0029a04 </span>
		</p> 
	<p>如果要計算冊數，Z、R 兩者的冊數對照關係為：</p>
		<p class='m2'>
			Z 1:19:418a （Z 第一編第19套第5冊）	 => R019（R 第19冊）<br/>
			Z 2:19:16c	（Z 第二編第19套第1冊）	 => 19+95=R114（R 第114冊）<br/>
			Z 2B:19:255d（Z 第二編乙第19套第3冊） => 19+127=R146（R 第146冊）<br/>
		</p>
	<p>許多學者習慣加上冊數，如：</p>
		<p class='m2'>
			<span class='orange'>Z 1:19:5:418a&nbsp;（第一編第19套第5冊,	
				第418葉, 第一欄）<br/> 
				Z 2:19:1:16c&nbsp;&nbsp;（第二編第19套第1冊, 第16葉, 第三欄）<br/> 
				Z 2B:19:3:255d（第二編乙第19套第3冊, 第255葉, 第四欄）</span>
		</p> 
	<p>但因一套之中各葉依序編號，不同套則各自編號。因此一套之中各冊「葉」的序號不會重複，所以冊可以省略，作：</p>
		<p class='m2'>
			<span class='orange'>Z 1:19:418a&nbsp;（第一編第19套, 第418葉,	
				第一欄）<br/>
				Z 2:19:16c&nbsp;&nbsp;（第二編第19套, 第16葉, 第三欄）<br/> 
				Z 2B:19:255d（第二編乙第19套, 第255葉, 第四欄）</span>
		</p> 

<br><br/>
</div>
	</body>
</html>