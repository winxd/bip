<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>▲TOP</a>
	</head>
	<body>

<div class='container'>
	<?php include 'header.php'; ?>

	<h1>Mission</h1>
	<p class='ball'/>
	<h2 class='center'>CBETA 目標</h2>

<h2>Taisho Tripitaka (1998.02~2003.02)</h2>
<ul class='m2'>
	<li>First and most importantly, complete input, proofreading and delivery of the volumes 1-55 and 85 of the Taiso Tripitaka.</li>
	<li>Complete it with the least possible rate of errors and create a high-quality electronic version of the scriptures.</li>
	<li>Come up with a method to properly handle characters that are missing from the system characterset.</li>
	<li>Use information technology to display different versions of the Tripitaka.</li>
	<li>Provide a complete search engine to ease the access to the electronic version of the Tripitaka.</li>
	<li>Use network technology to provide access to the Chinese Buddhist scriptures from any place in the world.</li>
	<li>Develop a user interface for convenient access also on stand-alone PC's that are not connected to the network.</li>
</ul>

<h2>Xuzangjing (Zokuzokyo) (2003.02~2008.02)</h2>
<ul class='m2'>
	<li>To release Zen Part of Xuzangjing Vol. 78-87 by end of 2003, and Vol. 63-73 before July 2004.</li>
	<li>To complete Vol. 54-88 by early 2005.</li>
	<li>And then Vol. 1-53 will be finished in another two more years (2005-2006).</li>
	<li>Input Vol. 1-88 Footnotes (2007).</li>
	<li>To publish the CBETA Chinese Electronic Tripitaka Collection (Taisho and Zokuzokyo) CD-ROM before Feb. 15, 2008.</li>
</ul>

<h2>Jiaxing Canon and others (2008.02~)</h2>
<ul class='m2'>
	<li>Apart from the Taisho Tripitaka and the Xuzangjing , we are planning to digitize 287 sutras in 1,490 fascicles from the Jiaxing Canon , as well as another 80 sutras in 1,008 fascicles which are scattered throughout different versions of the canon. Another 324 sutras are still awaiting confirmation. The focus of our future work will be on digitizing non-canonical texts edited by modern scholars, as well as related material.</li>
</ul>

<br><br/>

	</body>
</html>