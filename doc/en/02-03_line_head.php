<th><!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>▲TOP</a>
	</head>

	<body>

<div class='container'>
<?php include 'header.php'; ?>

<h1>Page Reference Number Explanation</h1>
	<p class='ball'/>
<h2 class='center'>本資料庫行首資訊規則說明</h2>
	<p>CBETA 版電子佛典，內文每行經文之前的行首資訊說明：</p>
	<p class='m2'>《大正藏》作：&nbsp;<span class='red22b'>T</span>xx<span class='red22'>n</span>yyyy<b>o</b><span class='red22b'>p</span>zzzz<b>c</b>ll</span></p>
	<p class='m2'>《卍續藏》作：&nbsp;<span class='red22b'>X</span>xx<span class='red22'>n</span>yyyy<b>o</b><span class='red22b'>p</span>zzzz<b>c</b>ll</span></p>
	<p class='m2'>各藏依此推類。</p>

	<p class='m1'><span class='orange'>▶▶▶ </span>For various editions of Xuzangjing citation please refer to the <a href='04-aca_quote-e.html' target='_blank' rel='nofollow'>How to Copy and Cite the CBETA Electronic Xuzangjing (Zokuzokyo)</a>.</p>
	<p ><span class='orange'>Explanation: </span></p>
	<p>以《大正藏》為例，意義如下：</p>
	<ul>
		<li class='m2'><span class='red22b'>T</span> ：表示大正藏（Taisho）經文。各藏代碼請參考 <a href='02-02_id-e.html' target='_blank' rel='nofollow'>CBETA 電子佛典集成代碼</a>。</li> 
		<li class='m2'>xx：二或三位數，表示冊數。</li>
		<li class='m2'><span class='red22b'>n</span>：固定不變，表示後面接經號。</li>
		<li class='m2'>yyyy：四位數，表示大正藏經號。</li>
		<li class='m2'><b>o</b>：大寫之A、B...表示<b>大正藏</b>有記載之同經號之別本，小寫之a、b...表示大正藏沒有記載之同經號之別本，完全沒有同經號資料者，用下底線符號填入&#39;_&#39;。<br>
			<span class='red'>但卍續藏經文中出現的&nbsp;No. yyyy-A&nbsp;及&nbsp;No. yyyy-1，與大正藏經號代表的意義不同。舉凡序、敘、緣起、跋、後記等，皆於典籍號碼之下，附以A、B、C&hellip;&hellip;等羅馬字母。若屬多數典籍之集錄，或集數篇著述而成一書者，則於典籍號碼之次，另附以1、2、3&hellip;&hellip;等數字&mdash;&mdash;《卍續藏．凡例．三》，所以卍續藏行首資訊並沒有代表別本的符號。</span></li>
		<li class='m2'><span class='red22b'>p</span>：固定不變，表示後面接頁碼。</li>
		<li class='m2'>zzzz：四位數，表示頁碼。</li>
		<li class='m2'><b>c</b>：表示第幾欄，a	表示上欄，b	表示中欄，c	表示下欄。有些特殊格式的經文會超過 3 欄。</li>
		<li class='m2'>ll：二位數，表示在該欄的行數。</li>
	</ul>
	<p>例如：T10n0279_p0001a01，即表示大正藏第十冊	0279	經第一頁上欄第一行。</p>
	<p><b>T10n0279_p0001a01 means:</b></p>
	<p><I>Taisho Tripitaka, Volumme 10, Sutra No. 279, page 1, column a, line 1.</I></p>
	<br><br>
		<!--	<p class='m2'>詳例：大正藏第一冊(T01)第一經(n0001_)第二頁(p0002)下欄(c)第二行至第十二行(c02~c12)</p>
			<blockquote>
				<p>T01n0001_p0002c02║頌曰<br>
					T01n0001_p0002c03║　毘婆尸名觀　　智慧不可量<br>
					T01n0001_p0002c04║　遍見無所畏　　三會弟子眾<br>
					T01n0001_p0002c05║　尸棄光無動　　能滅諸結使<br>
					T01n0001_p0002c06║　無量大威德　　無能測量者<br>
					T01n0001_p0002c07║　彼佛亦三會　　弟子普共集<br>
					T01n0001_p0002c08║　毘舍婆斷結　　大仙人要集<br>
					T01n0001_p0002c09║　名聞於諸方　　妙法大名稱<br>
					T01n0001_p0002c10║　二會弟子眾　　普演深奧義<br>
					T01n0001_p0002c11║　拘樓孫一會　　哀愍療諸苦<br>
					T01n0001_p0002c12║　導師化眾生　　一會弟子眾</p>
			</blockquote>

		<h2>App 版行首</h2>
			<p class='m2'>App 版的內容與格式與上述相同，唯一的差別是為了方便部份檢索軟體能正確的檢索，因此每行行尾如果有不成句的字，則會移動到下一行的行首（亦即以句點作為行末切割處）。搬動的字數會在下一行的行首資訊後以括號顯示出，例如：</p>
			<p class='m2'><b>普及版：</b></p>
			<p class='m2'>T08n0221_p0001a08║聞如是。一時佛在羅閱祇耆闍崛山中。與大<br>
				T08n0221_p0001a09║比丘眾五千人俱。皆是阿羅漢。諸漏已盡意</p>
			<p class='m2'><b>App 版：</b></p>
			<p class='m2'>T08n0221_p0001a08<span class='red'>(00)</span>║聞如是。一時佛在羅閱祇耆闍崛山中。<br>
				T08n0221_p0001a09<span class='red'>(02)</span>║<span class='red'>與大</span>比丘眾五千人俱。皆是阿羅漢。</p>
			<p class='m2'>其中的<span class='red'>(02)</span>代表由前一行行尾將「<span class='red'>與大</span>」兩個字搬動到本行行首。如此一來，在普及版無法檢索「大比丘眾」 的情況在 App 版就可以順利解決了。</p>
			<p class='m2'>如果搬動字數大於100，則省略左括號。</p>
			<p class='m2'>例如：T08n0221_p0001a09<span class='red'>102)</span></p>
			<p class='m2'>其中的<span class='red'>102)</span>代表由上面的行數搬動102個字到本行行首。</p>

		<h2>精簡版行首</h2>
			<p class='m2'>精簡版的經文是在每一段文章之前留下頁欄行等資訊。</p>
			<p class='m2'>例如：</p>

			<blockquote>
				<p class='m2'>No. 1<br>
					　　長阿含經序<br>
					　　　　長安釋僧肇述<br>
					[0001a05]<br>
					夫宗極絕於稱謂。賢聖以之沖默。玄旨非言不傳。釋迦所以致教。是以如來出世。大教有三。約身口。則防之以禁律。明善惡。則導之以契經。演幽微。則辨之以法相。然則三藏之作也。本於殊應。會之有宗。則異途同趣矣。<br>
					[0001a10]<br>
					禁律。律藏也。四分十誦。法相。阿毗曇藏也。四分五誦。契經。四阿含藏也。增一阿含四分八誦。中阿含四分五誦。雜阿含四分十誦。此長阿含四分四誦。合三十經以為一部。</p>
			</blockquote>
			<p class='m2'>[0001a05] 表示是第一頁第一欄第五行。</p>
-->
</div>
	</body>
</html>