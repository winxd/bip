<?php
$header = <<<HEADER
		<div class='header'>
			<div class='header-left'>
			<a href='01-content.php'>&nbsp; CBETA Instruction</a>
			</div>
			<div class='header-right'>
				<span class='dropdown'>Supplement | 
				<div class='dropdown-content'>
					<a href='02-02_id.php'>Collection Notation</a>
					<a href='02-03_line_head.php'>Page Reference Number</a>
					<a href='02-04_create_word.php'>Rules of Assembled Rare Character </a>
					<a href='04-aca_quote.php'>How to Copy and Cite</a>
					<a href='02-06_abbr_app.php'>Abbr. of Textcritcal Apparatus</a>
					<a href='02-07_abbr_ver.php'>Abbr. of Tripitaka Editions </a>
					<a href='02-08_collate_clause.php'>Star Mark Used in Critical Notes</a>
					<a href='02-09_abbr_dz.php'>Abbr. of Text Collated </a>
					<a href='02-10_pali_title.php'>Abbr. of Palitext Titles </a>
					<a href='02-11_ver.php'>Edition Abbr. Amended by CBETA </a>
					<a href='02-12_cc_revise.php'>CBETA Textcritial Notes</a>
				</div>
				</span>
				<span class='dropdown' onclick="location.href='03-00_system_oprator.php'" style="cursor:pointer">CBETA Online Reader Operation Manual | 
				<div class='dropdown-content'>
					<a href='03-01_select1.php'>Reading Interface </a>
					<a href='03-02_select2.php'>Text Selector</a>
					<a href='03-03_read_setting.php'>Search Interface</a>
				</div>
				</span>

				<span class='dropdown'>CBETA | 
				<div class='dropdown-content'>
					<a href='05-01_related_org.php'>Concerned Units</a>
					<a href='05-02_aim.php'>Goal</a>
					<a href='05-03_origin.php'>Origin</a>
					<a href='05-04_goal.php'>Mission</a>
					<a href='05-05_org.php'>Org. Setup</a>
					<a href='05-06_tech.php'>Technology(Chinese)</a>
					<a href='05-07_contribution.php'>Contributors(Chinese)</a>
					<a href='05-08_donate.php'>Donation Account</a>
				</div>
				</span>
				<a class='dropdown' href='04-aca_quote.php'>Forms of Citation | </a>
		<a class='dropdown' href='06-copyright.php'>Copyright　</a>
			</div>
		</div>

HEADER;

echo $header;
?>