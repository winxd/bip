<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
		<link rel='stylesheet' type='text/css' href='style.css'>
		<style>
			body {background-image:url('pic/jing2.jpg');}
			p {font-size:1.1em;}
			.con1 {margin:0 5em;}
		</style>
	</head>
	<body>
	<div class='container'>
		<?php include 'header.php'; ?>
			<h1>Information Content</h1>
			<p class='ball'/>
		<div class='flex'>
			<div class='con1'>
				<p>Taishō Tripiṭaka</p>
				<p>卍Shinsan Zokuzōkyō (Xuzangjing)</p>
				<p>Selected Chinese Buddhist Canon from other editions</p>
				<p>Selections from the Taipei National Central Library Buddhist Rare Book Collection</p>
				<p>Chinese Translation of the Pāli Canon (Yuan Heng Temple edition)</p>
			</div>
			<div class='con'>
				<p>Selected Buddhist materials not contained in the Tripiṭaka:</p>
				<p class='m2'>Buddhist Texts not contained in the Tripiṭaka</p>
				<p class='m2'>Passages concerning Buddhism from the Official Histories</p>
				<p class='m2'>Selections of Buddhist Stone Rubbings from the Northern Dynasties</p>
				<p class='m2'>Supplement to the Dazangjing</p>
				<p class='m2'>Chinese Buddhist Temple Gazetteers</p>
				<p class='m2'>Corpus of Venerable Yin Shun's Buddhist Studies</p>
			</div>
		</div>
	</div>
	</body>
</html>