<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>▲TOP</a>
	</head>
	
	<body>

<div class='container'>
<?php include 'header.php'; ?>

	<h1>Rules for the Assembled Chinese Rare Characters</h1>
	<p class='ball'/>
	<h2 class='center'>一般組字式基本規則說明</h2>

	<p class='m2'>本組字法含<span class='navy22'>&nbsp;* / @ - + ?&nbsp;</span>六個半形基本符號，及<span class='navy22'>&nbsp;( ) [ ]&nbsp;</span>兩組半形分隔符號。舉例說明如下：</p>
	<table>
		<tr>
			<th class='center'>
				符號</th>
			<th class='center'>
				說明 </th>
			<th class='center'>
				範例 </th>
		</tr>
		<tr>
			<td class='center'>
				*</td>
			<td>
				表橫向連接</td>
			<td>
				明＝日*月</td>
		</tr>
		<tr>
			<td class='center'>
				/</td>
			<td>
				表縱向連接</td>
			<td>
				音＝立/日</td>
		</tr>
		<tr>
			<td class='center'>
				@</td>
			<td>
				表包含</td>
			<td>
				因＝囗@大　或　閒＝門@月</td>
		</tr>
		<tr>
			<td class='center'>
				-</td>
			<td>
				表去掉某部份</td>
			<td>
				青＝請-言</td>
		</tr>
		<tr>
			<td class='center'>
				-+</td>
			<td>
				若前後配合，表示去掉某部份，<br>
				而改以另一部份代替</td>
			<td>
				閒＝間-日+月</td>
		</tr>
		<tr>
			<td class='center'>
				?</td>
			<td>
				表字根特別，尚未找到足以表示者</td>
			<td>
				背＝(?*匕)/月</td>
		</tr>
		<tr>
			<td class='center'>
				()</td>
			<td>
				為運算分隔符號</td>
			<td>
				繞＝組-且+((土/(土*土))/兀)</td>
		</tr>
		<tr>
			<td class='center'>
				[]</td>
			<td>
				為文字分隔符號</td>
			<td>
				羅[目*侯]羅母耶輸陀羅比丘尼</td>
		</tr>
	</table>

	<p class='m2'>為求方便，不排除採用全形注音、標點及英文符號做為組字用字根。</p>
	<p class='m2'>組字字根在不同的字型下會有所差別。(下圖：不同字型下字體的差異，標楷體 / 新細明體)</p>
	<p align='center'><img border='0' src='http://www.cbeta.org/sites/default/files/img/diff-font.jpg' /></p> 
	<br><br>
</div>
	</body>
</html>