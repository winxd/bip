<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


<div class='container'>
<?php include 'header.php'; ?>

<h1>2017.Q3</h1> 
	<p class='ball'/>

	<h3>發行日期：Octber 20 , 2017</h3>
	<br/>
	<ol style="list-style-type:cjk-ideographic;">
		<li>T28,T29 新式標點經文</li>
		<li>T25-T28 大正藏、高麗藏比對修訂</li>
		<li>加入 Unicode 10.0 新增漢字1388 個</li>
		<li>讀者來函修訂</li>
		<li>涉及的文字修訂共 3,541 卷、186,013 行，詳見<a href="changelog/2017/cbeta-2017Q3-change-log.pdf">變更記錄</a>。</li>
	</ol>
	</body>
</html>
