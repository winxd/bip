<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


<div class='container'>
<?php include 'header.php'; ?>

<h1>2018.Q2</h1> 
	<p class='ball'/>
	
	<h3 style="color:red">本資料版本即為CBETA 2018 光碟預設資料內容</h3>
	<h3>發行日期：2018/07/26</h3>
	<br/>
	<ol style="list-style-type:cjk-ideographic;">
		<li style="margin-bottom:1em">移除：
			<ul style="list-style: initial;">
				<li>cbeta-normal-2018Q1/B/B24/B24n0138</li>
			</ul>		
		</li>
		<li>涉及的修訂共 25 卷，187 行，詳見變更記錄(<a href="changelog/2018/2018Q2.pdf">所有變更</a>、<a href="changelog/2018/2018Q2-punc.pdf">僅標點變更</a>、<a href="changelog/2018/2018Q2-text.pdf">僅文字變更</a>)。</li>
	</ol>
	</body>
</html>
