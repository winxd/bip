<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


<div class='container'>
<?php include 'header.php'; ?>

<h1>2018.Q3</h1> 
	<p class='ball'/>
	
	<h3>發行日期：2018/11/05</h3>
	<br/>
	<ol style="list-style-type:cjk-ideographic;">
		
		<li>修訂《印順法師佛學著作集》部份內文的呈現格式。 </li>
		<li>修訂若干缺字資訊及經文用字和標點。</li>	
		<li>涉及的修訂共 16663 卷，21,155 行，詳見變更記錄(<a href="changelog/2018/2018Q3.pdf">所有變更</a>、<a href="changelog/2018/2018Q3-punc.pdf">僅標點變更</a>、<a href="changelog/2018/2018Q3-text.pdf">僅文字變更</a>)。</li>
	</ol>
	</body>
</html>
