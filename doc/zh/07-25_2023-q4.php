<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include '../../include/config.php'; ?>
			<?php include 'header.php'; ?>

			<h1>2023.Q4</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2023/12/20</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>新收錄典籍共 25 部 28 卷：
					<ul>
						<li>G104n2250 南山律在家備覽略編(4卷)</li>
						<li>G104n2251 四分律含註戒本隨講別錄(1卷)</li>
						<li>G104n2252 四分律含註戒本科(1卷)</li>
						<li>G104n2253 四分律含註戒本疏略科(1卷)</li>
						<li>G104n2254 四分律含註戒本略釋(1卷)</li>
						<li>G104n2256 四分律刪補隨機羯磨疏略科(1卷)</li>
						<li>G104n2257 事鈔持犯方軌篇表記(1卷)</li>
						<li>G104n2268 授三歸依大意(1卷)</li>
						<li>G104n2269 釋門歸敬儀科(1卷)</li>
						<li>G104n2270 律學要略(1卷)</li>
						<li>G104n2271 菩薩瓔珞經自誓受菩薩五重戒法(1卷)</li>
						<li>G104n2272 僧尼十種受法料簡圖(1卷)</li>
						<li>G104n2273 剃髮儀式(1卷)</li>
						<li>G104n2274 表無表章科(1卷)</li>
						<li>G104n2275 盜戒釋相概略問答(1卷)</li>
						<li>G104n2276 南山律苑雜錄(1卷)</li>
						<li>G104n2277 毗柰耶質疑編(1卷)</li>
						<li>G104n2278 青年佛徒應注意的四項(1卷)</li>
						<li>G104n2279 人生之最後(1卷)</li>
						<li>G104n2280 根本說一切有部毗柰耶犯相摘記(1卷)</li>
						<li>G104n2281 根本說一切有部毗柰耶自行抄(1卷)</li>
						<li>G104n2282 學根本說一切有部律入門次第(1卷)</li>
						<li>G104n2283 四分律比丘戒相表記校註(1卷)</li>
						<li>G104n2284 榑桑國藏古袈裟圖(1卷)</li>
						<li>CC002n0002 敦博本六祖壇經校訂全文(1卷)</li>
					</ul>
				</li>			
				<li>新增新式標點共 1 部 2 卷（變更記錄忽略標點差異）：
					<ul>
						<li>X86n1607 居士分燈錄(2卷)</li>
					</ul>
				</li>
				<li>原書句讀更新共 2 部 11 卷（變更記錄忽略標點差異）：
					<ul>
						<li>X12n0274 楞嚴經正脉疏懸示(1卷)</li>
						<li>X12n0275 楞嚴經正脉疏(10卷)</li>
					</ul>
				</li>
				<li>目錄重新架構共 1 部 50 卷：
					<ul>
						<li>B22n0117 三藏法數(50卷)（變更記錄整部忽略全部差異）</li>
					</ul>
				</li>				
				
				
				<li>CBETA Online
					<ul>
						<li>新增功能
							<ol>
								<li>跨夾注搜尋選項化，可選擇要是否要跨夾注搜尋</li>								
								<li>閱讀頁「i」box 、搜尋結果頁加入經所屬之部類資訊</li>
							</ol>
						</li>
						<li>功能修正
							<ol>
								<li>經文檢索改成模糊搜尋，並加上 highlight</li>
							</ol>
						</li>						
					</ul>
				</li>
				
				<li>涉及的修訂共 928 卷 15,700 行，詳見變更記錄：<a href="changelog/2023/2023Q4.pdf">所有變更</a>(<?= formatSizeUnits(filesize('changelog/2023/2023Q4.pdf')) ?>)、<a href="changelog/2023/2023Q4-punc.pdf">僅標點變更</a>(<?= formatSizeUnits(filesize('changelog/2023/2023Q4-punc.pdf')) ?>)、<a href="changelog/2023/2023Q4-text.pdf">僅文字變更</a>(<?= formatSizeUnits(filesize('changelog/2023/2023Q4-text.pdf')) ?>)。</li></li>				
			</ol>
		</div>
</body>
</html>