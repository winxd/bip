<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
  <head>
  <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
  <link rel='stylesheet' type='text/css' href='style.css'>
  <a id='back2top' href='#' title='Back to Top'>回頁首</a>
  </head>
  
  <body>

<div class='container'>
<?php include 'header.php'; ?>
<h1>藏經代碼</h1> <!--http://www.cbeta.org/format/id.php-->
	<p class='ball'/>
<h2 align='center'>CBETA 電子佛典集成代碼</h2>

<table>
	<tr>
		<th>代碼</th>
		<th>典籍</th>
		<th>略稱<br>別稱</th>
		<th>略符</th>
		<th>紙本資訊</th>
	</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">A</td>
			<td>趙城金藏<br>
			Jin Edition of the Canon</td>
			<td style="text-align: center; white-space: nowrap;">趙城金藏</td>
			<td style="text-align: center; white-space: nowrap;">【金藏】</td>
			<td>北京：北京圖書館出版社，2008。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">AC</td>
			<td>趙城金藏（中國國家圖書館館藏）<br>
			Jin Edition of the Canon (National Library of China&nbsp;Collection)</td>
			<td style="text-align: center; white-space: nowrap;">趙城金藏</td>
			<td style="text-align: center; white-space: nowrap;">【金藏乙】</td>
			<td>中國國家圖書館館藏／山西趙城 : 廣勝寺，1149。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">B</td>
			<td>大藏經補編<br>
			Supplement to the Dazangjing</td>
			<td style="text-align: center; white-space: nowrap;">大藏經補編</td>
			<td style="text-align: center; white-space: nowrap;">【補編】</td>
			<td>藍吉富 主編／台北：華宇出版社，1985。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">C</td>
			<td>中華大藏經（中華書局版）<br>
			Zhonghua Canon (Chunghwa Book Edition)</td>
			<td style="text-align: center; white-space: nowrap;">中華藏</td>
			<td style="text-align: center; white-space: nowrap;">【中華】</td>
			<td>中華大藏經編輯局 編 ／北京：中華書局，1984-1997。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">D</td>
			<td>國家圖書館善本佛典<br>
			Selections from the Taipei National Central Library Buddhist Rare Book Collection</td>
			<td style="text-align: center; white-space: nowrap;">國圖善本佛典</td>
			<td style="text-align: center; white-space: nowrap;">【國圖】</td>
			<td>國家圖書館特藏組 收藏</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">F</td>
			<td>房山石經<br>
			Fangshan shijing</td>
			<td style="text-align: center; white-space: nowrap;">房山石經</td>
			<td style="text-align: center; white-space: nowrap;">【房山】</td>
			<td>中國佛教圖書文物館 編／北京：華夏出版社，2000。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">G</td>
			<td>佛教大藏經<br>
			Fojiao Canon</td>
			<td style="text-align: center; white-space: nowrap;">佛教大藏經</td>
			<td style="text-align: center; white-space: nowrap;">【佛教】</td>
			<td>佛教書局 編輯／台北：佛教書局，1978。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">GA</td>
			<td>中國佛寺史志彙刊<br>
			Zhongguo Fosi Shizhi Huikan</td>
			<td style="text-align: center; white-space: nowrap;">佛寺志彙刊</td>
			<td style="text-align: center; white-space: nowrap;">【志彙】</td>
			<td>杜潔祥 主編／台北：宗青圖書出版公司，1980-1994。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">GB</td>
			<td>中國佛寺志叢刊<br>
			Zhongguo Fosizhi Congkan</td>
			<td style="text-align: center; white-space: nowrap;">佛寺志叢刊</td>
			<td style="text-align: center; white-space: nowrap;">【志叢】</td>
			<td>張智等 編輯／揚州：廣陵書社，2006。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">GJ</td>
			<td>福州藏（崇寧藏、毗廬藏）（宮內廳書陵部圖書寮文庫藏）<br>
			Fuzhou Canon (The Imperial House Library Collection)</td>
			<td style="text-align: center; white-space: nowrap;">福州藏</td>
			<td style="text-align: center; white-space: nowrap;">【宮】</td>
			<td>宮內廳書陵部圖書寮文庫藏</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">I</td>
			<td>北朝佛教石刻拓片百品<br>
			Selections of Buddhist Stone Rubbings from the Northern Dynasties</td>
			<td style="text-align: center; white-space: nowrap;">
			<p>北朝佛拓百品</p>
			</td>
			<td style="text-align: center; white-space: nowrap;">【佛拓】</td>
			<td>顏娟英 主編／台北：中央研究院歷史語言研究所，2008。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">J</td>
			<td>嘉興大藏經（新文豐版）<br>
			Jiaxing Canon (Shinwenfeng Edition)</td>
			<td style="text-align: center; white-space: nowrap;">嘉興藏</td>
			<td style="text-align: center; white-space: nowrap;">【嘉興】</td>
			<td>徑山藏版版藏／台北：新文豐，1987。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">JC</td>
			<td>嘉興大藏經（民族版）<br>
			Jiaxing Canon (The Ethnic Publishing House&nbsp;Edition)</td>
			<td style="text-align: center; white-space: nowrap;">嘉興藏</td>
			<td style="text-align: center; white-space: nowrap;">【嘉興丙】</td>
			<td>北京：民族出版社，2008。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">JT</td>
			<td>嘉興大藏經（日本東京大學綜合圖書館收藏）<br>
			Jiaxing Canon (University of Tokyo General&nbsp;Library Collection)</td>
			<td style="text-align: center; white-space: nowrap;">嘉興藏</td>
			<td style="text-align: center; white-space: nowrap;">【嘉興乙】</td>
			<td>日本東京大學綜合圖書館收藏</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">K</td>
			<td>高麗大藏經（新文豐版）<br>
			Tripiṭaka Koreana (Shinwenfeng Edition)</td>
			<td style="text-align: center; white-space: nowrap;">高麗藏</td>
			<td style="text-align: center; white-space: nowrap;">【麗】</td>
			<td>高麗大藏經完刊推進委員會 原刊／台北：新文豐，1982。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">L</td>
			<td>乾隆大藏經（新文豐版）<br>
			Qianlong Edition of the Canon (Shinwenfeng Edition)</td>
			<td style="text-align: center; white-space: nowrap;">乾隆藏、清藏、<br>
			龍藏</td>
			<td style="text-align: center; white-space: nowrap;">【龍】</td>
			<td>台北：新文豐，1991。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">LC</td>
			<td>呂澂佛學著作集<br>
			Corpus of Lü Cheng's Buddhist Studies</td>
			<td style="text-align: center; white-space: nowrap;">呂澂著作集</td>
			<td style="text-align: center; white-space: nowrap;">【呂澂】</td>
			<td>台北：大千出版社，2003-2012。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">M</td>
			<td>卍正藏經（新文豐版）<br>
			Manji Daizōkyō (Shinwenfeng Edition)</td>
			<td style="text-align: center; white-space: nowrap;">卍正藏</td>
			<td style="text-align: center; white-space: nowrap;">【卍正】</td>
			<td>京都．藏經書院 原刊 ／台北：新文豐，1980。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">N</td>
			<td>漢譯南傳大藏經（元亨寺版）<br>
			Chinese Translation of the Pāḷi Tipiṭaka (Yuan Heng Temple Edition)</td>
			<td style="text-align: center; white-space: nowrap;">南傳大藏經</td>
			<td style="text-align: center; white-space: nowrap;">【南傳】</td>
			<td>元亨寺漢譯南傳大藏經編譯委員會／高雄：元亨寺妙林出版社，1995。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">P</td>
			<td>永樂北藏<br>
			Northern Yongle Edition of the Canon</td>
			<td style="text-align: center; white-space: nowrap;">永樂北藏</td>
			<td style="text-align: center; white-space: nowrap;">【北藏】</td>
			<td>永樂北藏整理委員會 編／北京：線裝書局，2000。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">Q</td>
			<td>磧砂大藏經（新文豐版）<br>
			Qisha Edition of the Canon (Shinwenfeng Edition)</td>
			<td style="text-align: center; white-space: nowrap;">磧砂藏</td>
			<td style="text-align: center; white-space: nowrap;">【磧】</td>
			<td>延聖院大藏經局 編輯 ／台北：新文豐，1987。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">QC</td>
			<td>磧砂大藏經（線裝書局版）<br>
			Qisha Edition of the Canon (Xian Zhuang Shu ju Edition)</td>
			<td style="text-align: center; white-space: nowrap;">磧砂藏</td>
			<td style="text-align: center; white-space: nowrap;">【磧乙】</td>
			<td>北京：線裝書局，2004。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">R</td>
			<td>卍續藏經（新文豐版）<br>
			Manji Zokuzōkyō (Shinwenfeng Edition)</td>
			<td style="text-align: center; white-space: nowrap;">卍續藏</td>
			<td style="text-align: center; white-space: nowrap;">&nbsp;</td>
			<td>京都．藏經書院 原刊 ／台北：新文豐，1994。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">S</td>
			<td>宋藏遺珍（新文豐版）<br>
			Songzang yizhen (Shinwenfeng Edition)</td>
			<td class="rtecenter" style="white-space: nowrap;">宋藏遺珍</td>
			<td style="text-align: center; white-space: nowrap;">【宋遺】</td>
			<td>範成 輯補／台北：新文豐，1978。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">SS</td>
			<td>大日本校訂大藏經（日本國立國會圖書館數位館藏）<br>
			Reduced Print edition (National Diet Library Digital Collections)</td>
			<td class="rtecenter" style="white-space: nowrap;">縮刷藏、縮刻藏、<br>
			縮藏、弘教本、<br>
			弘教藏</td>
			<td style="text-align: center; white-space: nowrap;">【縮刷】</td>
			<td>縮刷大藏經刊行會 編／東京：縮刷大藏經刊行會 再版，1939。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">SX</td>
			<td>思溪藏（中國國家圖書館館藏）<br>
			Sixi Canon (National Library of China&nbsp;Collection)</td>
			<td class="rtecenter" style="white-space: nowrap;">思溪藏</td>
			<td style="text-align: center; white-space: nowrap;">【思溪】</td>
			<td>中國國家圖書館館藏</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">T</td>
			<td>大正新脩大藏經<br>
			Taishō Tripiṭaka</td>
			<td style="text-align: center; white-space: nowrap;">大正藏</td>
			<td style="text-align: center; white-space: nowrap;">【大】</td>
			<td>大正新修大藏經刊行會 編／東京：大藏出版株式會社，Popular Edition in 1988。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">TX</td>
			<td>太虛大師全書<br>
			Corpus of Venerable Tai Xu's Buddhist Studies</td>
			<td style="text-align: center; white-space: nowrap;">太虛大師集</td>
			<td style="text-align: center; white-space: nowrap;">【太虛】</td>
			<td>太虛大師全書編纂委員會輯／台北：善導寺佛經流通處印行，1980。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">TW</td>
			<td>大正新脩大藏經<br>
			Taishō Tripiṭaka</td>
			<td style="text-align: center; white-space: nowrap;">大正藏</td>
			<td style="text-align: center; white-space: nowrap;">&nbsp;</td>
			<td>大正新修大藏經刊行會 編／台北：新文豐，1983。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">U</td>
			<td>洪武南藏<br>
			Southern Hongwu Edition of the Canon</td>
			<td style="text-align: center; white-space: nowrap;">洪武南藏</td>
			<td style="text-align: center; white-space: nowrap;">【洪武】</td>
			<td>四川省佛教協會／成都：四川省佛教協會，1999。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">X</td>
			<td>卍新纂大日本續藏經<br>
			Manji Shinsan Dainihon Zokuzōkyō</td>
			<td style="text-align: center; white-space: nowrap;">新纂卍續藏</td>
			<td style="text-align: center; white-space: nowrap;">【卍續】</td>
			<td>河村照孝 編集／東京：株式會社國書刊行會，1975-1989。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">Y</td>
			<td>印順法師佛學著作集<br>
			Corpus of Venerable Yin Shun's Buddhist Studies</td>
			<td style="text-align: center; white-space: nowrap;">印順法師集</td>
			<td style="text-align: center; white-space: nowrap;">【印順】</td>
			<td>新竹：正聞出版社，2003-2016。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">Z</td>
			<td>卍大日本續藏經<br>
			Manji Dainihon Zokuzōkyō</td>
			<td style="text-align: center; white-space: nowrap;">卍續藏</td>
			<td style="text-align: center; white-space: nowrap;">&nbsp;</td>
			<td>京都：藏經書院，1905-1912。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">ZS</td>
			<td>正史佛教資料類編<br>
			Passages concerning Buddhism from the Official Histories</td>
			<td style="text-align: center; white-space: nowrap;">正史佛教資料</td>
			<td style="text-align: center; white-space: nowrap;">【正史】</td>
			<td>杜斗城 輯編／蘭州：甘肅文化出版社，2006。</td>
		</tr>
		<tr>
			<td style="white-space: nowrap;" align="center">ZW</td>
			<td>藏外佛教文獻<br>
			Buddhist Texts not contained in the Tripiṭaka</td>
			<td style="text-align: center; white-space: nowrap;">藏外佛教文獻</td>
			<td style="text-align: center; white-space: nowrap;">【藏外】</td>
			<td>方廣錩 主編／北京：宗教文化出版社，1995-2003。</td>
		</tr>
</table>
<br><br>
</div>
  </body>
</html>