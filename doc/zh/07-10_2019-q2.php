<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include '../../include/config.php'; ?>
			<?php include 'header.php'; ?>

			<h1>2019.Q2</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2019/07/08</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>
					以下經文加上標點：<br/>【AI 標點（共 22 經 161 卷）】
					<ul>
						<li>J20nB098　黃蘗無念禪師復問(6卷)</li>
						<li>J20nB103　寒山子詩集(1卷)</li>
						<li>J21nB110　護法錄(10卷)</li>
						<li>J22nB116　憨山老人夢遊全集(5卷)</li>
						<li>J23nB118　密藏開禪師遺稿(2卷)</li>
						<li>J23nB127　無依道人錄(2卷)</li>
						<li>J23nB128　普明禪師牧牛圖頌(1卷)</li>
						<li>J23nB129　牧牛圖頌(1卷)</li>
						<li>J23nB134　五家語錄（選錄）(2卷)</li>
						<li>J23nB135　石門文字禪(30卷)</li>
						<li>J25nB164　曹溪一滴(1卷)</li>
						<li>J25nB166　浮山法句(1卷)</li>
						<li>J25nB171　天隱和尚語錄(15卷)</li>
						<li>J25nB176　大溈密印寺養拙明禪師語錄(1卷)</li>
						<li>J26nB182　萬如禪師語錄(10卷)</li>
						<li>J26nB184　牧雲和尚七會語錄(6卷)</li>
						<li>J26nB186　林野奇禪師語錄(8卷)</li>
						<li>J26nB188　入就瑞白禪師語錄(18卷)</li>
						<li>J27nB189　三宜盂禪師語錄(11卷)</li>
						<li>J27nB192　大休珠禪師語錄(12卷)</li>
						<li>J27nB193　隱元禪師語錄(16卷)</li>
						<li>J27nB196　鴛湖用禪師語錄(2卷)</li>
					</ul>
					【新式標點（共 3 經 22 卷）】
					<ul>
						<li>T18n0893a 蘇悉地羯羅經(3卷)</li>
						<li>T18n0893b 蘇悉地羯羅經(3卷)</li>
						<li>T18n0893c 蘇悉地羯羅經(3卷)</li>
						<li>J01nA042　大慧普覺禪師年譜(1卷)</li>
						<li>J10nA158　密雲禪師語錄(12卷)</li>					
					</ul>					
				</li>
				<li style="margin-top:1em">漢字資料庫新增 Unicode 10 用字 768 個。</li>
				<li>處理讀者於討論區及服務信箱所回饋的用字及標點問題。</li>
				<li>涉及的修訂共 1800 卷，94,179 行，詳見變更記錄：<a href="changelog/2019/2019Q2.pdf">所有變更</a>(<?= formatSizeUnits(filesize('changelog/2019/2019Q2.pdf')) ?>)、<a href="changelog/2019/2019Q2-punc.pdf">僅標點變更</a>(<?= formatSizeUnits(filesize('changelog/2019/2019Q2-punc.pdf')) ?>)、<a href="changelog/2019/2019Q2-text.pdf">僅文字變更</a>(<?= formatSizeUnits(filesize('changelog/2019/2019Q2-text.pdf')) ?>)。</li></li>	
			</ol>
		</div>
</body>
</html>
