<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include '../../include/config.php'; ?>
			<?php include 'header.php'; ?>

			<h1>2020.Q3</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2020/09/09</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>近代新編文獻新增「藏外佛教文獻第十輯」共 11 部典籍。</li>
				<li>
					新增的新式標點 1 部
					<ul>
						<li>T50n2059 高僧傳</li>
					</ul>
				</li>
				<li>
					新增的基本句讀共 8 部
					<ul>
						<li>X56n0925</li>
						<li>X56n0926</li>
						<li>X56n0927</li>
						<li>X56n0928</li>
						<li>X56n0929</li>
						<li>X56n0930</li>
						<li>X57n0969</li>
						<li>B25n0145</li>
					</ul>
				</li>
				<li style="margin-top:1em">處理讀者於討論區及服務信箱所回饋的用字及標點問題。</li>
				<li>全面調整偈頌標記及呈現方式。</li>
				<li>《印順法師著作全集》新增 svg 圖檔。 </li>
				<li>涉及的修訂共 312 卷，25,535 行，詳見變更記錄：<a href="changelog/2020/2020Q3.pdf">所有變更</a>(<?= formatSizeUnits(filesize('changelog/2020/2020Q3.pdf')) ?>)、<a href="changelog/2020/2020Q3-punc.pdf">僅標點變更</a>(<?= formatSizeUnits(filesize('changelog/2020/2020Q3-punc.pdf')) ?>)、<a href="changelog/2020/2020Q3-text.pdf">僅文字變更</a>(<?= formatSizeUnits(filesize('changelog/2020/2020Q3-text.pdf')) ?>)。</li></li>	
			</ol>
		</div>
</body>
</html>
