<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>回頁首</a>
	</head>
	<body>

<div class='container'>
<?php include 'header.php'; ?>

<h1>CBETA 組織</h1> <!--http://www.cbeta.org/intro/organization.php-->
	<p class='ball'/>

<p align='center'><br>
	中華電子佛典協會<br>
	│<br>
	委員會─常務委員─主任委員─執行顧問─顧問團<br>
	┌─┴─┐<br>
	副主委　　副主委<br>
	└─┬─┘<br>
	總幹事<br>
	┌─┬─┬─┬┴┬─┬─┬─┐<br>
	研　資　文　輸　校　網　總　財<br>
	發　訊　字　入　對　路　務　務<br>
	組　組　組　組　組　組　組　組</p>
<hr />
<h2 align='center'>各單位介紹：</h2>

<p style='margin-left:30%'>
<b>&#149&nbsp; </b>顧問團 ：由諸山長老及學術單位所組成之顧問團<br>
<b>&#149&nbsp; </b>委員會 ：本協會以委員會方式組成<br>
<b>&#149&nbsp; </b>常務委員：代表委員會監督協會事務之進行<br>
<b>&#149&nbsp; </b>主任委員：委員會設置主任委員一名，負責整體事宜<br>
<b>&#149&nbsp; </b>副主委 ：主任委員下設副主委二名，以協助主任委員<br>
<b>&#149&nbsp; </b>總幹事 ：協調各組工作及對外行政事宜<br>
<b>&#149&nbsp; </b>研發組 ：負責提出前瞻性、國際性及學術性之整體規劃<br>
<b>&#149&nbsp; </b>資訊組 ：負責技術程式以及網路、單機應用程式之開發<br>
<b>&#149&nbsp; </b>文字組 ：負責電腦上之漢字處理<br>
<b>&#149&nbsp; </b>輸入組 ：負責經典輸入之進度及規劃<br>
<b>&#149&nbsp; </b>校對組 ：負責成品之電腦比對及人工校對事宜<br>
<b>&#149&nbsp; </b>網路組 ：負責將各組工作之進況及成果利用網路呈現<br>
<b>&#149&nbsp; </b>財務組 ：負責總管財務之運用<br>
</p>
<br>
<h2 align='center'>籌備委員名單：(2/15 參與會議名單，依姓氏排列)</h2>

<table>
	<tr>
		<th><strong>姓名</strong></th>
		<th><strong>單位</strong></th>
		<th><strong>E-Mail(只呈現名稱部份)</strong></th>
	</tr>
	<tr>
		<td align='center'>Christian Wittern</td>
		<td>CBETA</td>
		<td>cwitter@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>丁榮錄</td>
		<td>中華佛教護僧協會</td>
		<td> </td>
	</tr>
	<tr>
		<td align='center'>何億達</td>
		<td>Open 98</td>
		<td>stan@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>吳寶原</td>
		<td>CBETA</td>
		<td>maha@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>李志夫</td>
		<td>法鼓山中華佛學研究所</td>
		<td>chibNET@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>杜正民</td>
		<td>法鼓山中華佛學研究所</td>
		<td>aming@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>邱大剛</td>
		<td>台灣大學電機系</td>
		<td>b83050@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>周海文</td>
		<td>CBETA</td>
		<td>heaven@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>周寶珠</td>
		<td>慧炬雜誌社</td>
		<td>tow@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>夏先生</td>
		<td></td>
		<td> </td>
	</tr>
	<tr>
		<td align='center'>張文明</td>
		<td>普賢護法會網路中心</td>
		<td>dnstudio@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>張景全</td>
		<td></td>
		<td> </td>
	</tr>
	<tr>
		<td align='center'>張鴻洋</td>
		<td>美國印順基金會</td>
		<td>hychang@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>莊德明</td>
		<td>中研院資訊所</td>
		<td>derming@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>童闓運</td>
		<td>CBETA</td>
		<td>tone@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>黃郁婷</td>
		<td>CBETA</td>
		<td>tracyh57@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>曾濟群</td>
		<td>法鼓人文社會學院籌備處</td>
		<td>dddu@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>劉信男</td>
		<td>中華佛教百科文獻基金會代表</td>
		<td>lsn46@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>歐陽崇榮</td>
		<td></td>
		<td> </td>
	</tr>
	<tr>
		<td align='center'>蔡耀明</td>
		<td>華梵大學東方人文思想研究所</td>
		<td> </td>
	</tr>
	<tr>
		<td align='center'>蕭清鳳</td>
		<td>永聯不動產鑑定中心</td>
		<td> </td>
	</tr>
	<tr>
		<td align='center'>譚德銘</td>
		<td>永聯不動產鑑定中心</td>
		<td> </td>
	</tr>
	<tr>
		<td align='center'>蕭鎮國</td>
		<td>永聯不動產鑑定中心</td>
		<td>Arthur_H@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>謝清俊</td>
		<td>中研院資訊所</td>
		<td>hsieh@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>應景輝</td>
		<td>中台禪寺資訊管理顧問</td>
		<td>cts@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>釋自衍</td>
		<td>伽耶山基金會</td>
		<td>library@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>釋見護</td>
		<td>中台禪寺</td>
		<td>cts@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>釋恒清</td>
		<td>臺大佛學研究中心</td>
		<td>hcshihhc@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>釋厚觀</td>
		<td>印順文教基金會</td>
		<td>mprajna@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>釋惠敏</td>
		<td>法鼓山中華佛學研究所</td>
		<td>huimin@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>釋廣淨</td>
		<td>福嚴佛學院</td>
		<td>sbhong@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>釋德晴</td>
		<td>靜思精舍</td>
		<td>pony@xxx.xxx </td>
	</tr>
	<tr>
		<td align='center'>釋慧瑞</td>
		<td>佛光山資訊室</td>
		<td>mstonexx@xxx.xxx </td>
	</tr>
</table>
 </td><br/>
</div>
	</body>
</html>