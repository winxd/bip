<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>回頁首</a>
	</head>
	
	<body>

<div class='container'>
<?php include 'header.php'; ?>

<h1>組字式基本規則</h1> <!--http://www.cbeta.org/format/rare-rule.php-->
	<p class='ball'/>

<p>不論是使用 Big5 字集或 Unicode 字集，處理佛典難免都會遇到現有字集所缺乏的文字，使用「組字式」來表達佛典缺字的方法，是考量使用者能在純文字環境下閱讀，不需另外安裝造字檔或是圖檔而設計的，這種方式提供了閱覽、傳播上的便利性，也不會佔用使用者對造字檔自行運用的空間。</p>
<p>本組字法含<span class='navy22'>&nbsp;* / @ - + ?&nbsp;</span>六個半形基本符號，及<span class='navy22'>&nbsp;( ) [ ]&nbsp;</span>兩組半形分隔符號。</p>
<p>舉例說明如下：</p>
<table>
	<tr>
		<th class='center'>
			符號</th>
		<th class='center'>
			說明 </th>
		<th class='center'>
			範例 </th>
	</tr>
	<tr>
		<td class='center'>
			*</td>
		<td>
			表橫向連接</td>
		<td>
			明＝日*月</td>
	</tr>
	<tr>
		<td class='center'>
			/</td>
		<td>
			表縱向連接</td>
		<td>
			音＝立/日</td>
	</tr>
	<tr>
		<td class='center'>
			@</td>
		<td>
			表包含</td>
		<td>
			因＝囗@大　或　閒＝門@月</td>
	</tr>
	<tr>
		<td class='center'>
			-</td>
		<td>
			表去掉某部份</td>
		<td>
			青＝請-言</td>
	</tr>
	<tr>
		<td class='center'>
			-+</td>
		<td>
			若前後配合，表示去掉某部份，<br>
			而改以另一部份代替</td>
		<td>
			閒＝間-日+月</td>
	</tr>
	<tr>
		<td class='center'>
			?</td>
		<td>
			表字根特別，尚未找到足以表示者</td>
		<td>
			背＝(?*匕)/月</td>
	</tr>
	<tr>
		<td class='center'>
			()</td>
		<td>
			為運算分隔符號</td>
		<td>
			繞＝組-且+((土/(土*土))/兀)</td>
	</tr>
	<tr>
		<td class='center'>
			[]</td>
		<td>
			為文字分隔符號</td>
		<td>
			羅[目*侯]羅母耶輸陀羅比丘尼</td>
	</tr>
</table>

<p>註1：為求方便，不排除採用全形注音、標點及英文符號做為組字用字根。</p>
<p>註2：組字字根在不同的字型下會有所差別。(下圖：不同字型下字體的差異，標楷體 / 新細明體)</p>
<p align='center'><img border='0' src='http://www.cbeta.org/sites/default/files/img/diff-font.jpg' /></p> <!--diff-font.jpg-->
<p>參考資料：</p>
<ul>
	<li>
		經文資料庫中被規範過的缺字，請參考【<a href='http://www.cbeta.org/download/regular.zip' rel='nofollow'>大正藏基本通用字形</a>】(MS Word 格式)。</li>
	<li>
		【<a href='http://www.cbeta.org/data/cbeta/rare.htm' target='_blank' rel='nofollow'>CBETA 電子佛典缺字處理</a>】</li>
	<li>
		【<a href='http://www.cbeta.org/data/cbeta/budaword1.htm' target='_blank' rel='nofollow'>佛教藏經的文字問題與解決方案</a>】</li>
</ul>
<br><br>
</div>
	</body>
</html>