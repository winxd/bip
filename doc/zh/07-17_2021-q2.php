<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include '../../include/config.php'; ?>
			<?php include 'header.php'; ?>

			<h1>2021.Q2</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2021/04/30</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>補入 B（大藏經補編）、GA（中國佛寺史志彙刊）、GB（中國佛寺志叢刊）說明文件。</li>
				<li>新增新式標點：T50n2061《高僧傳》(30卷)。</li>
				<li>處理讀者於討論區及服務信箱所回饋的用字及標點問題。</li>
				<li>涉及的修訂共 488 卷 27,211 行，詳見變更記錄：<a href="changelog/2021/2021Q2.pdf">所有變更</a>(<?= formatSizeUnits(filesize('changelog/2021/2021Q2.pdf')) ?>)、<a href="changelog/2021/2021Q2-punc.pdf">僅標點變更</a>(<?= formatSizeUnits(filesize('changelog/2021/2021Q2-punc.pdf')) ?>)、<a href="changelog/2021/2021Q2-text.pdf">僅文字變更</a>(<?= formatSizeUnits(filesize('changelog/2021/2021Q2-text.pdf')) ?>)。</li></li>	
			</ol>
		</div>
</body>
</html>
