<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>

		<div class='container'>
		<?php include 'header.php'; ?>

		<h1>異體字與近義詞</h1> 
			<p class='ball'/>
			
			<ul style="margin-top:7em">
				<li>異體字資料來源：<a target="_blank" href="https://dict.variants.moe.edu.tw/variants/rbt/home.do">教育部《異體字字典》</a>、<a target="_blank" href="https://home.unicode.org/">Unicode</a>、<a target="_blank" href="https://ctext.org/faq/normalization/zh">CText 漢字標準化</a>、<a target="_blank" href="http://dict.cbeta.org/">CBETA 漢字資料庫</a>。</li>
				<li>感謝 <a target="_blank" href="https://www.naer.edu.tw/">國家教育研究院</a> 授權使用 <a target="_blank" href="https://dict.variants.moe.edu.tw/variants/rbt/home.do">教育部異體字</a>對應表及相關字圖。</li>
				<li>近義詞資料來源：取自佛光大辭典。</li>
			</ul>
			
			<p style="margin:2em">如有其他來源建議，請聯絡：da@dila.edu.tw</p>
		</div>
	</body>
</html>
