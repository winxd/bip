<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include '../../include/config.php'; ?>
			<?php include 'header.php'; ?>

			<h1>2022.Q3</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2022/08/10</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>新增《太虛大師全書》16-24冊。</li>
				<li>
					新增新式標點19部69卷：
					<ul>
						<li>T40n1804 四分律刪繁補闕行事鈔(3卷)</li>
						<li>T40n1805 四分律行事鈔資持記(3卷)</li>
						<li>T40n1806 四分律比丘含注戒本(3卷)</li>
						<li>T40n1807 四分比丘戒本疏(2卷)</li>
						<li>T40n1808 四分律刪補隨機羯磨(2卷)</li>
						<li>T40n1816 金剛般若論會釋(3卷)</li>
						<li>T40n1817 略明般若末後一頌讚述(1卷)</li>
						<li>T40n1818 法華論疏(3卷)</li>
						<li>T40n1819 無量壽經優婆提舍願生偈註(2卷)</li>
						<li>T40n1820 佛遺教經論疏節要(1卷)</li>
						<li>T46n1917 六妙法門(1卷)</li>
						<li>A091n1057 新譯大方廣佛華嚴經音義(2卷)</li>
						<li>A110n1490 天聖釋教總錄(2卷)</li>
						<li>A111n1493 大中祥符法寶錄(第3卷-第12卷)(9卷)</li>
						<li>A112n1493 大中祥符法寶錄(第13卷-第20卷)(7卷)</li>
						<li>A112n1494 景祐新修法寶錄(14卷)</li>
						<li>A114n1504 佛說大乘僧伽吒法義經(3卷)</li>
						<li>A114n1505 佛說清淨毘奈耶最上大乘經(2卷)</li>
						<li>A119n1548 因明論理門十四過類疏(1卷)</li>
						<li>A121n1561 瑜伽師地論義演(第33卷-第40卷)(5卷)</li>
					</ul>
				</li>
				<li>處理讀者於討論區及服務信箱所回饋的用字及標點問題。</li>
				<li>涉及的修訂共 3636 卷 96,242 行，詳見變更記錄：<a href="changelog/2022/2022Q3.pdf">所有變更</a>(<?= formatSizeUnits(filesize('changelog/2022/2022Q3.pdf')) ?>)、<a href="changelog/2022/2022Q3-punc.pdf">僅標點變更</a>(<?= formatSizeUnits(filesize('changelog/2022/2022Q3-punc.pdf')) ?>)、<a href="changelog/2022/2022Q3-text.pdf">僅文字變更</a>(<?= formatSizeUnits(filesize('changelog/2022/2022Q3-text.pdf')) ?>)。</li></li>	
			</ol>
		</div>
</body>
</html>
