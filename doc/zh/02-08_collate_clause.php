<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>回頁首</a>
	</head>
	<body>

<div class='container'>
<?php include 'header.php'; ?>

<h1>大正藏校勘條目說明</h1> <!--佳瑜新寫-->
	<p class='ball'/>
	<p>《大正藏》中，凡校勘條目有 ＊ (星號) 者，表示以下本文所附星號的校勘內容與本條校勘內容相同，所以僅以星號標示。</p>
	<p>關於《大正藏》星號校勘條目的意義，引《大正藏》書後「校異略符」中的說明如下：</p>
	</p>
		<blockquote><!--灰底灰框的圓角方塊-->
			<p>下同 (以下之ニ同ジ, 上ニ同ジ 'So below,' 'So above,' 'et passim')
			例：
			本文 (text)　世尊正[1]遊知
			註 (Note)　遊三本俱作遍下同トスベキヲ　[1]遊＝遍【三】＊ 
			(i.e. The Three Editions read 遍 for 遊, so also below)
			</p>
		</blockquote>
	<br>
	<p><b>以下為CBETA Online 閱讀介面的「呈現」和「引用複製」功能的說明：</b></p>
	<p class='m2'><b>1. CBETA Online 的呈現</b></p>
	<p class='m3'>例：</p>

	<p class='m3'><span class='navy'>《長阿含經》：「謂四[9]扼。欲[＊]扼．有[＊]扼．見[＊]扼．無明[＊]扼。復有四法。謂四無[＊]扼。無欲[＊]扼．無有[＊]扼．無見[＊]扼．無無明[＊]扼」(CBETA, T01, no.1, p. 51, a22-24)</span></p>

	<p align='center'><img border='0' src='pic/02-08-1.png'/>

	<p class='m3'>有 [＊] (星號) 的校勘在<b>校勘條目欄</b>裡可以呈現出全部 [＊] 的校勘註解，並快速找到其他星號，以及原來所屬的校勘條目項目。</p>
	<p class='m2'><b>2. CBReader 的「引用複製」</b></p>
	<p class='m3'>如果選擇上面那一段文後，點選”引用複製” 功能，選擇校勘條目(連同 ＊ )一起複製，複製的結果如下：</p>
	<p align='center'><img border='0' src='pic/02-08-2.png'/>
<br><br>
</div>
	</body>
</html>
