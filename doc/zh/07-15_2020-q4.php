<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include '../../include/config.php'; ?>
			<?php include 'header.php'; ?>

			<h1>2020.Q4</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2020/11/12</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>近代新編文獻新增《藏外佛教文獻》第十一輯、第十二輯。</li>
				<li>處理讀者於討論區及服務信箱所回饋的用字及標點問題。</li>
				<li>涉及的修訂共 456 卷，1,192 行，詳見變更記錄：<a href="changelog/2020/2020Q4.pdf">所有變更</a>(<?= formatSizeUnits(filesize('changelog/2020/2020Q4.pdf')) ?>)、<a href="changelog/2020/2020Q4-punc.pdf">僅標點變更</a>(<?= formatSizeUnits(filesize('changelog/2020/2020Q4-punc.pdf')) ?>)、<a href="changelog/2020/2020Q4-text.pdf">僅文字變更</a>(<?= formatSizeUnits(filesize('changelog/2020/2020Q4-text.pdf')) ?>)。</li></li>	
			</ol>
		</div>
</body>
</html>
