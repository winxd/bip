<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>回頁首</a>
	</head>
	<body>

<div class='container'>
<?php include 'header.php'; ?>

<h1>校勘條目修訂</h1> <!--http://www.cbeta.org/format/jk_help.php-->
	<p class='ball'/>
<p>CBETA《大正藏》校勘版，對原《大正藏》部分校勘條目類型予以修訂，略舉其大要如下：</p>
<ul>
	<li>校勘條目呈現&nbsp;CBETA&nbsp;修訂的內容。</li>
	<li>數條校勘條目相互交錯。</li>
	<li>校勘條目註「作本文」、「作夾註」等。</li>
	<li>有刪節符號 (&hellip;) 的校勘條目。（字數在 30 以下，補齊；字數在 30 以上，保留原樣。）</li>
	<li>內文有校勘符號，但無對應之校勘條目（或直接略去該校勘符號，或註明「<span class='navy'>&nbsp;CBETA 按</span>」，直接描述校勘的相關情況）。</li>
	<li>版本略符<span class='navy'>【三】</span>轉換成<span class='navy'>【宋】【元】【明】</span>，版本略符<span class='navy'>【三】＊</span> 轉換成<span class='navy'>【宋】＊【元】＊【明】＊</span>。</li>
	<li>某些特殊校勘條目會註明「<span class='navy'>&nbsp;CBETA 按</span>」，直接描述校勘的相關情況。</li>
	<li>文字印刷不明者，以 &quot;<span class='navy'>●</span>&quot; 表示。</li>
	<li>版本略符漏印者，以 &quot;<span class='navy'>【■】</span>&quot; 或按書中原貌顯示。</li>
	<li>於校勘條目中，<span class='navy'>か</span> 一律改作 <span class='navy'>ヵ</span>。</li>
	<li>有些校勘數字分 a、b&nbsp;與&nbsp;A、B<br>
		<ul>
			<li>小寫的&nbsp;a、b&nbsp;，是指&nbsp;CBETA&nbsp;將一個校勘條目拆成二組的狀況，例：<br>
				<br>
				<span class='navy'>T01n0023_p0277a03║[1a]大[1b]樓炭經卷第一<br>
				[0277001a] 〔大〕－【宋】【元】【明】<br>
				[0277001b] （佛說）＋樓【明】＊</span><br></li>
			<br>
			<li>大寫的&nbsp;A、B&nbsp;，是指大正藏內文中有兩處相同的校勘數字，&nbsp;CBETA&nbsp;將它分成&nbsp;A&nbsp;及&nbsp;B ，例：<br>
				<br>
				<span class='navy'>T27n1545_p0003a19_##難覺不可尋思非尋思境。[02A]唯有微妙聰叡<br>
				T27n1545_p0003b05_##作種種異說。然阿毘達磨勝義自性[02B]唯無漏</span></li>
		</ul>
	</li>
</ul>
<br><br>

	</body>
</html>
