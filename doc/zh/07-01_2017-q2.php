<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


<div class='container'>
<?php include 'header.php'; ?>

<h1>2017.Q2</h1> 
	<p class='ball'/>

	<h3>發行日期：June 01 , 2017</h3>
	<br/>
	
	<h3>一、藏經代碼變更</h3>
	<p class="m3">
		<ol>
			<li>《正史佛教資料類編》舊代碼為 H 變更為 ZS</li>
			<li>《藏外佛教文獻》舊代碼為 W 變更為 ZW</li>
		</ol>
	</p>
	<h3>二、文字修訂共 525 卷，1604 行，詳如<a href="changelog/2017/cbeta-2017Q2-change-log.pdf">變更記錄</a>。</h3>
	
	</body>
</html>
