<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include '../../include/config.php'; ?>
			<?php include 'header.php'; ?>

			<h1>2021.Q1</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2021/01/15</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>連同上一季，補入ZW（藏外佛教文獻）、ZS（正史佛教資料類編）、I（北朝佛教石刻拓片百品）說明文件78篇。</li>
				<li>重新架構 T49n2034《歷代三寶紀》及 T27n1545《大毘婆沙論》目錄。</li>
				<li>處理讀者於討論區及服務信箱所回饋的用字及標點問題。</li>
				<li>涉及的修訂共 575 卷，1,291 行，詳見變更記錄：<a href="changelog/2021/2021Q1.pdf">所有變更</a>(<?= formatSizeUnits(filesize('changelog/2021/2021Q1.pdf')) ?>)、<a href="changelog/2021/2021Q1-punc.pdf">僅標點變更</a>(<?= formatSizeUnits(filesize('changelog/2021/2021Q1-punc.pdf')) ?>)、<a href="changelog/2021/2021Q1-text.pdf">僅文字變更</a>(<?= formatSizeUnits(filesize('changelog/2021/2021Q1-text.pdf')) ?>)。</li></li>	
			</ol>
		</div>
</body>
</html>
