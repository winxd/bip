<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include '../../include/config.php'; ?>
			<?php include 'header.php'; ?>

			<h1>2019.Q3</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2019/10/17</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>近代新編文獻新增「呂澂佛學著作集」之《經論講要》兩冊。</li>
				<li>
					以下經文加上新式標點（共 6 經 21 卷）
					<ul>
						<li>T18n0848  大毘盧遮那成佛神變加持經(7卷)【唐 善無畏．一行譯】</li>
						<li>T18n0849  大毘盧遮那佛說要略念誦經(1卷)【唐 菩提金剛譯】</li>
						<li>T18n0850  攝大毘盧遮那成佛神變加持經入蓮華胎藏海會悲生曼荼攞廣大念誦儀軌供養方便會(3卷)【唐 輸婆迦羅譯】</li>
						<li>T18n0851  大毘盧遮那經廣大儀軌(3卷) 【唐 善無畏譯】</li>
						<li>T18n0852a 大毘盧遮那成佛神變加持經蓮華胎藏悲生曼荼羅廣大成就儀軌供養方便會(2卷)【唐 法全撰】</li>
						<li>T18n0852b 大毘盧舍那成佛神變加持經蓮華胎藏悲生曼荼羅廣大成就儀軌(2卷)</li>
						<li>T18n0853  大毘盧遮那成佛神變加持經蓮華胎藏菩提幢標幟普通真言藏廣大成就瑜伽(3卷)【唐 法全集】</li>
					</ul>
				</li>
				<li style="margin-top:1em">卍續藏無效星號校註 1668 筆予以建立連結。</li>
				<li>處理讀者於討論區及服務信箱所回饋的用字及標點問題。</li>
				<li>涉及的修訂共 95 卷，11,223 行，詳見變更記錄：<a href="changelog/2019/2019Q3.pdf">所有變更</a>(<?= formatSizeUnits(filesize('changelog/2019/2019Q3.pdf')) ?>)、<a href="changelog/2019/2019Q3-punc.pdf">僅標點變更</a>(<?= formatSizeUnits(filesize('changelog/2019/2019Q3-punc.pdf')) ?>)、<a href="changelog/2019/2019Q3-text.pdf">僅文字變更</a>(<?= formatSizeUnits(filesize('changelog/2019/2019Q3-text.pdf')) ?>)。</li></li>	
			</ol>
		</div>
</body>
</html>
