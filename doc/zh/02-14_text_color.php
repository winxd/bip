<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>回頁首</a>
	</head>
	<style type="text/css">
		p	{
			margin-bottom:3em;
		}
		
		p img	{
			display:block;
			clear:both;
			padding-top:1em;
		}
		
		.color-item	{
			float:left;
			margin-left:.5em;
		}
		
		.color-item:last-of-type::after	{
			content:"";
			display:block;
			clear:both;
		}
		.color-block	{
			border:1px black solid;
			border-radius:5px;
			width:1.3em;
			height:1.3em;
			margin-top: 5px;
		}
		
	</style>
	<body>

		<div class='container'>
			<?php include 'header.php'; ?>
			<h1>本文區文字樣式說明</h1> 
			<p class='ball'/>

			<h2>標題</h2>
			<p>
				<span class="color-item">字體顏色：</span><span class="color-block color-item" style="background-color:#0000A0"></span><span class="color-item">、字體大小：20px</span>
				<img src="pic/text-color-head.png" />
			</p>

			<h2>卷首</h2>
			<p>
				<span class="color-item">字體顏色：</span><span class="color-block color-item" style="background-color:#0000FF"></span><span class="color-item">、字體大小：20px</span>
				<img src="pic/text-color-juan.png" />
			</p>

			<h2>作譯資訊</h2>
			<p>
				<span class="color-item">字體顏色：</span><span class="color-block color-item" style="background-color:#408080"></span><span class="color-item">、字體大小：17px</span>
				<img src="pic/text-color-byline.png" />
			</p>
			
			<h2>偈頌</h2>
			<p>
				<span class="color-item">字體顏色：</span><span class="color-block color-item" style="background-color:#008040"></span><span class="color-item">、字體大小：17px</span>
				<img src="pic/text-color-lg.png" />
			</p>
			
			<h2>CBETA 修訂字</h2>
			<p>
				<span class="color-item">字體顏色：</span><span class="color-block color-item" style="background-color:red"></span><span class="color-item">、字體大小：17px</span>
				<img src="pic/text-color-cbeta.png" />
			</p>
			
			<h2>雙行夾注</h2>
			<p>
				<span class="color-item">字體顏色：</span><span class="color-block color-item" style="background-color:purple"></span><span class="color-item">、字體大小：14px</span>
				<img src="pic/text-color-double-line-note.png" />
			</p>
			
			<h2>序文</h2>
			<p>
				<span class="color-item">字體顏色：</span><span class="color-block color-item" style="background-color:#0000a0"></span><span class="color-item">、字體大小：17px</span>
				<img src="pic/text-color-xu.png" />
			</p>
			
			<h2>附文</h2>
			<p>
				<span class="color-item">字體顏色：</span><span class="color-block color-item" style="background-color:#AD0C6A"></span><span class="color-item">、字體大小：17px</span>
				<img src="pic/text-color-append.png" />
			</p>
		

		</div>
</body>
</html>