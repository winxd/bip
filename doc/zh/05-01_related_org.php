<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>

	<body>

<div class='container'>
<?php include 'header.php'; ?>

<h1>CBETA 相關單位</h1> <!--http://www.cbeta.org/unit.php-->
	<p class='ball'/>
<p class='m2'>承辦單位：中華電子佛典協會</p> <!--style1約右移二個字-->
<p class='m2'>主辦單位：菩提文教基金會、<a href='http://www.seeland.org.tw' target='_blank' rel='nofollow'>西蓮教育基金會</a> （<a href='http://www.cbeta.org/gongde.htm#postno' target='_blank' rel='nofollow'>專案捐款帳戶</a>）</p><!--待更新連結為05-07_大德芳名錄.html-->
<p class='m2'>協辦單位：<a href='http://www.chibs.edu.tw' target='_blank' rel='nofollow'>中華佛學研究所</a>、<a href='http://www.dila.edu.tw/' target='_blank' rel='nofollow'>法鼓文理學院</a></p>
<p class='m2'>贊助者：<a href='http://www.yinshun.org' target='_blank' rel='nofollow'>北美印順導師基金會</a>、<a href='http://www.budaedu.org' target='_blank' rel='nofollow'>佛陀教育基金會</a>、黃淑玲居士與<a href='http://www.cbeta.org/gongde.htm' target='_blank' rel='nofollow'>諸大德</a></p><!--待更新連結為05-07_大德芳名錄.html-->
<p class='m2'>Organizer : CBETA</p>
<p class='m2'>Sponsors : Bodhi Foundation, Taipei<br>
	<a href='http://www.seeland.org.tw/' target='_blank' rel='nofollow'>The Seeland Education Foundation, Taipei</a> (<a href='http://www.cbeta.org/gongde.htm#postno' target='_blank' rel='nofollow'>the Donation Account</a>)</p><!--待更新連結為05-07_大德芳名錄.html-->
<p class='m2'>Co-sponsor : <a href='http://www.chibs.edu.tw' target='_blank' rel='nofollow'> Chung-Hwa Institute of Buddhist Studies</a><br>
	<a href='http://www.dila.edu.tw/' target='_blank' rel='nofollow'>Dharma Drum Institute of Liberal Arts</a></p>
<p class='m2'>Donators : <a href='http://www.yinshun.org' target='_blank' rel='nofollow'>Yinshun Foundation of North America</a><br>
	The Corporate Body of the <a href='http://www.budaedu.org' target='_blank' rel='nofollow'>Buddha Educational Foundation</a><br>
	Ms. Elaine Ng and <a href='http://www.cbeta.org/gongde.htm' target='_blank' rel='nofollow'>Other Contributors</a></p><!--待更新連結為05-07_大德芳名錄.html-->
<br><br/>

	</body>
</html>