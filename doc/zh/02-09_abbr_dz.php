<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>回頁首</a>
	</head>
	<body>

<div class='container'>
<?php include 'header.php'; ?>

<h1>對校略符</h1> <!--http://www.cbeta.org/format/abbr_dz.php-->
	<p class='ball'/>
	<p>本表依《大正藏》書末略符表而作。略符表雖僅舉甲、乙、丙為例說明，惟其餘當可依此類推，如書中實例及以丁、戊、己。丁、戊、己三例，於「說明」欄中兼載出現冊別。</p>
<table>
	<tr>
		<th class='center'>大正藏<br>略符</th>
		<th class='center'>CBETA<br>略符</th>
		<th class='center'>　　　說　　　明</th>
	</tr>
	<tr>
		<td class='center'><img border='0' height='37' src='pic/orig.gif' width='35' /></td>
		<td class='center'>【原】</td>
		<td>(The MS. or book on which the printed text is based.)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='34' src='pic/a.gif' width='37' /></td>
		<td class='center'>【甲】</td>
		<td>(The first text collated.)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='32' src='pic/b.gif' width='33' /></td>
		<td class='center'>【乙】</td>
		<td>(The second text collated.)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='31' src='pic/c.gif' width='33' /></td>
		<td class='center'>【丙】</td>
		<td>(The third text collated.)</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【丁】</td>
		<td>T18、T20、T21、T43、T47、T51、T85</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【戊】</td>
		<td>T43、T47、T51</td>
	</tr>
	<tr>
		<td class='center'>闕略</td>
		<td class='center'>【己】</td>
		<td>T43、T47、T51</td>
	</tr>
	<tr>
		<td colspan='2' class='center'><span lang='JA'>イ</span></td>
		<td>
			<p>底本又ハ對校本註記ノ校異 (A varius reading given in a note of the original text or the text collated.)</p>
			<p><span class='navy'>CBETA按：「底本」或某一「校對本」有註記說在其他本有不同字 (校異)</span></p>
			<p>例：</p>
			<p>(a) 底本註記ニ止一本作正トアルヲ　止＝正イ【原】 (i.e. In the original text (MS. or book) it is noted that a text reads 正 for 止)</p>
			<p>(b) 對校本 a 註記ニ止一本無トアルヲ　〔止〕イ－【甲】 (i.e. In the first text collated it is noted that 止 is wanting in a text)</p>
		</td>
	</tr>
	<tr>
		<td colspan='2' class='center'><span lang='JA'>カ</span></td>
		<td>
			<p>底本又ハ對校本註記ノ考偽&nbsp;(A correction given in a note of the original text or the text collated.)</p>
			<p><span class='navy'>CBETA按：「底本」或某一「校對本」的註記中所作的勘誤 (考偽)</span></p>
			<p>例：</p>
			<p>(a) 底本註記ニ捐疑損歟トアルヲ　捐＝損カ【原】 (i.e. In the original text (MS. or book) it is noted that 捐 may be a mistake for 損)</p>
			<p>(b) 對校本註記ニ如下疑有是字歟トアルヲ　如＋（是）カ【乙】 (i.e. In the second text collated it is noted that 是 is to be read under 如)</p>
		</td>
	</tr>
	<tr>
		<td colspan='2' class='center'><span lang='zh-tw'>？</span></td>
		<td>本藏校合者ノ考偽　朋＝明？ (i. e. An editiorial note: －明 to be read for 朋？)</td>
	</tr>
</table>
<br><br>
</div>
	</body>
</html>
