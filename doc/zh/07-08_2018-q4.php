<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include 'header.php'; ?>

			<h1>2018.Q4</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2019/01/09</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>
					重編「歷代藏經補輯-金藏」經號。<br/>
					<table style="width:600px;float:left;margin:1em 0;font-size:smaller">
						<thead>
							<tr><th>舊經號</th><th>新經號</th><th>經名</th></tr>
						</thead>
						<tbody>
							<tr><td>A091n1066</td><td>A091n1057</td><td>新譯大方廣佛華嚴經音義</td></tr>
							<tr><td>*A097n1276</td><td>A097n1267</td><td>大唐開元釋教廣品歷章</td></tr>
							<tr><td>*A098n1276</td><td>A098n1267</td><td></td></tr>
							<tr><td>A110n1499</td><td>A110n1490</td><td>天聖釋教總錄</td></tr>
							<tr><td>*A111n1501</td><td>A111n1493</td><td>大中祥符法寶錄</td></tr>
							<tr><td>*A112n1501</td><td>A112n1493</td><td></td></tr>
							<tr><td>A112n1502</td><td>A112n1494</td><td>景祐新修法寶錄</td></tr>
							<tr><td>A114n1510</td><td>A114n1504</td><td>佛說大乘僧伽吒法義經</td></tr>
							<tr><td>A114n1511</td><td>A114n1505</td><td>佛說清淨毘奈耶最上大乘經</td></tr>
							<tr><td>A119n1553</td><td>A119n1548</td><td>因明論理門十四過類疏</td></tr>
							<tr><td>*A120n1565</td><td>A120n1561</td><td>瑜伽師地論義演</td></tr>
							<tr><td>*A121n1565</td><td>A121n1561</td><td></td></tr>
						</tbody>
					</table>
					<div style="clear:both" />
					※行首標有 * 者，表示該經跨冊。
				</li>
				<li>擴展通用字範圍，並修訂若干缺字資訊。</li>
				<li>處理讀者於論壇及服務信箱所回饋的用字及標點問題。</li>
				<li>涉及的修訂共 441 卷、1,142 行，詳見變更記錄(<a href="changelog/2018/2018Q4.pdf">所有變更</a>、<a href="changelog/2018/2018Q4-punc.pdf">僅標點變更</a>、<a href="changelog/2018/2018Q4-text.pdf">僅文字變更</a>)。</li></li>
			</ol>
		</div>
</body>
</html>
