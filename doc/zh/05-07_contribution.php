<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
 <head>
 <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
 <link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>回頁首</a>
 </head>
 <body> <!--http://www.cbeta.org/cbreader/help/gongde_1.htm-->

<div class='container'>
<?php include 'header.php'; ?>

<p align='center'>
	<a href='#1' class='menu2'>義工名單</a> | 
	<a href='#2' class='menu2'>捐贈贊助大德</a> | 
	<a href='#tech' class='menu2'>技術協助</a> | 
	<a href='#provider' class='menu2'>經文提供</a> | 
	<a href='#colleague' class='menu2'>CBETA 同仁</a>
</p>

<br>
<h2>CBETA 感謝以下諸大德的成就因緣：</h2>
<h3>遠因 ：</h3>
	<p class='m3'>過去數年，經由眾人努力，網路上集結了不少佛教經典，也因此帶動佛典電子檔輸入熱潮。大家的目標主要著重在網路應用，比如將這些電子檔置放於 FTP上供人免費下傳，或是透過 GOPHER、WWW 方便使用者瀏覽，最近更在 WWW 上提供檢索查詢功能。另個發展是將電子檔包裝設計成電子書，以使經文的呈現更加精緻。所有這些努力，莫不希望能夠透過網路，使佛典普及，讓更多人同霑法益，並利用電腦的能力拓展佛典的應用範圍及閱讀方式。</p>

<h3>近因 ：</h3>
	<ol class='m2'>
		<li>蕭鎮國先生的來函，提供 25冊 CCCII 大正藏電子稿，並於 1997 年 11 月 6 日 25T小組籌備會議之後，授權台大佛研中心進行後續之處理。 </li>
		<li>網路上電子佛典討論版 (buda-tech) 的朋友草擬了電子版大藏經輸入計畫，開始有計畫的進行經典輸入。 </li>
		<li>1997 年 11 月 6 日由台大佛研中心成立 25T 小組，著手開始進行大規模的藏經電子化。 (二十五冊大正藏會議整理)</li>
	</ol>

<h3>成立 ：</h3>
	<p class='m3'>由台大佛研中心釋恆清法師開始籌募所需經費，並感謝「<span style='font-weight:bold; color:#800000'>北美印順導師基金會</span>」與「<span style='font-weight:bold; color:#800000'>中華佛學研究所</span>」全力支持贊助後，於 1998 年 2 月 15 日假法鼓山安和分院舉辦籌備會議並於當日正式成立 「中華電子佛典協會」。</p>

<h3>助緣 ：</h3>
	<ol class='m2'>
		<li>中研院資訊所謝清俊教授長期來推動與指導佛典資訊化工作，並擔任本協會顧問。 </li>
		<li>Buda-Tech 討論群：台大獅子吼 BBS 站及中山鹿苑佛教 BBS 專站的討論版，專門進行電子佛典化的相關問題討論，並由曾國豐先生架設 Mailing List 提供討論者使用。許多佛典資料及相關技術都是本版網友努友的結果。 </li>
		<li>電子佛典編輯小組 (EBTWG) ：由徐言輝及幾位朋友組成之小組，主要是利用 SCAN+OCR 技術，以佛教大藏經為主，而有系統的產生電子經文檔。 </li>
		<li>25T 小組：由台大佛研中心主導，負責處理蕭鎮國先生所提供之 25 冊 CCCII 格式的大正藏經文檔，本小組即為中華電子佛典協會之前身。 </li>
		<li>缺字小組 ：為了深入討論佛典缺字的解決方案，而另外成立的討論小組。 </li>
		<li>尚有很多熱心朋友與同道默默的支持與鼓勵，無法一一記名，謹此表達致謝之意。</li>
	</ol>
<br>
<hr><br>
<h2>感謝</h2>
	<p class='m3'><big>財團法人菩提文教基金會於 1998 年 2 月 15 日至 2001 年 1 月 31 日，負責 CBETA計畫之財務與行政等業務。</big></p>
	<p class='m3'><big>自 2001 年2月 1 日起，則改由財團法人西蓮教育基金會承辦此業務。</big></p>
	<p class='m3'><big>謹此銘謝該兩個基金會所做的一切努力。</big></p>
<br>
<hr><br>
<h2><a name='1'></a>義工名單：</h2>
	<p>(以事情先後排序，恕不另加稱呼)</p>
	<h4 class='m2'>東湖國小老師：</h4>
		<ul class='m2'>
			<li>姜雲書(連絡人)、胡台生、伍美櫻、呂慶焜、陳琳武、劉明惠、鄧善惠、曹碧瑤、黃[王*黎]慧、藍素蘭、呂仙美、林麗嬋</li>
		</ul>
	<h4 class='m2'>瑜伽師地論專案 ：</h4>
		<ul class='m2'>
			<li>黃明珠、黃俊義、許介瑋、葛賢敏、黃添盛、林盈璋、溫武男、鄭聰賢、吳綺君、黃俊榮、許明福、胡瑞祥、越建東</li>
		</ul>
	<h4 class='m2'>輸入義工：</h4>
		<ul class='m2'>
			<li>田萬頂、任家斌、余春榮、李佩璇、李素英、林士芳、林忠鈺、林鴻萱、施潔玲、徐誠廷、區宗明、張嵐、莊士億、陳妙如、陳麗琪、楊織任、楊穩燕、劉建良、蔡寧君、謝美華、蘇明健、釋慧然、林麗萍、陳麗娟、蔡麗娟</li>
		</ul>
	<h4 class='m2'>校對義工：</h4>
		<p class='m2'>依Big5內碼順序排列： </p>
		<ul class='m2'>
			<li>LAN SUM、王淑美、王瑞鄉、左菁華、伍美櫻、何淑嫻、呂隆霖、呂慶焜、李子復、李淑月、林文群、林伯慧、林惠川、林麗嬋、姜雲書、姜瑾莉、徐正筠、徐秀鈴、高蕊、張琬如、張福添、梁懷安、郭春櫻、陳杉吉、陳政谷、陳珊珊、陳崇仁、黃[王*黎]慧、廖彩賢、劉明惠、劉淳如、慧乘法師、戴承正、藍素蘭、李淑卿 </li>
			<li>何宗武、李子復、徐言輝</li>
		</ul>
	<h4 class='m2'>掃瞄校對義工：</h4>

	<p class='m3'>
		<b>&#149&nbsp; </b> 屏東：林韋儒<br>
		<b>&#149&nbsp; </b> 高雄：邱淑梅、曾毓承、林榮恩(連絡人)等<br>
		<b>&#149&nbsp; </b> 台中：吳憲明、葉瑞賢、張智薰、杜秀如(連絡人)等<br>
		<b>&#149&nbsp; </b> 宜蘭：游秀苓、杜宗昇等<br>
		<b>&#149&nbsp; </b> 台北：蔡豊姣、唐玲惠、林秀雲、蔣明珠、何美蓮、倫淑燕、周梅玲、邱秀珍、陳聖杰、賴雪杏、<br>
				<span class='m4'>許秋菊、黃素卿、何懷嵩、黃啟南、林祺安、黃淑貞<br>
				<span class='m4'>板橋文聖國小王老師(連絡人)等<br>
				<span class='m4'>基隆國中高老師(連絡人)等<br>
				<span class='m4'>高月嬌(連絡人)等<br>
				<span class='m4'>陳幸鈴(連絡人)等</span></p>

	<h4 class='m2'>網路校對義工：</h4>
		<p class='m3'>
			<span class='orange'>&#9654&#9654&#9654 </span><a href='http://www.cbeta.org/cbreader/help/gongde_web_jd.htm' name='web_jd'target='_blank' rel='nofollow'>《卍續藏》校對者名單</a><br>
			<span class='orange'>&#9654&#9654&#9654 </span><a href='http://www.cbeta.org/onepage/finish-B.htm' target='_blank' rel='nofollow'>《大藏經補編》第一波校對者名單</a><br>
			<span class='orange'>&#9654&#9654&#9654 </span><a href='http://www.cbeta.org/onepage/finish-B2.htm' target='_blank' rel='nofollow'>《大藏經補編》第二波校對者名單</a><br>
			<span class='orange'>&#9654&#9654&#9654 </span><a href='http://www.cbeta.org/cbreader/help/gongde_web_word100.htm' name='web_jd'target='_blank' rel='nofollow'>百字缺字查詢義工詳細名單</a><br>
		</p>
		
	<h4 class='m2'>其他：</h4>
		<ul class='m2'>
			<li>謝宗元、林佰慧、張琬如、王淑美、蔡淑萍、李毅郎、戴仁鴻、張家禎、黃彩媚、柯怡玟、林擁欣</li>
		</ul>
<br><br>
<hr><br>
<h2 class='m2'><a name='2'></a>捐贈及贊助&nbsp;CBETA&nbsp;的大德們：</h2>

		<p class='m2'>（因資料較多，於另頁記載）</p>
		<div class='flex'>
			<a target='_blank' href='http://www.cbeta.org/cbreader/help/gongde2001.htm'>2001年(含)之前</a>
			<a target='_blank' href='http://www.cbeta.org/cbreader/help/gongde2002.htm'>2002</a>
			<a target='_blank' href='http://www.cbeta.org/cbreader/help/gongde2003.htm'>2003</a>
			<a target='_blank' href='http://www.cbeta.org/cbreader/help/gongde2004.htm'>2004</a>
			<a target='_blank' href='http://www.cbeta.org/cbreader/help/gongde2005.htm'>2005</a>
			<a target='_blank' href='http://www.cbeta.org/cbreader/help/gongde2006.htm'>2006</a>
			<a target='_blank' href='http://www.cbeta.org/cbreader/help/gongde2007.htm'>2007</a>
			<a target='_blank' href='http://www.cbeta.org/cbreader/help/gongde2008.htm'>2008</a></div>
			<div class='flex'>
			<a target='_blank' href='http://www.cbeta.org/cbreader/help/gongde2009.htm'>2009
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
			<a target='_blank' href='http://www.cbeta.org/cbreader/help/gongde2010.htm'>2010</a>
			<a target='_blank' href='http://www.cbeta.org/cbreader/help/gongde2011.htm'>2011</a>
			<a target='_blank' href='http://www.cbeta.org/cbreader/help/gongde2012.htm'>2012</a>
			<a target='_blank' href='http://www.cbeta.org/cbreader/help/gongde2013.htm'>2013</a>
			<a target='_blank' href='http://www.cbeta.org/cbreader/help/gongde2014.htm'>2014</a>
			<a target='_blank' href='http://www.cbeta.org/cbreader/help/gongde2015.htm'>2015</a>
			<a target='_blank' href='http://www.cbeta.org/cbreader/help/gongde2016.htm'>2016</a>
		</div>
<br><br>
<hr><br>
<h2 class='m2'><a name='tech'></a>技術協助：</h2>
	<ul class='m2'>
		<li>謝清俊、許清琦、于凌波、張景全、葉健欣、楊忠權、Huiqun、蔡文榮、邱大剛、施得勝、釋自衍、朱秉正、蔡慶琦、王圳雄、王尚智、許素朱、林佩菁、張博智、甘仲銘、袁天紘、莊德明、黃郁婷、李志強、王志攀、蔡日新、釋聖性、莊臺龍、釋道中、國際禪友會、吳依柔</li>
		<li>悉曇字字型、輸入法提供：嘉豐出版社 林光明、陳惠珍 </li>
		<li>力新國際科技股份有限公司 (丹青 OCR 系統)</li>
		<li>法鼓山資訊中心、法鼓文化相關同仁、香光資訊網、中華佛典寶庫</li>
		<li>特一國際形象設計股份有限公司、Melon</li>
	</ul>
<br><br>
<hr><br>
<h2 class='m2'><a name='tech'></a>網站協助：</h2>
	<ul class='m2'>
		<li>網站維護：許陳候、黃國展 </li>
		<li>網頁設計：國立藝術學院 科技藝術研究中心 </li>
		<li>XML檢索：國立台灣大學資訊工程學系歐陽彥正教授率其實驗室團隊研發提供 </li>
		<li>CGI 協助設計：白明弘</li>
	</ul>
<br><br>
<hr><br>
<h2 class='m2'><a name='provider'></a>經文提供：</h2>
	<ul class='m2'>
		<li>釋厚觀、蕭鎮國、維習安 (Dr. Christian Wittern) 、張文明、Monica、北美某大德</li>
		<li>佛教電腦資訊庫功德會、妙雲蘭若、香光寺、靈山寺(南投名間)、眾生出版社、新雨佛學社、佛典推廣小組、西蓮淨苑、伽耶山基金會、法雲寺、日本 SAT 組織、日本瑜伽行思想研究會 </li>
		<li>EBTWG：徐言輝、何宗武等人 </li>
		<li>佛教經典系列：定廣法師、Gamblers、Grief、小八極、斯覺、姜佩君、莊德明、周金言、徐言輝、張自強、陳祈森、陳建銘、陳鋕雄、曾國豐、閻學新、郭鴻達、魏世杰、摩訶工作室、亞磬文化資訊站 </li>
		<li>佛教藏 OCR 小組：王宗裕、王志堅、王政仁、王建鎮、黃上銘、楊忠權、劉景文 </li>
		<li>其他大德</li>
	</ul>
		<p class='m3'>* 提供比對檔案資料名單，亦分別列於各經卷之前，隨經文流通。</p>
<br><br>
<hr><br>
<h2><a name='colleague'></a>本會同仁：</h2>
	<p class='m2'><b>主任委員：</b>惠敏法師</p>
	<p class='m2'><b>副主任委員：</b>厚觀法師、杜正民</p>
	<p class='m2'><b>常務委員：</b>恆清法師</p>
	<p class='m2'><b>總幹事：</b>吳寶原</p>
	<p class='m2'><b>研發組：</b>維習安 (兼本會顧問)</p>
	<p class='m2'><b>標記組：</b>呂美智、[劉黃淑儀]</p>
	<p class='m2'><b>輸入、校對組：</b>童闓澤、[謝淑歆]</p>
	<p class='m2'><b>網資組：</b>淨海</p>
	<p class='m2'><b>財會行政組：</b>黃璱璱</p>
	<p class='m2'>以上 [ ] 內為委外人員</p>
<br><br/>
</div>
 </body>
</html>