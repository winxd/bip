<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include '../../include/config.php'; ?>
			<?php include 'header.php'; ?>

			<h1>2021.Q4</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2021/11/24</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>
					新增新式標點（20部258卷）：
					<ul>
						<li>T34n1720 法華玄論(10卷)</li>
						<li>T34n1725 法華宗要(1卷)</li>
						<li>T34n1726 觀音玄義(2卷)</li>
						<li>T34n1727 觀音玄義記(4卷)</li>
						<li>T34n1728 觀音義疏(2卷)</li>
						<li>T34n1729 觀音義疏記(4卷)</li>
						<li>T34n1730 金剛三昧經論(3卷)</li>
						<li>T35n1731 華嚴遊意(1卷)</li>
						<li>T35n1732 大方廣佛華嚴經搜玄分齊通智方軌(10卷)</li>
						<li>T35n1733 華嚴經探玄記(20卷)</li>
						<li>T35n1734 花嚴經文義綱目(1卷)</li>
						<li>T35n1735 大方廣佛華嚴經疏(60卷)</li>
						<li>T36n1736 大方廣佛華嚴經隨疏演義鈔(90卷)</li>
						<li>T36n1737 大華嚴經略策(1卷)</li>
						<li>T36n1738 新譯華嚴經七處九會頌釋章(1卷)</li>
						<li>T36n1739 新華嚴經論(40卷)</li>
						<li>T36n1740 大方廣佛華嚴經中卷卷大意略敘(又名華嚴經大意)(1卷)</li>
						<li>T36n1741 略釋新華嚴經修行次第決疑論(又名華嚴經決疑論)(4卷)</li>
						<li>T36n1742 大方廣佛華嚴經願行觀門骨目(又名華嚴經骨目)(2卷)</li>
						<li>T36n1743 皇帝降誕日於麟德殿講大方廣佛華嚴經玄義一部(又名大周經玄義)(1卷)</li>
					</ul>
				</li>
				<li>處理讀者於討論區及服務信箱所回饋的用字及標點問題。</li>
				<li>涉及的修訂共 959 卷 226,203 行，詳見變更記錄：<a href="changelog/2021/2021Q4.pdf">所有變更</a>(<?= formatSizeUnits(filesize('changelog/2021/2021Q4.pdf')) ?>)、<a href="changelog/2021/2021Q4-punc.pdf">僅標點變更</a>(<?= formatSizeUnits(filesize('changelog/2021/2021Q4-punc.pdf')) ?>)、<a href="changelog/2021/2021Q4-text.pdf">僅文字變更</a>(<?= formatSizeUnits(filesize('changelog/2021/2021Q4-text.pdf')) ?>)。</li></li>	
			</ol>
		</div>
</body>
</html>
