<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include '../../include/config.php'; ?>
			<?php include 'header.php'; ?>

			<h1>2020.Q1</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2020/04/16</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>近代新編文獻新增「呂澂佛學著作集」之《西藏佛學原論及佛學論著選集》、《因明入正理論講解及相關論著》、《佛教重要名相釋義及經論攷證》、《歷朝藏經略考及新編漢文大藏經目錄》四冊。</li>
				<li>
					新增的新式標點有以下59部
					<ul>
						<li>T18n0854</li>
						<li>T18n0855</li>
						<li>T18n0856</li>
						<li>T18n0857</li>
						<li>T18n0858</li>
						<li>T18n0866</li>
						<li>T18n0867</li>
						<li>T18n0868</li>
						<li>T18n0870</li>
						<li>T18n0871</li>
						<li>T18n0872</li>
						<li>T18n0873</li>
						<li>T18n0874</li>
						<li>T18n0875</li>
						<li>T18n0876</li>
						<li>T18n0877</li>
						<li>T18n0878</li>
						<li>T18n0879</li>
						<li>T18n0880</li>
						<li>T18n0881</li>
						<li>T18n0882</li>
						<li>T18n0883</li>
						<li>T18n0884</li>
						<li>T18n0885</li>
						<li>T18n0886</li>
						<li>T18n0887</li>
						<li>T18n0888</li>
						<li>T18n0889</li>
						<li>T18n0890</li>
						<li>T18n0891</li>
						<li>T18n0892</li>
						<li>T18n0894a</li>
						<li>T18n0894b</li>
						<li>T18n0895a</li>
						<li>T18n0895b</li>
						<li>T18n0896</li>
						<li>T18n0897</li>
						<li>T18n0898</li>
						<li>T18n0899</li>
						<li>T18n0900</li>
						<li>T18n0901</li>
						<li>T18n0902</li>
						<li>T18n0903</li>
						<li>T18n0904</li>
						<li>T18n0905</li>
						<li>T18n0906</li>
						<li>T18n0907</li>
						<li>T18n0908</li>
						<li>T18n0909</li>
						<li>T18n0910</li>
						<li>T18n0911</li>
						<li>T18n0912</li>
						<li>T18n0913</li>
						<li>T18n0914</li>
						<li>T18n0915</li>
						<li>T18n0916</li>
						<li>T18n0917</li>
						<li>T54n2131</li>
						<li>X65n1290</li>

					</ul>
				</li>
				<li style="margin-top:1em">卍續藏無效星號校註 17 筆予以建立連結。</li>
				<li>處理讀者於討論區及服務信箱所回饋的用字及標點問題。</li>
				<li>涉及的修訂共 348 卷，59,185 行，詳見變更記錄：<a href="changelog/2020/2020Q1.pdf">所有變更</a>(<?= formatSizeUnits(filesize('changelog/2020/2020Q1.pdf')) ?>)、<a href="changelog/2020/2020Q1-punc.pdf">僅標點變更</a>(<?= formatSizeUnits(filesize('changelog/2020/2020Q1-punc.pdf')) ?>)、<a href="changelog/2020/2020Q1-text.pdf">僅文字變更</a>(<?= formatSizeUnits(filesize('changelog/2020/2020Q1-text.pdf')) ?>)。</li></li>	
			</ol>
		</div>
</body>
</html>
