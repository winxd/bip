<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include '../../include/config.php'; ?>
			<?php include 'header.php'; ?>

			<h1>2021.Q3</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2021/07/23</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>
					新增新式標點：
					<ul>
						<li>T26n1521 十住毘婆沙論(17卷)</li>
						<li>T33n1693 人本欲生經註(1卷)</li>
						<li>T33n1694 陰持入經註(2卷)</li>
						<li>T33n1695 大般若波羅蜜多經般若理趣分述讚(3卷)</li>
						<li>T33n1696 大品經遊意(1卷)</li>
						<li>T33n1697 大慧度經宗要(1卷)</li>
						<li>T33n1698 金剛般若經疏(1卷)</li>
						<li>T33n1699 金剛般若疏(4卷)</li>
						<li>T33n1700 金剛般若經贊述(2卷)</li>
						<li>T33n1701 金剛般若經疏論纂要(2卷)</li>
						<li>T33n1703 金剛般若波羅蜜經註解(1卷)</li>
						<li>T33n1704 金剛般若波羅蜜經略疏(2卷)</li>
						<li>T33n1705 仁王護國般若經疏(5卷)</li>
						<li>T33n1706 仁王護國般若波羅蜜經疏神寶記(4卷)</li>
						<li>T33n1707 仁王般若經疏(6卷)</li>
						<li>T33n1708 仁王經疏(6卷)</li>
						<li>T33n1709 仁王護國般若波羅蜜多經疏(7卷)</li>
						<li>T33n1710 般若波羅蜜多心經幽贊(2卷)</li>
						<li>T33n1711 般若波羅蜜多心經贊(1卷)</li>
						<li>T33n1712 般若波羅蜜多心經略疏(1卷)</li>
						<li>T33n1713 般若心經略疏連珠記(2卷)</li>
						<li>T33n1714 般若波羅蜜多心經註解(1卷)</li>
					</ul>
				</li>
				<li>處理讀者於討論區及服務信箱所回饋的用字及標點問題。</li>
				<li>涉及的修訂共 398 卷 51,625 行，詳見變更記錄：<a href="changelog/2021/2021Q3.pdf">所有變更</a>(<?= formatSizeUnits(filesize('changelog/2021/2021Q3.pdf')) ?>)、<a href="changelog/2021/2021Q3-punc.pdf">僅標點變更</a>(<?= formatSizeUnits(filesize('changelog/2021/2021Q3-punc.pdf')) ?>)、<a href="changelog/2021/2021Q3-text.pdf">僅文字變更</a>(<?= formatSizeUnits(filesize('changelog/2021/2021Q3-text.pdf')) ?>)。</li></li>	
			</ol>
		</div>
</body>
</html>
