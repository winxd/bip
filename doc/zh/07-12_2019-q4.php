<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include '../../include/config.php'; ?>
			<?php include 'header.php'; ?>

			<h1>2019.Q4</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2020/01/08</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>近代新編文獻新增「呂澂佛學著作集」之《印度佛學源流略講》、《中國佛學源流略講》兩冊。</li>
				<li>
					以下嘉興藏 43 部 291 卷加上新式標點
					<ul>
						<li>J15nB005 大方廣佛華嚴經普賢行願品別行疏鈔（選錄「華嚴宗七祖行蹟」及「附錄」）(1卷)</li>
						<li>J19nB044 諸經日誦集要(3卷)</li>
						<li>J19nB047 瑜伽集要燄口施食儀(1卷)</li>
						<li>J20nB090 續原教論(2卷)</li>
						<li>J20nB091 道餘錄(1卷)</li>
						<li>J20nB095 蕅益三頌(1卷)</li>
						<li>J20nB096 周易禪解(10卷)</li>
						<li>J20nB097 天樂鳴空集(3卷)</li>
						<li>J21nB109 大藏一覽(10卷)</li>
						<li>J23nB120 闢邪集(1卷)</li>
						<li>J23nB122 醒世錄(8卷)</li>
						<li>J23nB125 先覺宗乘（選錄「帝王問道錄」）(1卷)</li>
						<li>J24nB137 趙州和尚語錄(3卷)</li>
						<li>J24nB138 雲門匡真禪師語錄(3卷)</li>
						<li>J25nB154 松隱唯菴然和尚語錄(3卷)</li>
						<li>J25nB155 無趣老人語錄(1卷)</li>
						<li>J25nB156 無幻禪師語錄(2卷)</li>
						<li>J25nB159 天真毒峰善禪師要語(1卷)</li>
						<li>J25nB163 古庭禪師語錄輯略(4卷)</li>
						<li>J25nB165 大巍禪師竹室集(1卷)</li>
						<li>J25nB167 徹庸和尚谷響集(1卷)</li>
						<li>J25nB173 壽昌無明和尚語錄(2卷)</li>
						<li>J25nB174 天界覺浪盛禪師語錄(12卷)</li>
						<li>J25nB175 大溈五峰學禪師語錄(1卷)</li>
						<li>J26nB177 破山禪師語錄(20卷)</li>
						<li>J26nB178 費隱禪師語錄(14卷)</li>
						<li>J26nB180 天童弘覺忞禪師北遊集(6卷)</li>
						<li>J26nB181 布水臺集(32卷)</li>
						<li>J26nB183 雪竇石奇禪師語錄(15卷)</li>
						<li>J26nB185 浮石禪師語錄(10卷)</li>
						<li>J26nB187 天岸昇禪師語錄(20卷)</li>
						<li>J27nB190 石雨禪師法檀(20卷)</li>
						<li>J27nB191 象田即念禪師語錄(4卷)</li>
						<li>J27nB194 昭覺丈雪醉禪師語錄(10卷)</li>
						<li>J27nB197 博山無異大師語錄集要(6卷)</li>
						<li>J27nB198 雪關禪師語錄(13卷)</li>
						<li>J27nB199 雪關和尚語錄(6卷)</li>
						<li>J27nB200 石霜爾瞻尊禪師語錄(2卷)</li>
						<li>J28nB202 百癡禪師語錄(30卷)</li>
						<li>J28nB203 雲峨喜禪師語錄(2卷)</li>
						<li>J28nB204 斌雅禪師語錄(2卷)</li>
						<li>J28nB206 笑堂和尚語錄(1卷)</li>
						<li>J28nB209 永濟融禪師語錄(2卷)</li>
					</ul>
				</li>
				<li style="margin-top:1em">卍續藏無效星號校註 107 筆予以建立連結。</li>
				<li>處理讀者於討論區及服務信箱所回饋的用字及標點問題。</li>
				<li>涉及的修訂共 454 卷，137,619 行，詳見變更記錄：<a href="changelog/2019/2019Q4.pdf">所有變更</a>(<?= formatSizeUnits(filesize('changelog/2019/2019Q4.pdf')) ?>)、<a href="changelog/2019/2019Q4-punc.pdf">僅標點變更</a>(<?= formatSizeUnits(filesize('changelog/2019/2019Q4-punc.pdf')) ?>)、<a href="changelog/2019/2019Q4-text.pdf">僅文字變更</a>(<?= formatSizeUnits(filesize('changelog/2019/2019Q4-text.pdf')) ?>)。</li></li>	
			</ol>
		</div>
</body>
</html>
