<?php
$header = <<<HEADER
<div class='header'>
	<div class='header-left'>
		<a href='01-content.php'>&nbsp; CBETA 說明首頁</a>
	</div>
	<div class='header-right'>
		<span class='dropdown'>格式說明 | 
		<div class='dropdown-content'>
			<a href='02-01_XML.php'>XML格式說明</a>
			<a href='02-02_id.php'>藏經代碼</a>
			<a href='02-03_line_head.php'>行首資訊</a>
			<a href='02-04_create_word.php'>組字式基本規則</a>
			<a href='04-aca_quote.php'>引用複製</a>
			<a href='02-06_abbr_app.php'>校異略符</a>
			<a href='02-07_abbr_ver.php'>校勘版本略符</a>
			<a href='02-08_collate_clause.php'>大正藏校勘條目說明</a>
			<a href='02-09_abbr_dz.php'>對校略符</a>
			<a href='02-10_pali_title.php'>巴利語書名略符</a>
			<a href='02-11_ver.php'>版本略考</a>
			<a href='02-12_cc_revise.php'>校勘條目修訂</a>
			<a href='02-13_variants.php'>異體字與近義詞</a>
			<a href='02-14_text_color.php'>本文區文字樣式說明</a>
		</div>
		</span>
		<span class='dropdown' onclick="location.href='03-00_system_oprator.php'" style="cursor:pointer">系統操作說明 | 
		<div class='dropdown-content'>
			<a href='03-06_main_intro.php'>閱讀畫面介紹</a>
			<a href='03-07_jing_selector.php'>經典選擇</a>
			<a href='03-08_content_functions.php'>內容功能選單</a>
			<a href='03-09_data_search.php'>資料搜尋</a>
			<a href='03-11_similar_search.php'>相似句搜尋</a>
			<a href='03-10_font_install.php'>字型安裝</a>
			<span href="#" class='dropdown-sub'>操作手冊
				<div class='dropdown-content-sub'>
					<a href='03-01_select1.php'>經文選擇</a>
					<a href='03-02_select2.php'>內容段落選擇</a>
					<a href='03-03_read_setting.php'>閱讀選項鍵</a>
					<a href='03-04_quote.php'>引用與搜尋</a>
					<a href='03-05_feedback.php'>問題與回報</a>
				</div>
			</span>
		</div>
		</span>
		
		<a class='dropdown' href='04-aca_quote.php#research_ref_anchor'>學術引用範例 | </a>
		
		<span class='dropdown'>資料版本 | 
		<div class='dropdown-content'>
			<a href='07-28_2024-r3.php'>2024.R3</a>
			<a href='07-27_2024-r2.php'>2024.R2</a>
			<a href='07-26_2024-r1.php'>2024.R1</a>
			<a href='07-25_2023-q4.php'>2023.Q4</a>
			<a href='07-24_2023-q3.php'>2023.Q3</a>
			<a href='07-23_2023-q1.php'>2023.Q1</a>
			<a href='07-22_2022-q4.php'>2022.Q4</a>
			<a href='07-21_2022-q3.php'>2022.Q3</a>
			<a href='07-20_2022-q1.php'>2022.Q1</a>
			<a href='07-19_2021-q4.php'>2021.Q4</a>
			<a href='07-18_2021-q3.php'>2021.Q3</a>
			<a href='07-17_2021-q2.php'>2021.Q2</a>
			<a href='07-16_2021-q1.php'>2021.Q1</a>
			<a href='07-00_all_versions.php'>所有版本</a>
			
		</div>
		</span>
		
		<span class='dropdown'>CBETA | 
		<div class='dropdown-content'>
			<a href='05-01_related_org.php'>相關單位</a>
			<a href='05-02_aim.php'>宗旨</a>
			<a href='05-03_origin.php'>緣起</a>
			<a href='05-04_goal.php'>目標</a>
			<a href='05-05_org.php'>組織</a>
			<a href='05-06_tech.php'>技術</a>
			<a href='05-07_contribution.php'>大德芳名錄</a>
			<a href='05-08_donate.php'>贊助帳戶</a>
		</div>
		</span>
		<a class='dropdown' href='06-copyright.php'>版權&nbsp;&nbsp;&nbsp;</a>
	</div>
</div>

HEADER;

echo $header;
?>