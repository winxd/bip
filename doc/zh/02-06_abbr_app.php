<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>回頁首</a>
	</head>
	<body>

<div class='container'>
<?php include 'header.php'; ?>
<h1>校異略符</h1> <!--http://www.cbeta.org/format/abbr_app.php-->
	<p class='ball'/>
<h2 align='center'>Abbreviations of Textcritical Apparatus</h2>
<table>
	<tr>
		<th class='center'>大正藏<br>略符</th>
		<th class='center'>CBETA<br>略符</th>
		<th class='center'>說　　　　　　　　　　　明</th>
	</tr>
	<tr>
		<td colspan='2' class='center'><b>＝</b></td>
		<td>作 (異文&hellip;&hellip;ニ作ル, &#39;Various reading&#39;)
			<p>例：</p>
			<blockquote><!--灰底灰框的圓角方塊-->
				<p>(a) <u>得</u>元明俱作<u>相</u>トスベキヲ　得＝相【元】【明】<br>
					(i. e. The Yuan and the Ming Editions read 相 for 得)</p>
				<p>(b) 一切眾生依得生長＝諸眾生等依得生トスベキヲ　（（一切&hellip;長））八字＝（（諸眾生等依得生））七字&nbsp;<br>
					(i. e. For the eight letters（（一切&hellip;長））read the seven letters（（諸眾生等依得生））)</p>
			</blockquote>
		</td>
	</tr>
	<tr>
		<td colspan='2' class='center'><b>－</b></td>
		<td>無 (異本ニ〔&hellip;&hellip;〕無シ, &#39;Omit&#39; &#39;diest&#39;)
			<p>例：</p>
			<blockquote><!--灰底灰框的圓角方塊-->
				<p>宋元俱無<u>心</u>字トスベキヲ　〔心〕－【宋】【元】 (i. e. The Sung and the Yuan Editions omit	心)</p>
			</blockquote>
		</td>
	</tr>
	<tr>
		<td class='center' colspan='2'><b>＋</b></td>
		<td>有 (異本ニ（&hellip;&hellip;）有リ,&nbsp; &#39;have&#39;)〔<a href='#note1' name='text1' rel='nofollow'>CBETA 按一</a>〕
			<p>例：</p>
			<blockquote><!--灰底灰框的圓角方塊-->
				<p>(a) <u>我</u>下三本俱有<u>時於彼遇值世尊</u>七字トスベキヲ　我　＋（時於彼遇值世尊）【三】&nbsp;<br>
					(i. e. The Three Editions have 時於彼遇值世尊 after 我) 〔<a href='#note2' name='text2' rel='nofollow'>CBETA 按二</a>〕</p>
				<p>(b) <u>尊</u>上三本俱有<u>我時於彼遇值世</u>七字トスベキヲ　（我時於彼遇值世）＋尊【三】&nbsp;<br>
					(i. e. The Three Editions have 我時於彼遇值世	before	尊) 〔<a href='#note3' name='text3' rel='nofollow'>CBETA 按三</a>〕</p>
			</blockquote>
		</td>
	</tr>
	<tr>
		<td class='center' colspan='2'><b>＊</b></td>
		<td>下同	(以下之ニ同ジ, 上ニ同ジ	&#39;So below,&#39; &#39;So above,&#39; &#39;et passim&#39;)
			<p>例：</p>
			<blockquote><!--灰底灰框的圓角方塊-->
				<p>本文 (text)　世尊正[1]遊知</p>
				<p>註 (Note)　<u>遊</u>三本俱作<u>遍</u>下同トスベキヲ　[1]遊＝遍【三】＊&nbsp;<br>
					(i.e. The Three Editions read	遍	for	遊, so also below)</p>
			</blockquote>
		</td>
	</tr>
	<tr>
		<td class='center' colspan='2'><b>&hellip;</b></td>
		<td>略(字句省略, &#39;Letters or Sentences left out,&#39; &#39;down to&#39;)
			<p>例：</p>
			<blockquote><!--灰底灰框的圓角方塊-->
				<p>(a)<u>彼依此處觀覺興衰</u>八字トスベキヲ　（彼依&hellip;興衰）&nbsp;<br>
					(i.e. &#39;Form 彼依 down to 興衰, eight leters.&#39;)</p>
				<p>(b)<u>彼依</u>乃至<u>興衰</u>八字トスベキヲ　（彼依&hellip;興衰）&nbsp;<br>
					(i.e. &#39;Form 彼依 down to 興衰, eight leters.&#39;)</p>
				<p>(c)已上三本俱無<u>彼依</u>乃至<u>興衰</u>八字トスベキヲ　〔彼依&hellip;興衰〕八字－【三】&nbsp;<br>
					(i. e. &#39;From 彼依 down to 興衰, eight letters are left out in the Three Editions.&#39;)</p>
			</blockquote>
		</td>
	</tr>
	<tr>
		<td class='center' colspan='2'><b>◎</b></td>
		<td>異同	(卷品ノ同異，&#39;Various division&#39;)，(文章ノ出入，&#39;Various sentence&#39;)</td>
	</tr>
	<tr>
		<td class='center'><img border='0' height='28' src='pic/interchange.gif' width='43' /></td>
		<td class='center'><b>&infin;</b></td>
		<td>位置ノ轉換 (&#39;Interchange of position&#39;)</td>
	</tr>
</table>

<p><b>CBETA 按：</b></p>
<p><a href='#text1' name='note1' rel='nofollow'>一、have，原書「略符」表作：add。(下同)</a></p>
<p><a href='#text2' name='note2' rel='nofollow'> 二、原書「略符」表作：The Three Editions add 時於彼遇值世尊 after 我。</a></p>
<p><a href='#text3' name='note3' rel='nofollow'>三、原書「略符」表作：The Three Editions add	我時於彼遇值世 before 尊。</a></p>

<br><br>
</div>
	</body>
</html>
