<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>回頁首</a>
	</head>
	<body>

<div class='container'>
<?php include 'header.php'; ?>

<h1>技術簡介</h1> <!--http://www.cbeta.org/cbreader/help/technote.htm-->
	<p class='ball'/>

<h2 align='center'>CBETA 所使用的技術與技巧</h2>
<div class='flex'>

<div>
	<p><span class='orange'>&#9654 </span><span class='navy'><a class='menu2' href='#mark0' name='index0'>流程簡介</a></span></p>
	<p><span class='orange'>&#9654 </span><span class='navy'><a class='menu2' href='#mark4' name='index4'>經文大正藏格式化</a></span></p>
	<p><span class='orange'>&#9654 </span><span class='navy'><a class='menu2' href='#mark7' name='index7'>看圖校對程式</a></span></p>
	<p><span class='orange'>&#9654 </span><span class='navy'><a class='menu2' href='#mark9' name='index9'>文件標記</a></span></p>
	<p><span class='orange'>&#9654 </span><span class='navy'><a class='menu2' href='#mark3' name='index3'>成品介面</a></span></p>
</div>
<div>
	<p><span class='orange'>&#9654 </span><span class='navy'><a class='menu2' href='#mark1' name='index1'>去點程式</a></span></p>
	<p><span class='orange'>&#9654 </span><span class='navy'><a class='menu2' href='#mark5' name='index5'>檔案比對程式</a></span></p>
	<p><span class='orange'>&#9654 </span><span class='navy'><a class='menu2' href='#mark12' name='index12'>文字分析程式</a></span></p>
	<p><span class='orange'>&#9654 </span><span class='navy'><a class='menu2' href='#mark10' name='index10'>介面轉換</a></span></p>
</div>
<div>
	<p><span class='orange'>&#9654 </span><span class='navy'><a class='menu2' href='#mark2' name='index2'>OCR Plus</a></span></p>
	<p><span class='orange'>&#9654 </span><span class='navy'><a class='menu2' href='#mark6' name='index6'>常見錯誤字串取代</a></span></p>
	<p><span class='orange'>&#9654 </span><span class='navy'><a class='menu2' href='#mark8' name='index8'>缺字處理</a></span></p>
	<p><span class='orange'>&#9654 </span><span class='navy'><a class='menu2' href='#mark11' name='index11'>全文檢索</a></span></p>
</div>
</div>
<br><br>
<div>
<table>
	<tr>
		<td class='center'><a href='#index0' name='mark0'>
			流<br>
			程<br>
			簡<br>
			介</a></td>
		<td valign='top'>本會處理經文的方式，主要是採用不同來源兩個以上版本的經文電子檔，利用程式進行比對，再利用看圖校對的方式產生一較正確的經文電子檔，再交由人工校對。這樣的處理方式，在速度及品質上均令人滿意。</td>
	</tr>
	<tr>
		<td class='center'><a href='#index1' name='mark1'><font color='#000000'>去<br>
			點<br>
			程<br>
			式</a></td>
		<td valign='top'>當我們打算使用掃瞄書本及使用 OCR 技術來產生經文時，第一個遇到的問題就是經文圖檔的「雜點」，「雜點」是指非經文本身的符號，在OCR時，會造成較低的判斷正確率，所以就寫出了這支程式來協助解決這個問題。</td>
	</tr>
	<tr>
		<td class='center'><a href='#index2' name='mark2'>OCR<br>
			Plus</a></td>
		<td valign='top'>使用 OCR 判斷的同時，會發現許多常見而重複的誤判。部份可以經由 OCR 自動學習的功能來改善，但不易改善並且有經常規則性的錯誤，就可以使用這支程式來幫忙處理。本程式是由<b>楊忠權</b>先生完成，並授權本協會使用，在此特別感謝！<br><br>
		也因為這個概念，而得以產生製作「常見錯誤字串取代表」的行動，並將功能整合至「看圖校對程式」之中，以擴大其應用範圍。</td>
	</tr>
	<tr>
		<td class='center'><a href='#index4' name='mark4'>經<br>
			文<br>
			大<br>
			正<br>
			藏<br>
			格<br>
			式<br>
			化</a></td>
		<td valign='top'>在經文產生之後，我們希望在經文之中能依大正藏格式加上必要的資料。通常是在每一行之前留上 <冊數> <經號> <頁碼> <欄> <行> 等資料。這些資料在需要查對原書時，能夠讓我們很快的找到出處，對於後序的作業提供很大的方便。而這支程式主要就是針對蕭鎮國先生所提供 25 冊大正藏經文，進行格式化的動作。<br><br>
		<span class='orange'>&#9654&#9654&#9654 </span>詳情請參考<a class='menu2' href='02-03_line_head.html' name='rules' target='_blank' rel='nofollow'>&nbsp;CBETA 資料庫行首資訊規則說明</a></td><!--待更新連結為02-03_行首資訊.html-->
	</tr>
	<tr>
		<td class='center'><a href='#index5' name='mark5'>檔<br>
			案<br>
			比<br>
			對<br>
			程<br>
			式</a></td>
		<td valign='top'>在經文校對上，通常是最耗時的一個環節。經由中研院提供檔案比對的技巧與概念，寫出這支能處理相異格式經文的比對程式。利用這程式來比對二分文件，可以很輕鬆的找出二份文件差異處，再利用一些工具，就可以很快的協助我們修改資料。</td>
	</tr>
	<tr>
		<td class='center'><a href='#index6' name='mark6'>常<br>
			見<br>
			錯<br>
			誤<br>
			字<br>
			串<br>
			取<br>
			代</a></td>
		<td valign='top'>在經典文字處理的過程中，有許多錯誤的發生，其實都是有因有緣，有跡可尋的。OCR 有 OCR 常見的錯，使用注音、倉頡等輸入法的朋友，亦都有常犯而難以發現的失誤。故在校對的過程中，吸取錯誤經驗，並加以記錄統計，進而設計了各式的「常見錯誤字串取代表」。<br><br>
		除了「常見錯誤字串取代表」之外，亦有各式「缺字代換表」，故將字串取代功能整合至「看圖校對程式」之中，如此在找出不易發現的錯誤之同時，配合看圖功能，可立刻叫出原書圖檔，進而加以比較，加速了處理的速率。</td>
	</tr>
	<tr>
		<td class='center'><a href='#index7' name='mark7'>看<br>
			圖<br>
			校<br>
			對<br>
			程<br>
			式</a></td>
		<td valign='top'>在校對過程中，查詢原書也是一件花費工夫的事。由於我們已有大部份的掃瞄的圖檔，故寫了本程式，希望能在利用「比對程式」之結果，進行校對時，根據經文中大正藏格式的資料，能立刻在電腦上秀出原書的字，以增進校對判斷速度，並讓沒有書的人亦可進行校對工作。</td>
	</tr>
	<tr>
		<td class='center'><a href='#index12' name='mark12'>文<br>
			字<br>
			分<br>
			析<br>
			程<br>
			式</a></td>
		<td valign='top'>在大正藏的經文中，除了常見的文字與句點之外，尚有許多其他的符號，其中主要的有校勘數字（有數字的黑圈）、校勘星號（星號）、斷詞小黑點，而較不重要的符號則有一些日本使用的讀音符號，為了讓電腦能協助在現有的經文中自動補入這些符號，於是有了本程式的開發。</td>
	</tr>
	<tr>
		<td class='center'><a href='#index8' name='mark8'>缺<br>
			字<br>
			處<br>
			理</a></td>
		<td valign='top'>缺字處理是很重要的一環，目前有下列的方法在使用中：<br>
			<br>
			網路上常見的組字法	(需了解組字規則)<br>
			Dr. Christan	在高麗藏用的	&amp;C, &amp;K 表示法 (需要字碼對照表)<br>
			今昔文字鏡使用之	&amp;M	表示法 (需有字典資訊)<br>
			漢字組字法（中研院資訊所文獻處理實驗室）
			(需有造字檔及組字規則)<br>
			直接使用圖檔表示法	(純文字檔環境不能使用)<br>
			使用	Truetype	利用不同字面的方法 (純文字檔不能用)<br>
			Open98 使用漢字庫，利用漢語大字典之資訊為編碼原則	(需有字典資訊)<br>
		</td>
	</tr>
	<tr>
		<td class='center'><a href='#index9' name='mark9'>文<br>
			件<br>
			標<br>
			記</a></td>
		<td valign='top'>在核心資料加上適當的標記，就可以在各種應用中取得正確而需要的資訊，故標記的設計與標記程式，也是有待研發的項目。<br>
			<br>
			簡單標記介紹	-- CBETA	電子報第四期<br>
			校勘版	XML	標記簡介(上) -- CBETA	電子報第五期<br>
			校勘版	XML	標記簡介(下) -- CBETA	電子報第六期</p>
		</td>
	</tr>
	<tr>
		<td class='center'><a href='#index10' name='mark10'>介<br>
			面<br>
			轉<br>
			換</a></td>
		<td valign='top'>研發跨平台技術，希望將核心資料在不同作業平台轉成各種格式，以提供不同需求層面的使用者。</td>
	</tr>
	<tr>
		<td class='center'><a href='#index11' name='mark11'>全<br>
			文<br>
			檢<br>
			索</a></td>
		<td valign='top'>發展全文檢索核心，以提供單機成品及網路展現使用。<br>
			本協會提供一些簡單搜尋程式，供使用者下載。</td>
	</tr>
	<tr>
		<td class='center'><a href='#index3' name='mark3'>成<br>
			品<br>
			介<br>
			面</a></td>
		<td class='top'>用來將核心資料展示出來，變成單機成品或網路成品，以供流通。</td>
	</tr>
</table>
</div>

<p align='right'><span class='orange'>&#9654&#9654&#9654 </span><a class='menu2' href='http://www.cbeta.org/tech/index.htm'>【技術資訊】</a>(網站瀏覽)</p>
<br>
</div>
	</body>
</html>