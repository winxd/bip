<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include '../../include/config.php'; ?>
			<?php include 'header.php'; ?>

			<h1>2022.Q1</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2022/03/30</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>單機版改為 CBReader C# 版本</li>
				<li>新增「夾注搜尋」功能（<a onclick="document.getElementById('note-doc-iframe').style.display === 'none' ? document.getElementById('note-doc-iframe').style.display = 'block':document.getElementById('note-doc-iframe').style.display = 'none'" href="javascript:void(null)">說明文件</a>）。
					<!-- <a target="_blank" href="https://docs.google.com/document/d/e/2PACX-1vTfBhey6BMhr7KbAbnySc4elsxchUwkpOXbmopAUAb0qiLQbgWfZLBYQN5eY9y_DGzjLgS8ASzssVrB/pub">說明文件</a> -->
					<iframe id="note-doc-iframe" style="height: 1950px;display: none;width: 800px;border: none;" src="https://docs.google.com/document/d/e/2PACX-1vTfBhey6BMhr7KbAbnySc4elsxchUwkpOXbmopAUAb0qiLQbgWfZLBYQN5eY9y_DGzjLgS8ASzssVrB/pub?embedded=true"></iframe>
				</li>
				<li>新增《太虛大師全書》第1-15冊</li>
				<li>
					新增新式標點（50部339卷）：
					<ul>
						<li>T37n1744 勝鬘寶窟(6卷)</li>
						<li>T37n1745 無量壽經義疏(2卷)</li>
						<li>T37n1746 無量壽經義疏(1卷)</li>
						<li>T37n1747 兩卷無量壽經宗要(1卷)</li>
						<li>T37n1748 無量壽經連義述文贊(3卷)</li>
						<li>T37n1749 觀無量壽經義疏(2卷)</li>
						<li>T37n1750 觀無量壽佛經疏(1卷)</li>
						<li>T37n1751 觀無量壽佛經疏妙宗鈔(6卷)</li>
						<li>T37n1752 觀無量壽經義疏(1卷)</li>
						<li>T37n1754 觀無量壽佛經義疏(3卷)</li>
						<li>T37n1763 大般涅槃經集解(71卷)</li>
						<li>T37n1764 大般涅槃經義記(10卷)</li>
						<li>T38n1765 大般涅槃經玄義(2卷)</li>
						<li>T38n1766 涅槃玄義發源機要(4卷)</li>
						<li>T38n1767 大般涅槃經疏(33卷)</li>
						<li>T38n1769 涅槃宗要(1卷)</li>
						<li>T38n1770 本願藥師經古[這-言+亦](2卷)</li>
						<li>T38n1771 彌勒經遊意(1卷)</li>
						<li>T38n1772 觀彌勒上生兜率天經贊(2卷)</li>
						<li>T38n1773 彌勒上生經宗要(1卷)</li>
						<li>T38n1774 三彌勒經疏(1卷)</li>
						<li>T38n1775 注維摩詰經(10卷)</li>
						<li>T38n1776 維摩義記(8卷)</li>
						<li>T38n1777 維摩經玄疏(6卷)</li>
						<li>T38n1778 維摩經略疏(10卷)</li>
						<li>T38n1779 維摩經略疏垂裕記(10卷)</li>
						<li>T38n1780 淨名玄論(8卷)</li>
						<li>T38n1781 維摩經義疏(6卷)</li>
						<li>T38n1782 說無垢稱經疏(12卷)</li>
						<li>T39n1783 金光明經玄義(2卷)</li>
						<li>T39n1784 金光明經玄義拾遺記(6卷)</li>
						<li>T39n1785 金光明經文句(6卷)</li>
						<li>T39n1786 金光明經文句記(12卷)</li>
						<li>T39n1787 金光明經疏(1卷)</li>
						<li>T39n1788 金光明最勝王經疏(10卷)</li>
						<li>T39n1789 楞伽阿跋多羅寶經註解(8卷)</li>
						<li>T39n1790 入楞伽心玄義(1卷)</li>
						<li>T39n1791 注大乘入楞伽經(10卷)</li>
						<li>T39n1792 佛說盂蘭盆經疏(2卷)</li>
						<li>T39n1793 溫室經義記(1卷)</li>
						<li>T39n1794 註四十二章經(1卷)</li>
						<li>T39n1795 大方廣圓覺修多羅了義經略疏(4卷)</li>
						<li>T39n1796 大毘盧遮那成佛經疏(20卷)</li>
						<li>T39n1797 大毘盧遮那經供養次第法疏(2卷)</li>
						<li>T39n1798 金剛頂經大瑜伽祕密心地法門義訣(1卷)</li>
						<li>T39n1799 首楞嚴義疏注經(20卷)</li>
						<li>T39n1800 請觀音經疏(1卷)</li>
						<li>T39n1801 請觀音經疏闡義鈔(4卷)</li>
						<li>T39n1802 十一面神[口*兄]心經義疏(1卷)</li>
						<li>T39n1803 佛頂尊勝陀羅尼經教跡義記(2卷)</li>
					</ul>
				</li>
				<li>處理讀者於討論區及服務信箱所回饋的用字及標點問題。</li>
				<li>涉及的修訂共 1531 卷 252,493 行，詳見變更記錄：<a href="changelog/2022/2022Q1.pdf">所有變更</a>(<?= formatSizeUnits(filesize('changelog/2022/2022Q1.pdf')) ?>)、<a href="changelog/2022/2022Q1-punc.pdf">僅標點變更</a>(<?= formatSizeUnits(filesize('changelog/2022/2022Q1-punc.pdf')) ?>)、<a href="changelog/2022/2022Q1-text.pdf">僅文字變更</a>(<?= formatSizeUnits(filesize('changelog/2022/2022Q1-text.pdf')) ?>)。</li></li>	
			</ol>
		</div>
</body>
</html>
