<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include '../../include/config.php'; ?>
			<?php include 'header.php'; ?>

			<h1>2023.Q3</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2023/08/01</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>新收錄典籍共 1 部 6 卷：
					<ul>
						<li>CC001n0001 續比丘尼傳(6卷)</li>
					</ul>
				</li>			
				<li>新增新式標點共 10 部 279 卷：
					<ul>
						<li>T30n1579 瑜伽師地論(100卷)</li>
						<li>T41n1821 俱舍論記(30卷)  </li>
						<li>T41n1822 俱舍論疏(30卷)  </li>
						<li>T41n1823 俱舍論頌疏論本(30卷)</li>
						<li>T42n1824 中觀論疏(20卷)  </li>
						<li>T42n1825 十二門論疏(6卷)</li>
						<li>T42n1826 十二門論宗致義記(2卷)</li>
						<li>T42n1827 百論疏(9卷)</li>
						<li>T42n1828 瑜伽論記(48卷)</li>
					   <li> T50n2063 比丘尼傳(4卷)</li>
					</ul>
				</li>
				<li>處理讀者於討論區及服務信箱所回饋的用字及標點問題。</li>
				<!--
				<li>CBETA Online
					<ul>
						<li>新增功能
							<ol>
								<li>經文選擇冊別目錄換成原書目錄</li>
								
								<li>經典「i box」加入經典關係資料</li>
							</ol>
						</li>
						<li>功能修正
							<ol>
								<li>校注內容多段落呈現</li>
								<li>引用複製校注不自動加上句號</li>
							</ol>
						</li>						
					</ul>
				</li>
				-->
				<li>涉及的修訂共 1734 卷 218,981 行，詳見變更記錄：<a href="changelog/2023/2023Q3.pdf">所有變更</a>(<?= formatSizeUnits(filesize('changelog/2023/2023Q3.pdf')) ?>)、<a href="changelog/2023/2023Q3-punc.pdf">僅標點變更</a>(<?= formatSizeUnits(filesize('changelog/2023/2023Q3-punc.pdf')) ?>)、<a href="changelog/2023/2023Q3-text.pdf">僅文字變更</a>(<?= formatSizeUnits(filesize('changelog/2023/2023Q3-text.pdf')) ?>)。</li></li>				
			</ol>
		</div>
</body>
</html>