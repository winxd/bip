<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>回頁首</a>
	</head>
	<body>

<div class='container'>
<?php include 'header.php'; ?>

	<h1>巴利語書名略號</h1> <!--view-source:http://www.cbeta.org/format/pali.php-->
	<h1>Abbreviations of Titles of Pāli Works</h1>
	<p class='ball'/>
	<p></p>
	<table>
		<tr>
			<th>A.</th>
			<td>增支部</td>
			<td>Aṅguttara-nikaaya (Morris-Hardy ed. P. T. S.)</td>
		</tr>
		<tr>
			<th>Ud.</th>
			<td>小部、自說經</td>
			<td>Udāna (Steintha ed. P. T. S.)</td>
		</tr>
		<tr>
			<th>It.</th>
			<td>小部、如是語</td>
			<td>Itivuttaka (Windisoh ed. P. T. S.)</td>
		</tr>
		<tr>
			<th>Kh.</th>
			<td>小部、小誦</td>
			<td>Khuddaka-Pāṭha (Chil ers ed.)</td>
		</tr>
		<tr>
			<th>Jaa.</th>
			<td>小部、本生經</td>
			<td>Jātaka (Fausb&ouml;ll ed.)</td>
		</tr>
		<tr>
			<th>Th. 1.</th>
			<td>小部、長老偈</td>
			<td>Thera-gāthā (Olde berg ed. P. T. S.)</td>
		</tr>
		<tr>
			<th>Th. 2.</th>
			<td>小部、長老尼偈</td>
			<td>Therī-gāthā (Pischel ed. P. T. S.)</td>
		</tr>
		<tr>
			<th>D.</th>
			<td>長部</td>
			<td>Dīgha-nikāya. (Rhys Davids-Carpenter ed. P. T. S.)</td>
		</tr>
		<tr>
			<th>Dh.</th>
			<td>小部、法句</td>
			<td>Dhamma-Pada (Fausb&ouml;ll ed.)</td>
		</tr>
		<tr>
			<th>Nd.</th>
			<td>小部、義譯</td>
			<td>Niddesa (Stede ed. P. T. S.)</td>
		</tr>
		<tr>
			<th>Pv.</th>
			<td>小部、餓鬼事</td>
			<td>Peta-vatthu (Minayeff ed. P. T. S.)</td>
		</tr>
		<tr>
			<th>Bv.</th>
			<td>小部、佛種姓</td>
			<td>Buddha-vaṃsa (Morris ed. P. T. S.)</td>
		</tr>
		<tr>
			<th>M.</th>
			<td>中部</td>
			<td>Majjhima-nikāya (Trenekner-Chalmers ed. P. T. S.)</td>
		</tr>
		<tr>
			<th>Vin.</th>
			<td>律藏</td>
			<td>Vinaya-piṭaka (Oldenberg ed.)</td>
		</tr>
		<tr>
			<th>Vv.</th>
			<td>小部、天宮事</td>
			<td>Vimāna-vatthu (Gooneratne ed. P. T. S.)</td>
		</tr>
		<tr>
			<th>S.</th>
			<td>相應部</td>
			<td>Samyutta-nikāya (Feer ed. P. T. S.)</td>
		</tr>
		<tr>
			<th>Sn.</th>
			<td>小部、經集</td>
			<td>Sutta-nipāta (Andersen-Smith ed. P. T. S.)</td>
		</tr>
		<tr>
			<th>Sumv.</th>
			<td>長部注</td>
			<td>Sumaṅgala-vilāsirnī (Carpenter, J. P. T. S. 1885)</td>
		</tr>
		<tr>
			<th>Samp.</th>
			<td>善見律毗婆沙</td>
			<td>Samanta-pāsādikā (Takakusu-Nagai ed. P. T. S.)</td>
		</tr>
		<tr>
			<th>~</th>
			<td>&nbsp;</td>
			<td>Pāli equivalent.</td>
		</tr>
	</table>
	<br><br>
</div>
	</body>
</html>
