<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>
<div class='container'>
	<?php include 'header.php'; ?>
	
	<h1>資料版本</h1>
	<p class='ball'/>
	<p style="text-align:center;">
		CBETA Online 目前使用之資料底本為：<a href="07-28_2024-r3.php">CBETA 2024.R3</a>
	</p>

	
	<h1 style="margin-top:4rem">資料內容</h1>
	<p class='ball'/>
<div class='flex'>
<div>
	<p>大正新脩大藏經(<a target="_blank" href="http://www.cbeta.org/cbreader/help/list_Taisho.htm">收錄清單</a>)</p>
	<p>卍新纂續藏經(<a target="_blank" href="http://www.cbeta.org/cbreader/help/list_Xunangjing.htm">收錄清單)</a></p>
	<p>歷代藏經補輯(<a target="_blank" href="http://www.cbeta.org/cbreader/help/list_Others.htm">收錄清單</a>)</p>
	<p>國家圖書館善本佛典(<a target="_blank" href="http://www.cbeta.org/cbreader/help/list_GuoTu.htm">收錄清單</a>)</p>
	<p>漢譯南傳大藏經(<a target="_blank" href="http://www.cbeta.org/cbreader/help/list_NanChuan.htm">收錄清單</a>)</p>
	
</div>
<div>
	<p>近代新編文獻：</p>
	<p class='m2'>藏外佛教文獻(<a target="_blank" href="http://www.cbeta.org/cbreader/help/list_Zangwai.htm">收錄清單</a>)</p>
	<p class='m2'>正史佛教資料類編(<a target="_blank" href="http://www.cbeta.org/cbreader/help/list_Zhengshi.htm">收錄清單</a>)</p>
	<p class='m2'>北朝佛教石刻拓片百品(<a target="_blank" href="http://www.cbeta.org/cbreader/help/list_Rubbing.htm">收錄清單</a>)</p>
	<p class='m2'>大藏經補編(<a target="_blank" href="http://www.cbeta.org/cbreader/help/list_Bubian.htm">收錄清單</a>)</p>
	<p class='m2'>中國佛寺志(<a target="_blank" href="http://www.cbeta.org/cbreader/help/list_GAB.htm">收錄清單</a>)</p>
	<p class='m2'>印順法師佛學著作集</p>
	<p class='m2'>呂澂佛學著作集</p>
	<p class='m2'>太虛大師全書</p>
	<p class='m2'>CBETA 選集</p>
	
	
</div>
</div>

	<div style="margin-left:3em" class="flex1">
		<b>CBETA 電子佛典資料庫之底本為：</b>
		<ul>
			<li><a target="_blank" href="http://jinglu.cbeta.org/knowledge/versions.htm#30">《大正新脩大藏經》</a>（大藏出版株式會社 ©）第一冊至第八十五冊</li>
			<li><a target="_blank" href="http://jinglu.cbeta.org/knowledge/versions.htm#29">《卍新纂續藏經》</a>（株式會社國書刊行會 ©）第一冊至第九十冊</li>
			<li>歷代藏經補輯
				<ul>
					<li><a target="_blank" href="http://jinglu.cbeta.org/knowledge/versions.htm#17">《嘉興大藏經》</a>（新文豐出版公司）第一冊至第四十冊</li>
					<li><a target="_blank" href="http://jinglu.cbeta.org/knowledge/versions.htm#22">《房山石經》</a>、<a target="_blank" href="http://jinglu.cbeta.org/knowledge/versions.htm#7">《趙城金藏》</a>、<a target="_blank" href="http://jinglu.cbeta.org/knowledge/versions.htm#14">《永樂北藏》</a>等等</li>
				</ul>
			</li>
		</ul>
		<ul>
			<li><a target="_blank" href="http://rarebook.cbeta.org/">「國家圖書館善本佛典」</a>（國家圖書館 ©）</li>
			<li><a target="_blank" href="http://www.yht.org.tw/yhm04-5.html">《漢譯南傳大藏經》</a>（元亨寺 ©）第一冊至第七十冊</li>
			<li>近代新編文獻
				<ul>
					<li><a target="_blank" href="http://www.cbeta.org/cbreader/help/other/W01/W01a.htm">《藏外佛教文獻》</a>（方廣錩 ©) 第一輯至第九輯</li>
					<li><a target="_blank" href="http://www.cbeta.org/cbreader/help/other/H01/H01a.htm">《正史佛教資料類編》</a>（杜斗城 ©）</li>
					<li><a target="_blank" href="http://www.cbeta.org/cbreader/help/other/I01/I01.htm">《北朝佛教石刻拓片百品》</a>（中央研究院歷史語言研究所 ©）</li>
					<li><a target="_blank" href="http://www.cbeta.org/cbreader/help/other/B/0-1-Imprint.html">《大藏經補編》</a> （藍吉富 ©）第一冊至第三十六冊</li>
					<li><a target="_blank" href="http://www.cbeta.org/cbreader/help/other/Gab/readme.html">《中國佛寺史志彙刊》</a>（杜潔祥主編）</li>
					<li><a target="_blank" href="http://www.cbeta.org/cbreader/help/other/Gab/readme.html">《中國佛寺志叢刊》</a>（張智等編輯）</li>
					<li>《印順法師佛學著作集》（印順文教基金會 ©）</li>
					<li>呂澂現存著作（呂應中等 ©）</li>
					<li>太虛大師全書</li>
				</ul>
			</li>
		</ul>
		以上藏經的相關介紹，請直接點選連結。<br/>
		本資料庫正式取得底本版權者「大藏出版株式會社」、「株式會社國書刊行會」、國家圖書館、元亨寺、杜斗城先生、方廣錩先生、藍吉富先生以及「中央研究院歷史語言研究所」的輸入與公開之授權，特此致謝。<br/>
		附註：
		<ul>
			<li>大正藏收錄原則：基於與日本 SAT 合作關係，收錄範圍為 T01-T55 及 T85 等 56 冊。</li>
			<li>卍續藏收錄原則：卍續藏與大正藏經文重複者不錄（第6冊及第52冊二冊因與大正藏完全重複，故不錄）。</li>
			<li>嘉興藏收錄原則：與大正藏、卍續藏經文重複者不錄。</li>
		</ul>
	</div>


</div>
	</body>
</html>