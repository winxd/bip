<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


<div class='container'>
<?php include 'header.php'; ?>

<h1>系統操作說明</h1> 
	<p class='ball'/>

<div class=''>

	<ol>
		<li><a href="03-06_main_intro.php">閱讀畫面介紹</a></li>
		<li><a href="03-07_jing_selector.php">經典選擇</a></li>
		<li><a href="03-08_content_functions.php">內容功能選單</a></li>
		<li><a href="03-09_data_search.php">資料搜尋</a></li>
		<li><a href="03-10_font_install.php">字型安裝</a></li>
		<li>操作手冊</li>
		<ol>
			<li><a href="03-01_select1.php">經文選擇</a></li>
			<li><a href="03-02_select2.php">內文段落選擇</a></li>
			<li><a href="03-03_read_setting.php">閱讀選項鍵</a></li>
			<li><a href="03-04_quote.php">引用與搜尋</a></li>
			<li><a href="03-05_feedback.php">問題與回報</a></li>
			<li><a target="_blank" href="https://docs.google.com/document/d/e/2PACX-1vTfBhey6BMhr7KbAbnySc4elsxchUwkpOXbmopAUAb0qiLQbgWfZLBYQN5eY9y_DGzjLgS8ASzssVrB/pub">夾注搜尋</a></li>
		</ol>
	</ol>
	
</div>
	</body>
</html>
