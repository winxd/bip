<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>回頁首</a>
	</head>

	<body>

<div class='container'>
<?php include 'header.php'; ?>

<h1>CBETA 緣起</h1> <!--http://www.cbeta.org/intro/origin.php-->
	<p class='ball'/>

<h2>遠因</h2>
<p class='m2'>過去數年，經由眾人努力，網路上集結了不少佛教經典，也因此帶動佛典電子檔輸入熱潮。大家的目標主要著重在網路應用，比如將這些電子檔置放於 FTP上供人免費下傳，或是透過 GOPHER、WWW 方便使用者瀏覽，最近更在 WWW上提供檢索查詢功能。另個發展是將電子檔包裝設計成電子書，以使經文的呈現更加精緻。所有這些努力，莫不希望能夠透過網路，使佛典普及，讓更多人同霑法益，並利用電腦的能力拓展佛典的應用範圍及閱讀方式。</p>

<h2>近因</h2>
<ol class='m1'>
	<li>蕭鎮國先生的來函，提供 25冊 CCCII 大正藏電子稿，並於 1997 年 11 月 6 日 25T小組籌備會議之後，授權台大佛研中心進行後續之處理。</li>
	<li>網路上電子佛典討論版 (buda-tech) 的朋友草擬了<a href='http://www.cbeta.org/intro/budatech.htm' target='_blank' rel='nofollow'>電子版大藏經輸入計畫</a>，開始有計畫的進行經典輸入。</li>
	<li>1997 年 11 月 6 日由台大佛研中心成立 25T 小組，著手開始進行大規模的藏經電子化。 (<a href='http://www.cbeta.org/intro/25tmeet.htm' target='_blank' rel='nofollow'>二十五冊大正藏會議整理</a>)</li>
</ol>
<h2>助緣</h2>
<ol class='m1'>
	<li>Buda-Tech 討論群 : 台大獅子吼 BBS 站及中山鹿苑佛教 BBS 專站的討論版，專門進行電子佛典化的相關問題討論，並由曾國豐先生架設 Mailing List 提供討論者使用。許多佛典資料及相關技術都是本版網友努友的結果。</li>
	<li>電子佛典編輯小組 (EBTWG) : 由徐言輝及幾位朋友組成之小組，主要是利用 SCAN+OCR 技術，以佛教大藏經為主，而有系統的產生電子經文檔。</li>
	<li>25T 小組 : 由台大佛研中心主導，負責處理蕭鎮國先生所提供之 25 冊 CCCII 格式的大正藏經文檔，本小組即為中華電子佛典協會之前身。</li>
	<li>缺字小組 : 為了深入討論佛典缺字的解決方案，而另外成立的討論小組。</li>
</ol>
<h2>成立</h2>
<p class='m2'>由台大佛研中心釋恆清法師開始籌募所需經費，並感謝「北美印順導師基金會」與「中華佛學研究所」全力支持贊助後，於 1998 年 2 月 15 日假法鼓山安和分院舉辦籌備會議並於當日正式成立 「中華電子佛典協會」。</p>
<br><br/>
</div>
	</body>
</html>