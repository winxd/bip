<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>

<div class='container'>
<?php include 'header.php'; ?>

<h1>CBETA 目標</h1> <!--http://www.cbeta.org/intro/mission.php-->
	<p class='ball'/>

<h2>初期大正藏目標 (1998.02~2003.02)</h2>
<ol class='m2'>
	<li>初期以完成大正藏 1-55 冊及第 85 冊電子化為目標。</li>
	<li>完成極低錯誤率之高品質電子經文。</li>
	<li>提出電腦缺字之有效處理方案。</li>
	<li>利用電腦技術，以整合不同版本藏經校勘之查閱。</li>
	<li>整合全文檢索之工具，以提昇電子佛典之應用。</li>
	<li>利用網路特性，將漢文佛典呈現至世界各處。</li>
	<li>開發單機版之使用介面，以利大眾使用。</li>
</ol>
<h2>第二期卍續藏目標 (2003.02~2008.02)</h2>
<ul class='m2'>
	<li>2003.12 X78-87：史傳部．禪宗</li>
	<li>2004.07 X63-73：諸宗著述部．禪宗</li>
	<li>2005.02 X54-88 的剩餘冊數：包括所有諸宗著述部、禮懺部、史傳部</li>
	<li>2006.12 X01-53：包括印度撰述的經、律、論集、密經儀軌部，以及中國撰述的大小乘釋經、律、論等電子版資料</li>
	<li>2007.12：輸入與處理 X01-X88 各冊校注資料</li>
	<li>2008.02：發行光碟與網路版的《大正藏》與《卍新纂續藏經》CBETA 電子佛典集成</li>
</ul>
<br><br/>

	</body>
</html>