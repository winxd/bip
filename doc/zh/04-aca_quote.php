<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>回頁首</a>
	</head>
	<body>

<div class='container'>
<?php include 'header.php'; ?>

<h1>CBETA 引用複製</h1>
	<p class='ball'/>
	<p>CBETA 的引用複製是在使用者複製一小段經文時，同時將經文的相關出處一併帶出來，讓日後讀到此段經文的人，可以很快知道經文的來源及出處。</p>
	<p>除了《卍新纂續藏經》略有不同之外，《大正新脩大藏經》及其他各藏經出處是依冊數、經號、頁數、欄數、行數之順序紀錄，例如：</p>
	<div class='bibox'>
		<p>《雜阿含經》卷1：爾時，世尊告諸比丘：「當觀色無常。如是觀者，則為正觀。正觀者，則生厭離；厭離者，喜貪盡；喜貪盡者，說心解脫。」(CBETA 2022.Q4, T02, no. 99, p. 1a7-9)</p>
	</div>
	<p>這表示此段經文出自於大正藏第 2 冊，99 經，第 1 頁第一欄的第 7 行至 9 行。</p>
	<p>《卍新纂續藏經》出處的記錄，採用</p>
	<p>《卍新纂大日本續藏經》（ X: Xuzangjing 卍新纂續藏。東京：國書刊行會）、、<br>
			 《卍大日本續藏經》（ Z: Zokuzokyo 卍續藏。京都：藏經書院）、<br>
			 《卍續藏經．新文豐影印本》（ R: Reprint 。台北：新文豐）</p>
	<p>以上三種版本並列，例如： (CBETA 2022.Q4, X78, no. 1553, p. 420a4-5 // Z 2B:8, p. 298a1-2 // R135, p. 595a1-2) 。</p>
	<p>關於這三個版本更詳細的資料，請參考「<a target="_blank" href='http://cbeta.org/data-format/zrx.htm#zrx'>卍續藏三種版本及其對照說明</a>」。</p>
	<p>註：2017年八月起，<a target="_blank" href="https://cbetaonline.dila.edu.tw">CBETA Online</a> 已採用新的格式，詳細請<a target="_blank" href="http://cbeta.org/node/5128">參考此頁</a>。</p>
	<br>
	<h2>學術引用範例</h2>
	<p>論文寫作引用 CBETA 經文時，可參考底下格式，直接放在論文［參考文獻］前 ，用來說明 CBETA 引用複製的格式：</p>
	<div class='bibox'>
		<p>《大正新脩大藏經》與《卍新纂續藏經》的資料引用是出自「中華電子佛典協會」（Chinese Buddhist Electronic Text Association，簡稱 CBETA）的電子佛典集成。引用《大正新脩大藏經》出處是依冊數、經號、頁數、欄數、行數之順序紀錄，例如：CBETA 2022.Q4, T30, no. 1579, p. 517b6-17。引用《卍新纂續藏經》出處的記錄，採用《卍新纂大日本續藏經》（X: Xuzangjing 卍新纂續藏。東京：國書刊行會）、《卍大日本續藏經》（Z: Zokuzokyo 卍續藏。京都：藏經書院）、《卍續藏經．新文豐影印本》（R: Reprint。台北：新文豐）三種版本並列，例如：CBETA 2022.Q4, X78, no. 1553, p. 420a4-5 // Z 2B:8, p. 298a1-2 // R135, p. 595a1-2。<br/>
		（2022.Q4 表示 2022 年第 4 季之版本。若採用的線上版、單機版或光碟版本不同，或引用之藏經不同，請依實際情況調整。）</p>
	</div>
	<br/>
	<br/>
	<div class='bibox'>
		<p>    Citations and reference for the <i>Taishō Tripiṭaka</i> and the <i>Shinsan Zokuzōkyō (Xuzangjing)</i> are based on the corpus of the Chinese Buddhist Electronic Text Association (CBETA). Citations for the <i>Taishō Tripiṭaka</i> are referenced and enumerated according to the volume order, text number, page, column, and line, e.g. CBETA 2022.Q4, T30, no. 1579, p. 517b6-17. Citations for the <i>Shinsan Zokuzōkyō (Xuzangjing)</i> contains corresponding references for the three editions currently in print, which are the <i>Manji Shinsan Dainihon Zokuzōkyō</i> edition (X: Xuzangjing, Tokyo: Kokusho Kankōkai), the Manji Dainihon Zokuzōkyō edition (Z: Zokuzōkyō, Kyoto: Zokuzōkyō Shoin), and the <i>Manji Zokuzōkyō</i> reprinted edition (R: Reprint, Taipei: Shinwenfeng), e.g. CBETA 2022.Q4, X78, no. 1553, p. 420a4-5 // Z 2B:8, p. 298a1-2 // R135, p. 595a1-2.<br/>
		(2022.Q4 refers to the version released in the fourth quarter of the year 2022. Please be reminded to rephrase the above sample writing to reflect the actual cited canon and its data version as per online portal, desktop reader, or CD-ROM.)</p>
	</div>
	<p></p>
	<p></p>
	<p></p>
	<p></p>
	<p></p>
	<p></p>

</div>
	</body>
</html>