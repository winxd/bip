<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<style>
		img	{
			width:53%;
		}
	</style>
	<a id='back2top' href='#' title='Back to Top'>回頁首</a>
	</head>
	<body>


<div class='container'>
<?php include 'header.php'; ?>

<h1>字型安裝</h1> 
	<p class='ball'/>

	<div style='margin-top:4em'>
		由於經文內容有包含某些Unicode 10的字元，這些字可能會在您的電腦上無法正常顯示，本指引提供Unicode 10字型的安裝方法，請參考如下說明，若安裝完成後無法正確顯示，請重新開機。
		
		<div style="margin:1em 0 3em 0;">
			<ul>
				<li><a href="#windows">Windows 作業系統安裝說明</li>
				<li><a href="#mac">Mac 作業系統安裝說明</a></li>
			</ul>
		</div>
		
		<a name="windows"></a>
		<h3>Windows 作業系統</h3>
		1. 請至 <a target="_blank" href="https://github.com/cjkvi/HanaMinAFDKO/releases">https://github.com/cjkvi/HanaMinAFDKO/releases</a> 下載 HanaMinA.otf、HanaMinB.otf、HanaMinC.otf 三個字型。<br/><br/>
		<img src="pic/font-install-1.png" /><br/><br/><br/><br/>
		2. 下載回來後，滑鼠點兩下打開該字型，並點選安裝，HanaMinA.otf、HanaMinB.otf、HanaMinC.otf 三個都要安裝。<br/><br/>
		<img src="pic/font-install-2.png" /><br/><br/><br/><br/>
		3. 完成後，可<a target="_blank" href="https://cbetaonline.dila.edu.tw/A091n1057_p0314a07">打開這頁</a>測試，如有出現紅框內的字，即代表安裝成功。<br/><br/>
		<img src="pic/font-install-3.png" /><br/><br/><br/><br/><br/>
		
		<a name="mac"></a>
		<h3>Mac 作業系統</h3>
		1. 請至 <a target="_blank" href="https://github.com/cjkvi/HanaMinAFDKO/releases">https://github.com/cjkvi/HanaMinAFDKO/releases</a> 下載 HanaMinA.otf、HanaMinB.otf、HanaMinC.otf 三個字型。<br/><br/>
		<img src="pic/font-install-1.png" /><br/><br/><br/><br/>
		2. 下載回來後，請按兩下字型檔案打開該字型，並按一下字體預覽下方出現的「安裝字體」按鈕，HanaMinA.otf、HanaMinB.otf、HanaMinC.otf 三個都要安裝，關於mac的字型安裝細節可看這裡：<a target="_blank" href="https://support.apple.com/zh-tw/HT201749">https://support.apple.com/zh-tw/HT201749</a>。<br/><br/>
		3. 完成後，可<a target="_blank" href="https://cbetaonline.dila.edu.tw/A091n1057_p0314a07">打開這頁</a>測試，如有出現紅框內的字，即代表安裝成功。<br/><br/>
		<img src="pic/font-install-3.png" /><br/>
	</div>
	</body>
</html>
