<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


<div class='container'>
<?php include 'header.php'; ?>

<h1>2017.Q4</h1> 
	<p class='ball'/>

	<h3>發行日期：January 15 , 2018</h3>
	<br/>
	<ol style="list-style-type:cjk-ideographic;">
		<li>大正藏(T30-32)新式標點 104 經、303卷。</li>
		<li>讀者來函修訂 1790 筆。</li>
		<li>涉及的修訂共 937 卷，108,018 行，詳見變更記錄(<a href="changelog/2017/2017Q4.pdf">所有變更</a>、<a href="changelog/2017/2017Q4-punc.pdf">僅標點變更</a>、<a href="changelog/2017/2017Q4-text.pdf">僅文字變更</a>)。</li>
	</ol>
	</body>
</html>
