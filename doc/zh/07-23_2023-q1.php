<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include '../../include/config.php'; ?>
			<?php include 'header.php'; ?>

			<h1>2023.Q1</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2023/02/17</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>新增以下新式標點，共 5 部 17 卷。
					<ul>
						<li>T40n1811 菩薩戒義疏(2卷)</li>
						<li>T40n1812 天台菩薩戒疏(3卷)</li>
						<li>T40n1813 梵網經菩薩戒本疏(6卷)</li>
						<li>T40n1814 菩薩戒本疏(3卷)</li>
						<li>T40n1815 梵網經古迹記(3卷)</li>
					</ul>
				</li>
				<li>處理讀者於討論區及服務信箱所回饋的用字及標點問題。</li>
				<li>CBETA Online
					<ul>
						<li>新增功能
							<ol>
								<li>經文選擇冊別目錄換成原書目錄</li>
								
								<li>經典「i box」加入經典關係資料</li>
							</ol>
						</li>
						<li>功能修正
							<ol>
								<li>校注內容多段落呈現</li>
								<li>引用複製校注不自動加上句號</li>
							</ol>
						</li>						
					</ul>
				</li>
				
				<li>涉及的修訂共 780 卷 16,585 行，詳見變更記錄：<a href="changelog/2023/2023Q1.pdf">所有變更</a>(<?= formatSizeUnits(filesize('changelog/2023/2023Q1.pdf')) ?>)、<a href="changelog/2023/2023Q1-punc.pdf">僅標點變更</a>(<?= formatSizeUnits(filesize('changelog/2023/2023Q1-punc.pdf')) ?>)、<a href="changelog/2023/2023Q1-text.pdf">僅文字變更</a>(<?= formatSizeUnits(filesize('changelog/2023/2023Q1-text.pdf')) ?>)。</li></li>				
			</ol>
		</div>
</body>
</html>