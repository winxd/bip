<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


<div class='container'>
<?php include 'header.php'; ?>

<h1>2018.Q1</h1> 
	<p class='ball'/>

	<h3>發行日期：2018/04/29</h3>
	<br/>
	<ol style="list-style-type:cjk-ideographic;">
		<li style="margin-bottom:1em">新增：
			<ul style="list-style: initial;">
			<li>中國佛寺志 (35部 288卷)</li>
			<li>印順法師佛學著作集 (44冊)</li>
			</ul>		
		</li>
		<li>涉及的修訂共 1207 卷，12,793 行，詳見變更記錄(<a href="changelog/2018/2018Q1.pdf">所有變更</a>、<a href="changelog/2018/2018Q1-punc.pdf">僅標點變更</a>、<a href="changelog/2018/2018Q1-text.pdf">僅文字變更</a>)。</li>
	</ol>
	</body>
</html>
