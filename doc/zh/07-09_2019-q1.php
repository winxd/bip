<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include 'header.php'; ?>

			<h1>2019.Q1</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2019/04/10</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>
					大正藏 T31 以下經文加上新標（共20經132卷）：
					<ul>
					    <li>T31n1585 成唯識論(10卷)【護法等菩薩造唐玄奘譯】</li>

						<li>T31n1587 轉識論(1卷)【陳真諦譯】</li>

						<li>T31n1588 唯識論(1卷)【天親菩薩造後魏瞿曇般若流支譯】</li>

						<li>T31n1589 大乘唯識論(1卷)【天親菩薩造陳真諦譯】</li>

						<li>T31n1590 唯識二十論(1卷)【世親菩薩造唐玄奘譯】</li>

						<li>T31n1591 成唯識寶生論(5卷)【護法菩薩造唐義淨譯】</li>

						<li>T31n1592 攝大乘論(2卷)【阿僧伽作後魏佛陀扇多譯】</li>

						<li>T31n1593 攝大乘論(3卷)【無著菩薩造陳真諦譯】</li>

						<li>T31n1595 攝大乘論釋(15卷或18卷)【世親菩薩釋陳真諦譯】</li>

						<li>T31n1596 攝大乘論釋論(10卷)【世親菩薩造隋笈多共行矩等譯】</li>

						<li>T31n1597 攝大乘論釋(10卷)【世親菩薩造唐玄奘譯】</li>

						<li>T31n1598 攝大乘論釋(10卷)【無性菩薩造唐玄奘譯】</li>

						<li>T31n1599 中邊分別論(2卷)【天親菩薩造陳真諦譯】</li>

						<li>T31n1600 辯中邊論(3卷)【世親菩薩造唐玄奘譯】</li>

						<li>T31n1601 辯中邊論頌(1卷)【彌勒菩薩說唐玄奘譯】</li>

						<li>T31n1602 顯揚聖教論(20卷)【無著菩薩造唐玄奘譯】</li>

						<li>T31n1603 顯揚聖教論頌(1卷)【無著菩薩造唐玄奘譯】</li>

						<li>T31n1604 大乘莊嚴經論(13卷)【無著菩薩造唐波羅頗蜜多羅譯】</li>

						<li>T31n1605 大乘阿毘達磨集論(7卷)【無著菩薩造唐玄奘譯】</li>

						<li>T31n1606 大乘阿毘達磨雜集論(16卷)【安慧菩薩糅唐玄奘譯】</li>
					</ul>
				</li>
				<li>處理讀者於討論區及服務信箱所回饋的用字及標點問題。</li>
				<li>《印順法師佛學著作集》全部改以篇章為單位重新切卷，並修訂若干呈現問題。</li>
				<li>《印順法師佛學著作集》之外，涉及的修訂共 547 卷、61,077 行，詳見變更記錄(<a href="changelog/2019/2019Q1.pdf">所有變更</a>、<a href="changelog/2019/2019Q1-punc.pdf">僅標點變更</a>、<a href="changelog/2019/2019Q1-text.pdf">僅文字變更</a>)。</li>
				<li>《印順法師佛學著作集》之修訂另詳見 Y 變更記錄（<a href="changelog/2019/2019Q1-Y-all.pdf">所有變更</a>、<a href="changelog/2019/2019Q1-Y-text.pdf">僅文字變更</a>）。</li>
			</ol>
		</div>
</body>
</html>
