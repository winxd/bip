<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


<div class='container'>
<?php include 'header.php'; ?>

<h1>2016.CD</h1> 
	<p class='ball'/>

	<h3>發行日期：June 2016</h3>
	<h3>內容收錄</h3>
	<p class="m3">
		<div><p align="left"><font face="Times New Roman">
				  CBETA </font>電子佛典資料庫之底本為：</p>
				  <ul>
					<li>
					  <a target="_blank" href="http://jinglu.cbeta.org/knowledge/versions.htm#30">《大正新脩大藏經》</a>（大藏出版株式會社 ©）第一冊至第八十五冊</li>
					<li>
					  <a target="_blank" href="http://jinglu.cbeta.org/knowledge/versions.htm#29">《卍新纂續藏經》</a>（株式會社國書刊行會 ©）第一冊至第九十冊</li>
					<li>歷代藏經補輯 
					  <ul>
						<li><a target="_blank" href="http://jinglu.cbeta.org/knowledge/versions.htm#17">《嘉興大藏經》</a>（新文豐出版公司）第一冊至第四十冊</li>
						<li><a target="_blank" href="http://jinglu.cbeta.org/knowledge/versions.htm#22">《房山石經》</a>、<a target="_blank" href="http://jinglu.cbeta.org/knowledge/versions.htm#7">《趙城金藏》</a>、<a target="_blank" href="http://jinglu.cbeta.org/knowledge/versions.htm#14">《永樂北藏》</a>等等</li>
					  </ul>
					</li>
					<li>「<a target="_blank" href="http://rarebook.cbeta.org/">國家圖書館善本佛典</a>」（國家圖書館 ©） </li>
					<li><a target="_blank" href="http://www.yht.org.tw/yhm04-5.html">《漢譯南傳大藏經》</a>（元亨寺 ©）第一冊至第七十冊 </li>
					<li><a class="menu2" target="_blank" href="http://www.cbeta.org/cbreader/help/other/W01/W01a.htm">《藏外佛教文獻》</a>（方廣錩 ©) 
					  第一輯至第九輯</li>
					<li><span style="font-family: tahoma,sans-serif;">近代新編文獻</span>
					  <ul>
						<li><a class="menu2" target="_blank" href="http://www.cbeta.org/cbreader/help/other/H01/H01a.htm">《正史佛教資料類編》</a>（杜斗城 ©）</li>
						<li><a class="menu2" target="_blank" href="http://www.cbeta.org/cbreader/help/other/I01/I01.htm">《北朝佛教石刻拓片百品》</a>（中央研究院歷史語言研究所 ©）</li>
						<li><a class="menu2" target="_blank" href="http://www.cbeta.org/cbreader/help/other/B/0-1-Imprint.html">《大藏經補編》</a> （藍吉富 ©）第一冊至第三十六冊</li>
						<li><a class="menu2" target="_blank" href="http://www.cbeta.org/cbreader/help/other/Gab/readme.html">《中國佛寺史志彙刊》</a>（杜潔祥主編）</li>
						<li><a class="menu2" target="_blank" href="http://www.cbeta.org/cbreader/help/other/Gab/readme.html">《中國佛寺志叢刊》</a>（張智等編輯）</li>
					  </ul>
					</li>
					</ul>
				  <p>以上藏經的相關介紹，請直接點選連結；或是在 書目－開啟近代新編文獻，在左邊目錄下會有緣起、前言等作者的說明。</p>
				  <p>
					本資料庫正式取得底本版權者「大藏出版株式會社」、「株式會社國書刊行會」、國家圖書館、元亨寺、杜斗城先生、方廣錩先生、藍吉富先生以及「中央研究院歷史語言研究所」的輸入與公開之授權，特此致謝。</p>
				  <p>附註：</p>
				  <ul>
					<li>大正藏收錄原則：基於與日本 SAT 合作關係，收錄範圍為 T01-T55 及 T85 等 56 冊。</li>
					<li>卍續藏收錄原則：卍續藏與大正藏經文重複者不錄（第6冊及第52冊二冊因與大正藏完全重複，故不錄）。</li>
					<li>嘉興藏收錄原則：與大正藏、卍續藏經文重複者不錄。 </li>
					<li>各藏經收錄經目請參考：
			<ul>
				<li><a target="_blank" href="http://www.cbeta.org/cbreader/help/list_Taisho.htm" class="menu2">大正新脩大藏經收錄清單</a></li>
				<li><a target="_blank" href="http://www.cbeta.org/cbreader/help/list_Xunangjing.htm" class="menu2">卍新纂續藏經收錄清單</a></li>
				<li><a target="_blank" href="http://www.cbeta.org/cbreader/help/list_Others.htm" class="menu2">歷代藏經補輯收錄清單</a></li>
				<li><a target="_blank" href="http://www.cbeta.org/cbreader/help/list_GuoTu.htm">國家圖書館善本佛典收錄清單</a></li>
				<li><a target="_blank" href="http://www.cbeta.org/cbreader/help/list_NanChuan.htm">漢譯南傳大藏經(元亨寺版)&nbsp;收錄清單</a></li>
				<li><a target="_blank" href="http://www.cbeta.org/cbreader/help/list_Zangwai.htm" class="menu2">藏外佛教文獻收錄清單</a></li>
				<li><a target="_blank" href="http://www.cbeta.org/cbreader/help/list_Zhengshi.htm" class="menu2">正史佛教資料類編收錄清單</a></li>
				<li><a target="_blank" href="http://www.cbeta.org/cbreader/help/list_Rubbing.htm" class="menu2">北朝佛教石刻拓片百品收錄清單</a></li>
				<li><a target="_blank" href="http://www.cbeta.org/cbreader/help/list_Bubian.htm" class="menu2">大藏經補編收錄清單</a></li>
				<li><a target="_blank" href="http://www.cbeta.org/cbreader/help/list_GAB.htm" class="menu2">中國佛寺史志收錄清單</a></li></ul></li>
		  </ul></div>		
	</p>
	
	</body>
</html>
