<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>


		<div class='container'>
			<?php include '../../include/config.php'; ?>
			<?php include 'header.php'; ?>

			<h1>2022.Q4</h1> 
			<p class='ball'/>
			
			<h3>發行日期：2022/10/28</h3>
			<br/>
			<ol style="list-style-type:cjk-ideographic;">
				<li>新增《太虛大師全書》25-32 冊。</li>
				<li>新增《大正藏》T19-21 新式標點，共 526 部 747 卷。</li>
				<li>處理讀者於討論區及服務信箱所回饋的用字及標點問題。</li>
				<li>涉及的修訂共 1783 卷 182,231 行，詳見變更記錄：<a href="changelog/2022/2022Q4.pdf">所有變更</a>(<?= formatSizeUnits(filesize('changelog/2022/2022Q4.pdf')) ?>)、<a href="changelog/2022/2022Q4-punc.pdf">僅標點變更</a>(<?= formatSizeUnits(filesize('changelog/2022/2022Q4-punc.pdf')) ?>)、<a href="changelog/2022/2022Q4-text.pdf">僅文字變更</a>(<?= formatSizeUnits(filesize('changelog/2022/2022Q4-text.pdf')) ?>)。</li></li>				
			</ol>
		</div>
</body>
</html>
