<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>

<div class='container'>
<?php include 'header.php'; ?>

<h1>引用複製</h1> <!--佳瑜新寫-->
	<p class='ball'/>
<p>CBETA 的引用複製是在使用者複製一小段經文時，同時將經文的相關出處一併帶出來，讓日後讀到此段經文的人，可以很快知道經文的來源及出處。</p>
<p>藏經出處是依冊數、經號、頁數、欄數、行數之順序紀錄，例如：</p>
<blockquote><!--灰底灰框的圓角方塊-->
<p>《雜阿含經》卷1：爾時，世尊告諸比丘：「當觀色無常。如是觀者，則為正觀。正觀者，則生厭離；厭離者，喜貪盡；喜貪盡者，說心解脫。」(CBETA, T02, no. 99, p. 1a7-9)</p>
</blockquote>
<p>這表示此段經文出自於大正藏第 2 冊，99 經，第 1 頁第一欄的第 7 行至 9 行。</p>
<p>出處顯示除了採用書目格式外，亦可選擇行首資訊表示，如： (T02n0099_p0001a07 ~ T02n0099_p0001a09)</p>
<br><br>

	</body>
</html>
