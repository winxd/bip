<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	</head>

	<body>

<div class='container'>
<?php include 'header.php'; ?>
<h1>XML版格式說明</h1> <!--http://www.cbeta.org/format/xml.php-->
	<p class='ball'/>
<p>XML 版以最新<a href='http://www.tei-c.org/Guidelines/P5/' target='_blank' rel='nofollow'>TEI 指南（ P5 版本）</a>為主。</p>
<p>內碼全部使用<a href='http://www.unicode.org/' target='_blank' rel='nofollow'>統一碼 (Unicode) </a>， Unicode 中沒有對應的漢字，則使用 TEI 「 缺字」（ 所謂 'gaiji'） 模組。</p>
<p>中文資料可參考：<a href='http://metadata.teldap.tw/standard/rarebook/TEI921224/index_c.htm' target='_blank' rel='nofollow'>文件編碼組織 後設資料標誌集 選錄版 (TEI Lite)</a></p>
<br><br>
</div>
	</body>
</html>