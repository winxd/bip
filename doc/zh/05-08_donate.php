<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<link rel='stylesheet' type='text/css' href='style.css'>
	<a id='back2top' href='#' title='Back to Top'>回頁首</a>
	</head>
	<body>

<div class='container'>
<?php include 'header.php'; ?>

	<h1>贊助帳戶</h1><!--http://www.cbeta.org/cbr /eader/help/gongde_2.htm-->
		<p class='m2'><a name='postno'></a> </p>
	<h2>信用卡 線上捐款 ：</h2>
		<p class='m2'>本線上捐款與聯合信用卡中心合作，資料傳送採用 SSL (Secure Socket Layer) 傳輸加密，讓您能夠安全安心地進行線上捐款動作。<br>
		<br>
		<span class='orange'>&#9654&#9654 </span><a href='https://www.cbeta.org/donation/creditcard.php' target='_blank'><b>前往捐款</b></a></p>
		<hr>
	<h2>信用卡 單次 ／ 定期定額 捐款：</h2>
		<p class='m2'>本授權書可提供單次捐款或定期定額捐款之用途。<br>
		<br>
		請於下載並填妥捐款授權書後，請傳真至 &nbsp;02-2383-0649&nbsp; ，並請來電 02-2383-2182 確認。 <br>
		<br>
		或掛號寄至 10044 台北市中正區延平南路 77 號 8 樓 R812&nbsp; 財團法人西蓮教育基金會收。 <br>
		<br>
		請在此下載 <a href='http://www.cbeta.org/cbreader/help/creditcard.doc'><b>授權書</b></a> (MS Word 格式)</p>
		<hr>
	<h2>劃撥捐款帳號：</h2>
		<p class='m2'>郵政劃撥帳號：１９５３８８１１</p>
		<p class='m2'>戶名：財團法人西蓮教育基金會</p>
		<p class='m2'>此為 CBETA 專戶，若欲指定特殊用途者，請特別註明，我們會專款專用。</p>
		<hr>
		<p class='m2'><a name='paypal'></a> </p>
	<h2>線上信用卡 / PayPal 捐款</h2>
		<p class='m2'><a href='http://www.paypal.com' target='_blank'>PayPal</a> 是一個跨國線上付款機制的公司，CBETA 引用其服務，提供網友能在線上使用<b>信用卡</b>或 <b>PayPal 帳戶</b>贊助 CBETA 。</p>
		<p class='m2'>相關收據開立事宜，由於付款幣別為美元，我們除了會依您所贊助之美元金額開立收據外，另我們會依捐款當日公告匯率開立台幣收據，此收據為國內正式合法報稅憑證。</p>
		<p class='m2'><span class='orange'>&#9654 </span>第一次使用者請參考
			<a href='http://www.cbeta.org/donation/paypal.htm' target='_blank'>操作說明</a>。</p>
		<p class='m2'><span class='orange'>&#9654 </span>
			<a href='https://www.paypal.com/xclick/business=donation@cbeta.org&item_name=Chinese+Buddhist+Electronic+Text+Association+(CBETA)&page_style=Primary&no_shipping=2&return=http%3A//www.cbeta.org&cancel_return=http%3A//www.cbeta.org&tax=0&currency_code=USD' target='_blank'><b>線上信用卡 / PayPal 贊助</b>&nbsp;
			<img src='http://www.cbeta.org/cbreader/help/images/paypal.gif' alt='劃撥帳號 / 信用卡 / PayPal / 支票捐款' width='62' height='31' border='0' align='top'>
			<img src='http://www.cbeta.org/cbreader/help/images/donation.gif' alt='劃撥帳號 / 信用卡 / PayPal / 支票捐款' width='73' height='44' border='0' align='top'></a></p>
		<hr>
		<a name='check' id='check'></a>
	<h2>支票捐款抬頭：</h2>
		<p class='m2'>支票捐款的大德，支票抬頭請填寫「財團法人西蓮教育基金會」。</p>
		<!--線上信用卡 / PayPal 贊助--> 
<br><br/>
</div>
	</body>
</html>