<?php
$EXTENDED_SEARCH_PATTERN = array('&',';','|','^','~','-');	//進階搜尋的符號,(&與;都代表「and」運算，因「&」在網址有特別意義，故多一個「;」來使用)
$EXTENDED_SEARCH_PATTERN_MAP = array('&' => 'AND',';'=>'AND','|'=>'OR','^'=>'NOT','~'=>'NEAR','-'=>'EXCLUDE');	//進階搜尋符號對照其意義字串

if(trim($_SERVER['HTTP_HOST']) === 'cbetaonline-dev.dila.edu.tw')	{	//dev 版
	
	
	$CBDATA_API = 'https://cbdata.dila.edu.tw/dev/';
	$CBDATA_API_BASE_NAME = 'cbdata.dila.edu.tw/dev';
	
	/*
	$CBDATA_API = 'https://cbdata.dila.edu.tw/stable/';
	$CBDATA_API_BASE_NAME = 'cbdata.dila.edu.tw/stable';
	*/
}
else if(trim($_SERVER['HTTP_HOST']) === 'cbetaonline.cn')	{	//中國分部
	$CBDATA_API = 'https://api.cbetaonline.cn/';
	$CBDATA_API_BASE_NAME = 'api.cbetaonline.cn';	
}
else	{	//正式版或其他任何
	$CBDATA_API = 'https://cbdata.dila.edu.tw/stable/';
	$CBDATA_API_BASE_NAME = 'cbdata.dila.edu.tw/stable';	
}



//處理檔案大小顯示的單位
function formatSizeUnits($bytes)	{
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
}

function check_near_words($words)	{
	$tmp = explode(',',$words);
	foreach($tmp as $v)	{
		$v = intval($v);
		if(!is_numeric(trim($v)) || $v < 1 || $v > 30)	return false;
	}
	return true;
}
?>