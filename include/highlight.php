<?php
ini_set("pcre.backtrack_limit", "223001337");	//阿閒add：增加regex的複雜度和遞迴之最大值已因應大量關鍵字的查找，否則會跑出「regular expresstion is too large」之error
ini_set("pcre.recursion_limit", "23001337");

mb_internal_encoding('utf-8');
/**
 * kwic_highlight function
 *
 * highlights keyword matches in complicated x/html
 *
 * $options = array (
 *   'html'      => false,  // find matches *inside* html code (e.g., match 'a' in "<a href...") [default: false]
 *   'casesens'  => false,  // perform case-sensitive matches [default: false]
 *   'iFlags'    => 0,      // bitwise parameter expressing complicated 'ignore' patterns [default: 0 (none)]
 *   'iRegex'    => null,   // user-supplied regex to ignore [default: null]
 * );
 *
 * to set 'ignore' flags use, for example:
 *  $options['iFlags'] = KH_IGNORE_EMPTY_TAGS | KH_IGNORE_OPENING_TAGS | KH_IGNORE_CLOSING_TAGS
 *
 * for a full example, see example.php
 *
 * @author Simon <simonjwiles@mail.com>
 * @version 0.1
 * @copyright DDBC
 *
 * @param string        $text       text to search (the 'haystack')
 * @param string|array  $kwords     keyword (or array of keywords) to search for (the 'needle(s)')
 * @param array|null    $options    array of options (see above)
 * @param string|null   $highlight  markup to wrap matches in ('%s' will be replaced by the match)
 * @param int|null      $count      if specified, will be populated with number of keywords marked-up
 *
 * @return string|null  original string with keywords marked-up, or null on failure
 */


define('KH_IGNORE_EMPTY_TAGS', 1);      // empty or self-closing tags
define('KH_IGNORE_OPENING_TAGS', 2);    // opening tags - e.g. 'abc' matches 'ab<p>c'
define('KH_IGNORE_CLOSING_TAGS', 4);    // closing tags - e.g. 'abc' matches 'ab</p>c'
define('KH_IGNORE_FULL_TAGS', 8);       // complete tags with content e.g. 'abc' matches 'ab<span>lalal</span>c'
                                        //  note: nested tags not supported
define('KH_IGNORE_NONLETTER', 16);      // any non-letter character (according to Unicode spec.)

//阿閒add：此二全域變數為了計算group_num用(in colorKeyword.php)
$KEYWORD_LENGTH = 0;
$REPLACED_COUNTER = 0;
$KEYWORD_DEVIATION = 0;	//因計數器在 colorKeyword.php 歸0，因此這邊為每個關鍵字加上偏移值，一次多100，保證 group_num 不重複

function kwic_highlight($text, $kwords, $options=array(), $highlight=null, &$count=null) {
	global $KEYWORD_LENGTH;

    if ($highlight === null) $highlight = '<em>%s</em>';  // default highlighting

    $pattern = (isset($options['html']) && $options['html'] == true) ? '(%s)' : '(?!<.*?)(%s)(?![^<>]*?>)';
    $kwords = (array) $kwords;

    if ((isset($options['iFlags']) && $options['iFlags']) || (isset($options['iRegex']) && $options['iRegex'])) {

        $ignores = array();
        if ($options['iFlags'] & KH_IGNORE_EMPTY_TAGS) $ignores[] = '(?:<[^/]+/>)|(?:<[^>]*>\s*</[^>]*>)';
        if ($options['iFlags'] & KH_IGNORE_OPENING_TAGS) $ignores[] = '(?:<[^>/]+>)';
        if ($options['iFlags'] & KH_IGNORE_CLOSING_TAGS) $ignores[] = '(?:</[^>]+>)';
        $ignores[] = ($options['iFlags'] & KH_IGNORE_FULL_TAGS) ? '(?:<(\S*)\s+[^>]*>[^<>]*?</\%d>)' : '()';
        if ($options['iFlags'] & KH_IGNORE_NONLETTER) $ignores[] = '\P{L}';
        if ($options['iRegex']) $ignores[] = $options['iRegex'];

        $ignores = implode('|',$ignores);

        $GLOBALS['kwic_highlight'] = $highlight;
        foreach ($kwords as $kword) {
			$KEYWORD_LENGTH = mb_strlen($kword,'utf-8');	//阿閒add：先取的關鍵字的長度
            $chars = preg_split('//u',$kword, -1, PREG_SPLIT_NO_EMPTY);
            $regex = sprintf($pattern,preg_quote($chars[0]));
            for ($i=1,$il=count($chars);$i<$il;$i++)
                $regex .= '((?:'.sprintf($ignores,$i*3).')*?)'.sprintf($pattern,preg_quote($chars[$i]));
            $pattern = '#'.$regex.'#us';
            if (!isset($options['casesens']) || !$options['casesens']) $pattern .= 'i';
            $text = @preg_replace_callback($pattern, 'kwic_hl_markup', $text, -1, $count);
        }
    } else {
        $pattern = '#'.$pattern.'#u';
        if (!isset($options['casesens']) || !$options['casesens']) $pattern .= 'i';
        $highlight = sprintf($highlight, '\1');
        foreach ($kwords as $kword) {
            $kword = preg_quote($kword);
            $text = @preg_replace(sprintf($pattern, $kword), $highlight, $text, -1, $count);
        }
    }

    return $text;
}


function kwic_hl_markup($matches){
	global $REPLACED_COUNTER,$KEYWORD_LENGTH,$KEYWORD_DEVIATION;
    $result='';
    for ($i=0,$il=count($matches);$i<$il;$i++) {
        switch ($i % 3) {
            case 1: $result .= sprintf($GLOBALS['kwic_highlight'],((($REPLACED_COUNTER++)/$KEYWORD_LENGTH)+1)+$KEYWORD_DEVIATION ,$matches[$i]);	//阿閒modify：增加了 (($REPLACED_COUNTER++)/$KEYWORD_LENGTH)+1 的運算，用於計算colorKeyword.php裡面的group_num
            break;
            case 2: $result .= $matches[$i];
            break;
        }
    }
    return $result;
}

?>
