# BIP(佛學整合平台、CBETA Online)

這是洪老師國科會的專案，後來變成CBETA官方線上版，整合了CBETA經典，提供了豐富的佛典資源整合功能，目前仍在開發中，經典先提供大正藏(T)的部份。

## Browser Support

![IE](https://cloud.githubusercontent.com/assets/398893/3528325/20373e76-078e-11e4-8e3a-1cb86cf506f0.png) | ![Chrome](https://cloud.githubusercontent.com/assets/398893/3528328/23bc7bc4-078e-11e4-8752-ba2809bf5cce.png) | ![Firefox](https://cloud.githubusercontent.com/assets/398893/3528329/26283ab0-078e-11e4-84d4-db2cf1009953.png) | ![Opera](https://cloud.githubusercontent.com/assets/398893/3528330/27ec9fa8-078e-11e4-95cb-709fd11dac16.png) | ![Safari](https://cloud.githubusercontent.com/assets/398893/3528331/29df8618-078e-11e4-8e3e-ed8ac738693f.png)
--- | --- | --- | --- | --- |
IE 9+ ✔ | Latest ✔ | Latest ✔ | Latest ✔ | Latest ✔ |

## Site

__dev server__: [http://dev.ddbc.edu.tw/bip](http://dev.ddbc.edu.tw/bip)

## 安裝注意事項

1.  html的部份因檔案太大，沒有放到github，未來html的部份將直接結合github做版本管理，在此之前html需手動更新。
2.  include/db_config_sample.php為設定檔的範例，第一次fork時要手動將檔名改成db_config.php並填入db連線資訊方可使用。

## Author

李阿閒@DDBC

## License

Copyright ©2014 DDBC

