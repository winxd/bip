<?php
ini_set('display_errors','Off');	//不顯示錯誤訊息於網頁上
ini_set('log_errors','On');			//開啟錯誤紀錄
error_reporting(E_ALL & ~E_NOTICE);	//回報除了E_NOTICE外的所有錯誤

include "../include/config.php";

$type = isset($_POST['type']) ? trim($_POST['type']):false;


if($type == 'text_search' || trim($_POST['export']) === 'excel' || trim($_POST['export']) === 'csv')	{	//全文檢索，結果可能回給前端也可能是輸出excel , csv
	
	
	//表示要輸出excel or csv，把他們填成post吧...
	if((isset($_POST['export']) && trim($_POST['export']) == 'excel') || (isset($_POST['export']) && trim($_POST['export']) == 'csv'))	{
		if(isset($_POST['export-rows']))	$_POST['rows'] = trim($_POST['export-rows']);
		if(isset($_POST['export-qry']))		$_POST['qry'] = trim($_POST['export-qry']);
		if(isset($_POST['export-range']))	$_POST['range'] = trim($_POST['export-range']);
		if(isset($_POST['export-sortby']))	$_POST['sortby'] = trim($_POST['export-sortby']);
	}
	
	$_POST['qry'] = str_replace("'","\'", $_POST['qry']);	//跨過單引號，ray那邊有問題先不做
	//$_POST['qry'] = str_replace('"','\"', $_POST['qry']);	//跨過單引號，ray那邊有問題先不做
	//echo $_POST['qry'];
	
	//使用curl執行POST
	$params_tc = array();	//底下簡轉繁要用的
	$params = array();
	$params['rows'] = isset($_POST['rows']) ? trim($_POST['rows']):'100';				//傳回幾筆
	$params['start'] = isset($_POST['offset']) ? trim($_POST['offset']):'0';			//從結果的哪邊開始
	$params['around'] = isset($_POST['words']) ? trim($_POST['words']):'60';
	$params['note'] = isset($_POST['note']) ? trim($_POST['note']):'1';	//是否跨夾注選項，預設:不跨夾注=1
	$params['q'] = $_POST['qry'];
	
	if(isset($_POST['facet']) && trim($_POST['facet']) !== '')	{	//facet
		$params['facet'] = trim($_POST['facet']);
	}
	if(isset($_POST['sortby']) && trim($_POST['sortby']) !== '')	{	//排序
		$params['order'] = trim($_POST['sortby']);
	}
	
	if(isset($_POST['range']) && trim($_POST['range']) !== '')	{	//處理搜尋範圍參數
		$ranges = explode(';',trim($_POST['range']));
		foreach($ranges as $range)	{
			list($range_type,$filter_range) = explode(':',trim($range));
			$params[trim($range_type)] = trim($filter_range);
			$params_tc[trim($range_type)] = trim($filter_range);	//順便複製一份搜尋範圍給簡轉繁要用params_tc
		}
	}
	
	//$near_words = (isset($_POST['near_words']) && intval(trim($_POST['near_words'])) >=2 && intval(trim($_POST['near_words'])) <= 10 ? intval(trim($_POST['near_words'])):5);
	$near_words = (isset($_POST['near_words']) && trim($_POST['near_words']) !== '' && check_near_words(trim($_POST['near_words'])) ? array_reverse(explode(',',trim($_POST['near_words']))):array(5));

	/*
	//API的搜尋排序
	list($sortFileld,$sortMethod) = explode(':',trim($_POST['sortby']));
	$sortMethod = $sortMethod == 'asc' ? '+':'-';
	
	if($sortFileld == 'jing')			$params['order'] = 'canon'.$sortMethod;
	else if($sortFileld == 'title')		$params['order'] = 'title'.$sortMethod;
	else if($sortFileld == 'author')	$params['order'] = 'byline'.$sortMethod;
	else if($sortFileld == 'juan')		$params['order'] = 'juan'.$sortMethod;
	else if($sortFileld == 'term_hits')	$params['order'] = 'term_hits'.$sortMethod;
	else if($sortFileld == 'time')		$params['order'] = "time_from{$sortMethod},time_to{$sortMethod},canon{$sortMethod}";
	//else								$params['order'] = 'time_from,time_to,canon';	//預設照起始年、結束年、經號 依序排序
	else								$params['order'] = 'canon';	//2018-07-26: 改回照經號排就好
	*/
	
	//進階搜尋處理 - 檢視搜尋字串中是否有進些搜尋之pattern
	$extened_search = false;
	$extended_pattern_str = implode('',$EXTENDED_SEARCH_PATTERN);
	if(preg_match_all('/([^'.preg_quote($extended_pattern_str).']+)(['.preg_quote($extended_pattern_str).']?)/u',trim($_POST['qry']),$matches) && preg_match('/['.preg_quote($extended_pattern_str).']/u',trim($_POST['qry'])))	{
		//print_r($matches);
		$extened_search = true;
		$query_str = array();
		$near_flag = false;
		if(count($matches) > 1)	{
			for($i = 0;$i < count($matches[1]); $i++)	{
				/*
				if(trim($matches[2][$i]) == '&' || trim($matches[2][$i]) == ';')		$matches[2][$i] = '';
				else if(trim($matches[2][$i]) == '~')	$matches[2][$i] = 'NEAR/'.$near_words;
				$query_str[] = ' "'.(trim($matches[1][$i])).'" '.trim($matches[2][$i]);
				*/
				
				
				if(trim($matches[2][$i]) == '~')	{
					$near_flag = true;
					$matches[2][$i] = ' NEAR/'.trim(array_pop($near_words)).' ';
					//$matches[2][$i] = ' NEAR/'.$near_words.'';
					$query_str[] = '"'.trim($matches[1][$i]).'"'.$matches[2][$i];
				}
				else	{
					if($near_flag)	{						
						$query_str[] = '"'.trim($matches[1][$i]).'"';
					}
					else	{
						if(trim($matches[2][$i]) == '&' || trim($matches[2][$i]) == ';')	$matches[2][$i] = '';
						if(trim($matches[2][$i]) == '^')	$matches[2][$i] = '!';	//not 實際查詢需要用"!"，所以需由 "^" 換成 "!"
						if(trim($matches[2][$i]) == '|')	$matches[2][$i] = '| ';	//or 實際查詢後面需要多一個空格！什麼雞巴特殊規則，周邦信啥洨爛api
						$query_str[] = '"'.trim($matches[1][$i]).'" '.$matches[2][$i];
					}
				}
				
			}	
		}
		//print_r($query_str);
		$params['q'] = trim(implode('',$query_str));
		//echo $params['q'].'<br/>'; 
	}
	
	//var_dump($params);
	//exit;
	
	//加入 referer header 設定，讓雷可以統計
	$headers = array(
		'Referer: https://'.$_SERVER['HTTP_HOST']
	);
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $CBDATA_API."search/all_in_one/");
	curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);	//*加上curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);之後，他會將curl_exec()獲取的訊息以文件流的形式返回傳給$temp，而不是直接輸出。如果要顯示出畫面的話就加上 echo $temp;就可以了。
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);	//雷那邊有問題，關閉ssl連線認證
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
	$res = curl_exec($ch);
	curl_close($ch);	
	
	//echo $res;	//搜尋結果
	//exit;
	
	$json = json_decode($res);	//將搜尋結果轉成$json物件
	
	
	//將搜尋字串轉繁體，看一下筆數有多少，將此結果塞入回傳(需使用post，user可能會用搜尋範圍，因此需將搜尋範圍參數傳過去，筆數的計算與前端的字數compare才會正確)
	$params_tc['q'] = ($_POST['qry']);
	$ch_tc = curl_init();
	curl_setopt($ch_tc, CURLOPT_URL, $CBDATA_API.'search/sc');
	curl_setopt($ch_tc, CURLOPT_POST, true); // 啟用POST
	curl_setopt($ch_tc, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch_tc, CURLOPT_SSL_VERIFYPEER, 0);	//雷那邊有問題，關閉ssl連線認證
	curl_setopt($ch_tc, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch_tc, CURLOPT_POSTFIELDS, http_build_query($params_tc));
	$res_tc = curl_exec($ch_tc);
	curl_close($ch_tc);	
	//echo $res_tc;	//搜尋結果	
	$tc_counts = json_decode($res_tc);	//將簡轉繁結果塞回結果json
	$json->tc_counts = $tc_counts;
	

	
	if(isset($_POST['export']) && trim($_POST['export']) === 'excel')	{	//將搜尋結果輸出excel
		include "../include/excelwriter.inc.php";
		
		$excel = get_excel_header();
		$excel .= write_excel_line(array("經號","作譯者","年代","經名","卷","行號","Keyword in context","URL"));

		foreach($json->results as $r_obj)	{
			foreach($r_obj->kwics->results as $kwic_obj)	{
				$lb = $kwic_obj->lb;
				$ln_prefix = $r_obj->file.(preg_match('/[A-Za-z]/u',substr($r_obj->work,-1,1)) ? '':'_').'p';	//經號裡面的底線「_」需看一下work，如果work後面有ABCabc，行號就不能有底線，反之才能有底線
				
				$excel .= write_excel_line(array($r_obj->work,$r_obj->byline,((isset($r_obj->time_from) && $r_obj->time_from != '0'  ? $r_obj->time_from:'?').(isset($r_obj->time_to) && $r_obj->time_to != '0' ? '~'.$r_obj->time_to:'~?')),$r_obj->title,$r_obj->juan,$kwic_obj->lb,$kwic_obj->kwic,'https://cbetaonline.dila.edu.tw/'.$ln_prefix.$lb));
			}			
		}
		$excel .= get_excel_footer();
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=CBETA_export_".trim($_POST['qry'])."_".date('mdHi').".xls");
		echo $excel;
	}
	else if(isset($_POST['export']) && trim($_POST['export']) === 'csv')	{	//將搜尋結果輸出csv
		$csv = array();
		$csv[] = array("經號","作譯者","年代","經名","卷","行號","Keyword in context","URL");

		foreach($json->results as $r_obj)	{
			//$csv[] = array($r_obj->term_hits,$r_obj->file,$r_obj->title,$r_obj->juan,$r_obj->byline,($r_obj->time_from ? $r_obj->time_from:''),($r_obj->time_to ? $r_obj->time_to:''));
			foreach($r_obj->kwics->results as $kwic_obj)	{
				$lb = $kwic_obj->lb;
				$ln_prefix = $r_obj->file.(preg_match('/[A-Za-z]/u',substr($r_obj->work,-1,1)) ? '':'_').'p';	//經號裡面的底線「_」需看一下work，如果work後面有ABCabc，行號就不能有底線，反之才能有底線
			
				$csv[] = array($r_obj->work,$r_obj->byline,((isset($r_obj->time_from) && $r_obj->time_from != '0' ? $r_obj->time_from:'?').(isset($r_obj->time_to) && $r_obj->time_to != '0' ? '~'.$r_obj->time_to:'~?')),$r_obj->title,$r_obj->juan,$r_obj->byline,$kwic_obj->lb,$kwic_obj->kwic,'https://cbetaonline.dila.edu.tw/'.$ln_prefix.$lb);
			}			
		}
		
		header('Content-Encoding: UTF-8');
		header("Content-type: text/csv;charset=UTF-8");
		header("Content-disposition: attachment;filename=CBETA_export_".trim($_POST['qry'])."_".date('mdHi').".csv");
		
		echo "\xEF\xBB\xBF";
		echo implode("\n",array_map(function($csv_items)	{	return implode(',',$csv_items);	},$csv));
		
	}	
	else	{	//一般全文檢索
		echo json_encode($json);
	}
	
}
else if($type == 'facet_search')	{
	
	//file_get_contents 加入 referer header 設定，讓雷可以統計
	$context_opts = array(
		"http" => array(
			"method" => "GET",
			"header" => "Referer: https://".$_SERVER['HTTP_HOST']."\r\n"
				//."Cookie: foo=bar\r\n"
		)
	);
	$context = stream_context_create($context_opts);
	
	echo file_get_contents($CBDATA_API."search/facet/?q=".urlencode(trim($_POST['q'])),false,$context);
	
	
	/*
	$params = array();
	$params['q'] = urldecode($_POST['q']);
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $CBDATA_API."search/facet/");
	curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);	//雷那邊有問題，關閉ssl連線認證
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
	$res = curl_exec($ch);
	curl_close($ch);	
	
	//echo $res;	//搜尋結果
	
	$json = json_decode($res);	//將搜尋結果轉成$json物件	
	*/
}
else if($type == 'variant_synonym_search')	{	//全文檢索之異體字、同義詞查詢
	$res = array();
	
	//file_get_contents 加入 referer header 設定，讓雷可以統計
	$context_opts = array(
		"http" => array(
			"method" => "GET",
			"header" => "Referer: https://".$_SERVER['HTTP_HOST']."\r\n"
				//."Cookie: foo=bar\r\n"
		)
	);
	$context = stream_context_create($context_opts);
	
	
	//異體字可帶入後分類給的參數
	$variant_range_params = array();
	if(isset($_POST['range']) && trim($_POST['range']) !== '')	{	//處理搜尋範圍參數
		$ranges = explode(';',trim($_POST['range']));
		foreach($ranges as $range)	{
			list($range_type,$filter_range) = explode(':',trim($range));
			$variant_range_params[trim($range_type)] = trim($filter_range);
			
		}
	}	
	$variant_range_params = count($variant_range_params) > 0 ? '&'.http_build_query($variant_range_params):'';
	//$variant_range_params = '';	//測試用先清除
	
	if(trim($_POST['variant_active']) == 'true')	
		$res['variant'] = json_decode(file_get_contents($CBDATA_API."search/variants?q=".urlencode(trim($_POST['q'])).$variant_range_params,false,$context));
	
	if(trim($_POST['synonym_active']) == 'true')
		$res['synonym'] = json_decode(file_get_contents($CBDATA_API."search/synonym?q=".urlencode(trim($_POST['q'])),false,$context));

	echo json_encode($res);
}
else if($type == 'jinlu_variant_search')	{	//經名搜尋之異體字查詢
	$res = array();
	
	//file_get_contents 加入 referer header 設定，讓雷可以統計
	$context_opts = array(
		"http" => array(
			"method" => "GET",
			"header" => "Referer: https://".$_SERVER['HTTP_HOST']."\r\n"
				//."Cookie: foo=bar\r\n"
		)
	);
	$context = stream_context_create($context_opts);
	
	if(trim($_POST['variant_active']) == 'true')	
		$res['variant'] = json_decode(file_get_contents($CBDATA_API."search/variants?q=".urlencode(trim($_POST['q']))."&scope=title",false,$context));


	echo json_encode($res);
}
else if($type == 'footnote_search' || trim($_POST['export-footnote']) === 'excel' || trim($_POST['export-footnote']) === 'csv')	{	//校勘檢索，結果可能回給前端也可能是輸出excel , csv
	
	
	//表示要輸出excel or csv，把他們填成post吧...
	if((isset($_POST['export-footnote']) && trim($_POST['export-footnote']) == 'excel') || (isset($_POST['export-footnote']) && trim($_POST['export-footnote']) == 'csv'))	{
		if(isset($_POST['export-footnote-rows']))	$_POST['rows'] = trim($_POST['export-footnote-rows']);
		if(isset($_POST['export-footnote-qry']))	$_POST['qry'] = trim($_POST['export-footnote-qry']);
		if(isset($_POST['export-footnote-range']))	$_POST['range'] = trim($_POST['export-footnote-range']);
		if(isset($_POST['export-footnote-sortby']))	$_POST['sortby'] = trim($_POST['export-footnote-sortby']);
	}
	
	 //$_POST['qry'] = str_replace("'","\'", $_POST['qry']);	//跨過單引號，ray那邊有問題先不做
	
	//使用curl執行POST
	$params_tc = array();	//底下簡轉繁要用的
	$params = array();
	$params['rows'] = isset($_POST['rows']) ? trim($_POST['rows']):'100';				//傳回幾筆
	$params['start'] = isset($_POST['offset']) ? trim($_POST['offset']):'0';			//從結果的哪邊開始
	$params['around'] = isset($_POST['words']) ? trim($_POST['words']):'60';
	$params['q'] = $_POST['qry'];
	
	if(isset($_POST['facet']) && trim($_POST['facet']) !== '')	{	//facet
		$params['facet'] = trim($_POST['facet']);
	}
	if(isset($_POST['sortby']) && trim($_POST['sortby']) !== '')	{	//排序
		$params['order'] = trim($_POST['sortby']);
	}
	
	if(isset($_POST['range']) && trim($_POST['range']) !== '')	{	//處理搜尋範圍參數
		$ranges = explode(';',trim($_POST['range']));
		foreach($ranges as $range)	{
			list($range_type,$filter_range) = explode(':',trim($range));
			$params[trim($range_type)] = trim($filter_range);
			$params_tc[trim($range_type)] = trim($filter_range);	//順便複製一份搜尋範圍給簡轉繁要用params_tc
		}
	}
	
	$near_words = (isset($_POST['near_words']) && trim($_POST['near_words']) !== '' && check_near_words(trim($_POST['near_words'])) ? array_reverse(explode(',',trim($_POST['near_words']))):array(5));
	
	//進階搜尋處理 - 檢視搜尋字串中是否有進些搜尋之pattern
	$footnote_extened_search = false;
	$extended_pattern_str = implode('',$EXTENDED_SEARCH_PATTERN);
	if(preg_match_all('/([^'.preg_quote($extended_pattern_str).']+)(['.preg_quote($extended_pattern_str).']?)/u',trim($_POST['qry']),$matches) && preg_match('/['.preg_quote($extended_pattern_str).']/u',trim($_POST['qry'])))	{
		//print_r($matches);
		$footnote_extened_search = true;
		$query_str = array();
		$near_flag = false;
		if(count($matches) > 1)	{
			for($i = 0;$i < count($matches[1]); $i++)	{
				/*
				if(trim($matches[2][$i]) == '&' || trim($matches[2][$i]) == ';')		$matches[2][$i] = '';
				else if(trim($matches[2][$i]) == '~')	$matches[2][$i] = 'NEAR/'.$near_words;
				$query_str[] = ' "'.(trim($matches[1][$i])).'" '.trim($matches[2][$i]);
				*/
				
				
				if(trim($matches[2][$i]) == '~')	{
					$near_flag = true;
					$matches[2][$i] = ' NEAR/'.trim(array_pop($near_words)).' ';
					//$matches[2][$i] = ' NEAR/'.$near_words.'';
					$query_str[] = '"'.trim($matches[1][$i]).'"'.$matches[2][$i];
				}
				else	{
					if($near_flag)	{						
						$query_str[] = '"'.trim($matches[1][$i]).'"';
					}
					else	{
						if(trim($matches[2][$i]) == '&' || trim($matches[2][$i]) == ';')	$matches[2][$i] = '';
						if(trim($matches[2][$i]) == '^')	$matches[2][$i] = '!';	//not 實際查詢需要用"!"，所以需由 "^" 換成 "!"
						$query_str[] = ' "'.trim($matches[1][$i]).'" '.trim($matches[2][$i]);
					}
				}
				
			}	
		}
		//print_r($query_str);
		$params['q'] = implode('',$query_str);
		//echo $params['q'].'<br/>'; 
	}
	
	if(!$footnote_extened_search)	$params['q'] = '"'.$params['q'].'"';	//footnote 一般搜尋關鍵字也要加上雙引號
	
	
	//var_dump($params);
	//exit;
	
	//加入 referer header 設定，讓雷可以統計
	$headers = array(
		'Referer: https://'.$_SERVER['HTTP_HOST']
	);	
	
	$ch = curl_init();
	//curl_setopt($ch, CURLOPT_URL, $CBDATA_API."search/footnotes/");
	curl_setopt($ch, CURLOPT_URL, $CBDATA_API."search/notes/");
	curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);	//*加上curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);之後，他會將curl_exec()獲取的訊息以文件流的形式返回傳給$temp，而不是直接輸出。如果要顯示出畫面的話就加上 echo $temp;就可以了。
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);	//雷那邊有問題，關閉ssl連線認證
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
	$res = curl_exec($ch);
	curl_close($ch);	
	
	//echo $res;	//搜尋結果
	
	$json = json_decode($res);	//將搜尋結果轉成$json物件
	
	
	if(isset($_POST['export-footnote']) && trim($_POST['export-footnote']) === 'excel')	{	//將搜尋結果輸出excel
		include "../include/excelwriter.inc.php";
		
		$excel = get_excel_header();
		$excel .= write_excel_line(array("經號","經名","卷","行號","校勘"));

		foreach($json->results as $r_obj)	{
			$excel .= write_excel_line(array($r_obj->work,$r_obj->title,$r_obj->juan,$r_obj->file.'_p'.$r_obj->lb,$r_obj->content));
		}
		$excel .= get_excel_footer();
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=CBETA_footnote_export_".trim($_POST['qry'])."_".date('mdHi').".xls");
		echo $excel;
	}
	else if(isset($_POST['export-footnote']) && trim($_POST['export-footnote']) === 'csv')	{	//將搜尋結果輸出csv
		$csv = array();
		$csv[] = array("經號","經名","卷","行號","校勘");

		foreach($json->results as $r_obj)	{
			$csv[] = array($r_obj->work,$r_obj->title,$r_obj->juan,$r_obj->file.'_p'.$r_obj->lb,$r_obj->content);
		}
		
		header('Content-Encoding: UTF-8');
		header("Content-type: text/csv;charset=UTF-8");
		header("Content-disposition: attachment;filename=CBETA_footnote_export_".trim($_POST['qry'])."_".date('mdHi').".csv");
		
		echo "\xEF\xBB\xBF";
		echo implode("\n",array_map(function($csv_items)	{	return implode(',',$csv_items);	},$csv));
		
	}	
	else	{	//一般全文檢索
		echo json_encode($json);
	}
	
}
else if($type == 'jinlu_search' || trim($_POST['export-jinlu']) === 'excel' || trim($_POST['export-jinlu']) === 'csv')	{	//經錄檢索，結果可能回給前端也可能是輸出excel , csv
	
	/*
	//表示要輸出excel or csv，把他們填成post吧...
	if((isset($_POST['export-jinlu']) && trim($_POST['export-jinlu']) == 'excel') || (isset($_POST['export-jinlu']) && trim($_POST['export-jinlu']) == 'csv'))	{
		if(isset($_POST['export-jinlu-qry']))	$_POST['qry'] = trim($_POST['export-jinlu-qry']);
		if(isset($_POST['export-jinlu-range']))	$_POST['range'] = trim($_POST['export-jinlu-range']);
	}
	*/
	
	
	//使用curl執行POST
	$params_tc = array();	//底下簡轉繁要用的
	$params = array();
	$params['rows'] = isset($_POST['rows']) ? trim($_POST['rows']):'9999999';				//傳回幾筆
	$params['q'] = $_POST['qry'];
	
	
	if(isset($_POST['range']) && trim($_POST['range']) !== '')	{	//處理搜尋範圍參數
		$ranges = explode(';',trim($_POST['range']));
		foreach($ranges as $range)	{
			list($range_type,$filter_range) = explode(':',trim($range));
			$params[trim($range_type)] = trim($filter_range);
			$params_tc[trim($range_type)] = trim($filter_range);	//順便複製一份搜尋範圍給簡轉繁要用params_tc
		}
	}
	
	
	//進階搜尋處理 - 檢視搜尋字串中是否有進些搜尋之pattern
	$jinlu_extened_search = false;
	$extended_pattern_str = implode('',$EXTENDED_SEARCH_PATTERN);
	if(preg_match_all('/([^'.preg_quote($extended_pattern_str).']+)(['.preg_quote($extended_pattern_str).']?)/u',trim($_POST['qry']),$matches) && preg_match('/['.preg_quote($extended_pattern_str).']/u',trim($_POST['qry'])))	{
		//print_r($matches);
		$jinlu_extened_search = true;
		$query_str = array();
		$near_flag = false;
		if(count($matches) > 1)	{
			for($i = 0;$i < count($matches[1]); $i++)	{
				if(trim($matches[2][$i]) == '~')	{
					$near_flag = true;
					$query_str[] = trim($matches[1][$i]).' ';
				}
				else	{
					if($near_flag)	{
						$query_str[] = trim($matches[1][$i]);
					}
					else	{
						if(trim($matches[2][$i]) == '&' || trim($matches[2][$i]) == ';')	$matches[2][$i] = '';
						if(trim($matches[2][$i]) == '^')	$matches[2][$i] = '!';	//not 實際查詢需要用"!"，所以需由 "^" 換成 "!"
						$query_str[] = ' "'.trim($matches[1][$i]).'" '.trim($matches[2][$i]);
					}
				}
			}	
		}		
		//print_r($query_str);
		$params['q'] = $near_flag ? '"'.implode('',$query_str).'"~10' : implode('',$query_str);
		//echo $params['q'].'<br/>'; 
	}
	
	//if(!$jinlu_extened_search)	$params['q'] = '"'.$params['q'].'"';	//經名一般搜尋關鍵字也要加上雙引號
	if(!$jinlu_extened_search)	$params['q'] = ''.$params['q'].'';	//2023-11-23:雷說：經名一般搜尋關鍵字不用加上雙引號了
	
	//var_dump($params);
	//exit;
	
	//加入 referer header 設定，讓雷可以統計
	$headers = array(
		'Referer: https://'.$_SERVER['HTTP_HOST']
	);
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $CBDATA_API."search/title/");
	curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);	//雷那邊有問題，關閉ssl連線認證
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
	$res = curl_exec($ch);
	curl_close($ch);	
	
	//echo $res;	//搜尋結果
	
	$json = json_decode($res);	//將搜尋結果轉成$json物件
	
	echo json_encode($json);
	
	/*
	if(isset($_POST['export-jinlu']) && trim($_POST['export-jinlu']) === 'excel')	{	//將搜尋結果輸出excel
		include "../include/excelwriter.inc.php";
		
		$excel = get_excel_header();
		$excel .= write_excel_line(array("經號","經名","卷","行號","校勘"));

		foreach($json->results as $r_obj)	{
			$excel .= write_excel_line(array($r_obj->work,$r_obj->title,$r_obj->juan,$r_obj->file.'_p'.$r_obj->lb,$r_obj->content));
		}
		$excel .= get_excel_footer();
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=CBETA_footnote_export_".trim($_POST['qry'])."_".date('mdHi').".xls");
		echo $excel;
	}
	else if(isset($_POST['export-jinlu']) && trim($_POST['export-jinlu']) === 'csv')	{	//將搜尋結果輸出csv
		$csv = array();
		$csv[] = array("經號","經名","卷","行號","校勘");

		foreach($json->results as $r_obj)	{
			$csv[] = array($r_obj->work,$r_obj->title,$r_obj->juan,$r_obj->file.'_p'.$r_obj->lb,$r_obj->content);
		}
		
		header('Content-Encoding: UTF-8');
		header("Content-type: text/csv;charset=UTF-8");
		header("Content-disposition: attachment;filename=CBETA_footnote_export_".trim($_POST['qry'])."_".date('mdHi').".csv");
		
		echo "\xEF\xBB\xBF";
		echo implode("\n",array_map(function($csv_items)	{	return implode(',',$csv_items);	},$csv));
		
	}	
	else	{	//一般全文檢索
		echo json_encode($json);
	}
	*/
	
}
else if($type == 'similar_search' || trim($_POST['export-similar']) === 'excel' || trim($_POST['export-similar']) === 'csv')	{	//相似句檢索，結果可能回給前端也可能是輸出excel , csv
	
	
	//表示要輸出excel or csv，把他們填成post吧...
	if((isset($_POST['export-similar']) && trim($_POST['export-similar']) == 'excel') || (isset($_POST['export-similar']) && trim($_POST['export-similar']) == 'csv'))	{
		if(isset($_POST['export-similar-rows']))	$_POST['rows'] = trim($_POST['export-similar-rows']);
		if(isset($_POST['export-similar-qry']))	$_POST['qry'] = trim($_POST['export-similar-qry']);
		if(isset($_POST['export-similar-range']))	$_POST['range'] = trim($_POST['export-similar-range']);
		if(isset($_POST['export-similar-sortby']))	$_POST['sortby'] = trim($_POST['export-similar-sortby']);
		if(isset($_POST['export-similar-score']))	$_POST['score'] = trim($_POST['export-similar-score']);
		
	}
	
	 //$_POST['qry'] = str_replace("'","\'", $_POST['qry']);	//跨過單引號，ray那邊有問題先不做
	
	//使用curl執行POST
	$params_tc = array();	//底下簡轉繁要用的
	$params = array();
	$params['rows'] = isset($_POST['rows']) ? trim($_POST['rows']):'100';				//傳回幾筆
	$params['start'] = isset($_POST['offset']) ? trim($_POST['offset']):'0';			//從結果的哪邊開始
	$params['around'] = isset($_POST['words']) ? trim($_POST['words']):'60';
	$params['score_min'] = isset($_POST['score']) ? trim($_POST['score']):'16';			//score 值，預設為16
	$params['q'] = $_POST['qry'];
	
	if(isset($_POST['facet']) && trim($_POST['facet']) !== '')	{	//facet
		$params['facet'] = trim($_POST['facet']);
	}
	if(isset($_POST['sortby']) && trim($_POST['sortby']) !== '')	{	//排序
		$params['order'] = trim($_POST['sortby']);
	}
	
	if(isset($_POST['range']) && trim($_POST['range']) !== '')	{	//處理搜尋範圍參數
		$ranges = explode(';',trim($_POST['range']));
		foreach($ranges as $range)	{
			list($range_type,$filter_range) = explode(':',trim($range));
			$params[trim($range_type)] = trim($filter_range);
			$params_tc[trim($range_type)] = trim($filter_range);	//順便複製一份搜尋範圍給簡轉繁要用params_tc
		}
	}
	
	$near_words = (isset($_POST['near_words']) && trim($_POST['near_words']) !== '' && check_near_words(trim($_POST['near_words'])) ? array_reverse(explode(',',trim($_POST['near_words']))):array(5));
	
	//進階搜尋處理 - 檢視搜尋字串中是否有進些搜尋之pattern
	$similar_extened_search = false;
	$extended_pattern_str = implode('',$EXTENDED_SEARCH_PATTERN);
	if(preg_match_all('/([^'.preg_quote($extended_pattern_str).']+)(['.preg_quote($extended_pattern_str).']?)/u',trim($_POST['qry']),$matches) && preg_match('/['.preg_quote($extended_pattern_str).']/u',trim($_POST['qry'])))	{
		//print_r($matches);
		$similar_extened_search = true;
		$query_str = array();
		$near_flag = false;
		if(count($matches) > 1)	{
			for($i = 0;$i < count($matches[1]); $i++)	{
				/*
				if(trim($matches[2][$i]) == '&' || trim($matches[2][$i]) == ';')		$matches[2][$i] = '';
				else if(trim($matches[2][$i]) == '~')	$matches[2][$i] = 'NEAR/'.$near_words;
				$query_str[] = ' "'.(trim($matches[1][$i])).'" '.trim($matches[2][$i]);
				*/
				
				
				if(trim($matches[2][$i]) == '~')	{
					$near_flag = true;
					$matches[2][$i] = ' NEAR/'.trim(array_pop($near_words)).' ';
					//$matches[2][$i] = ' NEAR/'.$near_words.'';
					$query_str[] = '"'.trim($matches[1][$i]).'"'.$matches[2][$i];
				}
				else	{
					if($near_flag)	{						
						$query_str[] = '"'.trim($matches[1][$i]).'"';
					}
					else	{
						if(trim($matches[2][$i]) == '&' || trim($matches[2][$i]) == ';')	$matches[2][$i] = '';
						if(trim($matches[2][$i]) == '^')	$matches[2][$i] = '!';	//not 實際查詢需要用"!"，所以需由 "^" 換成 "!"
						$query_str[] = ' "'.trim($matches[1][$i]).'" '.trim($matches[2][$i]);
					}
				}
				
			}	
		}
		//print_r($query_str);
		$params['q'] = implode('',$query_str);
		//echo $params['q'].'<br/>'; 
	}
	
	if(!$similar_extened_search)	$params['q'] = ''.$params['q'].'';	//similar 一般搜尋關鍵字不需加上雙引號
	
	
	//var_dump($params);
	//exit;
	
	//加入 referer header 設定，讓雷可以統計
	$headers = array(
		'Referer: https://'.$_SERVER['HTTP_HOST']
	);	
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $CBDATA_API."search/similar/");
	curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);	//*加上curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);之後，他會將curl_exec()獲取的訊息以文件流的形式返回傳給$temp，而不是直接輸出。如果要顯示出畫面的話就加上 echo $temp;就可以了。
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);	//雷那邊有問題，關閉ssl連線認證
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
	$res = curl_exec($ch);
	curl_close($ch);	
	
	//echo $res;	//搜尋結果
	
	$json = json_decode($res);	//將搜尋結果轉成$json物件
	
	
	if(isset($_POST['export-similar']) && trim($_POST['export-similar']) === 'excel')	{	//將搜尋結果輸出excel
		include "../include/excelwriter.inc.php";
		
		$excel = get_excel_header();
		$excel .= write_excel_line(array("經號","經名","卷","行號","相似句"));

		foreach($json->results as $r_obj)	{
			$excel .= write_excel_line(array($r_obj->work,$r_obj->title,$r_obj->juan,$r_obj->linehead,$r_obj->content));
		}
		$excel .= get_excel_footer();
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=CBETA_similar_export_".trim($_POST['qry'])."_".date('mdHi').".xls");
		echo $excel;
	}
	else if(isset($_POST['export-similar']) && trim($_POST['export-similar']) === 'csv')	{	//將搜尋結果輸出csv
		$csv = array();
		$csv[] = array("經號","經名","卷","行號","相似句");

		foreach($json->results as $r_obj)	{
			$csv[] = array($r_obj->work,$r_obj->title,$r_obj->juan,$r_obj->linehead,$r_obj->content);
		}
		
		header('Content-Encoding: UTF-8');
		header("Content-type: text/csv;charset=UTF-8");
		header("Content-disposition: attachment;filename=CBETA_similar_export_".trim($_POST['qry'])."_".date('mdHi').".csv");
		
		echo "\xEF\xBB\xBF";
		echo implode("\n",array_map(function($csv_items)	{	return implode(',',$csv_items);	},$csv));
		
	}	
	else	{	//一般全文檢索
		echo json_encode($json);
	}
	
}
else if($type === 'jing_info')	{	//取得經典資訊
	$work = isset($_POST['work']) && trim($_POST['work']) !== '' ? trim($_POST['work']):false;
	if($work)	{		
		//file_get_contents 加入 referer header 設定，讓雷可以統計
		$context_opts = array(
			"http" => array(
				"method" => "GET",
				"header" => "Referer: https://".$_SERVER['HTTP_HOST']."\r\n"
					//."Cookie: foo=bar\r\n"
			)
		);
		$context = stream_context_create($context_opts);	
		
		$res = file_get_contents($CBDATA_API."works?work=".$work,false,$context);
		echo $res;
	}
}
else if(trim($_GET['type']) === 'jing_typeahead')	{	//經名自動完成	
	$final = array();
	
	//file_get_contents 加入 referer header 設定，讓雷可以統計
	$context_opts = array(
		"http" => array(
			"method" => "GET",
			"header" => "Referer: https://".$_SERVER['HTTP_HOST']."\r\n"
				//."Cookie: foo=bar\r\n"
		)
	);
	$context = stream_context_create($context_opts);

	$json = json_decode(file_get_contents($CBDATA_API.'toc?q='.urlencode(trim($_GET['q'])),false,$context));
	//var_dump($json);
	
	foreach($json->results as $row)	{
		if($row->type === 'work')	{
			$final[] = array('value' => $row->work , 'text' => $row->title,'creators'=> $row->creators);
		}
	}

	header('Content-Type: application/json; charset=utf-8');	//前端 lib 要求 header 是 application/json
	echo json_encode($final);
}
else if(trim($_GET['type']) === 'get_extended_search_pattern')	{	//輸出進階搜尋pattern給前端js
	$copyArr = $EXTENDED_SEARCH_PATTERN;	//PHP 是 copy by value
	echo 'var EXTENDED_SEARCH_PATTERN = '.json_encode($copyArr);
}

?>