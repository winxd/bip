function search(sortby,notFacet,filter_again)	{
	
	_GLOBALS.searchOffset = 0;	//每次搜尋時重設offset
	_GLOBALS.searchSortby = '';	//每次搜尋時重設searchSortby(此變數只有在表格排序事件處理時用到，為了排序後能處理infinite scroll時資料的一致性(沒有一樣的sortby取出順序不同資料會不連續、有誤))
	
	var deferred = $.Deferred();
	var is_normal_search = false;
	
	//關鍵字賦值，需處理進階搜尋部份
	if($('#start-search-btn-adv').is(':visible') && $('#normal-adv-search-item-form').is(':visible'))	{	//一般進階搜尋
		_GLOBALS.kw = get_adv_keyword_string('normal-adv-search-item-form');
	}
	else if($('#start-search-btn-adv').is(':visible') && $('#near-adv-search-item-form').is(':visible'))	{	//Near 搜尋
		_GLOBALS.kw = get_adv_keyword_string('near-adv-search-item-form');
	}
	else	{	//一般搜尋
		_GLOBALS.kw = $.trim($('#search-text').val());
		is_normal_search = true;
	}
	
	_GLOBALS.kw = _GLOBALS.kw.escape();
	document.title = _GLOBALS[_GLOBALS.lang]['23']+' '+_GLOBALS[_GLOBALS.lang]['25_1']+' - '+_GLOBALS.kw;	//更改標題
	
	$('#search-main-area , #cbtnarea').show();
	$('#search-result-area').html('<div style="padding-top:1.5em;font-size:large;margin-left:30%"><br/><img style="width:90px" src="../images/ajax-loader-type-3.gif"/></div>').attr('full-loaded','no');
	$('#search-res-info').html('');
	$('#variants-suggestion-area , #variants-suggestion-msg-area').hide();
	update_badge_counter('badge-counter-texts',0);
	
	if(!notFacet)	{
		$('#search-filter-area').html('<div style="margin:auto;padding-top:1.5em;font-size:large;text-align:center"> <br/><img style="width:90px;display:none" src="../images/ajax-loader-type-3.gif"/></div>');
	}
	
	
	//配合預設kwic是打開的，故全部收合按鈕重新搜尋後也要預設打開，這樣才不會跟前次混淆
	if($('#open-close-all-btn').find('i').hasClass('fa-caret-up'))	$('#open-close-all-btn').find('i').removeClass('fa-caret-up').addClass('fa-caret-down');	
	
	
	
	//if(parseInt(_GLOBALS.kw.replace(new RegExp('(['+_GLOBALS.punctuations.join('')+'])','gi'),'').length,10) > 85)	{	//全文檢索搜尋字數（含標點）長度不得大於85
	if(parseInt(_GLOBALS.kw.length,10) > 85)	{	//全文檢索搜尋字數（含標點）長度不得大於85
		$('#search-result-area').html('<span style="color:red;margin:2.5em;display:block">'+_GLOBALS[_GLOBALS.lang]['21_2']+'</span>');
		$('#search-filter-area,#selected-filter-area').html('');
		$('#cbtnarea').hide();
		$('.header-tabs').show();
		deferred.resolve();		
		return;
	}
	if(parseInt(_GLOBALS.kw.length,10) <= 0)	{
		$('#search-result-area').html('<span style="color:red;margin:2.5em;display:block">The keyword is empty.</span>');
		$('#search-filter-area,#selected-filter-area').html('');
		$('#cbtnarea').hide();
		$('.header-tabs').show();
		deferred.resolve();				
		return false;
	}
	
	if(sortby)	_GLOBALS.searchSortby = sortby;
	

	var search_range = _GLOBALS.searchRange == '' ? '':_GLOBALS.searchRange;	//search_range預設是搜全部(大正藏、卍續藏、嘉興藏)，故參數給空
	
	var facet = notFacet ? '':'&facet=1';
	var near_words = $('[name="near_adv_search_limit_conds[]"]').get().filter(o => $.trim($(o).next().val()) !== '').map(o => o.value);
	_GLOBALS.near_words = near_words;

	var xhr_text_search = $.ajax({
		url:'getData.php',
		data:'type=text_search'+facet+'&words='+_GLOBALS.kwic_words+'&near_words='+_GLOBALS.near_words+'&range='+search_range+'&rows='+_GLOBALS.searchScrollLimit+'&offset='+_GLOBALS.searchOffset+'&qry='+encodeURIComponent(_GLOBALS.kw)+(sortby ? '&sortby='+encodeURIComponent(sortby):'')+'&note='+(_GLOBALS.skipInlineNoteHL ? '0':'1'),
		type:'POST'
	}).done(function(json)	{
		var json = $.parseJSON(json);
		var use_tc_re_search = '';	//是否要用繁體中文重新搜尋之html
			
		if(!json || parseInt(json.num_found,10) <= 0)	{

			use_tc_re_search = get_tc_re_search_str(json);	//搜尋結果為0時判斷一下是不是要簡轉繁
			
			$('#search-result-area').html('<li style="list-style: none;margin-top:3em">「<span style="color:red">'+_GLOBALS.kw+'</span>」'+_GLOBALS[_GLOBALS.lang]['21_1']+(use_tc_re_search === '' ? '':'，'+use_tc_re_search)+'</li>');
			if(!filter_again)	{
				$('#selected-filter-area').html('');
				$('#cbtnarea').hide();
			}
			$('#search-filter-area').html('');
			
			deferred.resolve();
			return;
		}

		use_tc_re_search = get_tc_re_search_str(json);	//判斷一下是不是要簡轉繁
		use_tc_re_search = use_tc_re_search === '' || !is_normal_search ? '':'<div class="alert bg-light" style="margin-bottom:15px;display:table;padding:.75rem 1.75rem;">'+use_tc_re_search+'</div>';
	
		
		//同義詞、異體字的初始loading 效果擺這裡，因為如果照原設計非同步則會出現搜尋的loading+異體字的loading，很奇怪，故等搜尋結束後，異體字的loading再出現（這行放這邊實在是 anti-pattern，但沒辦法只好妥協）
		//if(_GLOBALS.is_variant || _GLOBALS.is_synonym)	$('#variants-suggestion-msg-area > div').html(_GLOBALS[_GLOBALS.lang]['25_32']).parent().show();
			
		var html = process_html(json);
			
			
		
		$('#search-res-info').html('<span class="text-primary font-weight-bold"><i class="fas fa-square"></i> '+_GLOBALS[_GLOBALS.lang]['25_33']+'</span> <button id="search-export-btn" type="button" class="btn btn-light minibtn"><i class="fas fa-file-export"></i> '+_GLOBALS[_GLOBALS.lang]['2_9']+'</button> <b>'+_GLOBALS[_GLOBALS.lang]['25_34']+' '+json.total_term_hits+' '+_GLOBALS[_GLOBALS.lang]['25_35']+' '+json.num_found+' '+_GLOBALS[_GLOBALS.lang]['25_36']+'</b>');
		$('#search-result-area').html(use_tc_re_search+html).attr('total-result-num',parseInt(json.num_found,10));
		$('.header-tabs').show();
		update_badge_counter('badge-counter-texts',json.total_term_hits);
		
		//後分類處理
		if(!notFacet)	{
			var json_facet = json.facet;
			var category = {category:_GLOBALS[_GLOBALS.lang]['25_37'],work:_GLOBALS[_GLOBALS.lang]['25_38'],creator:_GLOBALS[_GLOBALS.lang]['25_39'],dynasty:_GLOBALS[_GLOBALS.lang]['25_40']};
			var html = [];
			for(var index in category)	{
				var items = [];
				var more_btn = (index === 'category' ? '':(json_facet[index].length > 10 ? '<a class="viewmore viewmore-modal-btn" href="javascript:void(null)" onclick="handle_metadata_modal(\''+index+'\',\''+category[index]+'\');">More</a><span class="txtbtn viewmore viewmore-next" style="cursor:pointer;margin-left: 1em;">Next</span>&nbsp;&nbsp;<span class="viewmore-data-area" style="float:right;font-size:small;margin-left:1em"><span role="viewmore-data-now">1</span> / <span role="viewmore-data-all">'+(Math.floor(json_facet[index].length / 10) + (json_facet[index].length % 10 === 0 ? 0:1))+'</span></span><span href="javascript:void(null)" class="txtbtn viewmore viewmore-prev" style="cursor:pointer;display:none">Prev</span>':''));
				if(json_facet[index] && json_facet[index].length > 0)	{
					var facet_item_index = -1;
					for(var r of json_facet[index])	{
						var title , checkbox_value , counts = r.hits;
						if(index === 'category')	{
							title = r.category_name;
							checkbox_value = index+'_'+r.category_name;
						}
						else if(index === 'work')	{
							title = r.work+' '+r.title;	
							checkbox_value = index+'_'+r.work;							
						}
						else if(index === 'dynasty')	{
							title = r.dynasty;	
							checkbox_value = index+'_'+r.dynasty;
						}
						else if(index === 'creator')	{
							title = r.creator_name;
							checkbox_value = index+'_'+r.creator_id;
						}
						if($.trim(title) === '')	continue;
						items.push('<div class="custom-control custom-checkbox mb-1 metadata-items" data-original-order="'+(++facet_item_index)+'"><input type="checkbox" class="custom-control-input left-filters" data-type="'+index+'" id="'+checkbox_value+'" name="'+checkbox_value+'"><label class="custom-control-label" for="'+checkbox_value+'">'+title+' <span class="small text-danger">('+counts+')</span></label></div>');
					}
					var filter_blocks_search_res_div = index === 'work' || index === 'creator' ? '<div id="collapse_filter_search_'+index+'" class="collapse show textsearch-filter-blocks" style="display:none"><div class="card-body"></div></div>':'';
					html.push('<div class="card"><div class="card-header">'+category[index]+' <button type="button" class="btn btn-light minibtn" style="display:none"><i class="fas fa-chart-line"></i></button><a class="card-link float-right" data-toggle="collapse" href="#collapse_'+index+'"><i class="fas fa-caret-down"></i></a><div class="btn-group-toggle" data-toggle="buttons"></div></div><div id="collapse_'+index+'" class="collapse show textsearch-filter-blocks"><div class="card-body">'+items.join('')+more_btn+'</div></div>'+(filter_blocks_search_res_div)+'</div>');
				}
			}
				
			$('#search-filter-area').html(html.join(''));
			
			//後分類超過10筆的藏起來放到「更多」，部類除外
			for(var index in category)	{
				if(index === 'category')	continue;
				$('#collapse_'+index).find('.metadata-items').hide().slice(0,10).show();
			}
		}

			
		deferred.resolve();		
	}).fail(function()	{
		alert('Search fail , please try it again later.')
	});		
	
		//異體字、同義詞搜尋建議(只有單一詞才可用)	
		if((_GLOBALS.is_variant || _GLOBALS.is_synonym) && (is_normal_search || (($('#start-search-btn-adv').is(':visible') && $('#near-adv-search-item-form').is(':visible') && $('#near-adv-search-item-form input[name="adv_search_conds[]"]').get().filter(v => v.value !== '').map(v => v.value).length <= 0) || ($('#start-search-btn-adv').is(':visible') && $('#normal-adv-search-item-form').is(':visible') && $('#normal-adv-search-item-form input[name="adv_search_conds[]"]').get().filter(v => v.value !== '').map(v => v.value).length <= 0))))	{
			
			$('#variants-suggestion-content , #synonym-suggestion-content').html('');
			var xhr_variant_synonym_search = $.ajax({
				url:'getData.php',
				data:'type=variant_synonym_search&variant_active='+_GLOBALS.is_variant+'&synonym_active='+_GLOBALS.is_synonym+'&range='+search_range+'&q='+encodeURIComponent(_GLOBALS.kw),
				type:'POST'
			}).done(function(json)	{
				var json = JSON.parse(json);
				//console.log(json)
				if(json.variant && parseInt(json.variant.num_found,10) > 0)	{
					var variant_html = [];
					for(var o of json.variant.results)	{
						variant_html.push('<li><a href="javascript:void(null)" class="variant-or-synonym-kw variant-word" data-kw="'+o.q+'">'+o.q+'</a><span class="small text-danger" style="margin-left:.5em">('+o.hits+')</span></li>');
					}
					$('#variants-suggestion-content').html(variant_html.join(''));
					$('#variants-block').show();
				}
				else	{
					$('#variants-block').hide();
				}
				
				if(json.synonym && parseInt(json.synonym.num_found,10) > 0)	{
					var synonym_html = [];					
					synonym_html = json.synonym.results.map(sr => '<li><a href="javascript:void(null)" class="variant-or-synonym-kw" data-kw="'+sr+'">'+sr+'</a></li>')
					
					$('#synonym-suggestion-content').html(synonym_html.join(''));
					$('#synonym-block').show();
				}
				else	{
					$('#synonym-block').hide();
				}

				if($('.variant-or-synonym-kw').length <= 0)	{
					$('#variants-suggestion-area').hide();
					$('#variants-suggestion-msg-area').hide();
					//if($('.kwic-cbetaonline-btn').length > 0)	$('#variants-suggestion-msg-area > div').html('Variant or Synonym Character Suggestions are no results.').parent().show();	//(2020-10-19：老闆說不用加上找不到的訊息了) 如果搜尋有結果，再顯示這行異體字找不到訊息，不做此防護的話，一旦關鍵字為空，會出現搜尋+異體字兩條「找不到」的訊息，只留搜尋的就好
				}
				else	{
					$('#variants-suggestion-area').show();
					$('#variants-suggestion-msg-area').hide();
				}
				
			}).fail(function()	{
				var pass = true;
			});	
		}
		else	{
			$('#variants-suggestion-area , #variants-suggestion-msg-area').hide();
		}
}

function search_footnote(notFacet,filter_again)	{
	_GLOBALS.footnoteOffset = 0;	//每次搜尋時重設offset
	_GLOBALS.footnoteSortby = '';	//每次搜尋時重設searchSortby(此變數只有在表格排序事件處理時用到，為了排序後能處理infinite scroll時資料的一致性(沒有一樣的sortby取出順序不同資料會不連續、有誤))
	
	var deferred = $.Deferred();
	
	// _GLOBALS.kw 已於全文檢索裡面的邏輯處理了，全文檢索會先執行，這邊就無須處理

	
	$('#footnote-search-res').html('<div style="padding-top:1.5em;font-size:large;margin-left:30%"><br/><img style="width:90px" src="../images/ajax-loader-type-3.gif"/></div>').attr('full-loaded','no');
	$('#footnote-search-res-info').html('');
	update_badge_counter('badge-counter-footnote',0);

	
	if(!notFacet)	{
		$('#footnote-search-filter-area').html('<div style="margin:auto;padding-top:1.5em;font-size:large;text-align:center"> <br/><img style="width:90px;display:none" src="../images/ajax-loader-type-3.gif"/></div>');
	}
	
	
	
	
	if(parseInt(_GLOBALS.kw.replace(new RegExp('(['+_GLOBALS.punctuations.join('')+'])','gi'),'').length,10) > 85)	{	//校勘夾注搜尋字數長度不得大於85
		$('#footnote-search-res').html('<span style="color:red;margin:2.5em;display:block">'+_GLOBALS[_GLOBALS.lang]['21_2']+'</span>');
		$('#footnote-search-filter-area,#footnote-selected-filter-area').html('');
		$('.header-tabs').show();
		deferred.resolve();		
		return;
	}
	if(parseInt(_GLOBALS.kw.length,10) <= 0)	{
		$('#footnote-search-res').html('<span style="color:red;margin:2.5em;display:block">The keyword is empty.</span>');
		$('#footnote-search-filter-area,#footnote-selected-filter-area').html('');
		$('.header-tabs').show();
		deferred.resolve();				
		return false;
	}
	
	sortby = false;
	if(sortby)	_GLOBALS.footnoteSortby = sortby;
	

	var search_range = _GLOBALS.footnoteSearchRange == '' ? '':_GLOBALS.footnoteSearchRange;	//search_range預設是搜全部(大正藏、卍續藏、嘉興藏)，故參數給空
	var facet = notFacet ? '':'&facet=1';

	var xhr_footnote_search = $.ajax({
		url:'getData.php',
		data:'type=footnote_search'+facet+'&words='+_GLOBALS.kwic_words+'&range='+search_range+'&rows='+_GLOBALS.footnoteScrollLimit+'&offset='+_GLOBALS.footnoteOffset+'&qry='+encodeURIComponent(_GLOBALS.kw)+(sortby ? '&sortby='+encodeURIComponent(sortby):''),
		type:'POST'
	}).done(function(json)	{
		var json = $.parseJSON(json);
		
		if(!json || parseInt(json.num_found,10) <= 0)	{
			$('#footnote-search-res').html('<li style="list-style: none;">「<span style="color:red">'+_GLOBALS.kw+'</span>」'+_GLOBALS[_GLOBALS.lang]['21_1']+'</li>');			
			if(!filter_again)	{
				$('#footnote-selected-filter-area').html('');
				//$('#cbtnarea').hide();
			}
			$('#footnote-search-filter-area').html('');
			
			deferred.resolve();
			return;
		}
			
		var html = process_footnote_html(json);

		$('#footnote-search-res-info').html('<span class="text-primary font-weight-bold"><i class="fas fa-square"></i> '+_GLOBALS[_GLOBALS.lang]['25_41']+'</span> <button id="footnote-export-btn" type="button" class="btn btn-light minibtn"><i class="fas fa-file-export"></i> '+_GLOBALS[_GLOBALS.lang]['2_9']+'</button> '+_GLOBALS[_GLOBALS.lang]['25_34']+' '+json.total_term_hits+' '+_GLOBALS[_GLOBALS.lang]['25_35']+' '+json.num_found+' '+_GLOBALS[_GLOBALS.lang]['25_36']);
		$('#footnote-search-res').html(html).attr('total-result-num',parseInt(json.num_found,10));
		$('.header-tabs').show();
		update_badge_counter('badge-counter-footnote',json.total_term_hits);

		//後分類處理
		if(!notFacet)	{
			var json_facet = json.facet;
			var category = {category:_GLOBALS[_GLOBALS.lang]['25_37'],work:_GLOBALS[_GLOBALS.lang]['25_38'],creator:_GLOBALS[_GLOBALS.lang]['25_39'],dynasty:_GLOBALS[_GLOBALS.lang]['25_40']};
			var html = [];
			for(var index in category)	{
				var items = [];
				var more_btn = (index === 'category' ? '':(json_facet[index].length > 10 ? '<a class="viewmore viewmore-modal-btn" href="javascript:void(null)" onclick="handle_footnote_metadata_modal(\''+index+'\',\''+category[index]+'\');">More</a><span class="txtbtn viewmore viewmore-next" style="cursor:pointer;margin-left: 1em;">Next</span>&nbsp;&nbsp;<span class="viewmore-data-area" style="float:right;font-size:small;margin-left:1em"><span role="viewmore-data-now">1</span> / <span role="viewmore-data-all">'+(Math.floor(json_facet[index].length / 10) + (json_facet[index].length % 10 === 0 ? 0:1))+'</span></span><span href="javascript:void(null)" class="txtbtn viewmore viewmore-prev" style="cursor:pointer;display:none">Prev</span>':''));
				if(json_facet[index] && json_facet[index].length > 0)	{
					var facet_item_index = -1;
					for(var r of json_facet[index])	{
						var title , checkbox_value , counts = r.hits;
						if(index === 'category')	{
							title = r.category_name;
							checkbox_value = 'footnote_'+index+'_'+r.category_name;
						}
						else if(index === 'work')	{
							title = r.work+' '+r.title;	
							checkbox_value = 'footnote_'+index+'_'+r.work;							
						}
						else if(index === 'dynasty')	{
							title = r.dynasty;	
							checkbox_value = 'footnote_'+index+'_'+r.dynasty;
						}
						else if(index === 'creator')	{
							title = r.creator_name;
							checkbox_value = 'footnote_'+index+'_'+r.creator_id;
						}
						if($.trim(title) === '')	continue;
						items.push('<div class="custom-control custom-checkbox mb-1 metadata-items" data-original-order="'+(++facet_item_index)+'"><input type="checkbox" class="custom-control-input footnote-filters" data-type="'+index+'" id="'+checkbox_value+'" name="'+checkbox_value+'"><label class="custom-control-label" for="'+checkbox_value+'">'+title+' <span class="small text-danger">('+counts+')</span></label></div>');
					}
					html.push('<div class="card"><div class="card-header">'+category[index]+' <button type="button" class="btn btn-light minibtn" style="display:none"><i class="fas fa-chart-line"></i></button><a class="card-link float-right" data-toggle="collapse" href="#footnote_collapse_'+index+'"><i class="fas fa-caret-down"></i></a><div class="btn-group-toggle" data-toggle="buttons"></div></div><div id="footnote_collapse_'+index+'" class="collapse show textsearch-filter-blocks"><div class="card-body">'+items.join('')+more_btn+'</div></div></div>');
				}
			}
				
			$('#footnote-filter-area').html(html.join(''));
			
			//後分類超過10筆的藏起來放到「更多」，部類除外
			for(var index in category)	{
				if(index === 'category')	continue;
				$('#footnote_collapse_'+index).find('.metadata-items').hide().slice(0,10).show();
			}
		}
			
		deferred.resolve();		
	}).fail(function()	{
		alert('footnote search fail , please try it again later.')
	});	
}

function search_jinlu()	{
	var deferred = $.Deferred();
	
	// _GLOBALS.kw 已於全文檢索裡面的邏輯處理了，全文檢索會先執行，這邊就無須處理

	
	$('#jinlu-search-res').html('<div style="padding-top:1.5em;font-size:large;text-align:center"><br/><img style="width:90px" src="../images/ajax-loader-type-3.gif"/></div>');
	$('#jinlu-search-res-info').html('');
	$('#jinlu-search-variants-block').hide();
	update_badge_counter('badge-counter-jinlu',0);

	
	if(parseInt(_GLOBALS.kw.length,10) > 40)	{	//經名搜尋字數長度不得大於40
		$('#jinlu-search-res').html('<span style="color:red;margin:2.5em;display:block">'+_GLOBALS[_GLOBALS.lang]['21_2']+'</span>');
		$('.header-tabs').show();
		deferred.resolve();		
		return;
	}
	if(parseInt(_GLOBALS.kw.length,10) <= 0)	{
		$('#jinlu-search-res').html('<span style="color:red;margin:2.5em;display:block">The keyword is empty.</span>');
		$('.header-tabs').show();
		deferred.resolve();				
		return false;
	}
	

	var search_range = _GLOBALS.searchRange == '' ? '':_GLOBALS.searchRange;	//search_range預設是搜全部(大正藏、卍續藏、嘉興藏)，故參數給空

	var xhr_jinlu_search = $.ajax({
		url:'getData.php',
		data:'type=jinlu_search&range='+search_range+'&qry='+encodeURIComponent(_GLOBALS.kw),
		type:'POST'
	}).done(function(json)	{
		var json = $.parseJSON(json);
		
		if(!json || parseInt(json.num_found,10) <= 0)	{
			$('#jinlu-search-res').html('「<span style="color:red">'+_GLOBALS.kw+'</span>」'+_GLOBALS[_GLOBALS.lang]['21_1']+'');
			deferred.resolve();
			return;
		}
		
		var html = [];
		for(var index in json['results'])	{
			var add_jing_range_btn = '<button data-work="'+json['results'][index]['work']+'" class="btn btn-sm btn-info add-jinglu-to-range-btn">'+_GLOBALS[_GLOBALS.lang]['26_64']+'</button>';	//「搜尋本經」按鈕
			add_jing_range_btn = '';	//2021-01-15:搜尋本經按鈕先藏起來
			
			html.push(`<tr><td>${add_jing_range_btn}</td><td>${json['results'][index]['work']}</td><td><a target="_blank" href="${_GLOBALS.cbetaonlineURL+_GLOBALS.lang+'/'+json['results'][index]['work']}">${json['results'][index]['highlight'] || ''}</a></td><td>${json['results'][index]['juan'] || ''}</td><td>${json['results'][index]['creators_with_id'] || ''}</td><td>${json['results'][index]['byline'] || ''}</td></tr>`);
		}
		var jinlu_html_template = `<table class="table table-striped mt-4" id="jinlu-res-table">
							<thead>
								<tr>
									<th></th>
									<th>${_GLOBALS[_GLOBALS.lang]['2_11']}</th>
									<th>${_GLOBALS[_GLOBALS.lang]['1_14']}</th>
									<th>${_GLOBALS[_GLOBALS.lang]['25_42']}</th>
									<th>${_GLOBALS[_GLOBALS.lang]['1_19']}</th>
									<th>${_GLOBALS[_GLOBALS.lang]['1_17']}</th>
								</tr>
							</thead>
							<tbody>${html.join('')}</tbody>
						</table>`;
					

		//$('#jinlu-search-res-info').html('<span class="text-primary font-weight-bold"><i class="fas fa-square"></i> 經名搜尋結果</span> <button id="jinlu-export-btn" type="button" class="btn btn-light minibtn"><i class="fas fa-file-export"></i> 匯出</button> 共 '+json.num_found+' 筆。');
		$('#jinlu-search-res').html(jinlu_html_template);
		$('.header-tabs').show();
		update_badge_counter('badge-counter-jinlu',json.num_found);

			
		deferred.resolve();		
	}).fail(function()	{
		alert('jinlu search fail , please try it again later.')
	});	
	
	
	
	//經名異體字建議	
	if(_GLOBALS.is_variant)	{
			
		$('#jinlu-variants-suggestion-content').html('');
		var xhr_variant_search = $.ajax({
			url:'getData.php',
			data:'type=jinlu_variant_search&variant_active='+_GLOBALS.is_variant+'&q='+encodeURIComponent(_GLOBALS.kw),
			type:'POST'
		}).done(function(json)	{
			var json = JSON.parse(json);
			//console.log(json)
			if(json.variant && parseInt(json.variant.num_found,10) > 0)	{
				var variant_html = [];
				for(var o of json.variant.results)	{
					variant_html.push('<li><a href="javascript:void(null)" class="jinlu-variant-kw" data-kw="'+o.q+'">'+o.q+'</a></li>');
				}
				$('#jinlu-variants-suggestion-content').html(variant_html.join(''));
				$('#jinlu-search-variants-block').show();
			}
			else	{
				$('#jinlu-search-variants-block').hide();
			}
				
		}).fail(function()	{
			var pass = true;
		});	
	}
	else	{
		$('#jinlu-search-variants-block').hide();
	}	
}

function search_similar(notFacet,filter_again)	{
	_GLOBALS.similarOffset = 0;	//每次搜尋時重設offset
	_GLOBALS.similarSortby = '';	//每次搜尋時重設searchSortby(此變數只有在表格排序事件處理時用到，為了排序後能處理infinite scroll時資料的一致性(沒有一樣的sortby取出順序不同資料會不連續、有誤))
	
	var deferred = $.Deferred();
	
	// _GLOBALS.kw 已於全文檢索裡面的邏輯處理了，全文檢索會先執行，這邊就無須處理

	
	$('#similar-search-res').html('<div style="padding-top:1.5em;font-size:large;margin-left:30%"><br/><img style="width:90px" src="../images/ajax-loader-type-3.gif"/></div>').attr('full-loaded','no');
	$('#similar-search-res-info').html('');
	update_badge_counter('badge-counter-similar',0);

	
	if(!notFacet)	{
		$('#similar-search-filter-area').html('<div style="margin:auto;padding-top:1.5em;font-size:large;text-align:center"> <br/><img style="width:90px;display:none" src="../images/ajax-loader-type-3.gif"/></div>');
	}	
	
	
	if(parseInt(_GLOBALS.kw.replace(new RegExp('(['+_GLOBALS.punctuations.join('')+'])','gi'),'').length,10) > 80 || parseInt(_GLOBALS.kw.replace(new RegExp('(['+_GLOBALS.punctuations.join('')+'])','gi'),'').length,10) < 8)	{	//相似句搜尋字數長度不得大於80、最小不得小於8
		$('#similar-search-res').html('<span style="color:red;margin:2.5em;display:block">'+_GLOBALS[_GLOBALS.lang]['26_91']+'</span>');
		$('#similar-search-filter-area,#similar-selected-filter-area').html('');
		$('.header-tabs').show();
		deferred.resolve();		
		return;
	}
	if(parseInt(_GLOBALS.kw.length,10) <= 0)	{
		$('#similar-search-res').html('<span style="color:red;margin:2.5em;display:block">The keyword is empty.</span>');
		$('#similar-search-filter-area,#similar-selected-filter-area').html('');
		$('.header-tabs').show();
		deferred.resolve();				
		return false;
	}
	
	sortby = false;
	if(sortby)	_GLOBALS.similarSortby = sortby;
	

	var search_range = _GLOBALS.similarSearchRange == '' ? '':_GLOBALS.similarSearchRange;	//search_range預設是搜全部(大正藏、卍續藏、嘉興藏)，故參數給空
	var facet = notFacet ? '':'&facet=1';
	
	
	//拉霸元件 +
	_GLOBALS.similarScore['mmin'] = _GLOBALS.kw.replace(new RegExp('(['+_GLOBALS.punctuations.join('')+'])','gi'),'').length * 2 * 0.25;	//拉霸照比例 25%
	_GLOBALS.similarScore['min'] = _GLOBALS.kw.replace(new RegExp('(['+_GLOBALS.punctuations.join('')+'])','gi'),'').length * 2 * 0.33;	//拉霸照比例 33%
	_GLOBALS.similarScore['middle'] = _GLOBALS.kw.replace(new RegExp('(['+_GLOBALS.punctuations.join('')+'])','gi'),'').length * 2 * 0.5;	//拉霸照比例預設值 50%
	_GLOBALS.similarScore['middle2'] = _GLOBALS.kw.replace(new RegExp('(['+_GLOBALS.punctuations.join('')+'])','gi'),'').length * 2 * 0.67;	//拉霸照比例 67%
	_GLOBALS.similarScore['max'] = _GLOBALS.kw.replace(new RegExp('(['+_GLOBALS.punctuations.join('')+'])','gi'),'').length * 2 * 0.75;	//拉霸照比例 75%
	if(_GLOBALS.similarScore['now'] == 0)	_GLOBALS.similarScore['now'] = _GLOBALS.similarScore['middle'];
	
	var score_bar_val = 33;
	if(_GLOBALS.similarScore['mmin'] == _GLOBALS.similarScore['now'])	score_bar_val = 0;
	else if(_GLOBALS.similarScore['min'] == _GLOBALS.similarScore['now'])	score_bar_val = 25;
	else if(_GLOBALS.similarScore['middle'] == _GLOBALS.similarScore['now'])	score_bar_val = 50;
	else if(_GLOBALS.similarScore['middle2'] == _GLOBALS.similarScore['now'])	score_bar_val = 75;
	else if(_GLOBALS.similarScore['max'] == _GLOBALS.similarScore['now'])	score_bar_val = 100;
		
	var similar_score_bar = '<div id="similar-search-score-bar-box">'+_GLOBALS[_GLOBALS.lang]['26_92']+'：<input type="range" min="0" max="100" step="25" value="'+score_bar_val+'" list="similar-search-score-bar-tickmarks" id="similar-search-score-bar" style="width:100%"><datalist id="similar-search-score-bar-tickmarks"><option>25%</option><option>33%</option><option>50%</option><option>67%</option><option>75%</option></datalist>';
	//similar_score_bar = '';	//先藏起	
	//拉霸元件 -	
	

	var xhr_similar_search = $.ajax({
		url:'getData.php',
		data:'type=similar_search'+facet+'&words='+_GLOBALS.kwic_words+'&range='+search_range+'&rows='+_GLOBALS.similarScrollLimit+'&offset='+_GLOBALS.similarOffset+'&qry='+encodeURIComponent(_GLOBALS.kw)+'&score='+_GLOBALS.similarScore['now']+(sortby ? '&sortby='+encodeURIComponent(sortby):''),
		type:'POST'
	}).done(function(json)	{
		var json = $.parseJSON(json);
		
		if(!json || parseInt(json.num_found,10) <= 0)	{
			$('#similar-search-res').html('<li style="list-style: none;">'+_GLOBALS[_GLOBALS.lang]['26_96']+'「<span style="color:red">'+_GLOBALS.kw+'</span>」'+_GLOBALS[_GLOBALS.lang]['26_97']+similar_score_bar+'</li>');			
			if(!filter_again)	{
				$('#similar-selected-filter-area').html('');
				//$('#cbtnarea').hide();
			}
			$('#similar-search-filter-area').html('');
			
			deferred.resolve();
			return;
		}
			
		var html = process_similar_html(json);
		//console.log(html)


		$('#similar-search-res-info').html('<span class="text-primary font-weight-bold"><i class="fas fa-square"></i> '+_GLOBALS[_GLOBALS.lang]['26_89']+'</span> <button id="similar-export-btn" type="button" class="btn btn-light minibtn"><i class="fas fa-file-export"></i> '+_GLOBALS[_GLOBALS.lang]['2_9']+'</button> '+_GLOBALS[_GLOBALS.lang]['25_34']+' '+json.num_found+' '+_GLOBALS[_GLOBALS.lang]['2_4']+similar_score_bar);
		$('#similar-search-res').html(html).attr('total-result-num',parseInt(json.num_found,10));
		$('.header-tabs').show();
		update_badge_counter('badge-counter-similar',json.num_found);

		//後分類處理
		if(!notFacet)	{
			var json_facet = json.facet;
			var category = {category:_GLOBALS[_GLOBALS.lang]['25_37'],work:_GLOBALS[_GLOBALS.lang]['25_38'],creator:_GLOBALS[_GLOBALS.lang]['25_39'],dynasty:_GLOBALS[_GLOBALS.lang]['25_40']};
			var html = [];
			for(var index in category)	{
				var items = [];
				var more_btn = (index === 'category' ? '':(json_facet[index].length > 10 ? '<a class="viewmore viewmore-modal-btn" href="javascript:void(null)" onclick="handle_similar_metadata_modal(\''+index+'\',\''+category[index]+'\');">More</a><span class="txtbtn viewmore viewmore-next" style="cursor:pointer;margin-left: 1em;">Next</span>&nbsp;&nbsp;<span class="viewmore-data-area" style="float:right;font-size:small;margin-left:1em"><span role="viewmore-data-now">1</span> / <span role="viewmore-data-all">'+(Math.floor(json_facet[index].length / 10) + (json_facet[index].length % 10 === 0 ? 0:1))+'</span></span><span href="javascript:void(null)" class="txtbtn viewmore viewmore-prev" style="cursor:pointer;display:none">Prev</span>':''));
				if(json_facet[index] && json_facet[index].length > 0)	{
					var facet_item_index = -1;
					for(var r of json_facet[index])	{
						var title , checkbox_value , counts = r.hits;
						if(index === 'category')	{
							title = r.category_name;
							checkbox_value = 'similar_'+index+'_'+r.category_name;
						}
						else if(index === 'work')	{
							title = r.work+' '+r.title;	
							checkbox_value = 'similar_'+index+'_'+r.work;							
						}
						else if(index === 'dynasty')	{
							title = r.dynasty;	
							checkbox_value = 'similar_'+index+'_'+r.dynasty;
						}
						else if(index === 'creator')	{
							title = r.creator_name;
							checkbox_value = 'similar_'+index+'_'+r.creator_id;
						}
						if($.trim(title) === '')	continue;
						items.push('<div class="custom-control custom-checkbox mb-1 metadata-items" data-original-order="'+(++facet_item_index)+'"><input type="checkbox" class="custom-control-input similar-filters" data-type="'+index+'" id="'+checkbox_value+'" name="'+checkbox_value+'"><label class="custom-control-label" for="'+checkbox_value+'">'+title+' <span class="small text-danger">('+counts+')</span></label></div>');
					}
					html.push('<div class="card"><div class="card-header">'+category[index]+' <button type="button" class="btn btn-light minibtn" style="display:none"><i class="fas fa-chart-line"></i></button><a class="card-link float-right" data-toggle="collapse" href="#similar_collapse_'+index+'"><i class="fas fa-caret-down"></i></a><div class="btn-group-toggle" data-toggle="buttons"></div></div><div id="similar_collapse_'+index+'" class="collapse show textsearch-filter-blocks"><div class="card-body">'+items.join('')+more_btn+'</div></div></div>');
				}
			}
				
			$('#similar-filter-area').html(html.join(''));
			
			//後分類超過10筆的藏起來放到「更多」，部類除外
			for(var index in category)	{
				if(index === 'category')	continue;
				$('#similar_collapse_'+index).find('.metadata-items').hide().slice(0,10).show();
			}
		}
			
		deferred.resolve();		
	}).fail(function()	{
		alert('similar search fail , please try it again later.')
	});	
}

/*
//舊版後分類過濾備份
function filtered_search()	{	//全文檢索後分類過濾
	var range_query = [];
	var queries = {work:[],creator:[],category:[],dynasty:[]};
	var sortby = $('#sort-select').val();

	
	if($('#selected-filter-area .filter-items').length <= 0)	{
		set_search_range(_GLOBALS.originalSearchRange);	//如果都沒勾選，則用記錄下來的_GLOBALS.originalSearchRange 將搜尋範圍復原並重搜
		search(false,false,false);
		$('#select-tag-area').hide();
		return;
	}
		
	var selected_filters = $('#selected-filter-area .filter-items').each(function()	{
		queries[$(this).data('type')].push($(this).attr('id').split('_')[1]);
	});
		
	//console.log(queries)
	
	//** 注意：後分類過濾時，自選的搜尋範圍也要帶進去(如果有選的話)，而自選的搜尋範圍和後分類的搜尋範圍可能會重疊，重疊的部份應該是後分類的範圍要"取代"自選範圍的(因為後分類是再過濾)，如果沒有重疊又有自選的話，則需把自選的帶入
	var user_selected_range_merged = false;	//自選搜尋範圍+後分類搜尋範圍是否已經合併的flag
	for(var i in queries)	{
		var t = (i == 'work' ? 'works':i);
		if(queries[i].length > 0)	{
			
			//如有自選的搜尋範圍是經號或作譯者，因與後分類重疊，故設定 user_selected_range_merged flag為true，表示等等後面不需要再把自選範圍加入，以這邊的後分類為主
			if((_GLOBALS.lastSelectSearchRange.indexOf('works') != -1 && t === 'works') || (_GLOBALS.lastSelectSearchRange.indexOf('creator') != -1 && t === 'creator'))	{
				//queries[i] = [...new Set([...queries[i],..._GLOBALS.lastSelectSearchRange.split(t+':')[1].split(',')])];	//合併陣列並去除重複（兩者合併版）
				user_selected_range_merged = true;
			}
			
			range_query.push(t+':'+queries[i].join(','));
		}
	}
	
	
	//因使用者自選的範圍只會有一種type，假如上面後分類已經出現過（用 user_selected_range_merged flag 判斷 ）就不需要執行此行，反之則是自選的部份還沒處理，而使用者在介面已勾「自訂範圍」、並且已選擇的 _GLOBALS.lastSelectSearchRange 又有值的話，那就直接將使用者自選的範圍 append 到queries
	if(!user_selected_range_merged && $('input[name="change-search-range-radio"]:checked').val() === 'custom' && _GLOBALS.lastSelectSearchRange !== '')	range_query.push(_GLOBALS.lastSelectSearchRange);
	
	
	$('#select-tag-area').show();
	set_search_range(range_query.join(';'));
	search(sortby,false,true);	
}
*/

function filtered_search()	{	
	var range_query = [];
	var queries = {work:[],creator:[],category:[],dynasty:[]};
	var sortby = $('#sort-select').val();
	
	
	if($('#selected-filter-area .filter-items').length <= 0)	{
		set_search_range(_GLOBALS.originalSearchRange);	//如果都沒勾選，則用記錄下來的_GLOBALS.originalSearchRange 將搜尋範圍復原並重搜
		search(false,false,false);
		$('#select-tag-area').hide();
		return;
	}
	
	
	//注意：部類與經典可能有多重 and 的情況，ex：works:a,b+c,d = (a OR b) AND (c OR d)，這邊製作需考慮到，有 and 情況時用+號連接，以 .filter-items-groups 為單位
	$('#selected-filter-area .filter-items-groups:not(:empty)').each(function()	{
		var has_category = false;
		var has_work = false;
		$(this).find('.filter-items').each(function()	{
			if($(this).data('type') === 'category')	has_category = true;
			if($(this).data('type') === 'work')	has_work = true;
			
			queries[$(this).data('type')].push($(this).attr('id').split('_')[1]);
		});
		if(has_category)	queries['category'].push('+');
		if(has_work)		queries['work'].push('+');
	});
	
	for(var i in queries)	{
		if((i == 'work' || i == 'category') && queries[i][queries[i].length-1] == '+')	queries[i].pop();	//如果是category或work，最後一個元素是+的話，pop掉移除之
	}
	
	//console.log(queries)
	
	//** 注意：後分類過濾時，自選的搜尋範圍也要帶進去(如果有選的話)，而自選的搜尋範圍和後分類的搜尋範圍可能會重疊，重疊的部份應該是後分類的範圍要"取代"自選範圍的(因為後分類是再過濾)，如果沒有重疊又有自選的話，則需把自選的帶入
	var user_selected_range_merged = false;	//自選搜尋範圍+後分類搜尋範圍是否已經合併的flag
	for(var i in queries)	{
		var t = (i == 'work' ? 'works':i);
		if(queries[i].length > 0)	{
			
			//如有自選的搜尋範圍是經號或作譯者，因與後分類重疊，故設定 user_selected_range_merged flag為true，表示等等後面不需要再把自選範圍加入，以這邊的後分類為主
			if((_GLOBALS.lastSelectSearchRange.indexOf('works') != -1 && t === 'works') || (_GLOBALS.lastSelectSearchRange.indexOf('creator') != -1 && t === 'creator'))	{
				//queries[i] = [...new Set([...queries[i],..._GLOBALS.lastSelectSearchRange.split(t+':')[1].split(',')])];	//合併陣列並去除重複（兩者合併版）
				user_selected_range_merged = true;
			}
			
			range_query.push(t+':'+queries[i].join(',').replace(/\,?\+\,?/g,'+'));	//移除掉 ,+, 這種，取代為單純的 "+"
		}		
	}
	
	//因使用者自選的範圍只會有一種type，假如上面後分類已經出現過（用 user_selected_range_merged flag 判斷 ）就不需要執行此行，反之則是自選的部份還沒處理，而使用者在介面已勾「自訂範圍」、並且已選擇的 _GLOBALS.lastSelectSearchRange 又有值的話，那就直接將使用者自選的範圍 append 到queries
	if(!user_selected_range_merged && $('input[name="change-search-range-radio"]:checked').val() === 'custom' && _GLOBALS.lastSelectSearchRange !== '')	range_query.push(_GLOBALS.lastSelectSearchRange);	
	
	//console.log(range_query)
	
	$('#select-tag-area').show();
	set_search_range(range_query.join(';').replace(/\+/g,'%2b'));	//將"+"取代為%2b，ray的後端要求
	search(sortby,false,true);	
}

function filtered_footnote_search()	{	//校勘後分類過濾
	var range_query = [];
	var queries = {work:[],creator:[],category:[],dynasty:[]};
	
	/*
	//排序select回到預設的照卷排（回預設）
	$('#sort-select').val('');
	*/
	
	if($('#footnote-selected-filter-area .footnote-filter-items').length <= 0)	{
		set_footnote_search_range(_GLOBALS.originalFootnoteSearchRange);	//如果都沒勾選，則用記錄下來的 _GLOBALS.originalFootnoteSearchRange 將搜尋範圍復原並重搜
		search_footnote(false,false);
		$('#footnote-select-tag-area').hide();
		return;
	}
		
	var selected_filters = $('#footnote-selected-filter-area .footnote-filter-items').each(function()	{
		queries[$(this).data('type')].push($(this).attr('id').split('_')[2]);
	});
		
	//console.log(queries)
	
	//** 注意：後分類過濾時，自選的搜尋範圍也要帶進去(如果有選的話)，而自選的搜尋範圍和後分類的搜尋範圍可能會重疊，重疊的部份應該是後分類的範圍要"取代"自選範圍的(因為後分類是再過濾)，如果沒有重疊又有自選的話，則需把自選的帶入
	var user_selected_range_merged = false;	//自選搜尋範圍+後分類搜尋範圍是否已經合併的flag
	for(var i in queries)	{
		var t = (i == 'work' ? 'works':i);
		if(queries[i].length > 0)	{
			
			//如有自選的搜尋範圍是經號或作譯者，因與後分類重疊，故設定 user_selected_range_merged flag為true，表示等等後面不需要再把自選範圍加入，以這邊的後分類為主
			if((_GLOBALS.lastSelectSearchRange.indexOf('works') != -1 && t === 'works') || (_GLOBALS.lastSelectSearchRange.indexOf('creator') != -1 && t === 'creator'))	{
				//queries[i] = [...new Set([...queries[i],..._GLOBALS.lastSelectSearchRange.split(t+':')[1].split(',')])];	//合併陣列並去除重複（兩者合併版）
				user_selected_range_merged = true;
			}
			
			range_query.push(t+':'+queries[i].join(','));
		}
	}
	
	
	//因使用者自選的範圍只會有一種type，假如上面後分類已經出現過（用 user_selected_range_merged flag 判斷 ）就不需要執行此行，反之則是自選的部份還沒處理，而使用者在介面已勾「自訂範圍」、並且已選擇的 _GLOBALS.lastSelectSearchRange 又有值的話，那就直接將使用者自選的範圍 append 到queries
	if(!user_selected_range_merged && $('input[name="change-search-range-radio"]:checked').val() === 'custom' && _GLOBALS.lastSelectSearchRange !== '')	range_query.push(_GLOBALS.lastSelectSearchRange);
	
	
	$('#footnote-select-tag-area').show();
	set_footnote_search_range(range_query.join(';'));
	search_footnote(false,true);	
}

function filtered_similar_search()	{	//相似句後分類過濾
	var range_query = [];
	var queries = {work:[],creator:[],category:[],dynasty:[]};
	
	/*
	//排序select回到預設的照卷排（回預設）
	$('#sort-select').val('');
	*/
	
	if($('#similar-selected-filter-area .similar-filter-items').length <= 0)	{
		set_similar_search_range(_GLOBALS.originalSimilarSearchRange);	//如果都沒勾選，則用記錄下來的 _GLOBALS.originalSimilarSearchRange 將搜尋範圍復原並重搜
		search_similar(false,false);
		$('#similar-select-tag-area').hide();
		return;
	}
		
	var selected_filters = $('#similar-selected-filter-area .similar-filter-items').each(function()	{
		queries[$(this).data('type')].push($(this).attr('id').split('_')[2]);
	});
		
	//console.log(queries)
	
	//** 注意：後分類過濾時，自選的搜尋範圍也要帶進去(如果有選的話)，而自選的搜尋範圍和後分類的搜尋範圍可能會重疊，重疊的部份應該是後分類的範圍要"取代"自選範圍的(因為後分類是再過濾)，如果沒有重疊又有自選的話，則需把自選的帶入
	var user_selected_range_merged = false;	//自選搜尋範圍+後分類搜尋範圍是否已經合併的flag
	for(var i in queries)	{
		var t = (i == 'work' ? 'works':i);
		if(queries[i].length > 0)	{
			
			//如有自選的搜尋範圍是經號或作譯者，因與後分類重疊，故設定 user_selected_range_merged flag為true，表示等等後面不需要再把自選範圍加入，以這邊的後分類為主
			if((_GLOBALS.lastSelectSearchRange.indexOf('works') != -1 && t === 'works') || (_GLOBALS.lastSelectSearchRange.indexOf('creator') != -1 && t === 'creator'))	{
				//queries[i] = [...new Set([...queries[i],..._GLOBALS.lastSelectSearchRange.split(t+':')[1].split(',')])];	//合併陣列並去除重複（兩者合併版）
				user_selected_range_merged = true;
			}
			
			range_query.push(t+':'+queries[i].join(','));
		}
	}
	
	
	//因使用者自選的範圍只會有一種type，假如上面後分類已經出現過（用 user_selected_range_merged flag 判斷 ）就不需要執行此行，反之則是自選的部份還沒處理，而使用者在介面已勾「自訂範圍」、並且已選擇的 _GLOBALS.lastSelectSearchRange 又有值的話，那就直接將使用者自選的範圍 append 到queries
	if(!user_selected_range_merged && $('input[name="change-search-range-radio"]:checked').val() === 'custom' && _GLOBALS.lastSelectSearchRange !== '')	range_query.push(_GLOBALS.lastSelectSearchRange);
	
	
	$('#similar-select-tag-area').show();
	set_similar_search_range(range_query.join(';'));
	search_similar(false,true);	
}


function process_html(json)	{
	var jing_counter = 0;
	var html = [];
	var kwic_words_average = Math.floor(_GLOBALS.kwic_words / 3);	//kwic要被拆成前、中、後三段，所以除以3
	for(var index in json['results'])	{
		var kwic_counter = 0;
		var kwic_html = [];
		jing_counter++;
		for(var kwic_index in json['results'][index]['kwics']['results'])	{
			kwic_counter++;
			var kwic = json['results'][index]['kwics']['results'][kwic_index]['kwic'];
			/*
			//有前、中、後三段的備份
			var kwic_prefix = /(.+?)<mark>[^<]+<\/mark>(.+)/g.exec(kwic) ? /(.+?)<mark>[^<]+<\/mark>(.+)/g.exec(kwic)[1].substring(0,/(.+?)<mark>[^<]+<\/mark>(.+)/g.exec(kwic)[1].length-kwic_words_average):'';
			var kwic_suffix = /(.+?)<mark>[^<]+<\/mark>(.+)/g.exec(kwic) ? /(.+?)<mark>[^<]+<\/mark>(.+)/g.exec(kwic)[2].substr(kwic_words_average):'';
			var kwic_middle = new RegExp('(.{0,'+kwic_words_average+'}<mark>[^<]+<\/mark>.{0,'+kwic_words_average+'})','g').exec(kwic) ? new RegExp('(.{0,'+kwic_words_average+'}<mark>[^<]+<\/mark>.{0,'+kwic_words_average+'})','g').exec(kwic)[1]:'';
			*/
			var lb = json['results'][index]['kwics']['results'][kwic_index]['lb'];
			var linehead = json['results'][index]['kwics']['results'][kwic_index]['linehead'];
			var ln_prefix = json['results'][index]['file']+(json['results'][index]['work'].substr(-1).match(/[A-Za-z]/g) ? '':'_')+'p';	//經號裡面的底線「_」需看一下work，如果work後面有ABCabc，行號就不能有底線，反之才能有底線
			
			/*
			//有前、中、後三段的備份
			var kwic_html_template = `<li>
						<div class="btn-group close">
							<button type="button" class="btn btn-sm btn-light kwic-cbetaonline-btn" data-ln="${ln_prefix+lb}" data-kw="${_GLOBALS.kw}" data-l="${lb}"><i class="fas fa-external-link-alt"></i></button>
						  <button type="button" class="btn btn-sm btn-light" style="display:none"><i class="fas fa-thumbtack"></i></button>
						</div>
						<div class="pr-5 listtxt" data-target="demo${jing_counter}-${kwic_counter}">
							<span class="demo${jing_counter}-${kwic_counter} kwic-contexts">${kwic_prefix}</span>						
							${kwic_middle}
							<span class="demo${jing_counter}-${kwic_counter} kwic-contexts">${kwic_suffix}</span>
						</div>
						</li>`;
			*/
			var kwic_html_template = `<li>
						<div class="btn-group close">
							<button type="button" class="btn btn-sm btn-light kwic-cbetaonline-btn" data-ln="${linehead}" data-kw="${_GLOBALS.kw}" data-l="${lb}"><i class="fas fa-external-link-alt"></i></button>
						  <button type="button" class="btn btn-sm btn-light" style="display:none"><i class="fas fa-thumbtack"></i></button>
						</div>
						<div class="pr-5 listtxt">
							${kwic}
						</div>
						</li>`;
			
			kwic_html.push(kwic_html_template);
		}
				
		var time_str = (json['results'][index]['time_from'] ? json['results'][index]['time_from']:'') + (json['results'][index]['time_from'] && json['results'][index]['time_to'] ? '~':'') +(json['results'][index]['time_to'] ? json['results'][index]['time_to']:'');
		var html_template = `<li class="list-group-item pb-1 pt-1 pl-0 pr-0">
			  <p class="mb-0 card-cs-title">
			  <span class="search-results-title-juan">${json['results'][index]['work']} ${json['results'][index]['title']}<span class="small">【${_GLOBALS[_GLOBALS.lang]['25_42']}${json['results'][index]['juan']}】</span></span>
				<span class="badge badge-orange">${_GLOBALS[_GLOBALS.lang]['25_34']} ${json['results'][index]['kwics']['num_found']} ${_GLOBALS[_GLOBALS.lang]['12_1_2']}</span>
				<a class="kwic-opner card-link float-right" data-target="demo${jing_counter}" style="cursor:pointer">
				  <i class="fas fa-caret-down"></i>
				</a>
			  </p>				  
			<div class="jing show mb-2" id="demo${jing_counter}" >
				<span class="small jing-category-text">${json['results'][index]['category']}</span><span class="text-secondary small">${json['results'][index]['byline']} ${time_str == '' ? '':'('+_GLOBALS[_GLOBALS.lang]['26_83']+time_str+')'}</span>
				<div class="mt-0">
					<div class="card-body card-padding">
					  <ol class="rltlist">
						${kwic_html.join('')}
					  </ol>
					</div>
				</div>				
			</div>
		</li>`;				
				
		html.push(html_template);
	}	
	return html.join('');
}

function process_footnote_html(json)	{
	var html = [];
	for(var index in json['results'])	{
		var ln = json['results'][index]['file']+(json['results'][index]['work'].substr(-1).match(/[A-Za-z]/g) ? '':'_')+'p'+json['results'][index]['lb'];	//經號裡面的底線「_」需看一下work，如果work後面有ABCabc，行號就不能有底線，反之才能有底線
		var page = /([abcxyz]?[0-9]{3,4})[a-c][0-9]{2}/.exec(json['results'][index]['lb'])[1];
		var note_num = json['results'][index]['note_place'] === 'inline' ? '':json['results'][index]['n'].indexOf('cb_note') != -1 ? 'A'+json['results'][index]['n'].split('_')[2]:json['results'][index]['n'].substr(-3).replace(/0+([^0]+)/g,"$1").replace(/0+/g,'0');	//1.取非0的部份顯示 2.超過一次的0變成一次就好		
		var button_content = json['results'][index]['vol']+', p. '+json['results'][index]['lb']+(json['results'][index]['note_place'] === 'inline' ? '':' ['+note_num+']');
		var note_class = json['results'][index]['note_place'] === 'inline' ? ' note-inline':' note-foot';
		
		var footnote_html_template = `<div class="card p-2 footnote-items${note_class}">
           <div class=" d-flex justify-content-between">
               <div class="mb-0 card-cs-title ml-2">
                   ${json['results'][index]['work']} ${json['results'][index]['title']}<span class="small">【${_GLOBALS[_GLOBALS.lang]['25_42']}${json['results'][index]['juan']}】</span>
               </div>
               <button type="button" class="btn btn-sm btn-link footnote-cbetaonline-btn" data-place="${json['results'][index]['note_place']}" data-ln="${ln}" data-lb="${json['results'][index]['lb']}" data-clue="${json['results'][index]['n']}" data-kw="${_GLOBALS.kw}">${button_content} <i class="fas fa-external-link-alt"></i></button>
           </div>
           <div class="card-body">${json['results'][index]['highlight']}</div>
       </div>`;
		html.push(footnote_html_template);

	}
	return html.join('');
}

function process_similar_html(json)	{
	var html = [];
	for(var index in json['results'])	{
		var ln = json['results'][index]['linehead'];
		var lb = /.+_p(.+)/g.exec(json['results'][index]['linehead']) ? /.+_p(.+)/g.exec(json['results'][index]['linehead'])[1]:ln;
		var title = json['results'][index]['title'];
		var work = json['results'][index]['work'];
		var content = json['results'][index]['content'];
		var kw_no_punc_length = _GLOBALS.kw.replace(new RegExp('(['+_GLOBALS.punctuations.join('')+'])','gi'),'').length;
		/*
		//舊版備份
		var marked_kw = /<mark>(.+?)<\/mark>/u.exec(json['results'][index]['highlight'])[1];	//關鍵字改取 highlight 裡面 <mark> 下面的字串
		marked_kw = $('<div>'+marked_kw+'</div>').text();	//<mark> 底下可能有 tag，去除 <mark> 裡面所有 tag
		*/
		
		
		//關鍵字改取 highlight 裡面 <mark> 下面的字串
		var nn = 0;
		var marked_kw = [];
		var ree = /<mark>(.+?)<\/mark>/g;
		while (lastMatch = ree.exec(json['results'][index]['highlight'])) {
			//console.log(lastMatch);
			//console.log(re.lastIndex);
			marked_kw.push($('<div>'+lastMatch[1]+'</div>').text());	//<mark> 底下可能有 tag，去除 <mark> 裡面所有 tag
			if(!ree.global) break;	// Avoid infinite loop
			if(nn++ > 10)	break;	// Avoid infinite loop
		}
		
		
		var similar_html_template = `<div class="card p-2 similar-items">
           <div class=" d-flex justify-content-between">
               <div class="mb-0 card-cs-title ml-2">
                   ${json['results'][index]['work']} ${json['results'][index]['title']}<span class="small">【${_GLOBALS[_GLOBALS.lang]['25_42']}${json['results'][index]['juan']}】</span><span class="similar-score-percent">${((json['results'][index]['score']/(2*kw_no_punc_length))*100).toFixed()}%</span>
               </div>
               <button type="button" class="btn btn-sm btn-link similar-cbetaonline-btn"  data-ln="${ln}" data-no-kwic="yes" data-kw="${marked_kw.join('@')}">${json['results'][index]['linehead']} <i class="fas fa-external-link-alt"></i></button>
           </div>
           <div class="card-body">${json['results'][index]['highlight']}</div>
       </div>`;
		html.push(similar_html_template);
	}
	return html.join('');
}

function set_search_range(range)	{
	_GLOBALS.searchRange = range;
	$('#export-form input[name="export-range"]').val(range);
}
function set_footnote_search_range(range)	{
	_GLOBALS.footnoteSearchRange = range;
	$('#export-footnote-form input[name="export-footnote-range"]').val(range);	
}
function set_similar_search_range(range)	{
	_GLOBALS.similarSearchRange = range;
	$('#export-similar-form input[name="export-similar-range"]').val(range);	
}

function update_badge_counter(target,counter)	{
	if(target === 'to_zero')	{
		$('.badge-counters').html(0);
		return;
	}
	if(counter > 9999)	counter = '9999+';
	$('#'+target).html(counter);
}

function handle_metadata_modal(target,modal_title)	{	//打開後分類 modal 時的處理(全文檢索)
	if(target === 'dynasty')	{
		$('#metadata-more-modal-by-order-btn').show();
		$('#metadata-more-modal-by-name-btn').hide();
	}
	else	{
		$('#metadata-more-modal-by-order-btn').hide();
		$('#metadata-more-modal-by-name-btn').show();		
	}

	$('#metadata-more-modal .modal-title').html(modal_title+(_GLOBALS.lang === 'en' ? ' Filter':''));
	$('#metadata-modal-search-box').val('');
	$('#metadata-modal-data-container').html('');
	$('#metadata-modal-msg').hide();
	$('#metadata-modal-select-all').prop('checked',false);
	$('#collapse_'+target+' .metadata-items').clone().show().each(function()	{	
			var id = $(this).find(':checkbox').attr('id'); $(this).find(':checkbox').attr('id',id+'_fake');
			var name = $(this).find(':checkbox').attr('name'); $(this).find(':checkbox').attr('name',name+'_fake');
			var label_for = $(this).find('.custom-control-label').attr('for');	$(this).find('.custom-control-label').attr('for',label_for+'_fake');
	}).addClass('metadata-items-in-modal').appendTo('#metadata-modal-data-container');
	
	$('#metadata-more-modal').modal('show');
}

function handle_footnote_metadata_modal(target,modal_title)	{	//打開後分類 modal 時的處理(校勘)
	if(target === 'dynasty')	{
		$('#footnote-metadata-more-modal-by-order-btn').show();
		$('#footnote-metadata-more-modal-by-name-btn').hide();
	}
	else	{
		$('#footnote-metadata-more-modal-by-order-btn').hide();
		$('#footnote-metadata-more-modal-by-name-btn').show();		
	}

	$('#footnote-metadata-more-modal .modal-title').html(modal_title);
	$('#footnote-metadata-modal-search-box').val('');
	$('#footnote-metadata-modal-data-container').html('');
	$('#footnote_collapse_'+target+' .metadata-items').clone().show().each(function()	{	
			var id = $(this).find(':checkbox').attr('id'); $(this).find(':checkbox').attr('id',id+'_fake');
			var name = $(this).find(':checkbox').attr('name'); $(this).find(':checkbox').attr('name',name+'_fake');
			var label_for = $(this).find('.custom-control-label').attr('for');	$(this).find('.custom-control-label').attr('for',label_for+'_fake');
	}).addClass('footnote-metadata-items-in-modal').appendTo('#footnote-metadata-modal-data-container');
	
	$('#footnote-metadata-more-modal').modal('show');
}

function handle_similar_metadata_modal(target,modal_title)	{	//打開後分類 modal 時的處理(相似句)
	if(target === 'dynasty')	{
		$('#similar-metadata-more-modal-by-order-btn').show();
		$('#similar-metadata-more-modal-by-name-btn').hide();
	}
	else	{
		$('#similar-metadata-more-modal-by-order-btn').hide();
		$('#similar-metadata-more-modal-by-name-btn').show();		
	}

	$('#similar-metadata-more-modal .modal-title').html(modal_title);
	$('#similar-metadata-modal-search-box').val('');
	$('#similar-metadata-modal-data-container').html('');
	$('#similar_collapse_'+target+' .metadata-items').clone().show().each(function()	{	
			var id = $(this).find(':checkbox').attr('id'); $(this).find(':checkbox').attr('id',id+'_fake');
			var name = $(this).find(':checkbox').attr('name'); $(this).find(':checkbox').attr('name',name+'_fake');
			var label_for = $(this).find('.custom-control-label').attr('for');	$(this).find('.custom-control-label').attr('for',label_for+'_fake');
	}).addClass('similar-metadata-items-in-modal').appendTo('#similar-metadata-modal-data-container');
	
	$('#similar-metadata-more-modal').modal('show');
}

function filter_metadata_modal_items(target,kw)	{	//輸入關鍵字過濾後分類框裡面的item
	if(!kw || kw === '')	{
		$('#'+target+' .metadata-items').show();
		$('#metadata-modal-msg').hide();
		return;
	}

	$('#'+target+' .metadata-items').hide();
	var res = $('#'+target+' .metadata-items').filter(function()	{
		var label_text = $(this).find('.custom-control-label').text();
		return label_text.match(new RegExp(kw,'g'));		
	}).show();
	
	if(res.length <= 0)	
		$('#metadata-modal-msg').show();
	else
		$('#metadata-modal-msg').hide();


	/*
	var res = $('#collapse_'+target+' .metadata-items').clone().filter(function()	{
		var label_text = $(this).find('.custom-control-label').text();
		return label_text.match(new RegExp(kw,'g'));
	});
	if(res.length > 0)	{
		//因為這個小搜尋是直接過濾後「複製」分類框裡面的項目，所以checkbox的id等資料會一樣，這個each將id、name、for都加一個「_fake」讓它成為唯一，又因為真正搜尋時是用split['_'][1]取中間的值，所以加了「_fake」也無影響
		res.each(function()	{	
			var id = $(this).find(':checkbox').attr('id'); $(this).find(':checkbox').attr('id',id+'_fake');
			var name = $(this).find(':checkbox').attr('name'); $(this).find(':checkbox').attr('name',name+'_fake');
			var label_for = $(this).find('.custom-control-label').attr('for');	$(this).find('.custom-control-label').attr('for',label_for+'_fake');
		});
		
		res.show();	//隱藏項目的全部顯示
		$('#collapse_'+target).hide();
		$('#collapse_filter_search_'+target+' .card-body').html(res);
		$('#collapse_filter_search_'+target).show();
		//console.log(res)
	}
	*/
	
}

function filter_footnote_metadata_modal_items(target,kw)	{	//輸入關鍵字過濾後分類框裡面的item
	if(!kw || kw === '')	{
		$('#'+target+' .metadata-items').show();
		$('#footnote-metadata-modal-msg').hide();
		return;
	}

	$('#'+target+' .metadata-items').hide();
	var res = $('#'+target+' .metadata-items').filter(function()	{
		var label_text = $(this).find('.custom-control-label').text();
		return label_text.match(new RegExp(kw,'g'));		
	}).show();
	
	if(res.length <= 0)	
		$('#footnote-metadata-modal-msg').show();
	else
		$('#footnote-metadata-modal-msg').hide();
}

function filter_similar_metadata_modal_items(target,kw)	{	//輸入關鍵字過濾後分類框裡面的item
	if(!kw || kw === '')	{
		$('#'+target+' .metadata-items').show();
		$('#similar-metadata-modal-msg').hide();
		return;
	}

	$('#'+target+' .metadata-items').hide();
	var res = $('#'+target+' .metadata-items').filter(function()	{
		var label_text = $(this).find('.custom-control-label').text();
		return label_text.match(new RegExp(kw,'g'));		
	}).show();
	
	if(res.length <= 0)	
		$('#similar-metadata-modal-msg').show();
	else
		$('#similar-metadata-modal-msg').hide();
}


//選定後分類後啟動搜尋過濾(全文檢索)
function handle_textsearch_selected_filters(selector)	{
	var filtered_items = $(selector);
	if(filtered_items.length <= 0)	return;
		
	var filter_items_html = [];
	filtered_items.each(function()	{
		filter_items_html.push('<span id="'+$(this).attr('id')+'" class="badge badge-light filter-items" data-type="'+$(this).data('type')+'">'+$(this).next().clone().find('span').remove().end().text()+'  <span class="remove-filter-item-btn" style="color: #85b3da;font-weight: normal;padding-left: 4px;font-size:larger"><span aria-hidden="true">&times;</span></span></span>');
					
	});
	$('#selected-filter-area').append('<span class="filter-items-groups">'+filter_items_html.join('')+'</span>');
			
	filtered_search();		
}

//選定後分類後啟動搜尋過濾(校勘)
function handle_footnote_selected_filters(selector)	{	
	var filtered_items = $(selector);
	if(filtered_items.length <= 0)	return;
		
	var filter_items_html = [];
	filtered_items.each(function()	{
		filter_items_html.push('<span id="'+$(this).attr('id')+'" class="badge badge-light footnote-filter-items" data-type="'+$(this).data('type')+'">'+$(this).next().clone().find('span').remove().end().text()+'  <span class="footnote-remove-filter-item-btn" style="color: #85b3da;font-weight: normal;padding-left: 4px;font-size:larger"><span aria-hidden="true">&times;</span></span></span>');
	});
	$('#footnote-selected-filter-area').append('<span class="footnote-filter-items-groups">'+filter_items_html.join('')+'</span>');
	
	filtered_footnote_search();	
}

//選定後分類後啟動搜尋過濾(相似句)
function handle_similar_selected_filters(selector)	{	
	var filtered_items = $(selector);
	if(filtered_items.length <= 0)	return;
		
	var filter_items_html = [];
	filtered_items.each(function()	{
		filter_items_html.push('<span id="'+$(this).attr('id')+'" class="badge badge-light similar-filter-items" data-type="'+$(this).data('type')+'">'+$(this).next().clone().find('span').remove().end().text()+'  <span class="similar-remove-filter-item-btn" style="color: #85b3da;font-weight: normal;padding-left: 4px;font-size:larger"><span aria-hidden="true">&times;</span></span></span>');
	});
	$('#similar-selected-filter-area').append('<span class="similar-filter-items-groups">'+filter_items_html.join('')+'</span>');
	
	filtered_similar_search();	
}

function sort_modal_filters(target,method)	{	//後分類 modal 排序
	if(!method || $('#'+target+' .metadata-items').length <= 0)	return;
	var items = $('#'+target+' .metadata-items').get();
	if(method === 'name')	{	//依照標題排序
		items.sort(function(a,b)	{
			var a_html = $(a).find('label').clone().find('.small').remove().end().text();
			var b_html = $(b).find('label').clone().find('.small').remove().end().text();
			if ( a_html < b_html ) return -1;
			if ( a_html > b_html ) return 1;
			return 0;			
		});
		
	}
	else if(method === 'count')	{ 	//依照筆數排序
		items.sort(function(a,b)	{
			var a_count = parseInt($(a).find('label .small').text().replace(/[\(\)]/g,''),10);
			var b_count = parseInt($(b).find('label .small').text().replace(/[\(\)]/g,''),10);
			return b_count - a_count;
		});		
	}
	else if(method === 'order')	{	//依照api回來的原始順序排序
		items.sort(function(a,b)	{
			var a = parseInt($(a).data('original-order'),10);
			var b = parseInt($(b).data('original-order'),10);
			return a - b;
		});			
	}
	//console.log(items)
	
	if(target === 'metadata-more-modal')	{
		$('#metadata-modal-search-box').val('');
		$('#metadata-modal-data-container').html(items);
			
	}
	else if(target === 'footnote-metadata-more-modal')	{
		$('#footnote-metadata-modal-search-box').val('');
		$('#footnote-metadata-modal-data-container').html(items);			
	}
	else if(target === 'similar-metadata-more-modal')	{
		$('#similar-metadata-modal-search-box').val('');
		$('#similar-metadata-modal-data-container').html(items);			
	}
}


function change_lang()	{	//更換語系
	$('[data-lang-id]').each(function()	{			
		$(this).text(_GLOBALS[_GLOBALS.lang][$(this).attr('data-lang-id')]);
	});
}

function addUserAction(type,data)	{	//記錄使用者操作(此function需與reader裡面的function相同)
	if(_GLOBALS.userActions.length >= 100)	{	//操作記錄100筆，超過的部份移除
		_GLOBALS.userActions.shift();
	}
	
	//先檢查有沒有重複，有重複的就移到最前面
	var ua_index = -1;
	for(var ua of _GLOBALS.userActions)	{
		ua_index++;
		if(ua.type === 'read' && $.trim(ua.data.title) === $.trim(data.title) && parseInt(ua.data.juan,10) === parseInt(data.juan,10))	{	_GLOBALS.userActions.splice(ua_index,1);	_GLOBALS.userActions.push({type:type,data:data});	localStorage.setItem("cbetaonline-user-actions",JSON.stringify(_GLOBALS.userActions));	return;	}
		else if(ua.type === 'fulltext-search' && ua.data.kw == data.kw || (data.near_words && ua.data.near_words && data.near_words.toString() == ua.data.near_words.toString()))	{	_GLOBALS.userActions.splice(ua_index,1);	_GLOBALS.userActions.push({type:type,data:data});	localStorage.setItem("cbetaonline-user-actions",JSON.stringify(_GLOBALS.userActions));	return;	}
		else if(ua.type === 'search' && ua.data.kw == data.kw)	{	_GLOBALS.userActions.splice(ua_index,1);	_GLOBALS.userActions.push({type:type,data:data});	localStorage.setItem("cbetaonline-user-actions",JSON.stringify(_GLOBALS.userActions));	return;	}
	}
	
	_GLOBALS.userActions.push({type:type,data:data});
	localStorage.setItem("cbetaonline-user-actions",JSON.stringify(_GLOBALS.userActions));	//將actions存到localStorage	
	
}

function add_pjax(config)	{
	var data = {
		kw:config.kw,
		search_range:config.search_range ? config.search_range:'',
		original_search_range:config.original_search_range ? config.original_search_range:'',
		last_select_search_range:config.last_select_search_range ? config.last_select_search_range:'',
		recent_reading:config.recent_reading ? config.recent_reading:'',
		form_type:config.form_type,
		adv_checked_option:config.adv_checked_option,
		near_words:config.near_words
	};
	
	
	var url_exist_params = new URLSearchParams(location.search);
	
	if(!url_exist_params.has('q'))	url_exist_params.set('q',config.kw);	//如果網址沒有q幫它新增q
	if(config.near_words)	url_exist_params.set('near_words',config.near_words);	else	url_exist_params.delete('near_words');	//如果設定值裡面有 near_word，就幫網址新增 near_words，沒有就移除
	
	url_exist_params.forEach((value , key) => {
		if(key === 'q')	{
			url_exist_params.set('q',config.kw);
		}
		else	{
			url_exist_params.set(key,value);
		}
	});
	
	history.pushState(data, "",'?'+url_exist_params.toString()+location.hash);	//於pushState第三個參數修改網址
	
	//換網址時加入ga pageview、搜尋事件追蹤
	if(location.hostname != "cbetaonline-dev.dila.edu.tw")	{
		//GA3 event
		//ga('send', 'pageview', '?'+url_exist_params.toString());
		//ga('send', 'event', 'Full Text Search', 'start search', config.kw);
		
		//GA4 event
		gtag('event', 'Full Text Search', {'event_category': 'Search Keyword','event_label': config.kw,'value': config.kw});
	}
	
}

function get_jing_data(work)	{
	return new Promise((resolve,reject) => {
		$.ajax({
			url:'getData.php',
			data:'type=jing_info&work='+work,
			type:'POST'
		}).done(function(json)	{
			json = JSON.parse(json);
			if(parseInt(json.num_found,10) > 0)	{
				//console.log(json);
				resolve(json);
			}
			else	{
				reject('get_jing_fail');
			}
		}).fail(function()	{
			reject('get_jing_fail');
		});
		
	});
}

function update_custom_range_data()	{	//用從 indexed DB 讀回的 _GLOBALS.customSearchRange 更新搜尋範圍中的「已儲存搜尋範圍」頁面
	var data = _GLOBALS.customSearchRange;
	if(data)	{
		let custom_set_lis = [];
		for(var range_name in data)	{
			custom_set_lis.push('<li data-key="'+range_name+'" data-real-range="'+data[range_name][data[range_name].length-1].real_range+'"><div class="btn-group close"><button type="button" class="btn btn-sm btn-link text-muted custom-range-modify-collection-btn" data-key="'+range_name+'"><i class="fas fa-pen"></i></button><button type="button" class="btn btn-sm btn-link text-muted custom-range-delete-collection-btn" data-key="'+range_name+'"><i class="fas fa-trash-alt"></i></button></div>'+range_name+'</li>');
		}
		$('#JSCS_tab_custom #custom-set').html(custom_set_lis.join(''));
		$('#JSCS_tree_custom #custom-set li:eq(0)').trigger('click');		
	}
}


function saveCustomRangeToTmp()	{
	var exportedData = {id:'',data:{}};
	if(Object.keys(_GLOBALS.customSearchRange).length >= 0)	{
		exportedData.data = _GLOBALS.customSearchRange;
		exportedData.id = 'myCustomRangeData';

		var request = indexedDB.open('storedCustomRange', 2);

		request.onerror = function(event) {
		  // Handle errors.
		  //console.log(event)
		};
		
		request.onupgradeneeded = function(event) {
			var db = event.target.result;
			var objectStore = db.createObjectStore("rangeData", { keyPath: "id" });
		};
		
		request.onsuccess = function(event) {
			var db = event.target.result;
			var gridObjectStore = db.transaction("rangeData", "readwrite").objectStore("rangeData");
			gridObjectStore.count().onsuccess = function(e)	{
				
				if(e.target.result > 0)	{
					gridObjectStore.put(exportedData);
				}
				else	{
					gridObjectStore.add(exportedData);
				}
			}
		};			
	}	
}

function loadCustomRangeFromTmp()	{
	var request = indexedDB.open('storedCustomRange', 2);
	request.onupgradeneeded = function(event) {
		var db = event.target.result;
		var objectStore = db.createObjectStore("rangeData", { keyPath: "id" });
	};
		
	request.onsuccess = function(event) {
		var db = event.target.result;
		try	{
			db.transaction("rangeData","readonly").objectStore("rangeData").get("myCustomRangeData").onsuccess = function(e) {
				if(e.target.result)	{	//將暫存檔讀回來
					//console.log(e.target.result);
					_GLOBALS.customSearchRange = e.target.result.data;
				}
			};
		}
		catch(e)	{
			console.log('get indexed db data error')
		}

	};	
}

function get_adv_keyword_string(target_form)	{
	var adv_conds = $.trim($('#search-text').val());
	$('#'+target_form+' input[name="adv_search_conds[]"]').filter(function() { return $.trim($(this).val()) !== ''}).each(function()	{
		var operator = $(this).parent().find('select').val();
		var adv_kw = $.trim($(this).val());
		adv_conds += operator+adv_kw;	
	});	
	return adv_conds;
}

function get_tc_re_search_str(json)	{
	if(json && json.tc_counts && parseInt(json.total_term_hits,10) < parseInt(json.tc_counts.hits,10))	{	//如果繁體搜尋結果的筆數大於json.num_found（可能這邊對方輸入是簡體），則提示要不要改用繁體搜尋
		return _GLOBALS[_GLOBALS.lang]['24_25']+'<a href="javascript:void(null)" q="'+json.tc_counts.q+'" onclick="$(\'#search-text\').val(this.getAttribute(\'q\'));$(\'#start-search-btn\').trigger(\'click\');">'+json.tc_counts.q+'</a>';
	}
	return '';
}

function handle_exclude_form_item(operator)	{
	if(operator === '-')	{	//exclude 的話，要把其他條件選擇都藏起來，因為 exclude 只能接受兩個關鍵字
		$('#normal-adv-search-item-form > .input-group:not(:eq(0))').hide();
		setTimeout(function()	{
			$('#add-adv-search-cond-btn').hide();
		},100);
	}
	else	{	//否則就都打開
		$('#normal-adv-search-item-form > .input-group:not(:eq(0))').show();
		setTimeout(function()	{
			$('#add-adv-search-cond-btn').show();
		},100);		
	}
}

/*
function handle_url_q(s)	{
	if(s.search(new RegExp('['+regex_quote(EXTENDED_SEARCH_PATTERN.join(''))+']','g')) !== -1)	{	//進階
		//var match = new RegExp('([^'+regex_quote(EXTENDED_SEARCH_PATTERN.join(''))+']+)(['+regex_quote(EXTENDED_SEARCH_PATTERN.join(''))+'])','gm').exec(s);
		//console.log(match)
		console.log(s.split(new RegExp('['+regex_quote(EXTENDED_SEARCH_PATTERN.join(''))+']','g')))
		console.log(s.split(new RegExp('[^'+regex_quote(EXTENDED_SEARCH_PATTERN.join(''))+']','g')).filter(v => v))
	}
	else	{	//一般
		console.log('normal')
	}
}
*/
function regex_quote(s)	{	//escape regex special char
	return (s+'').replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&");
}