function register_events()	{
	//搜尋啟動
	$('#start-search-btn , #start-search-btn-adv').on('click',function()	{
		//下面兩行的初始化是按搜尋按鈕專有，只能放在這，不能放在search，否則排序呼叫search也會讓選單被重設，故下面兩行不應放在search()

		//排序select回到預設的照卷排
		$('#sort-select').val('');
		$('#selected-filter-area , #footnote-filter-area , #similar-filter-area').html('');	//新啟動的搜尋清空之前可能留下的後分類過濾條件
		$('#select-tag-area , #footnote-select-tag-area , #similar-select-tag-area ,.header-tabs').hide();
		
		set_search_range(_GLOBALS.originalSearchRange);	//如原本有設搜尋範圍，因為左邊filter造成原範圍被取代掉，因為此event是按下搜尋按鈕，即全部重搜的意思，所以於此將_GLOBALS.originalSearchRange 原本的搜尋範圍設回，而搜尋範圍僅可在搜尋下拉選單修改，搜尋範圍修改時記錄原搜尋範圍變數 _GLOBALS.originalSearchRange當然也要跟著改，請參考該段程式
		set_footnote_search_range(_GLOBALS.originalFootnoteSearchRange);
		set_similar_search_range(_GLOBALS.originalSimilarSearchRange);
		
		update_badge_counter('to_zero');	//tab menu全部歸零

		
		if($(this).attr('from_popstate') != 'yes')	{	//用按鈕上的 from_popstate attr 判斷是不是從 popstate 來的，是的話就不要再做 pjax，不然 popstate 又 pushstate 會無窮迴圈
			var pjax_kw;
			var near_words = false;
			if($('#start-search-btn-adv').is(':visible') && $('#normal-adv-search-item-form').is(':visible'))	{	//一般進階搜尋
				pjax_kw = get_adv_keyword_string('normal-adv-search-item-form');
			}
			else if($('#start-search-btn-adv').is(':visible') && $('#near-adv-search-item-form').is(':visible'))	{	//Near 搜尋
				pjax_kw = get_adv_keyword_string('near-adv-search-item-form');
				near_words = $('[name="near_adv_search_limit_conds[]"]').get().filter(o => $.trim($(o).next().val()) !== '').map(o => o.value);
			}
			else	{
				pjax_kw = $.trim($('#search-text').val());
			}		
			
			addUserAction('fulltext-search',{kw:pjax_kw,near_words:near_words});	//將使用者搜尋加入操作記錄
		
			add_pjax({	//不是從 poptstate 來的才加入 pushstate
				kw:$.trim(pjax_kw),
				search_range:_GLOBALS.searchRange,
				original_search_range:_GLOBALS.originalSearchRange,
				last_select_search_range:_GLOBALS.lastSelectSearchRange,
				recent_reading:_GLOBALS.recentReading,
				form_type:$('#start-search-btn-adv').is(':visible') ? ($('#normal-adv-search-item-form').is(':visible') ? 'adv':'near'):'normal',
				adv_checked_option:$('input[name="change-search-range-radio"]:checked').val(),
				near_words:near_words
			});
		}
		$(this).removeAttr('from_popstate');	//用完即丟
		
		
		
		search();
		search_footnote();
		search_jinlu();
		search_similar();
		
	}); 
	
	//search-text 按 enter 啟動
	$('#search-text').on('keyup',function(e)	{
		if(e.which === 13)	{
			if($('#start-search-btn-adv').is(':visible'))	{	//進階搜尋
				$('#start-search-btn-adv').trigger('click');
			}
			else	{	//一般搜尋
				$('#start-search-btn').trigger('click');
			}
			
		}
	});

	//檢索之infinite scroll顯示更多結果
	$(document).on('scroll',function(e)	{
		
		//Top按鈕顯示 or 消失
		if(parseInt($(this).scrollTop(),10) > 500)	{
			if($('#tab-texts').is(':visible'))		$('#text-to-top-btn').show();
			if($('#tab-footnote').is(':visible'))	$('#footnote-to-top-btn').show();
			if($('#tab-similar').is(':visible'))	$('#similar-to-top-btn').show();
		}
		else	{
			if($('#tab-texts').is(':visible'))		$('#text-to-top-btn').hide();
			if($('#tab-footnote').is(':visible'))	$('#footnote-to-top-btn').hide();
			if($('#tab-similar').is(':visible'))	$('#similar-to-top-btn').hide();
		}		
		
		//以下是後分類過濾相關程式
		if(parseInt($(window).scrollTop(),10) <= 30)	return false;	//避免當scroll高度不足時啟動此事件，在按下過濾按鈕時畫面清空可能會誤觸造成兩個loading，所以寫此判斷（因為剛過濾時還不需要觸發scroll）
		if(_GLOBALS.infiniteScrollLock)	return false;
		if(!_GLOBALS.kw)	return false;
		
		//console.log($(document).height()+' '+$(window).scrollTop() +' '+$(window).height())
		if($(document).height() - ($(window).scrollTop() + $(window).height()) <= 0+5) {	//scroll到底時
		
			//console.log('scroll to bottom trigger!!')
		
			if($('#tab-texts').is(':visible'))	{	//全文檢索
				_GLOBALS.infiniteScrollLock = true;
				if(parseInt($('#search-result-area .list-group-item').length,10) >= parseInt($('#search-result-area').attr('total-result-num'),10))	{	//如果已讀取的列數>=總筆數(json.num_found)表示已全部讀取，則將search-result-area標上full-loaded attr讓下次不在執行此事件了
					$('#search-result-area').attr('full-loaded','yes');
				}			
			
				if($('#search-result-area').attr('full-loaded') === 'yes')	{
					_GLOBALS.infiniteScrollLock = false;	//這邊重設lock，否則上面 _GLOBALS.infiniteScrollLock 設為 true 後、因為這邊會 return，所以 _GLOBALS.infiniteScrollLock 永遠會是 true，造成下次查詢時 infinitescroll 到底會被一直 lock 無法啟動
					return false;	//有full-loaded attr表示全部資料已讀取完畢，不再做此scroll事件了}	
				}
			
				if($('#infinite_search_msg').length <= 0)	$('#search-result-area').after('<div id="infinite_search_msg" style="margin:0 auto;padding-top:1.5em;font-size:large;text-align:center"> <br/><img style="width:90px" src="../images/ajax-loader-type-3.gif"/></div>');
				
				var kw = _GLOBALS.kw;
				var search_range = _GLOBALS.searchRange == '' ? '':_GLOBALS.searchRange;
				
				_GLOBALS.searchOffset += _GLOBALS.searchScrollLimit;
				
				$.ajax({
					url:'getData.php',
					data:'type=text_search&range='+search_range+'&words='+_GLOBALS.kwic_words+'&near_words='+_GLOBALS.near_words+'&rows='+_GLOBALS.searchScrollLimit+'&offset='+_GLOBALS.searchOffset+'&qry='+encodeURIComponent(_GLOBALS.kw)+(_GLOBALS.searchSortby ? '&sortby='+encodeURIComponent(_GLOBALS.searchSortby):''),	//_GLOBALS.searchSortby 於表格表頭欄位排序事件裡面設定
					type:'POST',
					success:function(json)	{
						_GLOBALS.infiniteScrollLock = false;
						$('#infinite_search_msg').remove();
						var json = $.parseJSON(json);
						if(!json || parseInt(json.num_found,10) <= 0)	{
							return;
						}

						var html = process_html(json);
						
						$('#search-result-area').append(html);	//將結果append至現有的tbody裡面
						
						
						if(parseInt($('#search-result-area .list-group-item').length,10) >= parseInt($('#search-result-area').attr('total-result-num'),10))	{	//如果已讀取的列數>=總筆數(json.num_found)表示已全部讀取，則將search-result-area標上full-loaded attr讓下次不在執行此事件了
							$('#search-result-area').attr('full-loaded','yes');
						}
						
					}
				});
			}
			else if($('#tab-footnote').is(':visible'))	{	//校勘
				_GLOBALS.infiniteScrollLock = true;
				if(parseInt($('#footnote-search-res .footnote-items').length,10) >= parseInt($('#footnote-search-res').attr('total-result-num'),10))	{	//如果已讀取的列數>=總筆數(json.num_found)表示已全部讀取，則將 #footnote-items 標上full-loaded attr讓下次不在執行此事件了
					$('#footnote-search-res').attr('full-loaded','yes');
				}
				
				if($('#footnote-search-res').attr('full-loaded') === 'yes')	{
					_GLOBALS.infiniteScrollLock = false;	//這邊重設lock，否則上面 _GLOBALS.infiniteScrollLock 設為 true 後、因為這邊會 return，所以 _GLOBALS.infiniteScrollLock 永遠會是 true，造成下次查詢時 infinitescroll 到底會被一直 lock 無法啟動
					return false;	//有full-loaded attr表示全部資料已讀取完畢，不再做此scroll事件了
				}
			
				if($('#infinite_search_msg').length <= 0)	$('#footnote-search-res').after('<div id="infinite_search_msg" style="margin:0 auto;padding-top:1.5em;font-size:large;text-align:center"> <br/><img style="width:90px" src="../images/ajax-loader-type-3.gif"/></div>');
				
				var kw = _GLOBALS.kw;
				var search_range = _GLOBALS.footnoteSearchRange == '' ? '':_GLOBALS.footnoteSearchRange;
				
				
				_GLOBALS.footnoteOffset += _GLOBALS.footnoteScrollLimit;
				
				$.ajax({
					url:'getData.php',
					data:'type=footnote_search&range='+search_range+'&words='+_GLOBALS.kwic_words+'&rows='+_GLOBALS.footnoteScrollLimit+'&offset='+_GLOBALS.footnoteOffset+'&qry='+encodeURIComponent(_GLOBALS.kw)+(_GLOBALS.footnoteSortby ? '&sortby='+encodeURIComponent(_GLOBALS.footnoteSortby):''),
					type:'POST',
					success:function(json)	{
						_GLOBALS.infiniteScrollLock = false;
						$('#infinite_search_msg').remove();
						var json = $.parseJSON(json);
						if(!json || parseInt(json.num_found,10) <= 0)	{
							return;
						}

						var html = process_footnote_html(json);
						
						$('#footnote-search-res').append(html);	//將結果append至現有的tbody裡面
						
						
						if(parseInt($('#footnote-search-res .footnote-items').length,10) >= parseInt($('#footnote-search-res').attr('total-result-num'),10))	{	//如果已讀取的列數>=總筆數(json.num_found)表示已全部讀取，則將 #footnote-items 標上full-loaded attr讓下次不在執行此事件了
							$('#footnote-search-res').attr('full-loaded','yes');
						}
						
					}
				});
			}
			/*
			else if($('#tab-similar').is(':visible'))	{	//相似句
				_GLOBALS.infiniteScrollLock = true;
				if(parseInt($('#similar-search-res .similar-items').length,10) >= parseInt($('#similar-search-res').attr('total-result-num'),10))	{	//如果已讀取的列數>=總筆數(json.num_found)表示已全部讀取，則將 #similar-items 標上full-loaded attr讓下次不在執行此事件了
					$('#similar-search-res').attr('full-loaded','yes');
				}
				
				if($('#similar-search-res').attr('full-loaded') === 'yes')	{
					_GLOBALS.infiniteScrollLock = false;	//這邊重設lock，否則上面 _GLOBALS.infiniteScrollLock 設為 true 後、因為這邊會 return，所以 _GLOBALS.infiniteScrollLock 永遠會是 true，造成下次查詢時 infinitescroll 到底會被一直 lock 無法啟動
					return false;	//有full-loaded attr表示全部資料已讀取完畢，不再做此scroll事件了
				}
			
				if($('#infinite_search_msg').length <= 0)	$('#similar-search-res').after('<div id="infinite_search_msg" style="margin:0 auto;padding-top:1.5em;font-size:large;text-align:center"> <br/><img style="width:90px" src="../images/ajax-loader-type-3.gif"/></div>');
				
				var kw = _GLOBALS.kw;
				var search_range = _GLOBALS.similarSearchRange == '' ? '':_GLOBALS.similarSearchRange;
				
				
				_GLOBALS.similarOffset += _GLOBALS.similarScrollLimit;
				
				$.ajax({
					url:'getData.php',
					data:'type=similar_search&range='+search_range+'&words='+_GLOBALS.kwic_words+'&rows='+_GLOBALS.similarScrollLimit+'&offset='+_GLOBALS.similarOffset+'&qry='+encodeURIComponent(_GLOBALS.kw)+(_GLOBALS.similarSortby ? '&sortby='+encodeURIComponent(_GLOBALS.similarSortby):''),
					type:'POST',
					success:function(json)	{
						_GLOBALS.infiniteScrollLock = false;
						$('#infinite_search_msg').remove();
						var json = $.parseJSON(json);
						if(!json || parseInt(json.num_found,10) <= 0)	{
							return;
						}

						var html = process_similar_html(json);
						
						$('#similar-search-res').append(html);	//將結果append至現有的tbody裡面
						
						
						if(parseInt($('#similar-search-res .similar-items').length,10) >= parseInt($('#similar-search-res').attr('total-result-num'),10))	{	//如果已讀取的列數>=總筆數(json.num_found)表示已全部讀取，則將 #similar-items 標上full-loaded attr讓下次不在執行此事件了
							$('#similar-search-res').attr('full-loaded','yes');
						}
						
					}
				});
			}
			*/
		}
		
		//if($(this).scrollTop() <= 0)	alert('top!')	//scroll到頭

	});	
	
	//back to top 按鈕
	$('#text-to-top-btn , #footnote-to-top-btn , #similar-to-top-btn').on('click',function()	{
		document.body.scrollTop = 0; // For Safari
		document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
	});
	
	//每卷結果kwic的收合
	$(document).on('click','.card-link',function()	{
		var target = $(this).attr('data-target');
		var icon = $(this).find('i');
		if(icon.hasClass('fa-caret-down'))	{	//收合
			icon.removeClass('fa-caret-down').addClass('fa-caret-up');
			$('#'+target).hide();
		}
		else	{	//打開
			icon.removeClass('fa-caret-up').addClass('fa-caret-down');
			$('#'+target).show();
		}		
	});
	
	//每個kwic前後文收合
 	$(document).on('click','.listtxt',function()	{
		var target = $(this).attr('data-target');
		$('.'+target).toggle();
	});
	
	//開啟或關閉全部的經
 	$('#open-close-all-btn').on('click',function()	{
		var target = $(this).attr('data-target');
		var icon = $(this).find('i');
		if(icon.hasClass('fa-caret-down'))	{	//收合
			icon.removeClass('fa-caret-down').addClass('fa-caret-up');
			$('.kwic-opner').find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
			$('.'+target).hide();
		}
		else	{	//打開
			icon.removeClass('fa-caret-up').addClass('fa-caret-down');
			$('.kwic-opner').find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
			$('.'+target).show();
		}
		
	});	
	
	//排序選單
	$('#sort-select').on('change',function()	{
		var sortMethod = $(this).val();
		search(sortMethod,true);
	});
	
	//全文檢索匯出按鈕
	$(document).on('click','#search-export-btn',function()	{
		$('#export-form input[name="export-qry"]').val(_GLOBALS.kw);
		$('#export-form').get(0).submit();
	});
	
	//校勘檢索匯出按鈕
	$(document).on('click','#footnote-export-btn',function()	{
		$('#export-footnote-form input[name="export-footnote-qry"]').val(_GLOBALS.kw);
		$('#export-footnote-form').get(0).submit();
	});
	
	//相似句檢索匯出按鈕
	$(document).on('click','#similar-export-btn',function()	{
		$('#export-similar-form input[name="export-similar-qry"]').val(_GLOBALS.kw);
		$('#export-similar-form').get(0).submit();
	});
	
	/*
	//後分類的「更多」按鈕
	$(document).on('click','.viewmore',function()	{				
		$(this).siblings('.metadata-items:hidden').slice(0,10).show();	//一次讀10筆
		if($(this).siblings('.metadata-items:hidden').length <= 0)	{
			$(this).hide();
			return;
		}
	});
	*/
	
	//後分類分頁「next」按鈕
	$(document).on('click','.viewmore-next',function()	{		
		var counts = 10;
		var visible_first = $(this).siblings('.metadata-items:visible:first').index();
		var visible_last = $(this).siblings('.metadata-items:visible:last').index();
		var next_contents = $(this).siblings('.metadata-items').slice(visible_last+1,visible_last+1+counts);
		var now_page = parseInt($(this).siblings('.viewmore-data-area').find('[role="viewmore-data-now"]').text(),10);
		
		//console.log(visible_first+' '+visible_last+' '+next_contents.length)
		
		$(this).siblings('.metadata-items').hide();
		next_contents.show();
		$(this).siblings('.viewmore-data-area').find('[role="viewmore-data-now"]').text(++now_page);
		
		if(now_page >=  parseInt($(this).siblings('.viewmore-data-area').find('[role="viewmore-data-all"]').text(),10))	{	//最後一頁
			$(this).hide();
		}
		$(this).siblings('.viewmore-prev').show();
		
		
	});
	
	//後分類分頁「prev」按鈕
	$(document).on('click','.viewmore-prev',function()	{		
		var counts = 10;
		var visible_first = $(this).siblings('.metadata-items:visible:first').index();
		var visible_last = $(this).siblings('.metadata-items:visible:last').index();
		var prev_contents = $(this).siblings('.metadata-items').slice(visible_first-counts,visible_first);
		var now_page = parseInt($(this).siblings('.viewmore-data-area').find('[role="viewmore-data-now"]').text(),10);
		
		//console.log('prev:'+visible_first+' '+visible_last+' '+prev_contents.length)
		
		$(this).siblings('.viewmore-data-area').find('[role="viewmore-data-now"]').text(--now_page);
		$(this).siblings('.metadata-items').hide();
		prev_contents.show();
		
		if(now_page <= 1)	{	//最後一頁
			$(this).hide();
		}

		$(this).siblings('.viewmore-next').show();

		
	});	
	
	//後分類項目checked
	$(document).on('click','.textsearch-filter-blocks .left-filters',function()	{		
		if($('.textsearch-filter-blocks .left-filters:checked').length > 0)	{
			$('#selected-tag-action-box').show();
		}
		else	{
			$('#selected-tag-action-box').hide();
		}
	});
	
	
	//後分類搜尋
	$('#do-filtered-search-btn').on('click',function()	{
		handle_textsearch_selected_filters('.textsearch-filter-blocks .left-filters:checked');
		$(this).parent().hide();		
	});
	
	//全文檢索 後分類 modal 文字框搜尋過濾
	$('#metadata-modal-search-box').on('keyup',function()	{
		filter_metadata_modal_items('metadata-modal-data-container',$.trim($(this).val()));
	});
	
	//校勘footnote 後分類項目checked
	$(document).on('click','.textsearch-filter-blocks .footnote-filters',function()	{		
		if($('.textsearch-filter-blocks .footnote-filters:checked').length > 0)	{
			$('#footnote-selected-tag-action-box ').show();
		}
		else	{
			$('#footnote-selected-tag-action-box ').hide();
		}
	});
	
	//校勘footnote 後分類搜尋
	$('#do-footnote-filtered-search-btn').on('click',function()	{
		handle_footnote_selected_filters('.textsearch-filter-blocks .footnote-filters:checked');	
		$(this).parent().hide();
	});	
	
	//校勘footnote 後分類 modal 文字框搜尋過濾
	$('#footnote-metadata-modal-search-box').on('keyup',function()	{
		filter_footnote_metadata_modal_items('footnote-metadata-modal-data-container',$.trim($(this).val()));
	});
	
	//相似句similar 後分類項目checked
	$(document).on('click','.textsearch-filter-blocks .similar-filters',function()	{		
		if($('.textsearch-filter-blocks .similar-filters:checked').length > 0)	{
			$('#similar-selected-tag-action-box ').show();
		}
		else	{
			$('#similar-selected-tag-action-box ').hide();
		}
	});
	
	//相似句similar 後分類搜尋
	$('#do-similar-filtered-search-btn').on('click',function()	{
		handle_similar_selected_filters('.textsearch-filter-blocks .similar-filters:checked');	
		$(this).parent().hide();
	});	
	
	//相似句similar 後分類 modal 文字框搜尋過濾
	$('#similar-metadata-modal-search-box').on('keyup',function()	{
		filter_similar_metadata_modal_items('similar-metadata-modal-data-container',$.trim($(this).val()));
	});
	
	
	//移除已選擇後分類的按鈕
	$(document).on('click','.remove-filter-item-btn',function()	{
		$(this).parent().remove();
		filtered_search();
	});
	
	//清除所有後分類按鈕
	$('#clear-all-filter-btn').on('click',function()	{
		var sortby = $('#sort-select').val();
		$('#selected-filter-area').html('');
		set_search_range(_GLOBALS.originalSearchRange);	//用記錄下來的_GLOBALS.originalSearchRange 將搜尋範圍復原並重搜
		search(sortby,false,false);
		$('#select-tag-area').hide();
	});
	
	//校勘footnote 移除已選擇後分類的按鈕
	$(document).on('click','.footnote-remove-filter-item-btn',function()	{
		$(this).parent().remove();
		filtered_footnote_search();
	});
	
	//校勘footnote 清除所有後分類按鈕
	$('#footnote-clear-all-filter-btn').on('click',function()	{
		$('#footnote-selected-filter-area').html('');
		set_footnote_search_range(_GLOBALS.originalFootnoteSearchRange);	//用記錄下來的_GLOBALS.originalFootnoteSearchRange 將搜尋範圍復原並重搜
		search_footnote(false,false);
		$('#footnote-select-tag-area').hide();
	});	

	//相似句similar 移除已選擇後分類的按鈕
	$(document).on('click','.similar-remove-filter-item-btn',function()	{
		$(this).parent().remove();
		filtered_similar_search();
	});
	
	//相似句similar 清除所有後分類按鈕
	$('#similar-clear-all-filter-btn').on('click',function()	{
		$('#similar-selected-filter-area').html('');
		set_similar_search_range(_GLOBALS.originalSimilarSearchRange);	//用記錄下來的_GLOBALS.originalSimilarSearchRange 將搜尋範圍復原並重搜
		search_similar(false,false);
		$('#similar-select-tag-area').hide();
	});		

	//異體字、同義詞 click 執行該字搜尋
	$(document).on('click','.variant-or-synonym-kw , .jinlu-variant-kw',function()	{
		/*
		var kw = $(this).data('kw');
		$('#search-text').val(kw);
		$('#start-search-btn').trigger('click');
		*/
		
		var kw = $(this).data('kw');
		if($(this).hasClass('variant-word'))	{	//異體字點的時候如有勾後分類要保留之，所以用要 filtered_search() 處理
			$('#search-text').val(kw);
			filtered_search();
		}
		else	{	//同義詞就等於用同義詞重新搜尋，故使用 $('#start-search-btn').trigger('click');
			$('#search-text').val(kw);
			$('#start-search-btn').trigger('click');
		}		
		
	});

	
	//全文檢索點選經文標題可回cbetaonline，啟動第一筆kwic的搜尋結果
	$(document).on('click','.search-results-title-juan',function()	{
		var target = $(this).parent().next('.jing').find('.rltlist li:eq(0) .kwic-cbetaonline-btn');	//尋找底下kwic的第一筆
		if(target.length === 1)	target.trigger('click');
	});
	
	
	//經文回cbetaonline按鈕
	$(document).on('click','.kwic-cbetaonline-btn',function()	{
		var ln = $(this).data('ln');
		var kw = $(this).data('kw');
		var l = $(this).data('l');
		window.open(_GLOBALS.cbetaonlineURL+_GLOBALS.lang+'/'+ln+'?q='+kw+'&l='+l+'&near_word='+_GLOBALS.near_words+'&kwic_around='+_GLOBALS.kwic_words);
	});
	
	//校勘回cbetaonline按鈕
	$(document).on('click','.footnote-cbetaonline-btn',function()	{
		var ln = $(this).data('ln');
		var lb = $(this).data('lb');
		var kw = $(this).data('kw');
		var clue = $(this).data('clue');
		var place = $(this).data('place');
		
		if(place && place === 'inline')	{
			window.open(_GLOBALS.cbetaonlineURL+_GLOBALS.lang+'/'+ln+'?q='+kw+'&hl_all=yes&inline_note_l='+lb);
		}
		else	{
			window.open(_GLOBALS.cbetaonlineURL+_GLOBALS.lang+'/'+ln+'?q='+kw+'&note_clue='+clue);
		}
	});
	
	//相似句回cbetaonline按鈕
	$(document).on('click','.similar-cbetaonline-btn',function()	{
		var ln = $(this).data('ln');
		var l = $(this).data('l');
		var kw = $(this).data('kw');
		window.open(_GLOBALS.cbetaonlineURL+_GLOBALS.lang+'/'+ln+'?q='+kw+'&no_kwic=yes&near_word='+_GLOBALS.near_words+'&kwic_around='+_GLOBALS.kwic_words);
	});
	
	//搜尋設定儲存按鈕
	$('#search-config-btn').on('click',function()	{
		_GLOBALS.kwic_words = $("#myRange").val();
		$('#search-config-variant-label').text(_GLOBALS.is_variant ? 'ON':'OFF');
		$('#search-config-synonym-label').text(_GLOBALS.is_synonym ? 'ON':'OFF');
		$('#search-config-kwicWord-label').text(_GLOBALS.kwic_words);
		$('#search_setting').modal('hide');
		
		localStorage.setItem("s-config-is_variant",_GLOBALS.is_variant);
		localStorage.setItem("s-config-is_synonym",_GLOBALS.is_synonym);
		localStorage.setItem("s-config-kwic_words",_GLOBALS.kwic_words);
		localStorage.setItem('skipInlineNoteHL',_GLOBALS.skipInlineNoteHL ? 'yes':'no');
	});
	
	/*
	//更改搜尋範圍選單
	$('#search-range-select-menu > a').on('click',function()	{
		var range = $(this).data('range');
		var range_text = '所有經典';
		if(range === 'all')	{
			set_search_range('');
			_GLOBALS.originalSearchRange = '';
			range_text = '所有經典';
			$('#start-search-btn').trigger('click');
		}
		else if(range === 'recent')	{
			set_search_range('works:T0001');
			_GLOBALS.originalSearchRange = 'works:T0001';
			range_text = '最近閱讀：T0001';
			$('#start-search-btn').trigger('click');
		}
		else if(range === 'custom')	{
			ContextSearchRange.show();
			range_text = '自訂範圍';
		}
		$('#search-range-now [role="range-text"]').html(range_text);
	});
	*/
	
	//更改搜尋範圍 radio button
	$('input[name="change-search-range-radio"]').on('change',function()	{
		var selected_range = $('input[name="change-search-range-radio"]:checked').val();
		if(selected_range === 'all')	{
			set_search_range('');
			set_footnote_search_range('');
			set_similar_search_range('');
			_GLOBALS.originalSearchRange = '';
			_GLOBALS.originalFootnoteSearchRange = '';
			_GLOBALS.originalSimilarSearchRange = '';
		}
		else if(selected_range === 'recent')	{
			set_search_range('works:'+_GLOBALS.recentReading);
			set_footnote_search_range('works:'+_GLOBALS.recentReading);
			set_similar_search_range('works:'+_GLOBALS.recentReading);
			_GLOBALS.originalSearchRange = 'works:'+_GLOBALS.recentReading;
			_GLOBALS.originalFootnoteSearchRange = 'works:'+_GLOBALS.recentReading;
			_GLOBALS.originalSimilarSearchRange = 'works:'+_GLOBALS.recentReading;
		}
		else if(selected_range === 'custom')	{	
			//_GLOBALS.lastSelectSearchRange 為使用者上次自己選的自訂範圍，把它設回去
			set_search_range(_GLOBALS.lastSelectSearchRange);	
			set_footnote_search_range(_GLOBALS.lastSelectSearchRange);
			set_similar_search_range(_GLOBALS.lastSelectSearchRange);
			
			_GLOBALS.originalSearchRange = _GLOBALS.lastSelectSearchRange;			
			_GLOBALS.originalFootnoteSearchRange = _GLOBALS.lastSelectSearchRange;
			_GLOBALS.originalSimilarSearchRange = _GLOBALS.lastSelectSearchRange;
		}
	});
	
	//切換一般搜尋及進階搜尋
	$("#searchbox-left").css("width","100%");
	$("#search-switch").change(function(){
		if($(this).val()=="adv"){	//進階
			$("#searchbox-left").css("width","50%");
			$(".advitem").show();
			$("#start-search-btn").hide();
			$("#topbar").addClass("bluebubble");
			$('#normal-adv-search-item-form').show();
			$('#near-adv-search-item-form').hide();
			$('#clear-adv-search-cond-btn').show();	//進階搜尋時全部清除、增加條件按鈕顯示
			if($('[name="adv_search_opeators[]"]:eq(0)').val() !== '-')	{	//如果有選 exclude、則 #add-adv-search-cond-btn 不該呈現
				$('#add-adv-search-cond-btn').show();
			}
			else	{
				$('#add-adv-search-cond-btn').hide();
			}
		}
		else if($(this).val()=="nearadv")	{
			$("#searchbox-left").css("width","50%");
			$(".advitem").show();
			$("#start-search-btn").hide();
			$("#topbar").addClass("bluebubble");
			$('#normal-adv-search-item-form').hide();
			$('#near-adv-search-item-form').show();
			$('#clear-adv-search-cond-btn , #add-adv-search-cond-btn').hide();	//Near 搜尋時全部清除、增加條件按鈕隱藏
		}
		else{	//一般
			$("#searchbox-left").css("width","100%");
			$(".advitem").hide();
			$("#start-search-btn").show();
			$("#topbar").removeClass("bluebubble");
			$('#change-search-range-radio-1').trigger('click');	//切回一般版時將搜尋範圍切回「搜尋全部」
		}
	});	
	
	
	
	//進階搜尋增加條件
	$('#add-adv-search-cond-btn').on('click',function()	{
		if($('.remove-adv-cond-button').length < 3)	{
			$('#normal-adv-search-item-form').append('<div class="input-group mt-2"><div class="input-group-prepend"><select class="custom-select" name="adv_search_opeators[]"><option value=";">AND</option><option value="|">OR</option value="^"><option>NOT</option></select></div><input type="text" class="form-control form-control" name="adv_search_conds[]"><div class="input-group-append remove-adv-cond-button"> <span class="btn btn btn-danger"><i class="far fa-trash-alt"></i></span></div></div>');
		}
	});
	
	//移除進階搜尋條件
	$(document).on('click','.remove-adv-cond-button', function()	{		
		$(this).parent().remove();
	});	
	
	//進階搜尋運算子選擇
	$(document).on('change','select[name="adv_search_opeators[]"]:visible',function()	{
		var val = $.trim($(this).val());
		handle_exclude_form_item(val);
	});

	//儲存自訂範圍按鈕按下
	$('#save-search-range-btn').on('click',function()	{
		var range_name = $.trim(prompt(_GLOBALS[_GLOBALS.lang]['26_56']));
		if(_GLOBALS.customSearchRange[range_name] && _GLOBALS.customSearchRange[range_name].length > 0)	{	//名稱已有不給存
			alert(_GLOBALS[_GLOBALS.lang]['26_61']);
			return;
		}
		if(range_name)	{
			//console.log(_GLOBALS.searchRange)
			
			var all_range_data = _GLOBALS.searchRange.split(':')[1].split(',');
			
			
			/*
			//用api搭配promise all 取資料版本
			var async_range_data = [];
			for(const work of all_range_data)	async_range_data.push(get_jing_data(work));
			Promise.all(async_range_data).then(values => { 
				var jing_data = [];
				for(let jing of values)	{
					jing = jing.results[0];
					jing_data.push({work:jing.work,title:jing.title,creators:jing.creators});
				}
				jing_data.push({real_range:_GLOBALS.searchRange});	//於資料的最後放入真正的搜尋範圍資料
				_GLOBALS.customSearchRange[range_name] = jing_data;
				saveCustomRangeToTmp();	//回存 indexed DB
				update_custom_range_data();	//更新搜尋範圍中的「已儲存搜尋範圍」頁面
			}).catch(function(reason)	{	//取得經典資訊有誤就不給存
				alert(_GLOBALS[_GLOBALS.lang]['26_60'])
			});	
			*/
			
			//2021-01-04:改用 _GLOBALS.allJing 變數版本，速度非常快也不會有 api 錯誤
			var jing_data = [];
			for(const work of all_range_data)	{
				var finded_jing = $.grep(_GLOBALS.allJing,function(n,i)	{
					return n.key.match(new RegExp(work,'g'));
				});
				
				if(finded_jing.length > 0)	{
					jing_data.push({work:finded_jing[0].key,title:finded_jing[0].title});
				}				
			}
			jing_data.push({real_range:_GLOBALS.searchRange});	//於資料的最後放入真正的搜尋範圍資料
			_GLOBALS.customSearchRange[range_name] = jing_data;
			saveCustomRangeToTmp();	//回存 indexed DB
			update_custom_range_data();	//更新搜尋範圍中的「已儲存搜尋範圍」頁面			
			
			
			$('#selected-search-range-title-text').html(range_name);	//將選擇的標題改成自訂的名稱
			
			//儲存完幫忙開一下 modal 並到已儲存範圍 tab
			ContextSearchRange.show();
			ContextSearchRange.active_tab('JSCS_tab_custom');
			$('#JSCS_tree_custom #custom-set li[data-key="'+range_name+'"]').trigger('click');	//並且啟動這個剛新增的 collection
			
		}
	});
	
	//已儲存搜尋範圍頁籤裡面的範圍標題click
	$(document).on('click','#JSCS_tree_custom #custom-set li',function(e)	{
		var key = $(this).data('key');
		if(key)	{
			var trs = [];
			var datas = _GLOBALS.customSearchRange[key];
			var index = 0;
			for(const data of datas)	{
				if(!data.work)	continue;
				trs.push('<tr><td>'+data.title+'</td><td><a href="javascript:void(null)" class="custom-range-delete-jing-btn" data-key="'+key+'" data-work="'+data.work+'" data-index="'+index+'">'+_GLOBALS[_GLOBALS.lang]['26_62']+'</a></td></tr>');
				index++;
			}
			$('#JSCS_tree_custom #custom-jing tbody').html(trs.join(''));
			
			$('.custom-range-selected-this').removeClass('custom-range-selected-this');
			$(this).addClass('custom-range-selected-this');
			
			$('#JSCS_tree_custom #custom-set li').removeClass('active');
			$(this).addClass('active');
			$('#custom-range-add-jing-container').show();
		}
		e.stopPropagation();
	});
	
	//已儲存搜尋範圍 - 修改 collection 名稱按鈕
	$(document).on('click','.custom-range-modify-collection-btn',function(e)	{
		var now_key = $(this).data('key');
		var new_key = false;
		
		if(now_key && _GLOBALS.customSearchRange[now_key].length > 0)	{
			new_key = $.trim(prompt(_GLOBALS[_GLOBALS.lang]['26_58'], now_key));
			if(new_key)	{
				
				if(_GLOBALS.customSearchRange[new_key] && _GLOBALS.customSearchRange[new_key].length > 0)	{	//名稱已有則不給存
					alert(_GLOBALS[_GLOBALS.lang]['26_61']);
					return;
				}
				
				_GLOBALS.customSearchRange[new_key] = _GLOBALS.customSearchRange[now_key];
				delete _GLOBALS.customSearchRange[now_key];
				update_custom_range_data();
				saveCustomRangeToTmp();	//_GLOBALS.customSearchRange 有更動，回存 indexed DB
			}
		}
		
		e.stopPropagation();
	});

	//已儲存搜尋範圍 - 刪除 collection 按鈕
	$(document).on('click','.custom-range-delete-collection-btn',function(e)	{
		var now_key = $(this).data('key');
		if(confirm(_GLOBALS[_GLOBALS.lang]['26_59']) && _GLOBALS.customSearchRange[now_key].length > 0)	{
			delete _GLOBALS.customSearchRange[now_key];
			update_custom_range_data();
			saveCustomRangeToTmp();	//_GLOBALS.customSearchRange 有更動，回存 indexed DB	
			
			if($('#JSCS_tree_custom #custom-set li').length <= 0)	{
				$('#JSCS_tree_custom #custom-jing tbody').html('<tr><td>無資料 / No data.</td><td></td></tr>');
				$('#custom-range-add-jing-container').hide();	//隱藏新增經名區塊
			}
		}
		e.stopPropagation();
	});
	
	//已儲存搜尋範圍 - 刪除單筆經按鈕
	$(document).on('click','.custom-range-delete-jing-btn',function(e)	{
		var key = $(this).data('key');
		var work = $(this).data('work');
		var index = $(this).data('index');
		if(confirm(_GLOBALS[_GLOBALS.lang]['26_59']) && _GLOBALS.customSearchRange[key].length > 0)	{
			//delete _GLOBALS.customSearchRange[key][index];
			_GLOBALS.customSearchRange[key].splice(index,1);	//用 delete 會在陣列留下 empty slot，改用splice
			var new_real_range = _GLOBALS.customSearchRange[key][_GLOBALS.customSearchRange[key].length-1].real_range.replace(new RegExp('('+work+'\,|\,'+work+')','g'),'');
			_GLOBALS.customSearchRange[key][_GLOBALS.customSearchRange[key].length-1].real_range = new_real_range;
			$('#custom-set li[data-key="'+key+'"]').attr('data-real-range',new_real_range).data('real-range',new_real_range);
			$('#custom-set li[data-key="'+key+'"]').trigger('click');
			
			update_custom_range_data();	//重整表格
			$('#custom-set li[data-key="'+key+'"]').trigger('click');	//回到該筆collection
			saveCustomRangeToTmp();	//_GLOBALS.customSearchRange 有更動，回存 indexed DB			
		}
		e.stopPropagation();
	});		
	
	//已儲存搜尋範圍 - 新增經名自動完成選擇下去
	$(document).on('autocomplete.select','.custom-range-add-jing-autoComplete', function (evt, item) {
		if(!item || !item.value)	return;
		
		var work = item.value;
		var title = item.text;
		var creators = item.creators;
		var key = $('.custom-range-selected-this').data('key');
		
		if(key)	{
			var now_real_range = _GLOBALS.customSearchRange[key].splice(_GLOBALS.customSearchRange[key].length-1,1)[0].real_range;	//先 splice 取出陣列最後一筆的 real_range
			//console.log(key)
			
			if(now_real_range.match(new RegExp(work,'g')))	{
				alert('This jing has exist.');
				_GLOBALS.customSearchRange[key].push({real_range:now_real_range});	//要把上面 splice 出來的push回去
				$('.custom-range-add-jing-autoComplete').autoComplete('clear');	
				return;
			}
						
			_GLOBALS.customSearchRange[key].push({work:work,title:title,creators:creators});
			_GLOBALS.customSearchRange[key].push({real_range:now_real_range += ','+work});
			
			update_custom_range_data();	//重整表格
			$('#custom-set li[data-key="'+key+'"]').trigger('click');	//回到該筆collection
			saveCustomRangeToTmp();	//_GLOBALS.customSearchRange 有更動，回存 indexed DB		

			$('.custom-range-add-jing-autoComplete').autoComplete('clear');	//清除autocomplete
			$('#JSCS_tree_custom').get(0).scrollTop = $('#JSCS_tree_custom').get(0).scrollHeight;	//scroll 到最底部
			$('#custom-jing tr:last td').addClass('hl_row');	//為新加入的一列加上特效提醒user
			setTimeout(function()	{	//特效展示後移除
				$('#custom-jing tr:last td').removeClass('hl_row');
			},1200);
		}
		
		//console.log(item)
	});
	
	
	//經錄搜尋時，「搜尋本經」按鈕
	$(document).on('click','.add-jinglu-to-range-btn',function()	{
		//var now_range = _GLOBALS.lastSelectSearchRange;	//_GLOBALS.searchRange會隨著選擇的範圍（搜尋全部、單經、自訂）更動，故要用不會隨著更動的 _GLOBALS.lastSelectSearchRange
		var work = $(this).data('work');
		var new_range = '';
		
		if(work)	{
			new_range = 'works:'+work;
			$('#selected-range-jing-text').html('(<span id="selected-search-range-title-text">'+_GLOBALS[_GLOBALS.lang]['25_43']+'</span>：<span id="selected-search-range-content-text">'+new_range.split(':')[1]+'</span>)').show();
			
			//重設範圍一定要執行下面這段程式全部，跟 contextSearchRange.js 的 callback 行為必須一致
			set_search_range(new_range);
			set_footnote_search_range(new_range);
			set_similar_search_range(new_range);
			_GLOBALS.originalSearchRange = new_range;
			_GLOBALS.originalFootnoteSearchRange = new_range;
			_GLOBALS.originalSimilarSearchRange = new_range;
			_GLOBALS.lastSelectSearchRange = new_range;
			
			$('#search-switch').val('adv').trigger('change');	//切換到進階表單
			$('input[id="change-search-range-radio-3"]').trigger('click');	//選擇第三個自訂範圍（自訂範圍已設為本經）
			$('#start-search-btn-adv').trigger('click');	//啟動搜尋
			$('#save-search-range-btn').show();
		}
		//$(this).removeClass('btn-info').addClass('btn-light').html(_GLOBALS[_GLOBALS.lang]['26_65']).prop('disabled',true);
	});
	
	//near 搜尋的字數設定，限制為30字，超過30給30
	$('.near-number-fields').on('blur',function()	{
		var v = $.trim($(this).val());
		if(v < 1)	$(this).val(1);
		else if(v > 30)	$(this).val(30);
	});
	
	//相似搜尋 score 拉霸調整時，要重新啟動相似句搜尋
	$(document).on('change','#similar-search-score-bar',function()	{
		var bar_val = $.trim($(this).val());
		
		if(bar_val == 0)	{
			_GLOBALS.similarScore['now'] = _GLOBALS.kw.replace(new RegExp('(['+_GLOBALS.punctuations.join('')+'])','gi'),'').length * 2 * 0.25;	//拉霸照比例 25%
		}
		else if(bar_val == 25)	{
			_GLOBALS.similarScore['now'] = _GLOBALS.kw.replace(new RegExp('(['+_GLOBALS.punctuations.join('')+'])','gi'),'').length * 2 * 0.33;	//拉霸照比例 33%
		}
		else if(bar_val == 50)	{
			_GLOBALS.similarScore['now'] = _GLOBALS.kw.replace(new RegExp('(['+_GLOBALS.punctuations.join('')+'])','gi'),'').length * 2 * 0.5;	//拉霸照比例預設值 50%
		}
		else if(bar_val == 75)	{
			_GLOBALS.similarScore['now'] = _GLOBALS.kw.replace(new RegExp('(['+_GLOBALS.punctuations.join('')+'])','gi'),'').length * 2 * 0.67;	//拉霸照比例 67%
		}
		else if(bar_val == 100)	{
			_GLOBALS.similarScore['now'] = _GLOBALS.kw.replace(new RegExp('(['+_GLOBALS.punctuations.join('')+'])','gi'),'').length * 2 * 0.75;	//拉霸照比例之最大值 75%
		}		
		
		
		search_similar();	//僅啟動相似句搜尋
	});
	
	
	//問題回報-送出按鈕
	$('#feedback_modal_submit').click(function()	{
		var subject = $('#feedback_modal_subject').val();
		var mail = $('#feedback_modal_mail').val();
		var text = $('#feedback_modal_text').val();
		if($.trim(text) == '')	{
			alert(_GLOBALS[_GLOBALS.lang]['21_13']);
			return false;
		}
		
		//用email副本一份給林杯
		$.ajax({
			type: "POST",
			url: "../getData.php",
			data: "type=sendmail&info="+$('#feedback_modal_info').text()+"&text="+encodeURIComponent(text)+"&mail="+encodeURIComponent(mail),
			success: function(res)	{
				$('#feedback_modal_form').hide().parent().find('#feedback_modal_success').show();
			}
		});
	
		//bugify post issue
		var bugify_subject = $.trim(subject) != '' ? $.trim(subject):('bip 問題回報 '+(mail ? ' by '+mail:''));
		var bugify_desc = "<br/><br/> 問題內容：<br/>"+text;
		$.ajax({
			type: "POST",
			url: "../getData.php",
			data: "type=bugify_report&subject="+encodeURIComponent(bugify_subject)+"&description="+encodeURIComponent(bugify_desc),
			success: function(res)	{
				if(res === 'ok')	{
					var bugify_report_successful = true;
				}
			}
		});	
	});	
	
	
	//popstate 事件，實做pjax，啟動時切換經
	$(window).on('popstate',function()	{
		if(history.state && history.state != '')	{
			var state = history.state;
			//console.log(state)
			
			//將關鍵字設回去
			if(state.kw.search(new RegExp('['+regex_quote(EXTENDED_SEARCH_PATTERN.join(''))+']','g')) !== -1)	{	//進階搜尋
				var adv_keywords = state.kw.split(new RegExp('['+regex_quote(EXTENDED_SEARCH_PATTERN.join(''))+']','g'));
				var adv_patterns = state.kw.split(new RegExp('[^'+regex_quote(EXTENDED_SEARCH_PATTERN.join(''))+']','g')).filter(v => v);
				var near_words = $.trim(state.near_words);
				var is_near = state.kw.indexOf('~') !== -1 ? true:false;
				var adv_target_item_form = is_near ? '#near-adv-search-item-form':'#normal-adv-search-item-form';
				
				adv_keywords.forEach((v,i) => {
					if(i === 0)	$('#search-text').val(v);
					else	$(adv_target_item_form+' [name="adv_search_conds[]"]:eq('+(i-1)+')').val(v);
				});
				
				adv_patterns.forEach((v,i) => {
					$(adv_target_item_form+' [name="adv_search_opeators[]"]:eq('+(i)+')').val(v);
					handle_exclude_form_item(v);
				});
				
				if(is_near && near_words !== '')	{	//設定 near_words
					var near_word = near_words.split(',');
					near_word.forEach((v,i) =>	{
						$(adv_target_item_form+' [name="near_adv_search_limit_conds[]"]:eq('+(i)+')').val(v);
					});
				}
				
			}
			else	{	//一般搜尋
				$('#search-text').val(state.kw);
			}			
			
			
			//$('#search-text').val(state.kw);
			//console.log(state.kw)
			
			
			
			//將 range 相關設回去
			set_search_range(state.search_range);
			_GLOBALS.originalSearchRange = state.original_search_range;
			_GLOBALS.lastSelectSearchRange = state.last_select_search_range;
			_GLOBALS.recentReading = state.recent_reading;
				
			//設定「變更範圍」後面的字串
			if(state.search_range)	$('#selected-range-jing-text').html('(<span id="selected-search-range-title-text">'+_GLOBALS[_GLOBALS.lang]['25_43']+'</span>：<span id="selected-search-range-content-text">'+state.search_range+'</span>)');
			
			//設定「本經」字串
			if(_GLOBALS.recentReading)	$('#recent-reading-label').text(_GLOBALS.recentReading);
			
			//用經號取得經名塞回去 #recent-reading-label
			$.ajax({
				url:'getData.php',
				data:'type=jing_info&work='+_GLOBALS.recentReading,
				type:'POST'
			}).done(function(json)	{
				json = JSON.parse(json);
				if(parseInt(json.num_found,10) > 0)	{
					$('#recent-reading-label').text(json.results[0].work+' '+json.results[0].title);
				}
			});	
			
			//還原進階表單搜尋範圍的 radio button 選項，將它 check 回去
			$('input[name="change-search-range-radio"][value="'+state.adv_checked_option+'"]').trigger('click');
			
			//根據 form_type 設定顯示進階或基本表單
			if(state.form_type === 'adv')	{
				$('#search-switch').val('adv').trigger('change');
				$('#start-search-btn-adv').attr('from_popstate','yes').trigger('click');				
			}
			else if(state.form_type === 'near')	{
				$('#search-switch').val('nearadv').trigger('change');
				$('#start-search-btn-adv').attr('from_popstate','yes').trigger('click');					
			}
			else	{
				$('#search-switch').val('normal').trigger('change');
				$('#start-search-btn').attr('from_popstate','yes').trigger('click');				
			}
			
		}
	});	
	
	
	//page visibility api，當本搜尋頁分頁被顯示時，從 localStorage 更新 _GLOBALS.userActions，因為 reader那邊可能有刪除，這邊切過來不更新的話那刪除的資料會在、又切回去reader就會把刪除的資料又存回去，故做此處理
	document.addEventListener('visibilitychange', function()	{
		if(!document.hidden)	{
			_GLOBALS.userActions = (localStorage.getItem("cbetaonline-user-actions") ? JSON.parse(localStorage.getItem("cbetaonline-user-actions")):[]);
		}
	}, false);
	
	
	
	//以下程式為原承恩部份
	  $("#myRange").change(function(){
		$("#textcs").text($("#myRange").val());	
	});


}