var _GLOBALS = {};
_GLOBALS.kw = false;	//目前搜尋的關鍵字
_GLOBALS.searchOffset = 0;				//全文檢索infinite scroll專用：從哪裡開始的offset，會隨著infinite scroll改變，每次搜尋從0開始
_GLOBALS.searchSortby = '';				//全文檢索infinite scroll專用：此變數只有在表格排序事件處理時用到，為了排序後能處理infinite scroll時資料的一致性(沒有一樣的sortby取出順序不同資料會不連續、有誤)
_GLOBALS.searchScrollLimit = 10;		//全文檢索infinite scroll專用：一次scroll讀取幾筆資料
_GLOBALS.footnoteOffset = 0;			//校勘檢索infinite scroll專用：從哪裡開始的offset，會隨著infinite scroll改變，每次搜尋從0開始
_GLOBALS.footnoteSortby = '';			//校勘檢索infinite scroll專用：此變數只有在表格排序事件處理時用到，為了排序後能處理infinite scroll時資料的一致性(沒有一樣的sortby取出順序不同資料會不連續、有誤)
_GLOBALS.footnoteScrollLimit = 10;		//校勘檢索infinite scroll專用：一次scroll讀取幾筆資料
_GLOBALS.similarOffset = 0;				//相似句檢索infinite scroll專用：從哪裡開始的offset，會隨著infinite scroll改變，每次搜尋從0開始
_GLOBALS.similarSortby = '';			//相似句檢索infinite scroll專用：此變數只有在表格排序事件處理時用到，為了排序後能處理infinite scroll時資料的一致性(沒有一樣的sortby取出順序不同資料會不連續、有誤)
_GLOBALS.similarScrollLimit = 10;		//相似句檢索infinite scroll專用：一次scroll讀取幾筆資料
_GLOBALS.searchRange = '';				//記錄選下的搜尋範圍(全文檢索)
_GLOBALS.footnoteSearchRange = '';		//記錄選下的搜尋範圍(校勘)
_GLOBALS.similarSearchRange = '';		//記錄選下的搜尋範圍(相似句)
_GLOBALS.originalSearchRange = '';		//原始的搜尋範圍(全文檢索)
_GLOBALS.originalFootnoteSearchRange = '';//原始的搜尋範圍(校勘)
_GLOBALS.originalSimilarSearchRange = '';//原始的搜尋範圍(相似句)
_GLOBALS.recentReading = '';			//最近閱讀（從網址讀取）
_GLOBALS.lastSelectSearchRange = '';	//因承恩介面設計的關係，需記錄「上一次」選擇的搜尋範圍，跟上面 originalSearchRange 的用途不同			
_GLOBALS.infiniteScrollLock = false;	//搜尋時 infiniteScroll 為確保順序一次只執行一個
_GLOBALS.similarScore = {'mmin':16,'min':16,'middle':16,'middle2':16,'max':16,'now':0};	//相似搜尋的score，可由相似句搜尋頁面的拉霸調整值
_GLOBALS.is_variant = localStorage.getItem("s-config-is_variant") ? eval(localStorage.getItem("s-config-is_variant")):true;	//是否打開異體字搜尋建議(如localStorage就從localStorage載入)
_GLOBALS.is_synonym = localStorage.getItem("s-config-is_synonym") ? eval(localStorage.getItem("s-config-is_synonym")):true;	//是否打開同義詞搜尋建議(如localStorage就從localStorage載入)
_GLOBALS.kwic_words = localStorage.getItem("s-config-kwic_words") ? parseInt(localStorage.getItem("s-config-kwic_words"),10):30;		//kwic 前後顯示幾個字(如localStorage就從localStorage載入)
_GLOBALS.near_words = 5;				//near搜尋的字數(可能多組用逗號隔開，預設先一組)
_GLOBALS.userActions = (localStorage.getItem("cbetaonline-user-actions") ? JSON.parse(localStorage.getItem("cbetaonline-user-actions")):[]);				//記錄使用者的操作行為(在本搜尋介面應只記錄搜尋)，與reader介面共用localStorage
_GLOBALS.skipInlineNoteHL = (localStorage.getItem("skipInlineNoteHL") && localStorage.getItem("skipInlineNoteHL") === 'yes' ? true:false);		//是否跨夾注之選項（跨夾注=true），從 localStorage 取回值
_GLOBALS.customSearchRange = {};
_GLOBALS.allJing = [];
_GLOBALS.lang = 'zh'; 					//語系，預設為中文zh
_GLOBALS.zh = {};						//中文語系物件，資料待後端填入
_GLOBALS.en = {};						//英文語系物件，資料待後端填入
_GLOBALS.cbetaonlineURL = location.protocol+'//'+location.hostname+'/';
_GLOBALS.punctuations = ['\\(','\\)','，','。','！','…','、','「','」','\\.','\\,','》','《','：','）','（','；','？','　','─','︰','〉','〈','』','『','．','－','～','＼','｡','\\?'];

function init()	{

	//檢查網址有無語系設定
	_GLOBALS.lang = new URLSearchParams(location.search).get('lang') ? (new URLSearchParams(location.search).get('lang') == 'en' ? 'en':'zh'):'zh';
	$('body').addClass(_GLOBALS.lang);

	//更換語系
	change_lang();

	//設定上方語系選單
	$('#now-lang-text').html(_GLOBALS[_GLOBALS.lang]['24_23']);	
	//如果為中國版則設定字串為「簡中」
	if(location.hostname == 'cbetaonline.cn' && $('#now-lang-text').text() != "English")	$('#now-lang-text').html('簡中');	
	
	//檢視有無從網址來的關鍵字
	if(new URLSearchParams(location.search).has('q'))	{
		
		_GLOBALS.recentReading = new URLSearchParams(location.search).get('reading') ? $.trim(new URLSearchParams(location.search).get('reading')):'T0001';	//更新「最近閱讀」的經典到全域變數（重要），「最近閱讀」搜尋是以此變數判斷
		if(_GLOBALS.recentReading)	$('#recent-reading-label').text(_GLOBALS.recentReading);
		
		//用經號取得經名塞回去 #recent-reading-label
		$.ajax({
			url:'getData.php',
			data:'type=jing_info&work='+_GLOBALS.recentReading,
			type:'POST'
		}).done(function(json)	{
			json = JSON.parse(json);
			if(parseInt(json.num_found,10) > 0)	{
				$('#recent-reading-label').text(json.results[0].work+' '+json.results[0].title);
			}
		});			
		
		//更新搜尋text
		var q_from_url = $.trim(new URLSearchParams(location.search).get('q'));
		if(q_from_url.search(new RegExp('['+regex_quote(EXTENDED_SEARCH_PATTERN.join(''))+']','g')) !== -1)	{	//進階搜尋(含一般進階搜尋及near搜尋)
			var adv_keywords = q_from_url.split(new RegExp('['+regex_quote(EXTENDED_SEARCH_PATTERN.join(''))+']','g'));
			var adv_patterns = q_from_url.split(new RegExp('[^'+regex_quote(EXTENDED_SEARCH_PATTERN.join(''))+']','g')).filter(v => v);
			var near_words = $.trim(new URLSearchParams(location.search).get('near_words'));
			var is_near = q_from_url.indexOf('~') !== -1 ? true:false;
			var adv_target_item_form = is_near ? '#near-adv-search-item-form':'#normal-adv-search-item-form';
			
			adv_keywords.forEach((v,i) => {
				if(i === 0)	$('#search-text').val(v);
				else	$(adv_target_item_form+' [name="adv_search_conds[]"]:eq('+(i-1)+')').val(v);
			});
			
			adv_patterns.forEach((v,i) => {
				$(adv_target_item_form+' [name="adv_search_opeators[]"]:eq('+(i)+')').val(v);
				handle_exclude_form_item(v);	//如有 exclude 需額外處理表單呈現
			});
			
			
			if(is_near && near_words !== '')	{	//設定 near_words
				var near_word = near_words.split(',');
				near_word.forEach((v,i) =>	{
					$(adv_target_item_form+' [name="near_adv_search_limit_conds[]"]:eq('+(i)+')').val(v);
				});
			}
			
			if(is_near)	$('#search-switch').val('nearadv').trigger('change');//Near搜尋
			else $('#search-switch').val('adv').trigger('change');	//一般進階搜尋
		}
		else	{	//一般搜尋
			$('#search-text').val(q_from_url);
		}
		
		
		//如果有給參數 limitRangeToThisJing，表立即啟動「最近閱讀」搜尋，則需調整表單為「進階搜尋」、並幫它選擇範圍為「最近閱讀」，並啟動進階搜尋按鈕
		if(_GLOBALS.recentReading && new URLSearchParams(location.search).get('limitRangeToThisJing') && new URLSearchParams(location.search).get('limitRangeToThisJing') == 'yes')	{			
			$('#search-switch').val('adv').trigger('change');
			$('input[name="change-search-range-radio"][value="recent"]').trigger('click');
			$('#start-search-btn-adv').trigger('click');
		}
		else if(new URLSearchParams(location.search).get('normalAdvancedSearch') && new URLSearchParams(location.search).get('normalAdvancedSearch') == 'yes')	{	//一般的進階搜尋，直接啟用進階搜尋表單
			$('#search-switch').val('adv').trigger('change');
			
			if(new URLSearchParams(location.search).get('openRangeModal') && new URLSearchParams(location.search).get('openRangeModal') == 'yes')	{	//如果有要開啟範圍選擇器
				$('input[name="change-search-range-radio"][id="change-search-range-radio-3"]').trigger('click');
				setTimeout(function()	{
					ContextSearchRange.show();
					change_lang();					
				},500);
			}
			else	{
				$('#start-search-btn-adv').trigger('click');	//上面的if僅開啟 range modal 不需要啟動搜尋，所以啟動搜尋放這
			}
		}
		else	{	//否則為一般搜尋
			if(q_from_url != '')	{
				$('#start-search-btn').trigger('click');
			}
		}
		
		
		//處理預設要打開的 tab，從 location.hash 取值
		if(location.hash && $.trim(location.hash) !== '')	{
			var active_tab = $.trim(location.hash).replace('#','');
			$('.header-tabs[data-func="'+active_tab+'"] > a').trigger('click');
		}
	}
	else if(new URLSearchParams(location.search).get('openAdvancedSearchForm') && $.trim(new URLSearchParams(location.search).get('openAdvancedSearchForm')) === 'yes')	{	//僅打開進階搜尋表單，不做任何動作
		$('#search-switch').val('adv').trigger('change');
		
		if(new URLSearchParams(location.search).get('openRangeModal') && new URLSearchParams(location.search).get('openRangeModal') == 'yes')	{	//如果有要開啟範圍選擇器
			$('input[name="change-search-range-radio"][id="change-search-range-radio-3"]').trigger('click');
			setTimeout(function()	{
				ContextSearchRange.show();
				change_lang();					
			},500);
		}		
		
	}
	
	//從 indexed DB 讀回已儲存搜尋範圍，並將該物件設給 _GLOBALS.customSearchRange 
	loadCustomRangeFromTmp();
	
	//用上面讀回的 _GLOBALS.customSearchRange 更新搜尋範圍中的「已儲存搜尋範圍」頁面(自訂範圍的樹是非同步，要包一下 settimeout)
	setTimeout(function()	{
		update_custom_range_data();
	},500);

	
	//載入dilaDaLinks.js外掛
	dilaDaLinks.init({containerID:'da-links-area',anchorHTML:'<i class="fas fa-th"></i>',backgroundColor:'white'});
	
	//搜尋設定介面根據值調整
	$("#myRange").val(_GLOBALS.kwic_words);
	$('#textcs').text(_GLOBALS.kwic_words);
	$('#search-config-variant').prop('checked',_GLOBALS.is_variant);
	$('#search-config-synonym').prop('checked',_GLOBALS.is_synonym);
	$('#search-config-skipInlineNoteHL').prop('checked',_GLOBALS.skipInlineNoteHL);
	
	//儲存自訂範圍 - 加入經的 autocomplete
	setTimeout(function()	{
		$('.custom-range-add-jing-autoComplete').autoComplete({
			minLength:0,
			preventEnter:true,
			formatResult: function (item) {
				var qry = $.trim($('.custom-range-add-jing-autoComplete:eq(0)').val());
				return {
					value: item.value,
					text: item.text,
					html: [ 
							item.text.replace(qry,'<span style="color:red">'+qry+'</span>')
						] 
				};
			},
			resolver: 'custom',
			events:{
				search: function (qry, callback) {
					//console.log(qry)
					let res = $.grep(_GLOBALS.allJing,function(n,i)	{
						return n.title.match(new RegExp(qry,'g'));
					}).map(function(n,i)	{
						return {value:n.key,text:n.title}
					});
					//console.log(res)
					callback(res)
				}
			}
		});	
	},500);
	
	//取得所有經的資料並設給 _GLOBALS.allJing，使用朝代範圍的資料並整理過
	$.get('../jing_selector_source/dynasty.html',function(res)	{
		const r = JSON.parse(res)[0].children;
		for(const o of r)	{
			_GLOBALS.allJing.push(...o.children)
		}
		//console.log(_GLOBALS.allJing)
	});
	

	
	//搜尋範圍初始化
	ContextSearchRange.init(
		function(res)	{
			if(res && res !== '')	{	
				set_search_range('works:'+res);
				set_footnote_search_range('works:'+res);
				set_similar_search_range('works:'+res);
				_GLOBALS.originalSearchRange = 'works:'+res;
				_GLOBALS.originalFootnoteSearchRange = 'works:'+res;
				_GLOBALS.originalSimilarSearchRange = 'works:'+res;
				_GLOBALS.lastSelectSearchRange = 'works:'+res;
				$('#start-search-btn').trigger('click');
				$('#selected-range-jing-text').html('(<span id="selected-search-range-title-text">'+_GLOBALS[_GLOBALS.lang]['25_43']+'</span>：<span id="selected-search-range-content-text">'+res+'</span>)').show();
				$('#save-search-range-btn').show();
				//alert('選擇的部類'+res);
			}
			else	{
				_GLOBALS.originalSearchRange = '';
				_GLOBALS.originalFootnoteSearchRange = '';
				_GLOBALS.originalSimilarSearchRange = '';
				_GLOBALS.lastSelectSearchRange = '';
				set_search_range('');
				set_footnote_search_range('');
				set_similar_search_range('');
				$('#selected-range-jing-text').html('').hide();
				$('#save-search-range-btn').hide();
			}
			
		},
		function(res)	{
			if(res && res !== '')	{	
				set_search_range('works:'+res);
				set_footnote_search_range('works:'+res);
				set_similar_search_range('works:'+res);
				_GLOBALS.originalSearchRange = 'works:'+res;
				_GLOBALS.originalFootnoteSearchRange = 'works:'+res;
				_GLOBALS.originalSimilarSearchRange = 'works:'+res;
				_GLOBALS.lastSelectSearchRange = 'works:'+res;
				$('#start-search-btn').trigger('click');
				$('#selected-range-jing-text').html('(<span id="selected-search-range-title-text">'+_GLOBALS[_GLOBALS.lang]['25_43']+'</span>：<span id="selected-search-range-content-text">'+res+'</span>)').show();
				$('#save-search-range-btn').show();
				//alert('選擇的刊本'+res);
			}
			else	{
				_GLOBALS.originalSearchRange = '';
				_GLOBALS.originalFootnoteSearchRange = '';
				_GLOBALS.originalSimilarSearchRange = '';
				_GLOBALS.lastSelectSearchRange = '';
				set_search_range('');
				set_footnote_search_range('');
				set_similar_search_range('');
				$('#selected-range-jing-text').html('').hide();
				$('#save-search-range-btn').hide();
			}
		},
		function(res)	{
			if(res && res !== '')	{
				/*
				//2020-12-14:人名id版備份
				set_search_range('creator:'+res);
				set_footnote_search_range('creator:'+res);
				_GLOBALS.originalSearchRange = 'creator:'+res;
				_GLOBALS.originalFootnoteSearchRange = 'creator:'+res;
				_GLOBALS.lastSelectSearchRange = 'creator:'+res;
				$('#start-search-btn').trigger('click');
				$('#selected-range-jing-text').html('(<span id="selected-search-range-title-text">'+_GLOBALS[_GLOBALS.lang]['25_43']+'</span>：<span id="selected-search-range-content-text">'+res+'</span>)').show();
				$('#save-search-range-btn').show();
				*/
				
				set_search_range('works:'+res);
				set_footnote_search_range('works:'+res);
				set_similar_search_range('works:'+res);
				_GLOBALS.originalSearchRange = 'works:'+res;
				_GLOBALS.originalFootnoteSearchRange = 'works:'+res;
				_GLOBALS.originalSimilarSearchRange = 'works:'+res;
				_GLOBALS.lastSelectSearchRange = 'works:'+res;
				$('#start-search-btn').trigger('click');
				$('#selected-range-jing-text').html('(<span id="selected-search-range-title-text">'+_GLOBALS[_GLOBALS.lang]['25_43']+'</span>：<span id="selected-search-range-content-text">'+res+'</span>)').show();
				$('#save-search-range-btn').show();
			}
			else	{
				_GLOBALS.originalSearchRange = '';
				_GLOBALS.originalFootnoteSearchRange = '';
				_GLOBALS.originalSimilarSearchRange = '';
				_GLOBALS.lastSelectSearchRange = '';
				set_search_range('');
				set_footnote_search_range('');
				set_similar_search_range('');
				$('#selected-range-jing-text').html('').hide();
				$('#save-search-range-btn').hide();
			}
		},
		function(res)	{
			if(res && res !== '')	{
				set_search_range('works:'+res);
				set_footnote_search_range('works:'+res);
				set_similar_search_range('works:'+res);
				_GLOBALS.originalSearchRange = 'works:'+res;
				_GLOBALS.originalFootnoteSearchRange = 'works:'+res;
				_GLOBALS.originalSimilarSearchRange = 'works:'+res;
				_GLOBALS.lastSelectSearchRange = 'works:'+res;
				$('#start-search-btn').trigger('click');
				$('#selected-range-jing-text').html('(<span id="selected-search-range-title-text">'+_GLOBALS[_GLOBALS.lang]['25_43']+'</span>：<span id="selected-search-range-content-text">'+res+'</span>)').show();
				$('#save-search-range-btn').show();
			}
			else	{
				_GLOBALS.originalSearchRange = '';
				_GLOBALS.originalFootnoteSearchRange = '';
				_GLOBALS.originalSimilarSearchRange = '';
				_GLOBALS.lastSelectSearchRange = '';
				set_search_range('');
				set_footnote_search_range('');
				set_similar_search_range('');
				$('#selected-range-jing-text').html('').hide();
				$('#save-search-range-btn').hide();
			}
		},	
		function(res,key)	{
			if(res && res !== '')	{
				set_search_range(res);
				set_footnote_search_range(res);
				set_similar_search_range(res);
				_GLOBALS.originalSearchRange = res;
				_GLOBALS.originalFootnoteSearchRange = res;
				_GLOBALS.originalSimilarSearchRange = res;
				_GLOBALS.lastSelectSearchRange = res;
				$('#start-search-btn').trigger('click');
				$('#selected-range-jing-text').html('(<span id="selected-search-range-title-text">'+(key ? key:_GLOBALS[_GLOBALS.lang]['25_43'])+'</span>：<span id="selected-search-range-content-text">'+res.split(':')[1]+'</span>)').show();
				$('#save-search-range-btn').show();
			}
			else	{
				_GLOBALS.originalSearchRange = '';
				_GLOBALS.originalFootnoteSearchRange = '';
				_GLOBALS.originalSimilarSearchRange = '';
				_GLOBALS.lastSelectSearchRange = '';
				set_search_range('');
				set_footnote_search_range('');
				set_similar_search_range('');
				$('#selected-range-jing-text').html('').hide();
				$('#save-search-range-btn').hide();
			}			
		},		
		function()	{
			return;
		},
		'../jing_selector_source'
	);	
}

//擴充string的escape方法，前端防止xss，using：s.escape()
String.prototype.escape = function() {
	/*
	var tagsToReplace = {
		'&': '&amp;',
		'<': '&lt;',
		'>': '&gt;'
	};
	return this.replace(/[&<>]/g, function(tag) {
		return tagsToReplace[tag] || tag;
	});
	*/
	return this.replace('<script>','').replace('<script','').replace('</script>','').replace('script>','');
};