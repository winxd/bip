<?php
ini_set('display_errors','Off');	//不顯示錯誤訊息於網頁上
ini_set('log_errors','On');			//開啟錯誤紀錄
error_reporting(E_ALL & ~E_NOTICE);	//回報除了E_NOTICE外的所有錯誤

include "include/config.php";

$type = isset($_POST['type']) ? trim($_POST['type']):false;

if($type == 'sendmail')	{	//發出mail
	$text = isset($_POST['text']) && trim($_POST['text']) != '' ? trim($_POST['text']):'';
	$mail = isset($_POST['mail']) && trim($_POST['mail']) != '' ? trim($_POST['mail']):'';
	if($text == '')	{
		echo "text is null";
		exit;
	}	
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
	$headers .= ($mail != '' ? 'From: '.$mail.' <'.$mail.'>':'From: bip錯誤回報 <winxd@dila.edu.tw>') . "\r\n";

	$to = "winxd@dila.edu.tw";
	$subject = "bip feedback";
	$message = ($mail != '' ? "User：".$mail."<br/><br/>":"")."位置：".$_POST['info']."<br/><br/>問題：".$text;
	
	if(!mail($to, $subject, $message, $headers))	{
		echo 'send mail fail';
		exit;
	}
	
	echo "ok";
}
else if($type == 'dict')	{			//字典檢索
	echo file_get_contents('http://glossaries.dila.edu.tw/search.json?type=match&dicts=all&term='.urlencode($_POST['term']));
}
else if($type == 'authority')	{		//規範資料庫
	echo file_get_contents('http://authority.dila.edu.tw/webwidget/getAuthorityData.php?type='.$_POST['sub_type'].'&id='.urlencode($_POST['id']));
}
else if($type == 'dlmbs')	{
	echo file_get_contents('http://buddhism.lib.ntu.edu.tw/DLMBS/jsondata?action=getDLMBS&sort=topic&pagesize=999999&pageno=1&q='.urlencode($_POST['q']));
}
else if($type == 'dlmbs_by_page')	{	//台大dlmbs分頁取得資料
	echo file_get_contents('http://buddhism.lib.ntu.edu.tw/DLMBS/jsondata?action=getDLMBS&sort=topic&pagesize='.$_POST['pagesize'].'&pageno='.$_POST['pageno'].'&q='.urlencode($_POST['q']));	
}
else if($type == 'bugify_report')	{	//bugify 回報
	//google reptcha V3 驗證 +
    $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
    $recaptcha_secret = '6LfZEOopAAAAAK_0ayzOx6eh6rqMGSzZztEPpi6V';
    $recaptcha_response = $_POST['g-recaptcha-response'];

    $response = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
    $responseKeys = json_decode($response, true);

	//print_r($responseKeys);
	//exit;
    if(!$responseKeys["success"] || $responseKeys["score"] < 0.5) {	 //reptcha 驗證失敗，可能為機器人，擋！
		echo "recaptcha fail!";
		exit;
    } 
	//google reptcha V3 驗證 -
	
	$params = array();
	$params['api_key'] = '0821c26a7e95f956de66f0869644b2ef';
	$params['subject'] = trim($_POST['subject']);
	$params['project'] = 1;
	$params['assignee'] = 1;
	$params['description'] = trim($_POST['description']);
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'http://dev.dila.edu.tw/bugify/api/issues.json');
	curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
	$res = curl_exec($ch);
	curl_close($ch);	
	
	
	$res_obj = json_decode($res);
	if($res_obj->message == 'Issue has been created.')	echo 'ok';
	
}
else if($type == 'context_search' || trim($_POST['export']) === 'excel' || trim($_POST['export']) === 'csv')	{	//全文檢索，結果可能回給前端也可能是輸出excel , csv
	/*
	echo 'Dpwnloading';
	 ob_flush();
	sleep(500);
	*/
	/*
	//	俊廷版本備份
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://dev.dila.edu.tw:8080/LuceneSearch/Search01");
	curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);	//*加上curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);之後，他會將curl_exec()獲取的訊息以文件流的形式返回傳給$temp，而不是直接輸出。如果要顯示出畫面的話就加上 echo $temp;就可以了。
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( array( "format"=>"json", "ver"=>"0.1","range"=>$_POST['range'],"qry"=>urlencode($_POST['qry']))));
	$res = curl_exec($ch);
	curl_close($ch);
	
	list($sortby,$sortorder) = trim($_POST['sortby']) != '' ? explode(':',trim($_POST['sortby'])):array('jing','asc');	//預設是照經號、asc 排序
	$json = json_decode($res);	//將搜尋結果轉成物件，然後使用usort排序，再轉回json 字串
	usort($json->data, function($a, $b) { //Sort the array using a user defined function
		global $sortby,$sortorder;
		if($sortby == 'jing')	{
			return $a->collection.$a->vol.$a->sutra_no > $b->collection.$b->vol.$b->sutra_no ? ($sortorder == 'asc' ? 1:-1):($sortorder == 'asc' ? -1:1);
		}
		else if($sortby == 'title')	{
			return $a->title > $b->title ? ($sortorder == 'asc' ? 1:-1):($sortorder == 'asc' ? -1:1);
		}
		else if($sortby == 'author')	{
			return $a->author > $b->author ? ($sortorder == 'asc' ? 1:-1):($sortorder == 'asc' ? -1:1);
		}
	});
	echo json_encode($json);	
	*/
	
	
	//來自get的req，表示要輸出excel or csv，把他們填成post吧...
	if((isset($_POST['export']) && trim($_POST['export']) == 'excel') || (isset($_POST['export']) && trim($_POST['export']) == 'csv'))	{
		if(isset($_POST['rows']))	$_POST['rows'] = trim($_POST['rows']);
		if(isset($_POST['offset']))	$_POST['offset'] = trim($_POST['offset']);
		if(isset($_POST['qry']))		$_POST['qry'] = trim($_POST['qry']);
		if(isset($_POST['range']))	$_POST['range'] = trim($_POST['range']);
		if(isset($_POST['sortby']))	$_POST['sortby'] = trim($_POST['sortby']);
	}
	
	
	//使用curl執行POST
	$params_tc = array();	//底下簡轉繁要用的
	$params = array();
	$params['edition'] = "CBETA";
	$params['rows'] = isset($_POST['rows']) ? trim($_POST['rows']):'100';				//傳回幾筆
	$params['start'] = isset($_POST['offset']) ? trim($_POST['offset']):'0';			//從結果的哪邊開始
	$params['q'] = $_POST['qry'];
	
	if(isset($_POST['range']) && trim($_POST['range']) != '')	{	//處理搜尋範圍參數
		list($range_type,$filter_range) = explode(':',trim($_POST['range']));
		$params[trim($range_type)] = trim($filter_range);
		$params_tc[trim($range_type)] = trim($filter_range);	//順便複製一份搜尋範圍給簡轉繁要用params_tc
	}
	
	//API的搜尋排序
	list($sortFileld,$sortMethod) = explode(':',trim($_POST['sortby']));
	$sortMethod = $sortMethod == 'asc' ? '+':'-';
	
	if($sortFileld == 'jing')			$params['order'] = 'canon'.$sortMethod;
	else if($sortFileld == 'title')		$params['order'] = 'title'.$sortMethod;
	else if($sortFileld == 'author')	$params['order'] = 'byline'.$sortMethod;
	else if($sortFileld == 'juan')		$params['order'] = 'juan'.$sortMethod;
	else if($sortFileld == 'term_hits')	$params['order'] = 'term_hits'.$sortMethod;
	else if($sortFileld == 'time')		$params['order'] = "time_from{$sortMethod},time_to{$sortMethod},canon{$sortMethod}";
	//else								$params['order'] = 'time_from,time_to,canon';	//預設照起始年、結束年、經號 依序排序
	else								$params['order'] = 'canon';	//2018-07-26: 改回照經號排就好

	
	//進階搜尋處理 - 檢視搜尋字串中是否有進些搜尋之pattern
	$extened_search = false;
	$extended_pattern_str = implode('',$EXTENDED_SEARCH_PATTERN);
	if(preg_match_all('/([^'.preg_quote($extended_pattern_str).']+)(['.preg_quote($extended_pattern_str).']?)/u',trim($_POST['qry']),$matches) && preg_match('/['.preg_quote($extended_pattern_str).']/u',trim($_POST['qry'])))	{
		//print_r($matches);
		$extened_search = true;
		$query_str = array();
		$near_flag = false;
		if(count($matches) > 1)	{
			for($i = 0;$i < count($matches[1]); $i++)	{
				if(trim($matches[2][$i]) == '~')	{
					$near_flag = true;
					$query_str[] = trim($matches[1][$i]).' ';
				}
				else	{
					if($near_flag)	{
						$query_str[] = trim($matches[1][$i]);
					}
					else	{
						if(trim($matches[2][$i]) == '&')	$matches[2][$i] = '';
						$query_str[] = ' "'.trim($matches[1][$i]).'" '.trim($matches[2][$i]);
					}
				}
			}	
		}
		//print_r($query_str);
		$params['q'] = $near_flag ? '"'.implode('',$query_str).'"~10' : implode('',$query_str);
		//echo $params['q']; 
	}
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $CBDATA_API."search/".($extened_search ? 'extended':''));
	curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);	//*加上curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);之後，他會將curl_exec()獲取的訊息以文件流的形式返回傳給$temp，而不是直接輸出。如果要顯示出畫面的話就加上 echo $temp;就可以了。
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
	$res = curl_exec($ch);
	curl_close($ch);	
	
	//echo $res;	//搜尋結果
	
	$json = json_decode($res);	//將搜尋結果轉成$json物件
	
	
	//將搜尋字串轉繁體，看一下筆數有多少，將此結果塞入回傳(需使用post，user可能會用搜尋範圍，因此需將搜尋範圍參數傳過去，筆數的計算與前端的字數compare才會正確)
	$params_tc['q'] = ($_POST['qry']);
	$ch_tc = curl_init();
	curl_setopt($ch_tc, CURLOPT_URL, $CBDATA_API.'search/sc');
	curl_setopt($ch_tc, CURLOPT_POST, true); // 啟用POST
	curl_setopt($ch_tc, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch_tc, CURLOPT_POSTFIELDS, http_build_query($params_tc));
	$res_tc = curl_exec($ch_tc);
	curl_close($ch_tc);	
	//echo $res_tc;	//搜尋結果	
	$tc_counts = json_decode($res_tc);	//將簡轉繁結果塞回結果json
	$json->tc_counts = $tc_counts;
	

	/* 
	//以下為我自己做的資料排序之備份（將搜尋結果轉成$json物件，然後使用usort排序，再轉回json 字串）
	list($sortby,$sortorder) = trim($_POST['sortby']) != '' ? explode(':',trim($_POST['sortby'])):array('jing','asc');	//預設是照經號、asc 排序
	usort($json->results, function($a, $b) { //Sort the array using a user defined function
		global $sortby,$sortorder;
		if($sortby == 'jing')	{
			return $a->file > $b->file ? ($sortorder == 'asc' ? 1:-1):($sortorder == 'asc' ? -1:1);
		}
		else if($sortby == 'title')	{
			return $a->title > $b->title ? ($sortorder == 'asc' ? 1:-1):($sortorder == 'asc' ? -1:1);
		}
		else if($sortby == 'author')	{
			return $a->byline > $b->byline ? ($sortorder == 'asc' ? 1:-1):($sortorder == 'asc' ? -1:1);
		}
	});
	echo json_encode($json);
	*/
	
	if(isset($_POST['export']) && trim($_POST['export']) === 'excel')	{	//將搜尋結果輸出excel
		include "include/excelwriter.inc.php";
		
		$excel = get_excel_header();
		$excel .= write_excel_line(array("筆數","經號","經名","卷","作譯者","著書年代(起)","著書年代(訖)"));

		foreach($json->results as $r_obj)	{
			$excel .= write_excel_line(array($r_obj->term_hits,$r_obj->file,$r_obj->title,$r_obj->juan,$r_obj->byline,($r_obj->time_from ? $r_obj->time_from:''),($r_obj->time_to ? $r_obj->time_to:'')));
		}
		$excel .= get_excel_footer();
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=CBETA_export_".trim($_POST['qry'])."_".date('mdHi').".xls");
		echo $excel;
	}
	else if(isset($_POST['export']) && trim($_POST['export']) === 'csv')	{	//將搜尋結果輸出csv
		$csv = array();
		$csv[] = array("筆數","經號","經名","卷","作譯者","著書年代(起)","著書年代(訖)");

		foreach($json->results as $r_obj)	{
			$csv[] = array($r_obj->term_hits,$r_obj->file,$r_obj->title,$r_obj->juan,$r_obj->byline,($r_obj->time_from ? $r_obj->time_from:''),($r_obj->time_to ? $r_obj->time_to:''));
		}
		
		header('Content-Encoding: UTF-8');
		header("Content-type: text/csv;charset=UTF-8");
		header("Content-disposition: attachment;filename=CBETA_export_".trim($_POST['qry'])."_".date('mdHi').".csv");
		
		echo "\xEF\xBB\xBF";
		echo implode("\n",array_map(function($csv_items)	{	return implode(',',$csv_items);	},$csv));
		
	}	
	else	{	//一般全文檢索
		echo json_encode($json);
	}
	
}
else if($_GET['type'] == 'extended_search_pattern')	{	//輸出進階搜尋的pattern in js給前端
	echo "_GLOBALS.extended_search_pattern = [".preg_quote(implode(',',array_map(function($o)	{ return '"'.$o.'"';},$EXTENDED_SEARCH_PATTERN)))."];";
	echo "_GLOBALS.extended_search_pattern_map = {".implode(',',array_map(function($value,$index)	{ return '"'.preg_quote($index).'":"'.$value.'"';},$EXTENDED_SEARCH_PATTERN_MAP,array_keys($EXTENDED_SEARCH_PATTERN_MAP)))."};";
}
else if($_GET['type'] == 'lang')	{			//將語系設定輸出成js
	include 'include/lang.php';
	$output = array();
	foreach($_LANG as $lang => $texts)	{
		foreach($texts as $t_index => $t)	{
			$output[] = "_GLOBALS['{$lang}']['{$t_index}'] = '{$t}';";
		}
	}
	echo implode('',$output);
}
else if($_GET['type'] == 'cbapi')	{		//將ray api path輸出成js
	echo '_GLOBALS.catalogAPI = "'.$CBDATA_API.'";';
	echo '_GLOBALS.catalogAPIBaseName = "'.$CBDATA_API_BASE_NAME.'";';
}
else if($_GET['type'] == 'docusky')	{		//將ray的docusky包一層允取跨網域
	echo file_get_contents($_GET['url']);
}
else if($_GET['type'] == 'ebooks_export')	{	//ebook 匯出（pdf、epub、mobi）
	//$path = realpath(array_pop(array_reverse(glob('downloads/ebooks/'.trim($_GET['export_type']).'/'.trim($_GET['work']).'*.'.trim($_GET['export_type'])))));

	$path = trim($_GET['work']).'.'.trim($_GET['export_type']);
	//$content = file_get_contents('http://cbetaonline.dila.edu.tw/downloads/ebooks/'.trim($_GET['export_type']).'/'.trim($_GET['work']).'.'.trim($_GET['export_type']));	//主機下載版
	
	preg_match('/([^0-9]+)[0-9]+/u',trim($_GET['work']),$letter);
	$letter[1] = trim($letter[1]) === 'JA' || trim($letter[1]) === 'JB' ? 'J':$letter[1];	//是JA或JB就換成J，此為規則上的「例外」，無奈...
	$content = file_get_contents($CBDATA_API.'download/'.trim($_GET['export_type']).'/'.$letter[1].'/'.trim($_GET['work']).'.'.trim($_GET['export_type']));	//呼叫api版
	
	//if(filesize($path))	{
	    header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.basename(str_replace(' ','',$path)));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		//header('Content-Length: ' . filesize($path));		
		header('Content-Length: ' . strlen($content));
		
		//readfile($content);
		echo $content;
	//}

	
}


else if($type == 'get_jing_by_person')	{	//取得某位譯者所翻譯的經資料，因為要先call adb api，再call ray api取得經資料，cb裡面又有cb，會有非同步bug，故移至後端做處理
	$result = array();
	$jsonObj = json_decode(file_get_contents("http://authority.dila.edu.tw/webwidget/getAuthorityData.php?type=catalog&id=".$_POST['aid']));
	foreach($jsonObj as $jo)	{
		$ray_data = json_decode(file_get_contents($CBDATA_API.'works?work='.$jo->taisho_code));
		if(count($ray_data) > 0)	{
			$result[] = $ray_data->results[0];
		}
	}
	echo json_encode($result);
}
?>