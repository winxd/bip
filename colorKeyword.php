<?php
//error_reporting(0);
mb_internal_encoding('utf-8');

include "include/highlight.php";
include "include/config.php";

function get_specialPart_html($content,$wrapper,$type)	{	//取得某部份的html，因為html並不是所有部份都要做highlight
	$doc = new DOMDocument();
	if(!$doc->loadHTML('<?xml encoding="utf-8" ?>'.$content))	return '';	//轉換時將xml的encoding變為utf-8 避免產生亂碼
	$sxml = simplexml_import_dom($doc);	//將html轉成SimpleXmlElement物件
	if($type == 'id')
		$result = $sxml->xpath('//div[@id="'.$wrapper.'"]');
	else if($type == 'tag')	
		$result = $sxml->xpath('//'.$wrapper);
	
	
	return $result ? $result[0]->asXml():'';
}



$punctuation = array('(',')','，','。','！',' ','…','、','「','」','.',',','》','《','：','）','（','；','？','　','─','"',"'",'*','︰',']','[','〉','〈','』','『','．','－','～','＼','｡','?');

//$punctuation = array('，','。','！','…','、','「','」',',','》','《','：','）','（','；','？','─','\-','\"',"\'",'︰','\]','\[','〉','〈','』','『','．','－','～','＼','｡',':');

if(isset($_GET['kw']) && isset($_GET['book_url']) && trim($_GET['kw']) != '')	{
	//處理進階搜尋：從條件中取出複數個關鍵字
	if(preg_match_all('/([^'.preg_quote(implode('',$EXTENDED_SEARCH_PATTERN)).']+)(['.preg_quote(implode('',$EXTENDED_SEARCH_PATTERN)).']?)/u',trim($_GET['kw']),$matches) && preg_match('/['.preg_quote(implode('',$EXTENDED_SEARCH_PATTERN)).']/u',trim($_GET['kw'])))	{
		if(count($matches) > 1)	{
			$keywords = array();
			for($i = 0;$i < count($matches[1]); $i++)	{
				$keywords[] = trim($matches[1][$i]);
				if(trim($matches[2][$i]) == '-')	$i++;	//如果是not(-)不做hl，keyword就跳過下一個詞
			}	

			//進階搜尋的各個關鍵字也要去除標點符號
			$keywords = array_map(function($k)	{
				global $punctuation,$EXTENDED_SEARCH_PATTERN;
				return preg_replace('/['.preg_quote(preg_replace('/['.preg_quote(implode('',$EXTENDED_SEARCH_PATTERN)).']/u','',implode('',$punctuation))).']/u','',trim($k));
			},$keywords);
			
		}		
	}
	else	{	//一般單個關鍵字
		$keywords = array(preg_replace('/['.preg_quote(implode($punctuation)).']/u','',urldecode(trim($_GET['kw']))));
		$keywords = explode('@',$keywords[0]);	//如果有多個關鍵字前端會用@隔開傳過來，只有在一般單個關鍵字有此效果，進階不支援！主要給相似搜尋用的！
	}
	//print_r($keywords);
	
	
	
	
	if(!preg_match('/^'.preg_quote($CBDATA_API,'/').'.+/u',urldecode(trim($_GET['book_url']))))	{	//檢查book_url是否合法
		echo json_encode(array('num_found'=>0,'results'=>array()));
		exit;		
	}
	
	$content =  file_get_contents(urldecode(trim($_GET['book_url'])));	
	$content = json_decode($content);
	$juan_uuid = $content->uuid;	//單卷的uuid
	$work_uuid = $content->work_info->uuid;	//整經的uuid
	$work = $content->work_info->work;	//整經的work
	$title = $content->work_info->title;	//經名
	$content = $content->results[0];

	//echo $content;
	
	/*
	//[阿閒註]此為舊版 hl時跳過.lb的regex，因為組合起來會落落長，造成 pcre 引擎在關鍵字過長時 hl 失敗，故棄用之，改用下面一行的版本就好，此版註解備份起來
	//hl時跳過.lb的regex，因為vol可能有兩碼(GA,GB,ZS,ZW)，如果用|需加上小括號，會造成hl錯誤，不能用小括號只好分開組字串，如下
	$lb_customIgnore_regexs = array('[TXJABCFGHKLMNPQRSUWZDIY]','GA','GB','ZS','ZW','LC','TX','CC');
	$lb_customIgnore_regexs_str = implode('|',array_map(function($iregex)	{
		return "<span[^<>]+>{$iregex}[0-9]{2,3}n[ABab]?[0-9]{3,4}[A-Za-z]?_?p[a-z]?[0-9]{3,4}[a-c][0-9]{2}<\/span>";
	},$lb_customIgnore_regexs));
	*/
	
	//hl時跳過.lb的regex，取代上面舊版的
	$lb_customIgnore_regexs_str = '<span[^>]+class="lb"[^>]+>[^<]+<\/span>';
	
	//夾注之所以不用 iRegex ：「<small[^<>]+>.*?<\/small>」跳過是因為 iRegex 的作用是「跨過關鍵字」而不是「不highlight」，用這招small裡面還是會被 hl 到所以不通，故還是用下方把 $ignore_inline_note 把 small抽出的方法
	$options = array (
		'html'      => false,  // find matches *inside* html code (e.g., match 'a' in "<a href...") [default: false]
		'casesens'  => false,  // perform case-sensitive matches [default: false]
		'iFlags'    => 0,      // bitwise parameter expressing complicated 'ignore' patterns [default: 0 (none)]
		'iRegex'    => "<span[^<>]+>[a-z0-9]{7}<\/span>|WINXD_[0-9]+_WINXD|<span class=\"pc\">[^<]+<\/span>|".$lb_customIgnore_regexs_str,   	//user-supplied regex to ignore [default: null]   
																							// 略過<span class='lineInfo'>0001a04</span>、<span class='lb' id='T01n0001_p0001a07'>T01n0001_p0001a07</span>、<span class="pc">，</span>，後者由$lb_customIgnore_regexs_str產生
	);
	$options['iFlags'] = KH_IGNORE_EMPTY_TAGS | KH_IGNORE_OPENING_TAGS | KH_IGNORE_CLOSING_TAGS  | KH_IGNORE_NONLETTER;
	//echo $options['iFlags'];
	
	//只highlight <div id="body">部份
	
	//header("Content-type:text/html; charset=UTF-8");
	
	$ignore_inline_note = isset($_GET['skip_inline_note']) && trim($_GET['skip_inline_note']) === 'no' ? false:true;	//是否跳過夾註hl，為 true 時：1.取出文本所有夾註不highlight 2.highlight 其餘部份 3.再將夾註塞回去，預設值：true
	$text = $content;
	foreach($keywords as $kword)	{
		if($ignore_inline_note)	{
			$inlineNote_count = -1;
			$inlineNotes = array();
		}
		
		$body = get_specialPart_html($text,'body','id');
		
		if($ignore_inline_note)	{	//將夾註取出放到 $inlineNotes
			$body = preg_replace_callback('/(<small[^>]+>.*?<\/small>)/u',function($m) use (&$inlineNote_count , &$inlineNotes) { $inlineNotes[] = $m[1];return 'WINXD_'.(++$inlineNote_count).'_WINXD'; },$body);
		}
		
		$REPLACED_COUNTER = 0;	//每個關鍵字計數器歸0，因為每組關鍵字長度都不同，沒歸0一直算下去會讓 group_num 算錯
		$text = kwic_highlight($body, $kword, $options, '<span group_num="%d" class="hl">%s</span>', $count);	//阿閒add：group_num為關鍵字的群組化編號
		$KEYWORD_DEVIATION += 100;	//承上，因計數器歸0，因此 group_num 可能重複，這邊為每個關鍵字加上偏移值，一次多100，保證 group_num 不重複
		
		if($ignore_inline_note)	{	//將 $inlineNotes 的夾註塞回去 body html
			/*
			foreach($inlineNotes as $i => $n)	{
				$text = preg_replace('/WINXD_'.$i.'_WINXD/u',$n,$text);
			}
			*/
			
			$text = preg_replace_callback('/WINXD_([0-9]+)_WINXD/u',function($m) use ($inlineNotes)	{
				return $inlineNotes[$m[1]];
			},$text);
			
		}
		
	}
	
	$result = '<html>'.get_specialPart_html($content,'head','tag').'<body>'.$text.get_specialPart_html($content,'back','id').get_specialPart_html($content,'cbeta-copyright','id').'</body></html>';	//最後再將div#back重組回去，並加上首尾html
	echo json_encode(array('results' => array($result),'uuid' => $juan_uuid , 'work_info' => array('uuid' => $work_uuid ,'work' => $work , 'title' => $title)));	//將結果包裝成跟ray一樣的格式方便前端處理
}

?>