var _GLOBALS = {};
_GLOBALS.currentVersion = '2024.R3';	//目前資料版本
_GLOBALS.panelMode = 'SINGLE_COLUMN';	//預設為單欄模式
_GLOBALS.active_panel = 'reading-area-1';//使用中的panel(左邊或右邊)，改變值時間點為click經選擇器按鈕的時候，預設為reading-area-1(左邊)
_GLOBALS.hlObjs = [];					//文字highlight 物件
_GLOBALS.racObj = false;				//引用複製物件
_GLOBALS.racTexts = {pure:'',formatted:''};	//儲存引用複製文字
_GLOBALS.xhrObjs = [];					//整合功能各個搜尋時的xhr物件，可做為xhr.stop()用
_GLOBALS.sideFuncLastKw = false;		//整合功能本次搜尋關鍵字，因搜尋結束後user可以改框框內的字，因此不準，故留此變數保留剛剛搜尋的字串
_GLOBALS.showContextMenu = true;		//是否顯示內文功能選單，預設顯示
_GLOBALS.panelFrameDrag = false;		//拖曳panel調整是否作用中
_GLOBALS.sideFrameDrag = false;			//整合功能拖曳是否作用中
_GLOBALS.sideFuncFrameNeedMinus = true;	//在1024大小時，整合功能panel打開不知為何會被擠下去，要將寬度在減1才沒問題，這只須做一次，由此變數判斷(僅用於side_func_display() in functions.js)
_GLOBALS.lastMainSideWidth = {main_area:false,side_func:false,window:false};		//由於main_area,side_func可自行調大小，因此兩/三欄切換時，於此紀錄這一次調整寬度，下次打開三欄時會跟上次一樣
_GLOBALS.lastSearchRange = false;		//上一次全文檢索時選擇的檢索選項是什麼，因為選單是動態產生，又要紀錄上一次選擇的狀態，故有此變數
_GLOBALS.titleNow = '';				//紀錄目前閱讀的經名，於updateTitle_openJing()被賦值
_GLOBALS.juanNow = 1;					//紀錄目前閱讀的「卷」是第幾卷，內文上下卷切換功能會用到，於updateTitle_openJing()裡面被賦值
_GLOBALS.DLMBS_num_of_page = 100;		//整合功能 DLMBS 每頁顯示之筆數
_GLOBALS.firstLoad = true;				//初次讀取flag
_GLOBALS.highlight_adb_person_obj = false;//人名地名套疊用的anchor hl obj(人名)
_GLOBALS.highlight_adb_place_obj = false;//人名地名套疊用的anchor hl obj(地名)
_GLOBALS.copyEventFromBtnFlag = false;	//用於引用複製，複製到剪貼簿按鈕執行document.exec('copy')時判斷是否為按鈕按下的，與使用者ctrl+c複製做區別，原本是用事件的target element來判斷，但各瀏覽器回傳結果不同故放棄改用本變數當flag處理
_GLOBALS.isExtendedSearch = false;		//本次搜尋是否為進階搜尋之flag
_GLOBALS.searchOffset = 0;				//全文檢索infinite scroll專用：從哪裡開始的offset，會隨著infinite scroll改變，每次搜尋從0開始
_GLOBALS.searchSortby = '';				//全文檢索infinite scroll專用：此變數只有在表格排序事件處理時用到，為了排序後能處理infinite scroll時資料的一致性(沒有一樣的sortby取出順序不同資料會不連續、有誤)
_GLOBALS.searchScrollLimit = 100;		//全文檢索infinite scroll專用：一次scroll讀取幾筆資料
_GLOBALS.MARKUS_JUAN_UUID = false;		//每一卷要傳給MARKUS用的uuid
_GLOBALS.MARKUS_JING_UUID = false;		//每一經要傳給MARKUS用的uuid
_GLOBALS.isTouchSelection = false;		//判斷是由觸控還是由滑鼠選取文字
_GLOBALS.isTouchRange = false;			//存下由 selectionchange 事件取得的 range，因為蘋果系列（雞巴！）用原 rac 物件因反白會失去 focus 無法取得 range 物件，只好存在這邊給 refandcopy那邊使用
_GLOBALS.skipInlineNoteHL = (localStorage.getItem("skipInlineNoteHL") && localStorage.getItem("skipInlineNoteHL") === 'yes' ? true:false);		//是否跨夾注之選項（跨夾注=true），從 localStorage 取回值，預設：false，不跨夾注
_GLOBALS.userActions = (localStorage.getItem("cbetaonline-user-actions") ? JSON.parse(localStorage.getItem("cbetaonline-user-actions")):[]);				//記錄使用者的操作行為(讀什麼經、搜尋什麼)
_GLOBALS.isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
_GLOBALS.lang = 'zh'; 					//語系，預設為中文zh
_GLOBALS.zh = {};						//中文語系物件，資料待後端填入
_GLOBALS.en = {};						//英文語系物件，資料待後端填入
_GLOBALS.creators = [];							//全部做譯者清單
_GLOBALS.creators_firstletter = [];				//將做譯者清單首字分類物件，於init()裡面設定值
_GLOBALS.creators_stroke = [];					//做譯者清單先依據首字、在依據筆劃之最終結果
_GLOBALS.authorityROOT = 'https://authority.dila.edu.tw/';
_GLOBALS.prodHost = 'cbetaonline.dila.edu.tw';	//production 版本的hostname
//_GLOBALS.prodHost = 'cbetaonline.cn';	//中國版的production hostname
_GLOBALS.scannigImageURL = 'https://dia.dila.edu.tw/uv3/index.html?id=';	//原圖掃描檔server位置
_GLOBALS.catalogAPI = false;	//後端目錄資料api網址(於getData.php?type=cbapi 設定)
_GLOBALS.open_container = {
	'reading-area-1-container':{
		init_book:'html/T/T01/T01n0001_001.htm',
		reading_xmlID:'T01n0001'
	}/*,
	'reading-area-2-container':{
		init_book:'html/T/T01/T01n0002_001.htm',
		reading_xmlID:'T01n0002'
	}
	*/
};

//選擇器用到的日期列表
_GLOBALS.dynasties = ['漢,-202,220','西漢,-201,8','新,8,23','東漢,25,220','曹魏,220,265','蜀漢,221,263','吳,229,280','晉,265,420','西晉,265,316','東晉,317,420','前涼,301,387','前秦/符秦,351,394','後秦/姚秦,384,417','西秦/乞伏秦,385,431','北涼,397,460','南北朝,439,589','劉宋,420,479','元魏/北魏/後魏,386,534','東魏,534,550','西魏,535,557','蕭齊/南齊,479,502','北齊/高齊,550,577','梁/蕭梁,502,557','北周/宇文周,557,581','陳,557,589','隋,581,618','唐,618,907','武周,690,705','後梁/朱梁,907,923','後唐,923,936','後晉/石晉,936,947','後漢,947,951','後周,951,960','南唐,937,975','南漢,917,971','遼,907,1125','宋,960,1279','北宋,960,1127','南宋,1127,1279','夏/西夏,1032,1227','金,1115,1234','元,1271,1368','明,1368,1644','清,1644,1911','新羅,-56,936','高麗,918,1392'];

//冊別目錄額外階層定義：有些冊是歸類在「歷代藏經補輯」和「近代新編文獻」中，於此定義，選擇器「冊別目錄」會用到
_GLOBALS.vol_extra_levels = {'A':'003','K':'003','F':'003','S':'003','C':'003','U':'003','P':'003','J':'003','L':'003','G':'003','M':'003','ZW':'006','ZS':'006','I':'006','B':'006','GA':'006','GB':'006','Y':'006','LC':'006','TX':'006','CC':'006'};	

//CBETA 藏經略符
_GLOBALS.CBETAShortCode = {"A":"【金藏】","AC":"【金藏乙】","B":"【補編】","C":"【中華】","CC":"【選集】","D":"【國圖】","F":"【房山】","G":"【佛教】","GA":"【志彙】","GB":"【志叢】","GJ":"【宮】","I":"【佛拓】","J":"【嘉興】","JM":"【嘉興丙】","JT":"【嘉興乙】","K":"【麗】","KZ":"【麗乙】","L":"【龍】","LC":"【呂澂】","M":"【卍正】","N":"【南傳】","P":"【北藏】","PN":"【普寧】","Q":"【磧】","QC":"【磧乙】","S":"【宋遺】","SS":"【縮刷】","SX":"【思溪】","SZ":"【思溪乙】","T":"【大】","TX":"【太虛】","U":"【洪武】","X":"【卍續】","Y":"【印順】","YN":"【南藏】","ZS":"【正史】","ZW":"【藏外】"};

function init()	{
	
	//localStorage.clear();	//2018-04-12：因為要測試新的html，暫停快取功能，每次進來都把快取清掉
	
	//中國版特別處理：屏蔽掉TX 和 Y
	if(_GLOBALS.prodHost === 'cbetaonline.cn')	{
		
		//移除選擇器 > goto 查詢 > 經卷結構、藏經結構之下拉選單的 TX 和 Y
		$('#selector-goto-book-form select[name="selector-goto-canon"] option[value="TX"] , #selector-goto-book-form select[name="selector-goto-canon"] option[value="Y"]').remove();
		$('#selector-goto-realbook-form select[name="selector-goto-canon"] option[value="TX"] , #selector-goto-realbook-form select[name="selector-goto-canon"] option[value="Y"]').remove();
		
		//移除選擇器 > 經目查詢 > 冊號經號之下拉選單的 TX 和 Y
		$('#selector-mulu-form select[name="canon"] option[value="TX"] , #selector-mulu-form select[name="canon"] option[value="Y"]').remove();
	}
	
	var contentMenuContainer = [];
	for(var container_index in _GLOBALS.open_container)	{
		(function(container)	{
			
			//檢查網址有無語系設定
			_GLOBALS.lang = location.pathname.split('/')[1] == 'en' ? 'en':'zh';
			
			$('#now-lang-text').html(_GLOBALS[_GLOBALS.lang]['24_23']);	//設定上方語系選單
			//if((navigator.language || navigator.browserLanguage).toLowerCase() == 'zh-cn')	$('#now-lang-text').html('簡中');	//如果為簡中則設定字串為「簡中」
			if(location.hostname == 'cbetaonline.cn' && $('#now-lang-text').text() != "English")	$('#now-lang-text').html('簡中');	//如果為中國版則設定字串為「簡中」
			
			//為body套上語系類別
			$('body').removeClass('en').removeClass('zh').addClass(_GLOBALS.lang);
			
			
			//依據語系更換說明文件的網址
			var doc_link = 'doc/'+_GLOBALS.lang+'/01-content.php';
			$('#doc_link').attr('href',doc_link);
			
			//從cookie讀回上次的顯示設定並調整頁面
			$('.config-chkbox-hide').each(function()	{
				if($.cookie($(this).attr('id')))	{
					$('#'+$(this).attr('id')).prop('checked',$.cookie($(this).attr('id')) == 'yes' ? true:false);
				}
			});
			
			//從cookie讀回引用複製設定並調整項目預設選擇
			$('.rac_conf_item').each(function()	{
				if($.cookie($(this).attr('name')))	{
					if($(this).attr('type') === 'radio')	{
						$('#refAndCopy_config input[name="'+$(this).attr('name')+'"][value="'+$.cookie($(this).attr('name'))+'"]').prop('checked',true);
					}
					else if($(this).attr('type') === 'checkbox')	{
						$('#'+$(this).attr('name')).prop('checked',($.cookie($(this).attr('name')) == '1' ? true:false));
					}
				}
			});
			
			//顯示掃描圖預設打開，因後續是根據cookie而動故先設定cookie值為on(如果已經設了此cookie就使用cookie儲存的值，這邊是針對還沒設定過的狀況)
			if(!$.cookie('scan-image-display-btn'))
				$.cookie('scan-image-display-btn','on',{path:'/'});
			
			//更新首頁上次閱讀經典
			updateLastReadingString();
	
			//檢查有無從網址傳入欲開啟之文本，有的話就開、沒有的話就照預設
			var URLParam = location.pathname.split('/')[2] && location.pathname.split('/')[2] != '' ? location.pathname.split('/')[2]:location.pathname.split('/')[1];	//可接受參數格式是http://cbetaonline.dila.edu.tw/zh/T0001_001或http://cbetaonline.dila.edu.tw/T0001_001
			if(URLParam && URLParam.match(/^([^_]+)_([0-9]{3})$/i))	{	//來自網址的經和卷，接受格式：T0001_001
				var juan = (/^([^_]+)_([0-9]{3})$/i.exec(URLParam) && /^([^_]+)_([0-9]{3})$/i.exec(URLParam)[2]) || '001';
				var xml_id =(/^([^_]+)_([0-9]{3})$/i.exec(URLParam) && /^([^_]+)_([0-9]{3})$/i.exec(URLParam)[1]);
				
				
				$.getJSON(_GLOBALS.catalogAPI+'works?work='+xml_id.toRay()+'&callback=?',function(res)	{
					if(res.num_found == 0)	{
						alert(_GLOBALS[_GLOBALS.lang]['21_15_1']+URLParam+_GLOBALS[_GLOBALS.lang]['21_15_2']);
						location.href = '.';	//重新讀取頁面						
					}
					res = res.results[0];
					var jing_title = res.title;
					var work = res.work;

					_GLOBALS.open_container[container].reading_xmlID = xml_id;
					
					$.when(
						update_tree(xml_id),
						updateTitle_openJing(xml_id,work+' '+jing_title,parseInt(juan,10),false,false)
					).done(function()	{
						history.replaceState({ xml_id: _GLOBALS.open_container[container].reading_xmlID,juan:parseInt(juan,10) ,title:work+' '+jing_title,scroll:false}, "", "/"+_GLOBALS.lang+"/"+URLParam);	//初次load book時將瀏覽器的state換成自訂的
						addUserAction('read',{xml_id:_GLOBALS.open_container[container].reading_xmlID,title:work+' '+jing_title,juan:parseInt(juan,10)});	//將讀經加入上次操作
						$.cookie('last-reading-book',_GLOBALS.open_container[container].reading_xmlID+';'+parseInt(juan,10)+';'+work+' '+jing_title,{path:'/'});	//紀錄上次閱讀的經典
						
					});
				});					
			}
			else if((location.pathname.split('/')[1] && location.pathname.split('/')[1] === 'mulu') || (location.pathname.split('/')[2] && location.pathname.split('/')[2] === 'mulu'))	{	//網址是 http://cbetaonline.dila.edu.tw/zh/mulu/T 或 http://cbetaonline.dila.edu.tw/zh/mulu/T01 這種，mulu代表開啟目錄，即開啟經選擇器的T給它
				var work_set;
				if(location.pathname.split('/')[2] && location.pathname.split('/')[2].match(/^([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB|LC|TX|CC)([0-9]{2,3})?$/))	work_set = location.pathname.split('/')[2];
				else if(location.pathname.split('/')[3] && location.pathname.split('/')[3].match(/^([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB|LC|TX|CC)([0-9]{2,3})?$/))	work_set = location.pathname.split('/')[3];
				
				if(work_set)	{
					//alert(work_set)
					//背後預設還是要有畫面，開啟首頁
					showHome(true);
					history.replaceState({ xml_id: 'home'}, "", "/"+_GLOBALS.lang+'/');	//初次load book時將瀏覽器的state換成自訂的
					setTimeout(function()	{	change_lang();	},100);	//本來更換語系是在開完經後做，但剛進來顯示首頁沒有讀經，所以在這邊做一次切換語系					
					
					//分析網址來的參數 work_set，可能只有T，也有可能是T02，狀況不同需要拆分
					var work_set_analysis = /^([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB|LC|TX|CC)([0-9]{2,3})?$/.exec(work_set);
					const [pass , collection , collection_and_number] = work_set_analysis;
						
					//下面為自動化執行選擇器
					jing_selector_show(true); //開啟選擇器
					$('#selector-menu-bar li[desc="vol"]').trigger('click');	//左側按下冊別
					
					//自動按下網址給的 collection 按鈕，但按鈕現在改成動態 ajax 產生，必須確保有按鈕，暫時用 setInterval 解(如有按鈕或執行超過30次停止)
					let muluIntervalCount = 0;
					let muluIntervalObj = setInterval(function()	{						
						//冊別目錄很多藏別是放在「歷代藏經補輯」、「近代新編文獻」下，需特別處理，層次的定義放在 _GLOBALS.vol_extra_levels
						if(_GLOBALS.vol_extra_levels[collection])	{
							$('#selector-levels .selector-multi-level-btn[n="orig.'+_GLOBALS.vol_extra_levels[collection]+'"]').trigger('click');
						}
						
						//有 collection 就讓冊別按鈕自動按
						if(($('#selector-levels .selector-multi-level-btn[n="orig-'+collection+'"]').length > 0 || muluIntervalCount > 30) && (collection !== 'GA' && collection !== 'GB'))	{
							clearInterval(muluIntervalObj);
							$('#selector-levels .selector-multi-level-btn[n="orig-'+collection+'"]').trigger('click');
							//console.log('mulu stop!');
						}
						else if(($('#selector-levels .selector-multi-level-btn[n="orig.006.005"]').length > 0 || $('#selector-levels .selector-multi-level-btn[n="orig-'+collection+'"]').length > 0 || muluIntervalCount > 30) && (collection === 'GA' || collection === 'GB'))	{	//GA、GB位於第三層要另外處理
							$('#selector-levels .selector-multi-level-btn[n="orig.006.005"]').trigger('click');
							if($('#selector-levels .selector-multi-level-btn[n="orig-'+collection+'"]').length > 0 || muluIntervalCount > 30)	{
								clearInterval(muluIntervalObj);
								$('#selector-levels .selector-multi-level-btn[n="orig-'+collection+'"]').trigger('click');								
							}
						}
						muluIntervalCount++;
						//console.log('mulu!');
					},500);
					
					
					/*	
					//此為舊版冊別目錄寫法，備份起來
					var selected_collection_btn = $('#selector-levels .selector-levels-vol-sw-btn[collection="Vol-'+collection+'"]');	//冊別第一層，選澤給定的collection
					$('#selector-levels-vol-selected-collection').val(selected_collection_btn.attr('collection'));	//紀錄目前選擇的collection	
					bulei_vol_resHandle(	//將選擇的collection執行按下動作(呼叫bulei_vol_resHandle)
						selected_collection_btn.attr('text'),
						selected_collection_btn.attr('desc'),
						selected_collection_btn.attr('collection')
					).then(function() {		//bulei_vol_resHandle 為promise based，於此確保按下collection按鈕後，ajax完成的後續
						if(collection_and_number)	{	//如後面還有冊號則於此執行
							//console.log(collection+collection_and_number)
							//$('#selector-levels .selector-multi-level-btn[n="Vol-'+collection+'.'+PadDigits(parseInt(collection_and_number,10),3)+'"]').trigger('click');	
							$('#selector-levels .selector-multi-level-btn[label*="'+collection+collection_and_number+'"]:eq(0)').trigger('click');	//由於已確保ajax執行完畢，畫面應該有collection底下的冊別按鈕群，那就於此選擇冊別按鈕執行click，例如T01
							document.title = _GLOBALS[_GLOBALS.lang]['8_1']+' | '+_GLOBALS[_GLOBALS.lang]['23'];	//重設網頁標題為「經文選擇」
						}
					});
					*/
									
				}
				else	{	//如果work_set(最後面經號)不符規則就跳到首頁去
					location.href = './';
				}
			}
			else if(URLParam && URLParam.match(/([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB|LC|TX|CC)([0-9]{2,3})n([ABab]?[0-9]{3,4}[A-Za-z]?)_?p([a-z]?[0-9]{3,4})([a-z])([0-9]{2})/i))	{	//網址的行號，接受格式：T01n0051_p0844a29、T01n0051p0844a29
				$.getJSON(_GLOBALS.catalogAPI+'juans/goto?linehead='+URLParam+'&callback=?',function(res)	{
					if(!res || res.error)	{
						if(res.error.message)
							alert(res.error.message)
						else
							alert(_GLOBALS[_GLOBALS.lang]['21_12']+$.trim($('input[name="selector-goto-linehead"]').val()));
						
						location.href = '.';	//重新讀取頁面		
					}
					
					res = res.results[0];
					var xml_id = res.file;
					var jing_title = res.title;
					var work = res.work;
					var juan = res.juan;
					//var scroll = res.lb ? (xml_id+(res.work.substr(-1).match(/[A-Za-z]/u) ? '':'_')+'p'+res.lb):false;
					var scroll = res.linehead ? res.linehead:false;	//2023-07-10:改用 api 新增的 linehead
					var is_highlight = new URLSearchParams(location.search).get('q') && !new URLSearchParams(location.search).get('note_clue') ? new URLSearchParams(location.search).get('q'):false;	//如果有從網址參數q來的話(但不能有 note_clue，校勘要另外處理)，表示從搜尋回來，則啟動colorKeyword.php去做hl
					var is_hl_all = new URLSearchParams(location.search).get('hl_all') && $.trim(new URLSearchParams(location.search).get('hl_all')) === 'yes' ? true:false;
					
					if(is_hl_all) _GLOBALS.skipInlineNoteHL = false;	//如果網址參數收到 hl_all=yes，代表內文要 hl 全部包括夾註，那麼 _GLOBALS.skipInlineNoteHL 就要設為 false
					
					if(is_highlight)	{
						$('#side_func_search_text_hidden').text(is_highlight);
					}
					else	{	//如果不是從搜尋回來的，表示為一般行號開啟，故要照預設打開「顯示原書換行+顯示行首」(2024-01-31取消)
						//$('#config_breakLine,#config_showLineNo').prop('checked',true);	//行號開啟時，預設直接顯示原書換行+顯示行首
						var do_nothing = true;
					}
					
					_GLOBALS.open_container[container].reading_xmlID = xml_id;
					
					$.when(
						update_tree(xml_id),
						updateTitle_openJing(xml_id,work+' '+jing_title,parseInt(juan,10),is_highlight,scroll)
					).done(function()	{
						history.replaceState({ xml_id: _GLOBALS.open_container[container].reading_xmlID,juan:parseInt(juan,10) ,title:work+' '+jing_title,scroll:scroll}, "", "/"+_GLOBALS.lang+"/"+URLParam+location.search);	//初次load book時將瀏覽器的state換成自訂的
						addUserAction('read',{xml_id:_GLOBALS.open_container[container].reading_xmlID,title:work+' '+jing_title,juan:parseInt(juan,10)});	//將讀經加入上次操作
						$.cookie('last-reading-book',_GLOBALS.open_container[container].reading_xmlID+';'+parseInt(juan,10)+';'+work+' '+jing_title,{path:'/'});	//紀錄上次閱讀的經典
					});
				});					
			}
			else if(URLParam && URLParam.match(/^([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB|LC|TX|CC)([ABab]?[0-9]{3,4}[A-Za-z]?)$/i))	{	//支援網址 T0001、X0002
				var param = /^([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB|LC|TX|CC)([ABab]?[0-9]{3,4}[A-Za-z]?)$/i.exec(URLParam);
				var query = param[0] ? param[0]:false;
				if(query)	{
					$.getJSON(_GLOBALS.catalogAPI+'/works?work='+query+'&callback=?',function(res)	{
							if(parseInt(res.num_found,10) <= 0)	{
								location.href = '.';	//重新讀取頁面						
							}
							res = res.results[0];
							var xml_id = res.file;
							var jing_title = res.title;
							var work = res.work;
							var juan = res.juan_start;

							
							_GLOBALS.open_container[container].reading_xmlID = xml_id;
							
							$.when(
								update_tree(xml_id),
								updateTitle_openJing(xml_id,work+' '+jing_title,parseInt(juan,10),false,false)
							).done(function()	{
								history.replaceState({ xml_id: _GLOBALS.open_container[container].reading_xmlID,juan:parseInt(juan,10) ,title:work+' '+jing_title,scroll:false}, "", "/"+_GLOBALS.lang+"/"+URLParam);	//初次load book時將瀏覽器的state換成自訂的
								addUserAction('read',{xml_id:_GLOBALS.open_container[container].reading_xmlID,title:work+' '+jing_title,juan:parseInt(juan,10)});	//將讀經加入上次操作
								$.cookie('last-reading-book',_GLOBALS.open_container[container].reading_xmlID+';'+parseInt(juan,10)+';'+work+' '+jing_title,{path:'/'});	//紀錄上次閱讀的經典
							});
					});			
				}				
			}
			else if(URLParam && URLParam.match(/([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB|LC|TX|CC)([0-9]{2,3})([pn])?([ABa-z]?[0-9]{3,4}[A-C]?)?([a-z])?([0-9]{2})?/))	{	//支援網址：1.T70p0280b(連結定位到「欄」)、X146p0489(連結定位到「頁」)、X65n1279(連結定位到「經」)、X65(連結定位到「冊」)、T51p0197b10 連到行
				var param = /([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB|LC|TX|CC)([0-9]{2,3})([pn])?([ABa-z]?[0-9]{3,4}[A-C]?)?([a-z])?([0-9]{2})?/.exec(URLParam);
				var query;
				param = param.filter(function(e)	{	return e;	});	//過濾掉陣列中的空值
				//console.log(param)
				if(param.length == 3 && param[1] && param[2])	{	//X65 連到冊，直接開啟選擇器 - 經目查詢啟動
					/*
					//query = 'canon='+param[1]+'&vol='+parseInt(param[2],10);
					//預設先開啟第一經，否則後面會是空的
					update_tree(_GLOBALS.open_container[container].reading_xmlID);	
					updateTitle_openJing(_GLOBALS.open_container[container].reading_xmlID,'T0001 長阿含經',1,false,false);
					
					$('#selector-mulu-form select[name="canon"]').val(param[1]);
					$('#selector-mulu-form input[name="vol_start"]').val(parseInt(param[2],10));
					$('#selector-mulu-submit').trigger('click');
					$('.selector-side-menu-mulu[desc="mulu"]').trigger('click');
					jing_selector_show(true);
					return false;
					*/
					
					query = 'canon='+param[1]+'&vol='+parseInt(param[2],10);
				}
				else if(param.length == 5 && param[1] && param[2] && param[3] && param[4] && param[3] == 'n')	{	//X65n1279 連到經
					query = 'canon='+param[1]+'&work='+parseInt(param[4],10);
				}
				else if(param.length == 5 && param[1] && param[2] && param[3] && param[4] && param[3] == 'p')	{	//X146p0489 連到頁
					query = 'canon='+param[1]+'&vol='+parseInt(param[2],10)+'&page='+parseInt(param[4],10);
				}
				else if(param.length == 6 && param[1] && param[2] && param[3] && param[4] && param[5])	{	//X146p0489 連到頁
					query = 'canon='+param[1]+'&vol='+parseInt(param[2],10)+'&page='+parseInt(param[4],10)+'&col='+param[5];
				}
				else if(param.length == 7 && param[1] && param[2] && param[3] && param[4] && param[5] && param[6])	{	//T51p0197b10 連到行
					query = 'canon='+param[1]+'&vol='+parseInt(param[2],10)+'&page='+parseInt(param[4],10)+'&col='+param[5]+'&line='+param[6];
				}
				else	{	//都不符的話跑長阿含經
					query = 'canon=T&work=1';	
				}
				
				//console.log(_GLOBALS.catalogAPI+'juans/goto?'+query+'&callback=?')
				//return;
				
				if(query)	{
					$.getJSON(_GLOBALS.catalogAPI+'juans/goto?'+query+'&callback=?',function(res)	{
						if(parseInt(res.num_found,10) <= 0)	{
							location.href = '.';	//重新讀取頁面						
						}
						res = res.results[0];
						var xml_id = res.file;
						var jing_title = res.title;
						var work = res.work;
						var juan = res.juan;
						//var scroll = res.lb ? (xml_id+(res.work.substr(-1).match(/[A-Za-z]/u) ? '':'_')+'p'+res.lb):false;
						var scroll = res.linehead ? res.linehead:false;	//2023-07-10:改用 api 新增的 linehead
						

						//$('#config_breakLine,#config_showLineNo').prop('checked',true);	//行號開啟時，預設直接顯示原書換行+顯示行首(2024-01-31取消)
						
						_GLOBALS.open_container[container].reading_xmlID = xml_id;
						
						$.when(
							update_tree(xml_id),
							updateTitle_openJing(xml_id,work+' '+jing_title,parseInt(juan,10),false,scroll)
						).done(function()	{
							history.replaceState({ xml_id: _GLOBALS.open_container[container].reading_xmlID,juan:parseInt(juan,10) ,title:work+' '+jing_title,scroll:scroll}, "", "/"+_GLOBALS.lang+"/"+URLParam);	//初次load book時將瀏覽器的state換成自訂的
							addUserAction('read',{xml_id:_GLOBALS.open_container[container].reading_xmlID,title:work+' '+jing_title,juan:parseInt(juan,10)});	//將讀經加入上次操作
							$.cookie('last-reading-book',_GLOBALS.open_container[container].reading_xmlID+';'+parseInt(juan,10)+';'+work+' '+jing_title,{path:'/'});	//紀錄上次閱讀的經典
						});
					});						
				}
			}
			else	{	
				
				showHome(true);
				history.replaceState({ xml_id: 'home'}, "", "/"+_GLOBALS.lang+'/');	//初次load book時將瀏覽器的state換成自訂的
				setTimeout(function()	{	change_lang();	},100);	//本來更換語系是在開完經後做，但剛進來顯示首頁沒有讀經，所以在這邊做一次切換語系
				
				/*
				$.when(
					update_tree(_GLOBALS.open_container[container].reading_xmlID),
					updateTitle_openJing(_GLOBALS.open_container[container].reading_xmlID,'T0001 長阿含經',1,false,false)
				).done(function()	{
					history.replaceState({ xml_id: _GLOBALS.open_container[container].reading_xmlID,juan:1 ,title:"T0001 長阿含經",scroll:false}, "", "/"+_GLOBALS.lang+"/T0001_001");	//初次load book時將瀏覽器的state換成自訂的
					
					if($.cookie('welcome-holder-option') && $.trim($.cookie('welcome-holder-option')) != "")	{	//有cookie紀錄上一次選的就填回去選項
						$('input[name="welcome-holder-option"][value="'+$.cookie('welcome-holder-option')+'"]').prop('checked',true);
					}
					
					if(!$.cookie('welcome-holder-display'))	{		//沒有勾選「下次不要顯示」才顯示
						$('#welcome-holder').show();
					}
					else	{
						$('#welcome-holder-btn').trigger('click');	//不要顯示時，行為等於按下OK啟動，如有cookie則會依據上面填回去的選項開啟
					}
				});
				*/
				
			}
		})(container_index);
		
		_GLOBALS.hlObjs.push(container_index);
		_GLOBALS.hlObjs[container_index] = new highlight({'highlight_color':'#ccff66','highlight_target':'anchor'});
		
		contentMenuContainer.push('#'+container_index);
	}

	//文本閱讀區反白時的選單
	var menus = {};
	/*2014-06-11:不用自動產生、改hardcode
	$('#footer #footer_tab > li').each(function()	{
		menus[$(this).text()] = $(this).find('a').attr('href').replace('#','');
		return ;
	});
	*/
	
	menus['refAndCopy'] = {'title':_GLOBALS[_GLOBALS.lang]['18_2'],fa:'fa-clipboard'};	//手動加入「引用複製」選單
	menus['copy'] = {'title':_GLOBALS[_GLOBALS.lang]['18_4'],fa:'fa-copy'};
	menus['urlCopy'] = {'title':_GLOBALS[_GLOBALS.lang]['18_5'],fa:'fa-link'};	
	menus['fulltextSearch'] = {'title':_GLOBALS[_GLOBALS.lang]['18_8'],fa:'fa-search'};
	menus['fulltextSearchThisJing'] = {'title':_GLOBALS[_GLOBALS.lang]['18_8_2'],fa:'fa-search'};
	menus['query'] = {'title':_GLOBALS[_GLOBALS.lang]['18_1'],fa:'fa-book'};
	menus['feedback'] = {'title':_GLOBALS[_GLOBALS.lang]['18_3'],fa:'fa-comment'};
	menus['wordcount'] = {'title':_GLOBALS[_GLOBALS.lang]['18_6'],fa:'fa-pencil'};
	menus['autoDoPunc'] = {'title':_GLOBALS[_GLOBALS.lang]['18_7'],fa:'fa-quote-right'};
	menus['autoDoWordseg'] = {'title':_GLOBALS[_GLOBALS.lang]['26_76'],fa:'fa-font'};
	
	
	var customIgnore = ['([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB|LC|TX|CC)[0-9]{2,3}n[ABab]?[0-9]{3,4}[A-Za-z]?_?p[a-z]?[0-9]{3,4}[a-c][0-9]{2}','：','\[A[0-9]+\]','\[[a-z0-9]+\]','\\[[科標解][0-9]+\]'];	//傳入custom regexp以跳過反白選取文字不要的部份
	$(contentMenuContainer.join(',')).selectedTextSearch(menus,selectedTextSearchCB,customIgnore);	//selectedTextSearchCB在functions.js裡頭

	
	//引用複製物件
	_GLOBALS.racObj = new refAndCopy('rac_container');
	
	//docuSky uploader init
	docuskyUploader.init();
	
	new Clipboard(document.getElementById('cp2clipboard_btn'));	//引用複製-複製到剪貼簿按鈕初始化
	new Clipboard('a[url="copy"]');	//一般複製-複製到剪貼簿初始化
	new Clipboard('#url_copy_trigger_btn');	//URL複製-複製到剪貼簿初始化
	
	//載入dilaDaLinks.js外掛
	dilaDaLinks.init({containerID:'da_links_btn',anchorHTML:'<i class="fa fa-th" aria-hidden="true" style=""></i>',backgroundColor:'white'});
	
	//建立 anchor highlight (人名地名套疊用)
	_GLOBALS.highlight_adb_person_obj = new highlight_adb({'highlight_color':'#ccff66','highlight_target':'anchor','highlight_classname':'person_hl'});	//人名
	_GLOBALS.highlight_adb_place_obj = new highlight_adb({'highlight_color':'#ccff66','highlight_target':'anchor','highlight_classname':'place_hl'});	//地名
	
	//將 ga clientID 加入自訂事件送回去
	setTimeout(function()	{
		gtag('event', 'Client ID Dimension', {'event_category': 'Client ID','event_label':gaGlobal.vid ,'value':gaGlobal.vid });
	},2000);
	
	
	/*
	//2020-11-26:有了新搜尋介面，搜尋範圍已移至該頁，這邊已經不需要了
	//全文檢索搜尋範圍限制模組
	ContextSearchRange.init(
		function(res)	{			
			$('#textsearch_range_value').val('works:'+res);
			if($('#fake_search_range_holder').size() <= 0)	{	//由#fake_search_range_holder 判斷是否是首頁一打開的範圍選單，是的話不做搜尋
				do_textsearch('#side_func ',$('#side_func_search_text_hidden').text());
				do_ngram('side_func_ngram_iframe',$('#side_func_search_text_hidden').text());
			}
			//alert('選擇的部類'+res);
		},
		function(res)	{
			$('#textsearch_range_value').val('works:'+res);
			if($('#fake_search_range_holder').size() <= 0)	{	//由#fake_search_range_holder 判斷是否是首頁一打開的範圍選單，是的話不做搜尋
				do_textsearch('#side_func ',$('#side_func_search_text_hidden').text());
				do_ngram('side_func_ngram_iframe',$('#side_func_search_text_hidden').text());
			}
			//alert('選擇的刊本'+res);
		},
		function(res)	{
			$('#textsearch_range_value').val('creator:'+res);
			if($('#fake_search_range_holder').size() <= 0)	{	//由#fake_search_range_holder 判斷是否是首頁一打開的範圍選單，是的話不做搜尋
				do_textsearch('#side_func ',$('#side_func_search_text_hidden').text());
				do_ngram('side_func_ngram_iframe',$('#side_func_search_text_hidden').text());
			}
		},
		function(res)	{
			$('#textsearch_range_value').val('works:'+res);
			if($('#fake_search_range_holder').size() <= 0)	{	//由#fake_search_range_holder 判斷是否是首頁一打開的範圍選單，是的話不做搜尋
				do_textsearch('#side_func ',$('#side_func_search_text_hidden').text());
				do_ngram('side_func_ngram_iframe',$('#side_func_search_text_hidden').text());
			}
		},		
		function()	{
			return;
			//$('#textsearch_range_value').val('');
		}
	);
	*/

	$("#feedback_modal_text").markdown({autofocus:false,savable:false});	//啟用問題回報編輯器支援markdown格式
	
	//製作作譯者清單(2020-02-26:改用 API all_creators2 版本，增加處理人名別名的部份)
	$.getJSON(_GLOBALS.catalogAPI+'export/all_creators2?callback=?',function(all_creators)	{	
		_GLOBALS.creators = all_creators.results;	//將全部作譯者清單複製一份給_GLOBALS.creators，過濾搜尋會用到
		
		//製作符合API all_creators v1的格式給下方產生筆劃首字清單的程式使用，這部份程式照原本就不動了
		var all_creators_v1_format = [];
		for(let aid in all_creators.results)	{
			if(all_creators.results[aid])  all_creators_v1_format.push([aid,all_creators.results[aid].regular_name]);
		}
		
		all_creators = all_creators_v1_format;
		//console.log(all_creators)
		
		all_creators.sort(function(a,b)	{	//先照名字排序一下
			if(a[1] > b[1] )	return 1;
			else if(a[1] < b[1])return -1;
			else return 0;
		});
		
		//先依首字製作分類
		for(var ci in all_creators)	{
			var first_letter = $.trim(all_creators[ci][1]).substring(0,1);
			if(all_creators[ci][1] == '𧧌震')	first_letter = '𧧌';	//「𧧌」為extb，substring(0,1)會有問題，因此首字再這邊特案處理
			if(Array.isArray(_GLOBALS.creators_firstletter[first_letter]))
				_GLOBALS.creators_firstletter[first_letter].push(all_creators[ci][1]+':'+all_creators[ci][0]); 
			else	{
				_GLOBALS.creators_firstletter[first_letter] = [];
				_GLOBALS.creators_firstletter[first_letter].push(all_creators[ci][1]+':'+all_creators[ci][0]); 
			}			
		}

		//再依筆劃+首字製作最終分類結果
		for(var first_letter in _GLOBALS.creators_firstletter)	{			
			if(!Array.isArray(_GLOBALS.creators_stroke[G_STROKES[first_letter]]))	_GLOBALS.creators_stroke[G_STROKES[first_letter]] = [];
			_GLOBALS.creators_stroke[G_STROKES[first_letter]][first_letter] = (_GLOBALS.creators_firstletter[first_letter]);
		}
		
		G_STROKES = null;	//G_STROKES已無利用價值，幹掉它釋放記憶體，因為它太大了
	});
	
	
	//製作經選擇器-日期之朝代列表 +
	$.getJSON(_GLOBALS.catalogAPI+'export/dynasty?callback=?',function(dynasties_res)	{	
		var dy_html = ['<div>'];
		var dy_counter = 0;
		dynasties = dynasties_res.results.split("\n");
		dynasties.forEach(function(elm,index)	{
			if(index === 0 || $.trim(elm) === '')	return;	//跳過第一個資料：「朝代,起始年,結束年,典籍數」，這啥小？
			var d = elm.split(',');
			dy_html.push('<button class="btn btn-default selector-dates-btn" s="'+d[1]+'" e="'+d[2]+'" dy="'+d[0].replace(/\//g,',')+'"><span>'+d[0]+'<span class="selector-dates-dy-count">('+parseInt(d[3],10)+')</span></span>  <span class="selector-dates-btn-year">'+(d[1] < 0 ? d[1]+' BCE':d[1]+' CE')+' ~ '+(d[2] < 0 ? d[1]+' BCE':d[2]+' CE')+'</span></button>');		
			if(++dy_counter ==  Math.ceil(parseInt(dynasties_res.num_found,10) / 2))	dy_html.push('</div><div>');			
		});
		dy_html.push('</div>');
		$('#selector-date-dynasty-tabpanel').html(dy_html.join(''));		
	
	});
	//製作經選擇器-日期之朝代列表 -
	
	/*
	//製作經選擇器朝代列表 舊方法(使用的deferred queue去循序執行非同步，方法特別故此備份)
	//需要先check一次所有朝代的資料筆數，小於0的不顯示，又因為是非同步取得，所以使用$.when.apply處理
	var dy_btns = [];
	var generate_btns = function()	{	//先產生取得所有朝代筆數的非同步task queue
		var tasks = [];
		for(var i in _GLOBALS.dynasties)	{
			(function(i)	{
				tasks.push(
					$.getJSON(_GLOBALS.catalogAPI+'works?dynasty='+encodeURIComponent(_GLOBALS.dynasties[i].split(',')[0].replace('/',','))+'&callback=?',function(res)	{
						if(parseInt(res.num_found,10) > 0)	{	//筆數大於0才要
							var d = _GLOBALS.dynasties[i].split(',');
							dy_btns.push({start_year:(d[0] == '新羅'|| d[0] == '高麗' ? 9999:d[1]),btn:'<button class="btn btn-default selector-dates-btn" s="'+d[1]+'" e="'+d[2]+'" dy="'+d[0].replace('/',',')+'"><span>'+d[0]+'<span class="selector-dates-dy-count">('+parseInt(res.num_found,10)+')</span></span>  <span class="selector-dates-btn-year">'+(d[1] < 0 ? d[1]+' BCE':d[1]+' CE')+' ~ '+(d[2] < 0 ? d[1]+' BCE':d[2]+' CE')+'</span></button>'});
						}
					})
				);
			})(i);
		}
		return tasks;
	};
	
	var dy_html = ['<div>'];
	var dy_counter = 0;
	var deferreds_btn = generate_btns();
    $.when.apply(null, deferreds_btn).done(function() {
		dy_btns.sort(function(a,b)	{	return a.start_year - b.start_year;	});	//由於非同步回來的順序無法保證，因此在這用起始年(start_year)排序資料
		for(var dy_btns_index in dy_btns)	{
			dy_html.push(dy_btns[dy_btns_index].btn);
			if(++dy_counter ==  Math.ceil(dy_btns.length / 2))	dy_html.push('</div><div>');
		}
		dy_html.push('</div>');
		$('#selector-date-dynasty-tabpanel').html(dy_html.join(''));
	});	
	//製作經選擇器-日期之朝代列表 -
	*/
	
	adjustPage();	//調整頁面呈現
	side_func_display(false);	//啟動時預設隱藏右邊整合功能欄
	$(window).trigger('resize');
	//$('#jing_selector').modal('show');	//啟動書選擇器
	
	
	//擴充jquery，增加textNodes function，使用方法:$(xxx).textNodes().wrap('<span/>')
	$.fn.textNodes = function() {
		var ret = [];
		(function(el){
			if (!el) return;
			if ((el.nodeType == 3))	{	//||(el.nodeName =="BR")
				ret.push(el);
			}
			else	{
				if(!$(el).is('.lb,.lineInfo,.note-link,.lb_br'))	{	//列出的class不做textnode
					for (var i=0; i < el.childNodes.length; ++i)	{
						arguments.callee(el.childNodes[i]);
					}
				}
			}
		})(this[0]);
		return $(ret);
	}	


}

//加入hotjar(just in production：cbetaonline.dila.edu.tw才列入統計):
if(location.hostname == 'cbetaonline.dila.edu.tw')	{
	//Hotjar Tracking Code for cbetaonline.dila.edu.tw
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1624837,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
}


//以下為函式擴充
String.prototype.toRay = function()	{	//擴充字串func，將xml_id轉成僅ray自己定義的格式：T01n0001 => T0001
	var s;
	s = this.replace(/^([^0-9]+)[0-9]{2,3}n(.+)/,"$1$2");
	if(s.match(/T0220[a-z]/g))	return s.substring(0,s.length-1);	//大般若經特案處理，將T0220[a-z] => T0220
	else	return s;
}

var highlight_adb = function(config)	{	//作者：李阿閒，將人名地名上色專用
	var hightlight_color = config.highlight_color ? config.highlight_color : null;
	var highlight_target = config.highlight_target ? config.highlight_target : null;
	var highlight_classname = config.highlight_classname ? config.highlight_classname : null;

	if(!hightlight_color || !highlight_target || (highlight_target != 'anchor') || !highlight_classname)	{
		alert('初始化有誤，建立物件失敗');
		return;
	}
	var highlightedObjs = [];
	var lbEndFlag = false;	//anchor專用，用來標示已找到結束anchor
	var skip_classes = ['.scan-image-display-btn','.pc','.note-link','.lb','.graphic','.note-link-cbeta'];	//跳過不做hl的tag classes
	
	//anchor highlight專用之private func
	var traverseLbChild = function(elm,end_id)	{
		var adb_key;
		if(lbEndFlag)	return;
		
		if(elm.children().size() > 0)	{
			elm.children().each(function()	{
				if(lbEndFlag)	return false;
				if($(this).children().size() > 0)	{
					if($(this).attr('id') == end_id || $(this).get(0).tagName.toLowerCase() == 'body' || elm.get(0).tagName.toLowerCase() == 'script')	lbEndFlag = true;
					traverseLbChild($(this),end_id);
				}
				else	{
					if($(this).attr('id') == end_id || $(this).get(0).tagName.toLowerCase() == 'body' || elm.get(0).tagName.toLowerCase() == 'script')	lbEndFlag = true;
					//$(this).css({'background-color':hightlight_color});
					adb_key = $('#'+end_id).data('key');
					if(!elm.is(skip_classes.join(',')))	$(this).addClass(highlight_classname).attr('adb-key',adb_key);
					
					highlightedObjs.push($(this));
				}
			});
		}
		else	{
			if(elm.attr('id') == end_id || elm.get(0).tagName.toLowerCase() == 'body' || elm.get(0).tagName.toLowerCase() == 'script')	lbEndFlag = true;
			//elm.css({'background-color':hightlight_color});
			adb_key = $('#'+end_id).data('key');
			if(!elm.is(skip_classes.join(',')))	elm.addClass(highlight_classname).attr('adb-key',adb_key);;
			highlightedObjs.push(elm);
		}
	};
		
	var travaseNext = function(elm,end_id)	{
		elm.nextAll().each(function()	{
			if(lbEndFlag)	return false;

			traverseLbChild($(this),end_id);
		});
		return elm;
	};
	
	return {		
		go:function(from,to,option)	{	
			//console.log(from+','+to)
			
			if($('[id="'+from+'"]').length <= 0 || $('[id="'+to+'"]').length <= 0)	{
				console.log('起始或結束物件找不到，不做highlight');
				return false;
			}
			
			var ps = this;
			//ps.reset();
			lbEndFlag = false;
			if(highlight_target === 'anchor')	{
				
				//如果from和to同一行，則end_id取from起算的的"下一個" anchor
				//var end_id = (from == to ? $('.anchor').get($('.anchor').index($('.anchor[id="'+from+'"]'))+1).id : to);
				var end_id = to;
				
				travaseNext($('[id="'+from+'"]'),end_id);
				
				//如果還是沒找到end，就往parent找，找到為止
				if(!lbEndFlag)	{
					var parent = $('[id="'+from+'"]').parent();
					while(!lbEndFlag)	{
						elm = travaseNext(parent,end_id);
						parent = elm.parent();
					}
				}				
			}
		},
		reset:function()	{				
			if(!highlightedObjs)	return;			
			for(var i = 0 ; i < highlightedObjs.length ; i++)	{
				//highlightedObjs[i].css({'background-color':'inherit'});	//原本的背景色
				highlightedObjs[i].removeClass(highlight_classname);
			}
			highlightedObjs = [];
		}
	}
};


/* 
	物件多欄排序功能 +
	use - 排序物件為JS object假設叫home，先依照city欄再照price欄之用法 : homes.sort(SORT_MULTI_COL('city', {name:'price', primer: parseInt, reverse: true}));
	ref - http://stackoverflow.com/questions/6913512/how-to-sort-an-array-of-objects-by-multiple-fields
*/
var SORT_MULTI_COL;
(function() {
    // utility functions
    var default_cmp = function(a, b) {
            if (a == b) return 0;
            return a < b ? -1 : 1;
        },
        getCmpFunc = function(primer, reverse) {
            var dfc = default_cmp, // closer in scope
                cmp = default_cmp;
            if (primer) {
                cmp = function(a, b) {
                    return dfc(primer(a), primer(b));
                };
            }
            if (reverse) {
                return function(a, b) {
                    return -1 * cmp(a, b);
                };
            }
            return cmp;
        };

    // actual implementation
    SORT_MULTI_COL = function() {
        var fields = [],
            n_fields = arguments.length,
            field, name, reverse, cmp;

        // preprocess sorting options
        for (var i = 0; i < n_fields; i++) {
            field = arguments[i];
            if (typeof field === 'string') {
                name = field;
                cmp = default_cmp;
            }
            else {
                name = field.name;
                cmp = getCmpFunc(field.primer, field.reverse);
            }
            fields.push({
                name: name,
                cmp: cmp
            });
        }

        // final comparison function
        return function(A, B) {
            var a, b, name, result;
            for (var i = 0; i < n_fields; i++) {
                result = 0;
                field = fields[i];
                name = field.name;

                result = field.cmp(A[name], B[name]);
                if (result !== 0) break;
            }
            return result;
        }
    }
}());
//物件多欄排序功能 -

Date.prototype.YMDHIS = function(format,join) {	//擴充日期取得特定格式 use:new Date().YMDHIS('yyyy-mm-dd-hh-ii-ss'); ，pattern可拆開使用
	function pad2(n) {  // always returns a string
		return (n < 10 ? '0' : '') + n;
	}
	
	var join_pattern = join || '';
	var dates = {
		yyyy:this.getFullYear(),
		mm:pad2(this.getMonth()+1), // getMonth() is zero-based
		dd:pad2(this.getDate()),
		hh:pad2(this.getHours()),
		ii:pad2(this.getMinutes()),
		ss:pad2(this.getSeconds())
	};
	
	return format.split('-').map(function(o)	{
		return dates[o];
	}).join(join_pattern);
	

};

	/*
	 *	去除object重複的部份，可指定欄位，ref：http://stackoverflow.com/questions/16830159/distinct-value-from-the-javascript-array-of-object
	 * 	var list = [{'one':1},{'two':2},{'four':4},{'one':1},{'four':4},{'three':3},{'four':4},{'one':1},{'five':5},{'one':1}];
	 *	uniqueObjects(list,['one']);	去除欄位one的值重複部份
	*/
	uniqueObjects = function (obj,props) {		
		function compare(a, b) {
		  var prop;
			if (props) {
				for (var j = 0; j < props.length; j++) {
				  prop = props[j];
					if (a[prop] != b[prop]) {
						return false;
					}
				}
			} else {
				for (prop in a) {
					if (a[prop] != b[prop]) {
						return false;
					}
				}

			}
			return true;
		}
		return obj.filter(function (item, index, list) {
			for (var i = 0; i < index; i++) {
				if (compare(item, list[i])) {
					return false;
				}
			}
			return true;
		});
	};	


RegExp.quote = function(str) {	//為regexp加上quote
    return (str+'').replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&");
};

//擴充string的escape方法，前端防止xss，using：s.escape()
String.prototype.escape = function() {
	/*
	var tagsToReplace = {
		'&': '&amp;',
		'<': '&lt;',
		'>': '&gt;'
	};
	return this.replace(/[&<>]/g, function(tag) {
		return tagsToReplace[tag] || tag;
	});
	*/
	return this.replace('<script>','').replace('<script','').replace('</script>','').replace('script>','');
};	