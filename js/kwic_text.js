/**
 * KWIC_TEXT
 * 可取得某關鍵字前後n個文字之pure-js 函式 
 *
 * 作者：李阿閒 <winxd@dila.edu.tw>
 * 版本：0.1
 * All right reserved. 轉載需經原作者同意
 *
 * 使用前置：需先include jquery lib , 版本 1.9.1 以上
 * 使用方法：KWIC_TEXT.get(關鍵字之selector[ex: #container , .classname ...] , 目標html , 前後取幾個字 , [optional]是否忽略標點符號,default:false);
 * 回傳值：文件內所有關鍵字前後n字的陣列
 *
 */
 
var KWIC_TEXT = {
	ignore_punctuations: ['\\(','\\)','，','。','！','…','、','「','」','\\.','\\,','》','《','：','）','（','；','？','　','─','"',"'",'\\*','︰','〉','〈','』','『','．','－','～','＼','｡','\\?'],
	get:function(target,html,word_count,strip_punctuations,user_strip_text)	{	//bip專用：最後一個參數user_strip_text有提供的話表示由user自己做[W]..[/W]之工作，不由這邊做
		var text;
		
		if(user_strip_text)	{
			text = user_strip_text;
		}
		else	{
			text = $('<div/>').html(html).find(target).each(function()	{
				$(this).replaceWith('[W]'+$(this).text()+'[/W]');
			}).end().text().replace(/[\s\r\n]/g,'');
		}
		
		if(strip_punctuations)	text = text.replace(new RegExp('['+this.ignore_punctuations.join('')+']','g'),'');
		
		//var regex = /([^\]]{0,5})\[W\]([^\[]+?)\[\/W\]([^\[]{0,5})/g;

		var regex = new RegExp('([^\\]]{0,'+word_count+'})\\[W\\]([^\\[]+?)\\[\\/W\\]([^\\[]{0,'+word_count+'})','g');

		var matches = [],match;
		while(match = regex.exec(text)) {
			matches.push(match[1]+'<b>'+match[2]+'</b>'+match[3]);
			regex.lastIndex -= match[3].length;	//將下次match的起點設在match[3]之前，這樣下一次的前面文字就不會跳過去了
		}
		
		//console.log(matches);
		return matches;
		

	},
	quote:function(s)	{	//escape regex special char
		return (s+'').replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&");
	}
};