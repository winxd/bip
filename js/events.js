function register_events()	{

	$(window).resize(function()	{	//視窗resize時處理，tree_area比例不動、但reading-area-1根據畫面寬度重新調整大小
		var china_reading_area_container_height_plus = location.hostname == 'cbetaonline.cn' ? 40:0;	//中國版專用，讓右邊閱讀區panel高度小一點，因為下面要放備案號................
		$('#reading-area-1').css({'width':$(window).width()-15-$('#tree_area').get(0).offsetWidth-$('#side_func').get(0).offsetWidth-$('#bookAndTree_adjuster').get(0).offsetWidth+'px'});
		$('#reading-area-1-container').height(($(window).height()-$('#jing_info_area').get(0).offsetHeight-$('#book-toolbar').get(0).offsetHeight-32)-(china_reading_area_container_height_plus)+'px');
		$('#tree_area_structure').height(($(window).height()-$('#jing_info_area').get(0).offsetHeight-57)+'px');
		$('#side_func').height(($(window).height()-25)+'px');
		adjustPage();	//因為#side_func高度有調整、要重設side_func裡面accordions的高度符合頁面
		
		if($('#side_func').is(':visible'))	{
			$('#main_area').css({'width':'59.9%'});
			$('#side_func').css({'width':'39.9%'});			
			$('#tree_area').css({'width':'25%'});
			$('#reading-area-1').css({'width':'74%'});			
		}
		
		_GLOBALS.sideFuncFrameNeedMinus = true;	//resize時panel三欄開關判斷變數需重設
	});
	
	$(document).mousedown(function(e)	{	
		/*
		if($(e.target).parents('.g-body').size() <= 0)	{	//！處理測試用的編輯popop
			$('.text-menu').css({'left':'-999px','top':'-999px'});
		}
		*/
		if($(e.target).parents('#collate-window').size() <= 0 && $('#collate-window').is(':visible'))	collate_window_close();	//click粉紅小視窗之外的地方就關閉小視窗
		
		if($('#jing_selector').is(':visible') && $(e.target).parents('#jing_selector').size() <= 0)	{	//在頁面其他地方按一下可關閉經選擇器
			jing_selector_show(false);
		}
	});

	//樹目錄+閱讀區、整合功能+main_area 左右拖曳調整寬度功能 +
	$(document).mousemove(function(e)	{	
		//樹目錄+閱讀區
		if(_GLOBALS.panelFrameDrag)	{	
			/*
			// 以下語法可以限定拖曳時直線的範圍(允許拖曳的值是畫面寬度的15%~70%中間，超過範圍不可拖曳)
			if(e.pageX > ((($(window).width()-$('#side_func').get(0).offsetWidth) / 10)*1.5) && e.pageX < ((($(window).width()-$('#side_func').get(0).offsetWidth) / 10)*7))
				$('.adjust-panel-line-enabled').css({'left':(e.pageX-3)+'px'});	
			*/
			
			// 上面改為：允許拖曳的值是畫面寬度的1%~70%中間，超過範圍不可拖曳（左邊放寬）
			if(e.pageX > ((($(window).width()-$('#side_func').get(0).offsetWidth) / 10)*0.1) && e.pageX < ((($(window).width()-$('#side_func').get(0).offsetWidth) / 10)*7))
				$('.adjust-panel-line-enabled').css({'left':(e.pageX-3)+'px'});	
			
			e.stopPropagation();	//防止事件互相干擾，做完後停止事件傳遞
			return false;
			
		}
		
		//整合功能+main_area
		if(_GLOBALS.sideFrameDrag)	{
			
			if(e.pageX > (($(window).width() / 10)*4) && e.pageX < (($(window).width() / 10)*7))
				$('#adjust-panel-line-old > div').css({'left':(e.pageX-3)+'px'});	
			
			e.stopPropagation();	//防止事件互相干擾，做完後停止事件傳遞
			return false;			
		}
	});	
	
	//樹目錄+閱讀區左右調整線按下
	$('#bookAndTree_adjuster').mousedown(function(e)	{
		_GLOBALS.panelFrameDrag = true;
		//$('#side_func_ngram_iframe').css({'visibility':'hidden'});	//iframe會阻擋mousemove，開始拖曳時隱藏之
		$(this).find('div[role="adjuster"]').addClass('adjust-panel-line-enabled');
		if(_GLOBALS.panelFrameDrag)	$('.adjust-panel-line-enabled').css({'left':(e.pageX)+'px'});		//mousedown時先執行拖曳動作一次，避免沒mousemove執行mouseup事件(left為空)
		e.stopPropagation();	//防止事件互相干擾，做完後停止事件傳遞
		return false;
	});
	
	//整合功能+main_area 左右調整線按下
	$('#adjust-panel-line-old').mousedown(function(e)	{
		_GLOBALS.sideFrameDrag = true;
		$(this).find('div[role="adjuster"]').addClass('adjust-side-line-enabled');
		if(_GLOBALS.sideFrameDrag)	$('.adjust-side-line-enabled').css({'left':(e.pageX)+'px'});		//mousedown時先執行拖曳動作一次，避免沒mousemove執行mouseup事件(left為空)
		e.stopPropagation();	//防止事件互相干擾，做完後停止事件傳遞
		return false;
	});	
	
	////樹目錄+閱讀區panel調整線hover時顯示出來的效果
	$('#bookAndTree_adjuster').hover(
		function()	{
			$(this).find('div[role="adjuster"]').addClass('adjust-panel-line-enabled');
		},
		function()	{
			if(!_GLOBALS.panelFrameDrag)	{
				$(this).find('div[role="adjuster"]').removeClass('adjust-panel-line-enabled');
			}
		}
	);
	
	//整合功能side panel調整線hover時顯示出來的效果
	$('#adjust-panel-line-old').hover(
		function()	{
			$(this).find('div[role="adjuster"]').addClass('adjust-side-line-enabled');
		},
		function()	{
			if(!_GLOBALS.sideFrameDrag)	{
				$(this).find('div[role="adjuster"]').removeClass('adjust-side-line-enabled');
			}
		}
	);	
	
	//於樹目錄+閱讀區panel調整線double-click時將樹狀目錄關閉
	$('#bookAndTree_adjuster > div[role="adjuster"]').dblclick(function(e)	{
		var tree_area_width = 10;	//tree area的寬度固定縮小到10，其餘程式同下方scroll panel
		$('#tree_area').css({'width':tree_area_width+'px'});
		$('#reading-area-1').css({'width':$(window).width()-15-$('#tree_area').get(0).offsetWidth-$('#side_func').get(0).offsetWidth-$('#bookAndTree_adjuster').get(0).offsetWidth+'px'});
		$('#tree_area > ul[role="tablist"]').hide();
		$('#toc_open_btn > .glyphicon').next().text(_GLOBALS[_GLOBALS.lang]['1_2_1']).parent().attr('data-hint',_GLOBALS[_GLOBALS.lang]['1_2_1']);	//處理與toc按鈕連動
		
		e.stopPropagation();
		return false;
	});
	
	//$(document).on('mouseup','.adjust-panel-line-enabled',function(e)	{	
	$(document).mouseup(function(e)	{
		//樹目錄+閱讀區調整大小
		if(_GLOBALS.panelFrameDrag)	{
			_GLOBALS.panelFrameDrag = false;
			//$('#side_func_ngram_iframe').css({'visibility':'visible'});	//拖曳結束後再顯示iframe
			var tree_area_width = parseInt($('.adjust-panel-line-enabled').get(0).style.left,10);
			$('#tree_area').css({'width':tree_area_width+'px'});
			$('#reading-area-1').css({'width':$(window).width()-15-$('#tree_area').get(0).offsetWidth-$('#side_func').get(0).offsetWidth-$('#bookAndTree_adjuster').get(0).offsetWidth+'px'});
			$('.adjust-panel-line-enabled').css('left','auto').removeClass('adjust-panel-line-enabled');
			
			//如果向左邊拉的太深時樹狀目錄的頁籤會爆出，因此在這設定當寬度小於一定值後將頁籤隱藏，反之顯示
			if(tree_area_width <= 80)	$('#tree_area > ul[role="tablist"]').hide();
			else	$('#tree_area > ul[role="tablist"]').show();
			
			if(tree_area_width > 30)	$('#toc_open_btn > .glyphicon').next().text(_GLOBALS[_GLOBALS.lang]['1_2_2']).parent().attr('data-hint',_GLOBALS[_GLOBALS.lang]['1_2_2']);	//處理與toc按鈕連動
			
			/*
			e.stopPropagation();	//防止事件互相干擾，做完後停止事件傳遞
			return false;
			*/
		}
		
		//整合功能+main_area調整大小
		if(_GLOBALS.sideFrameDrag)	{
			
			_GLOBALS.sideFrameDrag = false;
			
			var main_area_orig_width = $('#main_area').get(0).offsetWidth;
			var main_area_width = parseInt($('.adjust-side-line-enabled').get(0).style.left,10);
			$('#main_area').css({'width':main_area_width+'px'});
			$('#side_func').css({'width':$(window).width()-2-$('#main_area').get(0).offsetWidth-$('#bookAndTree_adjuster').get(0).offsetWidth+'px'});
			$('.adjust-side-line-enabled').css('left','auto').removeClass('adjust-side-line-enabled');
			
			//main_area下的樹目錄跟閱讀區本來就可以拖曳調整大小，因此寬度會是固定值，固定值的話當然現在也要跟著調了否則會爆掉，方式為照原寬度比例放大或縮小
			var tree_area_percent = $('#tree_area').get(0).offsetWidth / (main_area_orig_width);		//取出樹狀目錄原本的寬度比例
			var reading_area_percent = $('#reading-area-1').get(0).offsetWidth / (main_area_orig_width);//取出閱讀區原本的寬度比例
			
			//樹狀目錄、閱讀區照寬度比例調整新值
			$('#tree_area').css({'width':((main_area_width) * tree_area_percent)+'px'});
			$('#reading-area-1').css({'width':((main_area_width) * reading_area_percent)+'px'});
			
			//重要！此行與resize第一行同！由於根據比例調整會有計算的誤差值造成閱讀區寬度過寬或過窄以致距離邊線太遠，本於此觸發resize事件，但因事件有其它作用會互相影響，故拆成此行，需注意此行與resize事件第一行相同
			$('#reading-area-1').css({'width':$(window).width()-15-$('#tree_area').get(0).offsetWidth-$('#side_func').get(0).offsetWidth-$('#bookAndTree_adjuster').get(0).offsetWidth+'px'});
			
			_GLOBALS.sideFuncFrameNeedMinus = true;	//resize時panel三欄開關判斷變數需重設
			
			if($('#side_func').get(0).offsetTop > 20)	{	//用offsetTop判斷side_func是否被擠下去 是的話手動將寬度-2排上去...
				var side_func_width_minus = parseInt($('#side_func').css('width'),10);
				//console.log(side_func_width_minus)
				side_func_width_minus-=2;
				$('#side_func').css('width',side_func_width_minus);
				//console.log($('#side_func').css('width'))
			}

		}		
		
		e.stopPropagation();	//防止事件互相干擾，做完後停止事件傳遞
		//return false;			//2017-04-18：此return false會造成關閉選單後反白文字事件失效(cus return false)，故於此取消，反正已有stopPropagation
		
	});		
	//樹目錄+閱讀區、整合功能+main_area 左右拖曳調整寬度功能 -
	
	

	

	//開啟經選擇器按鈕
	$('#open_jing_selector_btn').click(function()	{
		jing_selector_show(true);
	});
	
	//關閉經選擇器按鈕
	$('#jing_selector_close_btn').click(function()	{
		jing_selector_show(false);		
	});
	
	//設定選單取消預設click選單消失之行為
	$('#config_dropdown_menu').click(function(e)	{
		e.stopPropagation();
	});	
	
	//註解&缺字
	$(document).on('click','.note-link.cb,.gaijiAnchor,.note-link-cbeta',function(e)	{
		//於視窗中間彈出
		var link_cbeta = $(this).data('key') && $.trim($(this).data('key')) !== '' ? '<br/><a target="_blank" href="https://revisiondb.cbeta.org/?notekey='+$.trim($(this).data('key'))+'">CBETA 校訂考證資料庫</a>':''	//連回CBETA校訂考證資料庫
		var centerWidth = ($(window).width() / 2) - ($('#collate-window').width() / 2);
		var centerHeight = ($(window).height() / 2) - ($('#collate-window').height() / 2);
		var htmlContent = mainTitle = openingID = '';
		
		$('.active-note').removeClass('active-note');	//不論註解或缺字都要先remove .active-note
		if($(this).hasClass('note-link') && $(this).hasClass('cb'))	{	//cb註解
			$('span.note-link.cb[clue="'+$(this).attr('clue')+'"]').addClass('active-note');

			if($('#back div#'+$(this).attr('clue')).size() >= 1)	//註解裡面需用html()，因為可能有梵文之標記要顯示，不能用text()
				htmlContent = '<span>'+$('#back div#'+$(this).attr('clue')).html()+link_cbeta+'<span>';
			else
				htmlContent = '<span>'+$('#back div#'+$(this).attr('clue')+'.T').html()+link_cbeta+'<span>';
				
			//htmlContent = '<span>'+$('#back #'+$(this).attr('clue')).map(function(item,index)	{	return $(this).attr('class').replace('footnote_','')+'：'+$(this).html();	}).get().join('<br/>')+'</span>';
			
			mainTitle = _GLOBALS[_GLOBALS.lang]['21_4'];
			openingID = $(this).attr('clue');
			
		}
		else if($(this).hasClass('gaijiAnchor'))	{	//缺字
			var gaijiInfo = $('#back #'+$(this).attr('href').replace('#',''));
			var moe = (!gaijiInfo.attr('moe') || $.trim(gaijiInfo.attr('moe')) === '' ? '':'<a target="_blank" href="https://dict.variants.moe.edu.tw/search.jsp?ID=7&WTP=1&WORD='+$.trim(gaijiInfo.attr('moe'))+'&WORD=&WORD=&WORD=">連結至教育部異體字字典</a>'+'<br/>')
			var norm = '';
			if(gaijiInfo.attr('norm') && gaijiInfo.attr('norm'))	{	//通用字額外處理
				var norm_words = gaijiInfo.attr('norm').trim().split('；');
				norm_words = norm_words.map(w => '<a target="_blank" href="https://www.unicode.org/cgi-bin/GetUnihanData.pl?codepoint='+w.codePointAt(0).toString(16)+'" data-codepoint="'+w.codePointAt(0).toString(16)+'">'+w+'</a>');
				norm = norm_words.join('；');
			}
			
			
			htmlContent = '<span>'+('CBETA缺字碼：'+$(this).attr('href').replace('#','')+'<br/>')+(!gaijiInfo.attr('uni_char') || gaijiInfo.attr('uni_char') == '' ? '':'Unicode 字元：'+gaijiInfo.attr('uni_char')+'<br/>')+(!norm || norm === '' ? '':'通用字：'+norm+'<br/>')+(!gaijiInfo.attr('zzs') && gaijiInfo.attr('zzs') == '' ? '':'組字式：'+gaijiInfo.attr('zzs')+'<br/>')+moe+('<img src="'+gaijiInfo.attr('figure_url')+'" />')+(gaijiInfo.attr('unicode') && parseInt(gaijiInfo.attr('unicode'),16) >= 183984 && parseInt(gaijiInfo.attr('unicode'),16) <= 191471 ? '<div style="margin-top:1.5em">*'+_GLOBALS[_GLOBALS.lang]['24_5']+'<a target="_blank" href="http://cbetaonline-dev.dila.edu.tw/doc/zh/03-10_font_install.php">'+_GLOBALS[_GLOBALS.lang]['24_6']+'</a></div>':'')+'</span>';
			mainTitle = _GLOBALS[_GLOBALS.lang]['21_5'];
			openingID = $(this).attr('href').replace('#','');
		}
		else if($(this).hasClass('note-link-cbeta'))	{	//cbeta修訂註解
			var cbeta_note = $('#back #'+$(this).attr('clue').replace('#',''));
			$('span[class="note-link-cbeta"][clue="'+$(this).attr('clue')+'"]').addClass('active-note');
			htmlContent = '<span>'+cbeta_note.html()+link_cbeta+'</span>';
			mainTitle = _GLOBALS[_GLOBALS.lang]['21_6'];
			openingID = $(this).attr('clue');
		}		
		
		
		//處理小視窗顯示
		if($('#collate-window').is(':visible') && openingID == $('#collate-window').attr('openingID'))	{	//如果點選又是同一個note，則關閉小視窗
			$('#collate-window').hide();
			$('.active-note').removeClass('active-note');
		}		
		else{
			$('#collate-window').css({'left':centerWidth,'top':centerHeight}).attr({'openingID':openingID}).find('div[role="collate-window-content"]').html(htmlContent).end().show().find('[role="main-title"]').html(mainTitle);
		}	

		
		e.preventDefault();
		return false;
		
		/*
		//popup超出螢幕範圍時需調整left
		var screenWidth = parseInt(document.documentElement.clientWidth,10);
		if(e.pageX+parseInt(document.getElementById('collate-window').clientWidth,10) >= screenWidth)	{
			var now_left = parseInt(document.getElementById('collate-window').style.left,10);
			document.getElementById('collate-window').style.left = now_left - (parseInt(document.getElementById('collate-window').clientWidth,10)/2)+'px';
		}
		*/
	});
	
	/*
	//缺字
	$(document).on('click','.gaijiAnchor',function(e)	{
		$('#back #'+$(this).attr('href')).html()
		return false;
	});
	*/
	var scrollAndHighlight = function(container,treeID)	{
		var anchor_obj = $(container+' div.kepan_anchor_s[tree_id="'+treeID+'"]');
		$(container).scrollTo(anchor_obj.get(0),500);
		_GLOBALS['hlObj_'+container.replace('#','')].go(anchor_obj.attr('line_id'),anchor_obj.attr('to'));		
	};	
	
	/*
	//介面調整功能(按鈕於經標題旁邊的舊版)
	$(document).on('click','.interface-adjust-func',function()	{
		var func = $(this).attr('role');
		var activeContainerID = $(this).parents('.content-block').find('.content').attr('id');
		if(func == 'interface-adjust-showLineNo' || func == 'interface-adjust-hideLineNo')	{	//是否顯示行號
			var showLB = (func == 'interface-adjust-showLineNo' ? true:false);
			$('#'+activeContainerID+' .lb').css({'display':showLB ? '':'none'});
			$(this).parents('.dropdown-menu').find('.interface-adjust-enabled-label').hide().end().end().find('.interface-adjust-enabled-label').show();
		}
		else if(func == 'interface-adjust-fontLarger' || func == 'interface-adjust-fontSmaller')	{	//字型大小調整
			var nowFontSize = parseInt($('#'+activeContainerID).find('.text').eq(1).css('font-size'),10);
			if(func == 'interface-adjust-fontLarger')	nowFontSize += 2;
			else if(func == 'interface-adjust-fontSmaller')	nowFontSize -= 2;
			$('#'+activeContainerID).css({'font-size':nowFontSize+'px'});
		}
	});
	*/
	
	//「設定」功能 +
	$('.config-chkbox-hide').click(function()	{
		var func = $(this).attr('id');
		var containers = $.map(_GLOBALS.open_container,function(index,val)	{ return '#'+val+' #replaced';}).join(',');
		
		$.cookie(func,$('#'+func).prop('checked') ? 'yes':'no',{path:'/'});	//每次勾選時用cookie記下設定，下次開啟頁面時使用之
		
		if(func == 'config_breakLine')	{
			interface_adjust_breakLine(containers);	//介面調整：使用func，因為在after_load後需再執行此段，用trigger會有問題故改用func讓它可重複呼叫
		}
		else if(func == 'config_showLineNo')	{
			containers = containers.replace(/#replaced/g,$('#config_breakLine').prop('checked') ? '.lb':'.lineInfo');
			
			var showLB = ($(this).prop('checked') ? true:false);
			$(containers).css({'display':showLB ? '':'none'});			
		}
		else if(func == 'config_contextMenu')	{	//是否顯示內文功能選單
			_GLOBALS.showContextMenu = $(this).prop('checked') ? false:true;
		}
		else if(func == 'config_showVarients')	{	//是否顯示內文校勘
			containers = containers.replace(/#replaced/g,'');
			if($(this).prop('checked'))	{
				$(containers).removeClass('varients-hide').addClass('varients-show');
				//$('#rac_conf_isNote').prop('checked',true);	$('.refAndCopy_note').show();	//引用複製裡面的是否顯示校勘也跟著作動(2022-01-07:取消作動)
			}
			else	{
				$(containers).removeClass('varients-show').addClass('varients-hide');
				//$('#rac_conf_isNote').prop('checked',false);	$('.refAndCopy_note').hide();	//引用複製裡面的是否顯示校勘也跟著作動(2022-01-07:取消作動)
			}
		}
		else if(func == 'config_showPunctuation')	{	//顯示標點
			containers = containers.replace(/#replaced/g,'');
			$.cookie('config_showAIPunctuation',$(this).prop('checked') ? 'yes':'no',{path:'/'});	//與ai標點選項連動，此cookie記下設定，下次開啟頁面時使用之
			if($(this).prop('checked'))	{
				$(containers).removeClass('punctuations-hide').addClass('punctuations-show');
				$(containers).removeClass('ai-punctuations-hide').addClass('ai-punctuations-show');	//與ai標點連動，同時設定ai標點
				$('#config_showAIPunctuation').prop({'checked':true,'disabled':false}).parents('li:eq(0)').removeClass('disabled');	//與ai標點選項連動，設定checked與disabled狀態
				$('#rac_conf_isPunc').prop('checked',true);	$('.refAndCopy_punctuation').show();	//引用複製裡面的是否顯示標點也跟著作動
			}
			else	{
				$(containers).removeClass('punctuations-show').addClass('punctuations-hide');
				$(containers).removeClass('ai-punctuations-show').addClass('ai-punctuations-hide');	//與ai標點連動，同時設定ai標點
				$('#config_showAIPunctuation').prop({'checked':false,'disabled':true}).parents('li:eq(0)').addClass('disabled');	//與ai標點選項連動，設定checked與disabled狀態
				$('#rac_conf_isPunc').prop('checked',false);	$('.refAndCopy_punctuation').hide();	//引用複製裡面的是否顯示標點也跟著作動
			}	
			display_AI_punc_msg();	//有異動時需即時反應ai標點訊息顯示
		}
		else if(func == 'config_showAIPunctuation')	{	//顯示AI標點
			containers = containers.replace(/#replaced/g,'');
			if($(this).prop('checked'))	{
				$(containers).removeClass('ai-punctuations-hide').addClass('ai-punctuations-show');
			}
			else	{
				$(containers).removeClass('ai-punctuations-show').addClass('ai-punctuations-hide');
			}	
			display_AI_punc_msg();	//有異動時需即時反應ai標點訊息顯示
		}
		else if(func == 'config_eyeProtection')	{	//護眼色
			containers = containers.replace(/#replaced/g,'');
			if($(this).prop('checked'))	{
				$(containers).removeClass('theme_3').addClass('theme_3');
			}
			else	{
				$(containers).removeClass('theme_3');
			}			
		}
	});
	
	
	//字形設定by按鈕
	$('#config_fontsize_plus,#config_fontsize_minus').click(function()	{	
		setFontSize($(this).attr('id'));
	});
	
	//字形設定by輸入值
	$('#config_fontsize_text').keyup(function(e)	{
		var code = e.keyCode || e.which; 
		if (code  == 13) {
			setFontSize($(this).attr('id'));
		}
		$(this).val($(this).val().replace(/[^0-9]+/g,''));
	});
	
	//行距設定by按鈕
	$('#config_linespacing_plus,#config_linespacing_minus').click(function()	{	
		setLinespacingSize($(this).attr('id'));
	});
	
	//行距設定by輸入值
	$('#config_linespacing_text').keyup(function(e)	{
		var code = e.keyCode || e.which; 
		if (code  == 13) {
			setLinespacingSize($(this).attr('id'));
		}
		$(this).val($(this).val().replace(/[^0-9\.]+/g,''));
	});
	
	//字距設定by按鈕
	$('#config_letterspacing_plus,#config_letterspacing_minus').click(function()	{	
		setLetterspacingSize($(this).attr('id'));
	});
	
	//行距設定by輸入值
	$('#config_letterspacing_text').keyup(function(e)	{
		var code = e.keyCode || e.which; 
		if (code  == 13) {
			setLetterspacingSize($(this).attr('id'));
		}
		$(this).val($(this).val().replace(/[^0-9]+/g,''));
	});

	//「設定」功能 -
	
	
	//note小視窗拖曳功能 + 
	var gMapFrameDrag = false;
	var gMapFrameDragClickedX = gMapFrameDragClickedY =  0;
	$(document).mousemove(function(e)	{	
		if(gMapFrameDrag)	
			$('#collate-window').css({'left':e.pageX-(gMapFrameDragClickedX),'top':e.pageY-(gMapFrameDragClickedY)});
	});	
	$('#collate-window > div[role="collate-window-header"]').mousedown(function(e)	{	
		//alert(e.target.id)
		gMapFrameDrag = true;
		gMapFrameDragClickedX = e.pageX-parseInt($('#collate-window').css('left'));
		gMapFrameDragClickedY = e.pageY-parseInt($('#collate-window').css('top'));								
		
	});
	$('#collate-window > div[role="collate-window-header"]').mouseup(function()	{	
		gMapFrameDrag = false;
	});			
		
	//note小視窗拖曳功能 -	
	
	//footer垂直拖曳功能 +
	var footerFrameDrag = false;
	var footerFrameDragClickedY = 0;
	$(document).mousemove(function(e)	{	
		if(footerFrameDrag)	{	
			$('#footer_size_adjuster').css({'top':e.pageY-(footerFrameDragClickedY+5)+'px'});
			e.stopPropagation();	//防止事件互相干擾，做完後停止事件傳遞
			return false;
		}
	});	
	$('#footer_size_adjuster').mousedown(function(e)	{
		footerFrameDrag = true;
		$(this).addClass('footer_size_adjuster_move');
		footerFrameDragClickedY = e.pageY-parseInt($(this).css('top'));
		$('#footer_func_ngram_iframe').hide();	//拖曳時iframe會是最上層，會影響到拖曳動作，故先隱藏，等到mouseup完成拖曳時再顯示
		e.stopPropagation();	//防止事件互相干擾，做完後停止事件傳遞
		return false;
	});
	$('#footer_size_adjuster').mouseup(function(e)	{	
		footerFrameDrag = false;
		$('#footer_content .tab-content').css({'height':$(window).height()-($(this).offset().top+41)+'px'});
		$(this).removeClass('footer_size_adjuster_move').css({'top':'0'});
		$('#footer_func_ngram_iframe').show();
		e.stopPropagation();	//防止事件互相干擾，做完後停止事件傳遞
		return false;
		
	});		
	//footer垂直拖曳功能 -

	//整合功能的footer開關
	var slide = false;
	var height = $('#footer_content').height();	
	$('#go_footer').click(function() {
		var docHeight = $(document).height();
		var windowHeight = $(window).height();
		var scrollPos = docHeight - windowHeight + height;
		$('#footer_content').animate({ height: "toggle"}, 400);
		
		if(slide == false) {
			$('html, body').animate({scrollTop: scrollPos+'px'}, 400);
			slide = true;
		} else {
            slide = false;
			
        }
	});	
	
	
	//選擇器相關程式 +
	
	//經目查詢
	$(document).on('click','.selector-side-menu-mulu',function()	{
		$(this).addClass('active').siblings().removeClass('active');
		$('#selector-breadcrumb').html(_GLOBALS[_GLOBALS.lang]['8_4']);
		$('#selector-levels-search,#selector-levels-back-btn,#selector-levels-ajax-loader,#selector-levels,#selector-mulu-search-res,#selector-goto,#selector-author,#selector-date').hide();
		$('#selector-mulu,#selector-mulu-tabpanel').show();
		
	});
	
	//goto查詢
	$(document).on('click','.selector-side-menu-goto',function()	{
		$(this).addClass('active').siblings().removeClass('active');
		$('#selector-breadcrumb').html(_GLOBALS[_GLOBALS.lang]['8_5']);
		$('#selector-levels-search,#selector-levels-back-btn,#selector-levels-ajax-loader,#selector-levels,#selector-mulu,#selector-author,#selector-date').hide();
		$('#selector-goto,.selector-goto-form').show();
		$('.selector-goto-form').each(function()	{	this.reset();	});	//reset每一個form
		
	});	
	
	//作譯者查詢剛進去時
	$(document).on('click','.selector-side-menu-author',function(e)	{
		$(this).addClass('active').siblings().removeClass('active');
		$('#selector-breadcrumb').html('<li><a style="cursor:pointer" onclick="javascript:$(\'.selector-side-menu-author > a\').trigger(\'click\');$(\'#selector-author-search-text\').val(\'\');">'+_GLOBALS[_GLOBALS.lang]['8_6']+'</a></li>');
		$('#selector-levels-search,#selector-levels-back-btn,#selector-levels-ajax-loader,#selector-levels,#selector-mulu,#selector-goto,#selector-date').hide();
		$('#selector-author').show();
		$('#selector-author-search-text').parent().show();
		
		//判斷驅動的來源，如果class是selector-side-menu-author，表示由jquery trigger click，這種的就不做作譯者全部列表，直接return
		//程式在.selector-author-items-person click裡面
		if($(e.target).hasClass('selector-side-menu-author'))	{	
			return;
		}
		
		var html = [['0','<button class="selector-author-items-stroke btn btn-primary"  stroke="all">'+_GLOBALS[_GLOBALS.lang]['14_3']+'</button>']];
		for(var stroke in _GLOBALS.creators_stroke)	{
			html.push([stroke,'<div style="display:table-row"><div style="display:table-cell"><button class="selector-author-items-stroke btn btn-primary"  stroke="'+stroke+'">'+stroke+' '+_GLOBALS[_GLOBALS.lang]['14_4']+(_GLOBALS.lang == 'en' && stroke > 1 ? 's':'')+'</button></div><div style="display:table-cell">'+Object.keys(_GLOBALS.creators_stroke[stroke]).map(function(o)	{ return '<a class="selector-author-items-stroke" stroke="'+stroke+'" first_letter_anchor="'+o+'" style="width:auto">'+o+'</a>'; })+'</div></div>']);
		}
		html = html.sort(function(a,b)	{	return a[0].replace('劃','')-b[0].replace('劃','');});	//使用stroke去除掉「劃」變成數字做排序（即依照筆劃排序）
		html = html.map(function(item)	{	return item[1];});	//在將陣列中的stroke元素去除，回傳純html元素之陣列
		
		$('#selector-author-content').html('<div id="selector-author-items-stroke-table"><h4>'+_GLOBALS[_GLOBALS.lang]['14_2']+'</h4>'+html.join('')+'</div>');
	});	
	
	$(document).on('click','.selector-author-items-stroke',function(e)	{
		$('#selector-author-search-text').parent().hide();
		var stroke = $(this).attr('stroke');		
		var first_letter_anchor = $(this).attr('first_letter_anchor');	//判斷是否有first_letter_anchor，有的話需做scroll和hl
		var first_letter_data = _GLOBALS.creators_stroke[stroke];
		var html = [];
		for(var first_letter in _GLOBALS.creators_stroke[stroke])	{
			var persons = [];
			for(var j in _GLOBALS.creators_stroke[stroke][first_letter])	{
				persons.push('<a class="selector-author-items-person"  pname="'+_GLOBALS.creators_stroke[stroke][first_letter][j].split(':')[0]+'" pid="'+_GLOBALS.creators_stroke[stroke][first_letter][j].split(':')[1]+'">'+_GLOBALS.creators_stroke[stroke][first_letter][j].split(':')[0]+'</a><span class="selector-author-open-adb" pid="'+_GLOBALS.creators_stroke[stroke][first_letter][j].split(':')[1]+'"></span>');
			}
			html.push('<div class="selector-author-items"><div class="page-header"><span>'+first_letter+'</span></div>'+persons.join('<br/>')+'</div>');	
		}
		
		if(stroke == 'all')	{
			for(var i in _GLOBALS.creators_stroke)	{
				for(var j in _GLOBALS.creators_stroke[i])	{
					var persons = [];
					for(var k in _GLOBALS.creators_stroke[i][j])	{
						persons.push('<a class="selector-author-items-person"  pname="'+_GLOBALS.creators_stroke[i][j][k].split(':')[0]+'" pid="'+_GLOBALS.creators_stroke[i][j][k].split(':')[1]+'">'+_GLOBALS.creators_stroke[i][j][k].split(':')[0]+'</a><span class="selector-author-open-adb" pid="'+_GLOBALS.creators_stroke[i][j][k].split(':')[1]+'"></span>');
					}
					html.push('<div class="selector-author-items"><div class="page-header">'+j+'</div>'+persons.join('<br/>')+'</div>');
				}	
			}
		}		
		$('#selector-author-content').html(html.join(''));		
		$('#selector-breadcrumb').html('<li><a style="cursor:pointer" onclick="javascript:$(\'.selector-side-menu-author > a\').trigger(\'click\')">'+_GLOBALS[_GLOBALS.lang]['8_6']+'</a></li>'+'<li><a class="selector-author-items-stroke" stroke="'+stroke+'">'+(stroke == 'all' ? _GLOBALS[_GLOBALS.lang]['14_6']:_GLOBALS[_GLOBALS.lang]['14_7']+stroke+' '+_GLOBALS[_GLOBALS.lang]['14_4'])+'</a></li>');
		
		if(first_letter_anchor)	{	//有first_letter_anchor，有的話需做scroll和hl
			var scroll_target = $('.selector-author-items > .page-header > span:contains("'+first_letter_anchor+'")').addClass('hl hl_bk');
			$('#jing_selector #selector-content').scrollTo(scroll_target);			
		}
	});
	
	
	//作譯者查詢，點擊人名時
	$(document).on('click','.selector-author-items-person',function(e)	{
		if($('#jing_selector').is(':hidden'))	{	//如果selector沒開，表示是用jq trigger啟動，因此這邊要手動將modal顯示
			//$('.selector-side-menu-author[desc="author"]').trigger('click');
			jing_selector_show(true);
		}

		var person_btn_name = $(this).attr('pname');
		var person_names = $(this).attr('names');
		var pid = $(this).attr('pid');
		
		$('.selector-side-menu-author[desc="author"]').trigger('click');
		$('#selector-author-search-text').parent().hide();
		$('.selector-author-search-result').remove();
		$('#selector-author-content').html('<h3>Loading...</h3>');		
	
		$.getJSON(_GLOBALS.catalogAPI+'/works?creator_id='+encodeURIComponent(pid)+'&callback=?',function(json)	{
			json = json.results;
			var export_btn = '<div class="dropdown pull-right" style="margin-left:auto"><button class="btn btn-default btn-xs dropdown-toggle" type="button" id="" data-toggle="dropdown"><span class="glyphicon glyphicon-export"></span>&nbsp;&nbsp;'+_GLOBALS[_GLOBALS.lang]['2_9']+'<span class="caret"></span></button><ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1"><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(null)" onclick="javascript:$(\'#selector-author-search-table\').tableExport({type:\'excel\',escape:\'false\',winxdFileName:\'CBETA_export_'+new Date().YMDHIS('mm-dd-hh-ii')+'\',defaultTitle:\''+_GLOBALS[_GLOBALS.lang]['17_7']+' &lt;span style=color:red&gt;'+person_btn_name+'&lt;/span&gt;\'});">Excel</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(null)" onclick="javascript:$(\'#selector-author-search-table\').tableExport({type:\'csv\',escape:\'false\',winxdFileName:\'CBETA_export_'+new Date().YMDHIS('mm-dd-hh-ii')+'\',defaultTitle:\''+_GLOBALS[_GLOBALS.lang]['17_7']+' &lt;span style=color:red&gt;'+person_btn_name+'&lt;/span&gt;\'});">CSV</a></li></ul></div>';
			var html = '<div class="table-responsive" style="width:100%"><table id="selector-author-search-table" class="table table-hover table-condensed"><thead><tr><th>'+_GLOBALS[_GLOBALS.lang]['17_4']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['17_5']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['17_6']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['13_6']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['17_7']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['17_8']+'</th></tr></thead><tbody>';
			for(var index in json)	{
				html += '<tr xml_id="'+json[index]['file']+'"  jing_title="'+json[index]['work']+' '+json[index]['title']+'" juan="'+json[index]['juan_start']+'"><td>'+json[index]['category']+'</td><td>'+json[index]['file']+'</td><td>'+json[index]['title']+'</td><td>'+json[index]['juan']+'</td><td>'+json[index]['creators']+'</td><td>'+(json[index]['time_from'] ? json[index]['time_from']:'?')+' ~ '+(json[index]['time_to'] ? json[index]['time_to']:'?')+'</td></tr>';
			}
			html += '</tbody></table></div>';
					
			$('#selector-breadcrumb').append('<li>'+person_btn_name+' <span pid="'+pid+'" class="selector-author-open-adb"></span>'+(person_names ? '<span class="selector-author-names">('+person_names+')</span>':'')+'</li>');
			$('#selector-author-content').html(export_btn+html).show();				
		});	
	});	
	
	//作譯者查詢，click id開啟規範資料庫
	$(document).on('click','.selector-author-open-adb',function()	{
		var aid = $(this).attr('pid');
		window.open(_GLOBALS.authorityROOT+'person/?fromInner='+aid);
	});
	
	//日期查詢
	$(document).on('click','.selector-side-menu-date',function()	{
		$(this).addClass('active').siblings().removeClass('active');
		$('#selector-breadcrumb').html(_GLOBALS[_GLOBALS.lang]['8_7']);
		$('#selector-levels-search,#selector-levels-back-btn,#selector-levels-ajax-loader,#selector-levels,#selector-mulu,#selector-goto,#selector-author').hide();
		$('#selector-date,#selector-date-tabpanel').show();
		$('#selector-date-search-res').hide();
		
	});	

	//日期查詢 - 朝代列表
	$(document).on('click','.selector-dates-btn',function()	{
		var selected_dy = $(this).text().replace(/\([^\)]+\)/g,'');
		var dynasty = $(this).attr('dy');
		var thisBtn = $(this);
		var ajax_load_type_2 = $('<span/>').addClass('ajax-loader-type-2').css('visibility','visible').appendTo(thisBtn.find('span:eq(0) > span.selector-dates-dy-count'));
		
		$.getJSON(_GLOBALS.catalogAPI+'/works?dynasty='+encodeURIComponent(dynasty)+'&callback=?',function(res)	{
			var json = res.results;	
			/*
			res.results.sort(function(a, b) {	//依據time_from進行排序
				return parseFloat(a.time_from) - parseFloat(b.time_from);
			});	
			*/
			res.results.sort(SORT_MULTI_COL('time_from', {name:'file', primer: String, reverse: false}));	//先照time_from排序、再照file排
			
			//var export_btn = '<button onclick="javascript:$(\'#selector-dynasty-search-table\').tableExport({type:\'excel\',escape:\'false\',winxdFileName:\'CBETA_export_'+new Date().YMDHIS('mm-dd-hh-ii')+'\',defaultTitle:\''+_GLOBALS[_GLOBALS.lang]['17_9']+' &lt;span style=color:red&gt;'+selected_dy+'&lt;/span&gt;\'});" id="" type="button" class="btn btn-default btn-xs dropdown-toggle pull-right" style="margin-left: auto;"><span class="glyphicon glyphicon-export"></span>&nbsp;&nbsp;'+_GLOBALS[_GLOBALS.lang]['17_3']+' </button>';
			var export_btn = '<div class="dropdown pull-right"><button class="btn btn-default btn-xs dropdown-toggle" type="button" id="" data-toggle="dropdown"><span class="glyphicon glyphicon-export"></span>&nbsp;&nbsp;'+_GLOBALS[_GLOBALS.lang]['2_9']+'<span class="caret"></span></button><ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1"><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(null)" onclick="javascript:$(\'#selector-dynasty-search-table\').tableExport({type:\'excel\',escape:\'false\',winxdFileName:\'CBETA_export_'+new Date().YMDHIS('mm-dd-hh-ii')+'\',defaultTitle:\''+_GLOBALS[_GLOBALS.lang]['17_9']+' &lt;span style=color:red&gt;'+selected_dy+'&lt;/span&gt;\'});">Excel</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(null)" onclick="javascript:$(\'#selector-dynasty-search-table\').tableExport({type:\'csv\',escape:\'false\',winxdFileName:\'CBETA_export_'+new Date().YMDHIS('mm-dd-hh-ii')+'\',defaultTitle:\''+_GLOBALS[_GLOBALS.lang]['17_9']+' &lt;span style=color:red&gt;'+selected_dy+'&lt;/span&gt;\'});">CSV</a></li></ul></div>';
			var html = '<div class="table-responsive" style="width:100%"><table id="selector-dynasty-search-table" class="table table-hover table-condensed"><thead><tr><th>'+_GLOBALS[_GLOBALS.lang]['17_4']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['17_5']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['17_6']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['13_6']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['17_7']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['17_8']+'</th></tr></thead><tbody>';
			for(var index in json)	{
				html += '<tr xml_id="'+json[index]['file']+'"  jing_title="'+json[index]['work']+' '+json[index]['title']+'" juan="'+json[index]['juan_start']+'"><td>'+json[index]['category']+'</td><td>'+json[index]['file']+'</td><td>'+json[index]['title']+'</td><td>'+json[index]['juan']+'</td><td>'+(json[index]['creators'] ? json[index]['creators']:'')+'<td>'+(json[index]['time_from'] ? json[index]['time_from']:'?')+' ~ '+(json[index]['time_to'] ? json[index]['time_to']:'?')+'</td>'+'</td></tr>';
			}
			html += '</tbody></table></div>';
					
			$('#selector-breadcrumb').html(_GLOBALS[_GLOBALS.lang]['17_9']+' 「<span style="color:red">'+selected_dy+'</span>」，'+_GLOBALS[_GLOBALS.lang]['12_1_1']+' <span style="color:red">'+res.num_found+'</span> '+_GLOBALS[_GLOBALS.lang]['12_1_2']+' (<a onclick="javascript:$(\'.selector-side-menu-date\').trigger(\'click\')" style="" href="javascript:void(null)">'+_GLOBALS[_GLOBALS.lang]['17_2']+'</a>)');
			$('#selector-date-search-res').html(export_btn+html).show();
			$('#selector-date-tabpanel').hide();
			thisBtn.find('.ajax-loader-type-2').remove();
		});
	});
	
	//日期查詢 - 西元年範圍
	$('#selector-date-submit').click(function()	{
		var thisBtn = $(this);
		if($.trim($('#selector-date-ad-start').val()) == '' || $.trim($('#selector-date-ad-end').val()) == '')	{
			alert(_GLOBALS[_GLOBALS.lang]['21_8']);
			return false;
		}
		thisBtn.next('.ajax-loader-type-2').css('visibility','visible');
		$.getJSON(_GLOBALS.catalogAPI+'/works?time_start='+encodeURIComponent($.trim($('#selector-date-ad-start').val()))+'&time_end='+encodeURIComponent($.trim($('#selector-date-ad-end').val()))+'&callback=?',function(res)	{
			var json = res.results;
			/*
			res.results.sort(function(a, b) {	//依據time_from進行排序
				return parseFloat(a.time_from) - parseFloat(b.time_from);
			});
			*/
			res.results.sort(SORT_MULTI_COL('time_from', {name:'file', primer: String, reverse: false}));	//先照time_from排序、再照file排
			
			//var export_btn = '<button onclick="javascript:$(\'#selector-ad-search-table\').tableExport({type:\'excel\',escape:\'false\',winxdFileName:\'CBETA_export_'+new Date().YMDHIS('mm-dd-hh-ii')+'\',defaultTitle:\''+_GLOBALS[_GLOBALS.lang]['17_8']+' &lt;span style=color:red&gt;'+$.trim($('#selector-date-ad-start').val())+' ~ '+$.trim($('#selector-date-ad-end').val())+'&lt;/span&gt;\'});" id="" type="button" class="btn btn-default btn-xs dropdown-toggle pull-right" style="margin-left: auto;"><span class="glyphicon glyphicon-export"></span>&nbsp;&nbsp;'+_GLOBALS[_GLOBALS.lang]['17_3']+' </button>';
			var export_btn = '<div class="dropdown pull-right"><button class="btn btn-default btn-xs dropdown-toggle" type="button" id="" data-toggle="dropdown"><span class="glyphicon glyphicon-export"></span>&nbsp;&nbsp;'+_GLOBALS[_GLOBALS.lang]['2_9']+'<span class="caret"></span></button><ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1"><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:$(\'#selector-ad-search-table\').tableExport({type:\'excel\',escape:\'false\',winxdFileName:\'CBETA_export_'+new Date().YMDHIS('mm-dd-hh-ii')+'\',defaultTitle:\''+_GLOBALS[_GLOBALS.lang]['17_8']+' &lt;span style=color:red&gt;'+$.trim($('#selector-date-ad-start').val())+' ~ '+$.trim($('#selector-date-ad-end').val())+'&lt;/span&gt;\'});">Excel</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(null)" onclick="javascript:$(\'#selector-ad-search-table\').tableExport({type:\'csv\',escape:\'false\',winxdFileName:\'CBETA_export_'+new Date().YMDHIS('mm-dd-hh-ii')+'\',defaultTitle:\''+_GLOBALS[_GLOBALS.lang]['17_8']+' &lt;span style=color:red&gt;'+$.trim($('#selector-date-ad-start').val())+' ~ '+$.trim($('#selector-date-ad-end').val())+'&lt;/span&gt;\'});">CSV</a></li></ul></div>'
			var html = '<div class="table-responsive" style="width:100%"><table id="selector-ad-search-table" class="table table-hover table-condensed"><thead><tr><th>'+_GLOBALS[_GLOBALS.lang]['17_4']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['17_5']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['17_6']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['13_6']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['17_7']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['17_8']+'</th></tr></thead><tbody>';
			for(var index in json)	{
				html += '<tr xml_id="'+json[index]['file']+'"  jing_title="'+json[index]['work']+' '+json[index]['title']+'" juan="'+json[index]['juan_start']+'"><td>'+json[index]['category']+'</td><td>'+json[index]['file']+'</td><td>'+json[index]['title']+'</td><td>'+json[index]['juan']+'</td><td>'+(json[index]['creators'] ? json[index]['creators']:'')+'</td><td>'+(json[index]['time_from'] ? json[index]['time_from']:'?')+' ~ '+(json[index]['time_to'] ? json[index]['time_to']:'?')+'</td></tr>';
			}
			html += '</tbody></table></div>';
					
			$('#selector-breadcrumb').html(_GLOBALS[_GLOBALS.lang]['16_1']+': [<span style="color:red">'+$.trim($('#selector-date-ad-start').val()) +' ~ '+$.trim($('#selector-date-ad-end').val())+'</span>]，'+_GLOBALS[_GLOBALS.lang]['17_10']+' <span style="color:red">'+res.num_found+'</span> '+_GLOBALS[_GLOBALS.lang]['17_11']+' (<a onclick="javascript:$(\'.selector-side-menu-date\').trigger(\'click\')" style="" href="javascript:void(null)">'+_GLOBALS[_GLOBALS.lang]['17_2']+'</a>)');
			$('#selector-date-search-res').html(export_btn+html).show();
			$('#selector-date-tabpanel').hide();
			thisBtn.next('.ajax-loader-type-2').css('visibility','hidden');
		});
	});	
	
	
	

	
	$(document).on('click','.selector-side-menu',function()	{
		$(this).addClass('active').siblings().removeClass('active');
		$('#selector-levels-search,#selector-levels-ajax-loader,#selector-levels').show();
		$('#selector-mulu,#selector-goto,#selector-author,#selector-date,#selector-levels-back-btn').hide();
		var text = $(this).text();
		var desc = $(this).attr('desc');
		var query_param = 'CBETA';	//預設是部類查詢，第一層，參數給cbeta
		_GLOBALS.jing_selector_method = desc;
		
		if(desc == 'vol')	{	//原書目錄
			bulei_vol_resHandle(text,desc,'orig');
		}
		else if(desc == 'bulei')	{	//部類
			bulei_vol_resHandle(text,desc,query_param);
		}
	});
	
	$(document).on('click','.selector-levels-vol-sw-btn,.selector-levels-vol-collection-title',function()	{		//藏別切換按鈕+選擇器-冊別上方連結 click
		$('#selector-levels-vol-selected-collection').val($(this).attr('collection'));	//紀錄目前選擇的collection		
		bulei_vol_resHandle($(this).attr('text'),$(this).attr('desc'),$(this).attr('collection'));
	});

	
	$(document).on('click','.selector-multi-level-btn',function()	{
		var level = parseInt($(this).attr('level'),10)+1;
		var text = $(this).text();
		var n = $(this).attr('n');
		var class_name = $('#selector-menu-bar > li.active').attr('desc');
		var is_top_menu = $(this).hasClass('selector-top-menu') ? true:false;	//是否為上方的階層式選單(.selector-breadcrumb)
		
		$('#selector-levels-ajax-loader > .ajax-loader-type-1').css('visibility','visible');
		$.getJSON(_GLOBALS.catalogAPI+'/catalog_entry?q='+n+'&callback=?',function(res)	{
			var json = res.results;
			var html = '<div style="float:left;width:410px">';
			var counter = 0;
			if(is_top_menu)	{
				var breadcrumb_html = [];
				$('#selector-breadcrumb > li').slice(0,level).each(function()	{	//如果使用的是.selector-breadcrumb選單，則需要做裁剪(slice)的動作再塞回去
					breadcrumb_html.push($("<div />").append($(this).clone()).html());	//get li self html(含自己的html)
				});
				$('#selector-breadcrumb').html(breadcrumb_html.join(''));
			}	
			else	{	//使用按鈕一直點下去選的，只要一直append下去就好
				$('#selector-breadcrumb').append('<li level="'+(level-1)+'" class="selector-multi-level-btn selector-top-menu" n="'+n+'"><a>'+text+'</a></li>');
			}
				
			for(var i in json)	{
				if(counter++ == Math.round(Object.size(json) / 2))	html += '</div><div style="float:left;width:410px">';
			
				if(json[i]['file'])		//有file的話表示為最後一層，點了要準備開啟經
					html += '<button type="button" level="'+level+'" class="btn btn-default btn-sm selector-btn-final" label="'+json[i]['label']+'" xml_id="'+json[i]['file']+'"  '+(json[i]['node_type'] == 'html' ? 'data-open-new="yes"':'')+'  juan_start="'+(json[i]['juan_start'] ? json[i]['juan_start']:1)+'" '+(json[i]['alt_title'] ? 'alt_title="'+json[i]['alt_title']+'" alt_word="'+json[i]['alt_work']+'"':'')+'  '+(json[i]['lb'] ? 'lb="'+json[i]['lb']+'"':'')+'><span class="glyphicon glyphicon-file"></span>'+json[i]['label']+'</button>';
				else					//否則的話就是一般目錄
					html += '<button type="button" level="'+level+'" class="btn btn-default btn-sm selector-multi-level-btn" label="'+json[i]['label']+'" n="'+json[i]['n']+'"><span class="glyphicon glyphicon-folder-open"></span>'+json[i]['label']+'</button>';
			}
			html += '</div>';
			$('#selector-levels').html(html);	
			$('#selector-levels-ajax-loader > .ajax-loader-type-1').css('visibility','hidden');
			$('#selector-levels-back-btn').show();
		});
	});
	
	var selector_btn_final_open = function(xml_id,btn_text,juan_start,lb)	{	//此func為經選擇器最後一層按鈕要開啟經使用
		if(isOpenNewWindow(xml_id,juan_start,btn_text,false))	{	//如果將經文開在新視窗，下面也不用跑了，直接return false
			jing_selector_show(false);
			return false;	
		}
	
		update_tree(xml_id);	//更新左邊樹狀目錄	
		push_historyState(xml_id,juan_start,btn_text,false);
		updateTitle_openJing(xml_id,btn_text,juan_start,false,lb);	//重設標題+開啟經+指定卷		
	};
	
	$(document).on('click','.selector-btn-final',function(e)	{	//點選最後一層，要開啟經了
		var xml_id = $(this).attr('xml_id');
		var btn_text = $(this).text().replace(/\([0-9]+卷\)/g,'');	//把標題結尾的"(x卷)"這種字串刪除
		var juan_start = $(this).attr('juan_start');
		var lb = $(this).attr('lb') ? xml_id+'_p'+$(this).attr('lb'):false;
		
		
		if($(this).data('open-new') === 'yes')	{	//如果有data-open-new屬性表示需另開視窗顯示，不做原本開啟經的行為
			window.open(_GLOBALS.catalogAPI+xml_id);
		}
		else	{
			selector_btn_final_open(xml_id,btn_text,juan_start,lb);
		}
		
	});
	
	//經目查詢(經號、冊號)送出按鈕
	$('#selector-mulu-submit').click(function()	{
		var thisBtn = $(this);
		thisBtn.next('.ajax-loader-type-2').css('visibility','visible');
		$.getJSON(_GLOBALS.catalogAPI+'works?'+$('#selector-mulu-form :input').filter(function(index, element) {	return $(element).val() != "";}).serialize()+'&callback=?',function(res)	{
			var json = res.results;
			var html = '<div class="table-responsive"><table id="selector-mulu-search-table" class="table table-hover table-condensed"><thead><tr><th>'+_GLOBALS[_GLOBALS.lang]['2_14']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['2_11']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['2_12']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['13_6']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['2_13']+'</th></tr></thead><tbody>';
			for(var index in json)	{
				if(!json[index]['file'])	continue;	//T=X這種跳過不顯示
				html += '<tr xml_id="'+json[index]['file']+'"  jing_title="'+json[index]['work']+' '+json[index]['title']+'" juan="'+json[index]['juan_start']+'" '+(json[index]['alt'] ? 'alt_info="'+json[index]['alt']+'"':'')+'><td>'+json[index]['category']+'</td><td>'+(json[index]['file'] ? json[index]['file']:'')+'</td><td>'+json[index]['title']+'</td><td>'+(json[index]['juan'] ? json[index]['juan']:'')+'</td><td>'+(json[index]['creators'] ? json[index]['creators']:'')+'</td></tr>';
			}
			html += '</tbody></table></div>';
				
			$('#selector-breadcrumb').html(_GLOBALS[_GLOBALS.lang]['12_1_1']+' <span style="color:red">'+json.length+'</span> '+_GLOBALS[_GLOBALS.lang]['12_1_2']+' (<a href="javascript:void(null)" style="" onclick="javascript:$(\'.selector-side-menu-mulu\').trigger(\'click\')">'+_GLOBALS[_GLOBALS.lang]['12_2']+'</a>)');
			$('#selector-mulu-tabpanel').hide();
			$('#selector-mulu-search-res').html(html).show();
			thisBtn.next('.ajax-loader-type-2').css('visibility','hidden');
		});
		
	});
	
	//經目查詢-(經號、冊號)直接按enter可以啟動 - 使用form submit trick實現
	$('#selector-mulu-form').submit(function()	{
		$('#selector-mulu-submit').trigger('click');
		return false;
	});
	
	//經目查詢(關鍵字)送出按鈕
	$('#selector-mulu-title-submit').click(function()	{
		var thisBtn = $(this);
		var kw = $.trim($('#selector-mulu-title').val());
		var hl_kw = function(t)	{
			return t.replace(new RegExp(kw,'gi'),'<span class="hl">'+kw+'</span>');
		};
		
		thisBtn.next('.ajax-loader-type-2').css('visibility','visible');
		
		$.getJSON(_GLOBALS.catalogAPI+'search/toc?q='+encodeURIComponent($.trim($('#selector-mulu-title').val()))+'&callback=?',function(res)	{
			var json = res.results;
			//var export_btn = '<button onclick="javascript:$(\'#selector-mulu-search-table\').tableExport({type:\'excel\',escape:\'false\',winxdFileName:\'CBETA_export_'+new Date().YMDHIS('mm-dd-hh-ii')+'\',defaultTitle:\'Search &lt;span style=color:red&gt;'+$('#selector-mulu-title').val()+'&lt;/span&gt;\'});" id="" type="button" class="btn btn-default btn-xs dropdown-toggle pull-right" style="margin-left: auto;"><span class="glyphicon glyphicon-export"></span>&nbsp;&nbsp;'+_GLOBALS[_GLOBALS.lang]['2_9']+' </button>';
			var export_btn = '<div class="dropdown pull-right"><button class="btn btn-default btn-xs dropdown-toggle" type="button" id="" data-toggle="dropdown"><span class="glyphicon glyphicon-export"></span>&nbsp;&nbsp;'+_GLOBALS[_GLOBALS.lang]['2_9']+'<span class="caret"></span></button><ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1"><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(null)" onclick="javascript:$(\'#selector-mulu-search-table\').tableExport({type:\'excel\',escape:\'false\',winxdFileName:\'CBETA_export_'+new Date().YMDHIS('mm-dd-hh-ii')+'\',defaultTitle:\'Search &lt;span style=color:red&gt;'+$('#selector-mulu-title').val()+'&lt;/span&gt;\'});">Excel</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(null)" onclick="javascript:$(\'#selector-mulu-search-table\').tableExport({type:\'csv\',escape:\'false\',winxdFileName:\'CBETA_export_'+new Date().YMDHIS('mm-dd-hh-ii')+'\',defaultTitle:\'Search &lt;span style=color:red&gt;'+$('#selector-mulu-title').val()+'&lt;/span&gt;\'});">CSV</a></li></ul></div>';
			var html = '<div class="table-responsive"><table id="selector-mulu-search-table" class="table table-hover table-condensed"><thead><tr><th>'+_GLOBALS[_GLOBALS.lang]['2_14']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['12_4']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['2_12']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['12_6']+'</th><th>'+_GLOBALS[_GLOBALS.lang]['2_13']+'</th></tr></thead><tbody>';
			var res_counter = 0;
			for(var index in json)	{
				if(json[index].type != 'work' && json[index].type != 'toc')	continue;	//僅接受work 和 toc
				json[index]['juan_start'] = json[index]['juan_start'] ? json[index]['juan_start']:1;	//juan_start = 第幾卷，如果沒有預設開第一卷
				//json[index]['lb'] = json[index]['lb'] ? json[index]['file']+(json[index]['work'].substr(-1).match(/[A-Za-z]/u) ? '':'_')+'p'+json[index]['lb']:'';	//lb = 要不要scoll，直接給行號
				json[index]['linehead'] = json[index]['linehead'] ? json[index]['linehead']:'';	//linehead = 要不要scoll，直接給行號
				html += '<tr xml_id="'+json[index]['file']+'" juan="'+json[index]['juan_start']+'" to="'+json[index]['linehead']+'" jing_title="'+json[index]['work']+' '+json[index]['title']+'" '+(json[index]['alt'] ? 'alt_info="'+json[index]['alt']+'"':'')+'><td>'+json[index]['category']+'</td><td>'+(json[index]['work'] ? json[index]['work']:'')+'</td><td>'+hl_kw(json[index]['title'])+(json[index]['juan'] ? '('+json[index]['juan']+' '+_GLOBALS[_GLOBALS.lang]['2_3']+')':'')+'</td><td>'+(json[index]['label_path'] ? hl_kw(json[index]['label_path']):'')+'</td><td>'+(json[index]['creators'] ? json[index]['creators']:'')+'</td></tr>';
				res_counter++;
			}
			html += '</tbody></table></div>';
				
			$('#selector-breadcrumb').html(_GLOBALS[_GLOBALS.lang]['12_1_1']+' <span style="color:red">'+res_counter+'</span> '+_GLOBALS[_GLOBALS.lang]['12_1_2']+' (<a href="javascript:void(null)" style="" onclick="javascript:$(\'.selector-side-menu-mulu\').trigger(\'click\')">'+_GLOBALS[_GLOBALS.lang]['12_2']+'</a>)');
			$('#selector-mulu-tabpanel').hide();
			$('#selector-mulu-search-res').html(export_btn+html).show();
			thisBtn.next('.ajax-loader-type-2').css('visibility','hidden');
			
			//經目查詢加入異體字建議
			$.getJSON(_GLOBALS.catalogAPI+'search/variants?q='+encodeURIComponent($.trim(kw))+'&scope=title&callback=?',function(res)	{
				var variants_list = [];
				for(var vi in res.results)	{
					variants_list.push('<a href="javascript:void(null)" class="selector-mulu-title-search-variants" data-kw="'+res.results[vi].q+'">'+res.results[vi].q+'</a>');
				}
				if(variants_list.length > 0)	{
					$('#selector-mulu-search-res').prepend('<div><strong style="float:left" data-lang-id="25_3">'+_GLOBALS[_GLOBALS.lang]['25_3']+'：</strong><div>'+variants_list.join('、')+'</div></div>');
				}
			});
			
		});		
	});
	//經目查詢(關鍵字) 按下 enter trigger
	$('#selector-mulu-title').keyup(function(e)	{
		if(e.which == 13)	$('#selector-mulu-title-submit').trigger('click');
	});	
	
	//經目查詢(關鍵字)裡面的異體字建議click
	$(document).on('click','.selector-mulu-title-search-variants',function()	{
		$('.selector-side-menu-mulu').trigger('click');	//回上一頁
		$('#selector-mulu-title').val($(this).data('kw'));	//經目查詢搜尋框填入本異體字關鍵字
		$('#selector-mulu-title-submit').trigger('click');	//啟動經目查搜尋
	});
	
	//經目查詢、作譯者查詢、日期查詢-表格開啟經
	$(document).on('click','#selector-mulu-search-table tr,#selector-author-search-table tr,#selector-dynasty-search-table tr,#selector-ad-search-table tr',function()	{
		//平行文本(X0002=Txxx)使用對話框處理，處理完就return false中止程序 +
		if(false && $.trim($(this).attr('alt_info')) != '')	{	//(4/19發表會後在上)
			var original_jing_title = $(this).attr('jing_title');
			$.getJSON(_GLOBALS.catalogAPI+'works?work='+encodeURIComponent($.trim($(this).attr('alt_info')))+'&callback=?',function(res)	{
				if(res.results.length == 1)	{
					var parallax_jing = res.results[0];
					$("#parallax_XT_dialog").find('div').html('「'+original_jing_title+'」與「'+parallax_jing.file+' '+parallax_jing.title+'」是重複的，<br/>即將開啟的是「<span style="color:red">'+parallax_jing.file+' '+parallax_jing.title+'</span>」').end().dialog({
						resizable: false,
						modal: true,
						closeText: "hide",
						title:original_jing_title,
						width: 600,
						buttons: {
							"確定": function() {
								selector_btn_final_open(parallax_jing.file,parallax_jing.title,parallax_jing.juan_start);
								$('#parallax_XT_dialog').dialog("close");
							},
							'取消': function() {
								$('#parallax_XT_dialog').dialog("close");
							}
						}
					});
						
					$('#parallax_XT_dialog').prev('.ui-dialog-titlebar').find('.ui-dialog-titlebar-close').remove().end().parent('.ui-dialog').css('z-index','1060').prev('.ui-widget-overlay').css('z-index','1060');
				}
				else	{
					alert('開啟平行文本時發生錯誤。');
				}
			});
			return false;
		}
		//平行文本處理 -
		
		
		//這邊接收經目查詢-關鍵字、冊號，兩種結果的開啟經，關鍵字可能需要指定卷+scroll，冊號的都開啟第一卷、不scroll，皆由attr值來判斷
		var isScroll = $(this).attr('to') ? $(this).attr('to'):false;
		var juanNo = $(this).attr('juan') ? $(this).attr('juan'):false;
		
		if(!$(this).attr('xml_id') || $(this).attr('xml_id') == 'undefined')	{	//連xml_id都沒有表示可能是平行文本或是有問題，直接顯示錯誤訊息並跳開
			alert("此經不存在，請重新調整搜尋條件\nThis file does not exist, please check search texts.");
			return false;
		}
		
		
		if(isOpenNewWindow($(this).attr('xml_id'),juanNo,$(this).attr('jing_title'),isScroll))	{	//如果將經文開在新視窗，下面也不用跑了，直接return false
			jing_selector_show(false);
			return false;	
		}
		
		update_tree($(this).attr('xml_id'));	//更新左邊樹狀目錄
		push_historyState($(this).attr('xml_id'),juanNo,$(this).attr('jing_title'),isScroll);
		updateTitle_openJing($(this).attr('xml_id'),$(this).attr('jing_title'),juanNo,false,isScroll);	//重設標題+開啟經+指定卷+不做highlight+視需要scroll
	});
	
	//goto 查詢送出按鈕(群)
	$('.selector-goto-submits').click(function()	{
		var thisBtn = $(this);
		var target_form = $(this).attr('related_form');
		//必填欄位check +
		if(target_form == 'selector-goto-book-form' && $.trim($('#selector-goto-book-form input[name="selector-goto-work"]').val()) == '')	{
			alert(_GLOBALS[_GLOBALS.lang]['21_9']);
			return false;
		}
		if(target_form == 'selector-goto-realbook-form' && $.trim($('#selector-goto-realbook-form input[name="selector-goto-vol"]').val()) == '')	{
			alert(_GLOBALS[_GLOBALS.lang]['21_10']);
			return false;
		}
		if(target_form == 'selector-goto-lineno-form' && $.trim($('#selector-goto-lineno-form :input:eq(0)').val()) == '')	{
			alert(_GLOBALS[_GLOBALS.lang]['21_11']);
			return false;		
		}
		//必填欄位check -
			
		if(target_form != 'selector-goto-lineno-form')	{	//target_form 不為selector-goto-lineno-form才需要做補0（即書本結構和經卷結構查詢），否則是直接行號查詢不用補0
			//幫數字欄位依照maxlength補0
			$('#'+target_form+' :input[type="text"]').each(function()	{	
				if($.trim($(this).val()) != '')
					$(this).val(PadDigits($(this).val(),$(this).attr('wordlength')));
			});
		}
			
		/*	
		else	{	//goto 查詢預處理
			//2015-11-27:goto改成cbeta api，大般若經個案就不用了
			if($('input[name="selector-goto-line_no"]').val().match(/(T0[567]n0220)[a-z]_(p[0-9]{4}[a-c][0-9]{2})/g))	{	//大般若經個案處理，將輸入如T06n0220「b」_p0001a08的b去掉
				$('input[name="selector-goto-line_no"]').val(RegExp.$1+'_'+RegExp.$2);
			}
		}
		*/
		
		
		goto_params = $('#'+target_form).serialize().split('&').filter(function(elm)	{	return $.trim(elm.split('=')[1]) != ''; }).join('&').replace(/selector-goto-/g,'');	//去除值為空白的參數、並將selector-goto-取代掉（ray api不處理上述兩個格式）
		if(target_form == 'selector-goto-lineno-form')	goto_params = 'linehead='+$.trim($('input[name="selector-goto-linehead"]').val());	//如果是經號goto，不需要做serialize()，在這邊另外處理，因做serialize似乎會auto-escape，造成ray那邊非預期的結果
		
		thisBtn.next('.ajax-loader-type-2').css('visibility','visible');
		$.getJSON(_GLOBALS.catalogAPI+'juans/goto?'+goto_params+'&callback=?',function(res)	{
			thisBtn.next('.ajax-loader-type-2').css('visibility','hidden');
			if(!res || res.error)	{
				if(res.error.message)
					alert(res.error.message)
				else
					alert(_GLOBALS[_GLOBALS.lang]['21_12']+$.trim($('input[name="selector-goto-linehead"]').val()));
				return false;
			}
			res = res.results[0];
			
			var xml_id = res.file;
			var title = res.work+' '+res.title;
			var juan = parseInt(res.juan,10);
			//var scroll = res.lb ? (xml_id+(res.work.substr(-1).match(/[A-Za-z]/u) ? '':'_')+'p'+res.lb):false;	//goto 查詢的scroll回來可能只有頁碼，沒有明確的col和line，在updateTitle_openJing裡面有額外針對此情況處理
			scroll = res.linehead ? res.linehead:false;	//2023-07-10:改用 api 新增的 linehead
			
			if(isOpenNewWindow(xml_id,juan,title,scroll,(target_form == 'selector-goto-lineno-form' && scroll ? scroll:false)))	{	//如果將經文開在新視窗，下面也不用跑了，直接return false
				jing_selector_show(false);
				return false;	
			}
	
			update_tree(xml_id);	//更新左邊樹狀目錄
			updateTitle_openJing(xml_id,title,juan,false,scroll);	//重設標題+開啟經+指定卷+不做highlight+需要scroll到定位
			
			if(target_form == 'selector-goto-lineno-form' && scroll)	{	//如果是給經號的goto，則網址需變換成行號格式，於push_historyState第五個參數指定之(scroll變數本身已是完整行號，故傳入之)
				push_historyState(xml_id,juan,title,scroll,scroll);
			}
			else	{
				push_historyState(xml_id,juan,title,scroll);
			}
		});	
	});
	
	//goto 查詢按enter啟動（須先disable form裡頭按enter將表單submit之瀏覽器預設行為，不然會直接submit空白表單）
	$('.selector-goto-form').on("keypress", function(e) {
		var code = e.keyCode || e.which; 
		if (code  == 13) {               
			e.preventDefault();	//disable default submit action in browser
			$(this).find('.selector-goto-submits:eq(0)').trigger('click');	//按下enter啟動click事件
			return false;
		}
	});
	
	
	
	//選擇器相關程式 -
	
	
	/* //最早版本的下拉式卷選單已經移除啦
	//點選了卷選單後，要開啟個別的卷
	$(document).on('click','.juan_item_menu',function()	{
		var jaun_no = PadDigits(parseInt($(this).attr('juan_no'),10),3);
		//var whichSide = $(this).parents('.content-block').attr('id');
		var whichSide = 'reading-area-1';	//2014-11-07：直接寫死就好，因為介面不會有兩個閱讀框了
		var thisSideXmlID = _GLOBALS.open_container[whichSide+'-container']['reading_xmlID'];
		
		$('#'+whichSide+'-container').html('<h1>Reading...</h1>');
		$('#juan-choose-'+whichSide).html($(this).html()+'<span class="caret" style="margin-left:10px"></span>');

		push_historyState(thisSideXmlID,parseInt(jaun_no,10),$(this).parents('.breadcrumb').find('li').eq(0).text(),false);
		load_book({type:'GET',url:'html/T/'+thisSideXmlID.substr(0,3)+'/'+thisSideXmlID+'_'+jaun_no+'.htm',data:null,cb:function(res)	{
				$('#'+whichSide+'-container').html(res);
				after_load('#'+whichSide+'-container');	//讀取完內容後調整content
			}
		});		
	});
	*/
	
	//經選擇器簡單的過濾功能
	$('#selector-levels-search-text').keyup(function()	{
		if($.trim($(this).val()) == '')	{
			$('#selector-levels .btn').show();
			return;
		}

		$('#selector-levels .btn:not(:contains("'+$(this).val()+'"))').hide();
		$('#selector-levels .btn:contains("'+$(this).val()+'")').show();
	});
	
	/*
	//作譯者過濾功能（舊版無別名查詢， fit with API:export/all_creators）
	$('#selector-author-search-text').keyup(function()	{
		$('.selector-author-search-result').remove();
		if($.trim($(this).val()) == '')	{
			$('#selector-author-content').show();
			return;
		}
		
		var thisTextBox = $(this);
		var filtered_person = $.grep(_GLOBALS.creators,function(val,index)	{	//使用全做譯者陣列做過濾搜尋
			var person_pattern = new RegExp(thisTextBox.val(),"g");
			return person_pattern.test(val[1]);
		});
		
		var html = [];
		for(var i in filtered_person)	{
			html.push('<div class="selector-author-keywordFilter-results" style="border-bottom:1px dotted lightgray;padding:5px"><a class="selector-author-items-person"  pname="'+filtered_person[i][1]+'" pid="'+filtered_person[i][0]+'">'+filtered_person[i][1]+'</a><span class="selector-author-open-adb" pid="'+filtered_person[i][0]+'"></span></div>');
		}
		$('#selector-author-content').after('<div class="selector-author-search-result"><span style="color:red">'+_GLOBALS[_GLOBALS.lang]['24_3']+' '+filtered_person.length+' '+_GLOBALS[_GLOBALS.lang]['24_4']+'</span>'+html.join('')+'</div>');
		$('#selector-author-content').hide();
	});	
	*/

	//作譯者過濾功能（含別名查詢）
	$('#selector-author-search-text').keyup(function()	{
		var kw = $.trim($(this).val());
		var results = [];
		
		$('.selector-author-search-result').remove();
		if($.trim($(this).val()) == '')	{
			$('#selector-author-content').show();
			return;
		}
		
		for(let aid in _GLOBALS.creators)	{
			if(!_GLOBALS.creators[aid])	continue;
			if(_GLOBALS.creators[aid].regular_name.indexOf(kw) !== -1 || (_GLOBALS.creators[aid].aliases_all && _GLOBALS.creators[aid].aliases_all.some(names => (new RegExp(kw,"g")).test(names) )))	{	//搜尋常名+aliases_all，常名可用str的indexof模糊搜尋，陣列alias則必須用regexp做模糊搜尋（陣列的indexof會完全比對元素，不是我要的）
				var user_search_in_all_name = _GLOBALS.creators[aid].aliases_all && _GLOBALS.creators[aid].aliases_all.length > 0 ? _GLOBALS.creators[aid].aliases_all.filter(names => (new RegExp(kw,"g")).test(names) ):[];
				var alias_include_kw = [...new Set(user_search_in_all_name.concat((_GLOBALS.creators[aid].aliases_byline ? _GLOBALS.creators[aid].aliases_byline:[])))];	//將user輸入的字串搜到別名的部份併入aliases_byline並去除重複
				results.push({aid:aid,pure_reg_name:_GLOBALS.creators[aid].regular_name,display_reg_name:_GLOBALS.creators[aid].regular_name.replace(kw,'<span style="color:red">'+kw+'</span>'),pure_alias:alias_include_kw.length > 0 ? alias_include_kw.join('、'):'',alias:alias_include_kw.length > 0 ? alias_include_kw.map(name => name.replace(kw,'<span style="color:red">'+kw+'</span>')):false});
			}
		}		

		var html = [];
		for(var i in results)	{
			html.push('<div class="selector-author-keywordFilter-results" style="border-bottom:1px dotted lightgray;padding:5px"><a class="selector-author-items-person"  pname="'+results[i].pure_reg_name+'" pid="'+results[i].aid+'" names="'+results[i].pure_alias+'">'+results[i].display_reg_name+'</a><span class="selector-author-open-adb" pid="'+results[i].aid+'"></span>'+(results[i].alias ? '<span class="selector-author-names"><span class="label">'+_GLOBALS[_GLOBALS.lang]['24_26']+'</span><span style="margin-left:0.5em">'+results[i].alias.join('、')+'</span></span>':'')+'</div>');
		}
		$('#selector-author-content').after('<div class="selector-author-search-result"><span style="color:red">'+_GLOBALS[_GLOBALS.lang]['24_3']+' '+results.length+' '+_GLOBALS[_GLOBALS.lang]['24_4']+'</span>'+html.join('')+'</div>');
		$('#selector-author-content').hide();		
		
	});		
	
	//調整submenu對齊根選單的top以及調整submenu高度
	$(document).on('mouseover','.juan-group-menu > a',function()	{
		var fixedTop = parseInt($(this).parents('.breadcrumb').find('ul[role="menu"]').offset().top,10);//第一層根選單距離頁面的top
		var rootHeight = parseInt($(this).parents('.breadcrumb').find('ul[role="menu"]').height(),10);	//第一層根選單的高度
		var submenu = $(this).parent().find('.dropdown-menu');
		submenu.css({'top':'0'});	//視窗resize的話，top的偏移值會有所影響(因為計算是取top)，所以每次必須把top的偏移值歸0再做
		var submenu_top = submenu.offset().top;
		var submenu_height = submenu.height();
		
		if(submenu_top > fixedTop)
			submenu.css({'top':(fixedTop-submenu_top)+'px','height':submenu_height < rootHeight ? (rootHeight+15)+'px':'auto'});
			
		
	});
	
	/*
	//啟動查字典功能
	$('#footer_func_dic_toolbar_btn').click(function()	{
		$('#footer_func_dic_content').html('<h3>Loading...</h3>');
		var selText = $('#footer_func_dic_toolbar_text').val();
		$.getJSON('http://buddhistinformatics.dila.edu.tw/glossaries/api/get.php?type=match&term='+encodeURIComponent(selText)+'&jsoncallback=?',function(json)	{
			if(!json || json.length <= 0)	{
				$('#footer_func_dic_content').html('找不到詞條 <span style="color:red">'+selText+'</span>。');
				$('#footer_func_dic_toolbar_labels').html('');
				return false;
			}
			var html = [],
				label_html = [],
				dic_c = 0;
			for(var index in json)	{
				dic_c++;
				label_html.push('<span items_link="dic_item_'+dic_c+'" class="label label-default footer_func_dic_items_labels">'+json[index].dict+'</span>');
				html.push('<div items_link="dic_item_'+dic_c+'" class="footer_func_dic_items"><span>'+json[index].term+'</span><span>'+json[index].dict+'</span><br/><span>'+json[index].desc+'</span></div>');
			}
			$('#footer_func_dic_toolbar_labels').html(label_html.join(''));
			$('#footer_func_dic_content').html(html.join('')).find('.footer_func_dic_items').eq(0).show();
			$('.footer_func_dic_items_labels').eq(0).removeClass('label-default').addClass('label-success');
			
		});		
	});
	*/
	
	//雙框整合功能(footer)
	/*
	$('#footer_func_search_btn').click(function()	{
		var text = $('#footer_func_search_text').val();
		do_dictionary('#footer ',text);
		do_ngram('footer_func_ngram_iframe',text);
		do_authority('#footer ',text);
	});
	*/
	
	//單框整合功能按鈕(side)
	$('#side_func_search_btn').click(function(e)	{
		var text = $('#side_func_search_text').val();
		_GLOBALS.sideFuncLastKw = $('#side_func_search_text').val();
		if($.trim(text) == '')	return false;
		if(new RegExp('~','g').test(text) && !(new RegExp('^[^'+RegExp.quote(_GLOBALS.extended_search_pattern.join(''))+']+~[^'+RegExp.quote(_GLOBALS.extended_search_pattern.join(''))+']+$','g').test(text)))	{	//如果是進階搜尋的near搜尋，則只接受兩個關鍵字不得多個，即 term1 ~ term2
			alert(_GLOBALS[_GLOBALS.lang]['22_8']);
			return false;
		}
			
		
		$('#side_func_search_text').addClass('x');	//在搜尋框右邊增加清除搜尋關鍵字的"x"
		$('.func_ajax_loading,#side_func_search_cancel').show();
		$('#side_func_search_text_hidden').text(text);	//另存整合功能的關鍵字，以備之後給任何功能用（user可能會改搜尋框內容，故本次搜尋另存之）
		
		//檢查關鍵字是否為進階搜尋並設定全域變數flag
		var exrended_search = new RegExp('['+RegExp.quote(_GLOBALS.extended_search_pattern.join(''))+']','g');
		_GLOBALS.isExtendedSearch = exrended_search.test($('#side_func_search_text_hidden').text()) ? true:false;
		
		
		//將搜尋關鍵字加入歷程框，使用datalist
		if($('#my_search_history > option').filter('[value="'+text+'"]').size() <= 0)	{	
			$('#my_search_history').append('<option value="'+text+'" />');
		}
		
		$('#side_func_search_controls :input').prop('disabled',true);	//鎖定按鈕和搜尋框，以防user一直按
		
		//2015-10-27:由上面的不保證順序非同步改由deferred object來處理，可確保所有工作都執行完後在done裡面做事情
		
		addUserAction('search',{kw:text});	//將參考搜尋加入上次操作
		
		$.when(
			do_dictionary('#side_func ',text),
			do_ngram('side_func_ngram_iframe',text),
			do_authority('#side_func ',text),
			//do_textsearch('#side_func ',text),	//2020-09-17：原全文檢索因新版搜尋上線，暫停使用
			do_dlmbs('#side_func ',text)
		).done(function()	{	
			//console.log(_GLOBALS.xhrObjs);
			_GLOBALS.xhrObjs = [];
			$('#side_func_search_controls :input').prop('disabled',false);	//全部非同步工作完成後再解除按鈕和搜尋框之鎖定
			$('#side_func_search_cancel').hide();
		});
		

	});
	
	//搜尋框按下enter時啟動整合功能搜尋
	$('#side_func_search_text').keyup(function(e)	{
		if(e.which == 13)	
			$('#side_func_search_btn').trigger('click');
	});
	
	//取消搜尋
	$('#side_func_search_cancel').click(function()	{
		for(var i in _GLOBALS.xhrObjs)	{
			if(_GLOBALS.xhrObjs[i].status != '200' && _GLOBALS.xhrObjs[i].readyState != '4')	{	//如果這個xhr還沒完成，就abort並清除content
				_GLOBALS.xhrObjs[i].abort();
				$(_GLOBALS.xhrObjs[i].contentID).html('');	//contentID屬性在xhr被push的時候指定，即該功能內容容器的id，取消時用來各別清除				
			}
		}
		_GLOBALS.xhrObjs = [];
		$('#side_func ul.nav-tabs').find('.func_ajax_loading').hide();	//清除loading轉轉轉、筆數label
		//$('#side_func ul.nav-tabs').find('.label-as-badge').html('');
		
		$('#side_func_search_controls :input').prop('disabled',false);	//全部非同步工作完成後再解除按鈕和搜尋框之鎖定
		$(this).hide();
	});
	
	//整合功能-字典查詢結果的label切換
	$(document).on('click','.footer_func_dic_items_labels',function()	{
		$(this).removeClass('label-default').addClass('label-success').siblings().removeClass('label-success').addClass('label-default');
		$('.footer_func_dic_items').hide().end().find('[items_link="'+$(this).attr('items_link')+'"]').show();
	});
	
	//整合功能-authority 查詢結果label切換
	$(document).on('click','.func_authority_labels',function()	{
		$(this).removeClass('label-default').addClass('label-success').siblings().removeClass('label-success').addClass('label-default');
		//var parentID = (_GLOBALS.panelMode == 'SINGLE_COLUMN' ? '#side_func ':'#footer ');
		$('#func_authority_content > .func_authority_items').hide();
		$('#func_authority_content > .func_authority_items[id="'+$(this).attr('link_clue')+'"]').show();
	});	

	//整合功能-DLMBS 查詢結果label切換
	$(document).on('click','.footer_func_dlmbs_pages.label-default',function()	{
		var page = parseInt($(this).attr('page_num'),10);
		var selText = $('#side_func_search_text_hidden').text();
		$(this).removeClass('label-default').addClass('label-success').siblings('.footer_func_dlmbs_pages').removeClass('label-success').addClass('label-default');
		$('#footer_func_dlmbs_content').html('<h3>Loading...</h3>');

		$.ajax({
			type: "POST",
			url: "getData.php",
			data: "type=dlmbs_by_page&pagesize="+_GLOBALS.DLMBS_num_of_page+"&pageno="+page+'&q='+encodeURIComponent(selText),
			success: function(res)	{
				var json = JSON.parse(res);
				$('#footer_func_dlmbs_content').html('<ol start="'+(_GLOBALS.DLMBS_num_of_page*page-_GLOBALS.DLMBS_num_of_page+1)+'" style="padding-left: 30px;">'+produce_dlmbs_content(json,selText)+'</ol>');
			}
		});	
	});	
	
	//單欄模式 全文檢索結果每個table tr click時，要啟動換經、更新標題、highlight、跟設定搜尋結果 上一筆/下一筆 功能
	$(document).on('click','#side_func_textsearch_table > tbody tr',function()	{
		if($('#side_func_enlarge_modal').is(':visible'))	$('#side_func_enlarge_modal').modal('hide');	//如果表格是在整合功能放大modal時，點選後要關閉modal
		$('#side_func_textsearch_table .textsearch_table_hl').removeClass('textsearch_table_hl');
		$(this).addClass('textsearch_table_hl');
			
		var kwic_obj = {
			kw:$('#side_func_search_text_hidden').text(),
			work:$(this).attr('xml_id').toRay(),
			juan:parseInt($(this).attr('juan'),10)
		};
		
		/*
		update_tree($(this).attr('xml_id'),kwic_obj);	//更新樹狀目錄,且加上第二個kwic參數資料用以顯示kwic tab
		push_historyState($(this).attr('xml_id'),parseInt($(this).attr('juan'),10),$(this).attr('xml_id')+' '+$(this).attr('jing_title'));
		updateTitle_openJing($(this).attr('xml_id'),$(this).attr('xml_id')+' '+$(this).attr('jing_title'),parseInt($(this).attr('juan'),10),true);	//重設標題+開啟經+指定卷+highlight
		*/
		
		var xml_id = $(this).attr('xml_id');
		var title = $(this).attr('jing_title');
		var juan = parseInt($(this).attr('juan'),10);
		
		if($('#tree_area_kwic').get(0))	$('#tree_area_kwic').html('<h3 style="padding:10px">Loading...</h3>');	//如果#tree_area_kwic存在就先設定kwic_tab為loading字樣
		$.when(
			update_tree(xml_id,kwic_obj),	//更新樹狀目錄,且加上第二個kwic參數資料用以顯示kwic tab
			updateTitle_openJing(xml_id,xml_id+' '+title,juan,true)	//重設標題+開啟經+指定卷+highlight
		).done(function()	{	
			update_kwic_tab();	//文本、樹狀目錄非同步動作都完成後，更新kwic tab！因為kwic tab需等兩個都做完才可動作
			push_historyState(xml_id,juan,xml_id+' '+title);
		});		
	});
	
	//kwic tab table click scroll(原本我做的kwic版本)
	$(document).on('click','#tree_area_kwic_table > tbody tr',function()	{	
		var group = $(this).attr('group');
		var recoverFlag = false;
		var group_target = $('.hl[extended-search-group]').size() > 0 ? 'extended-search-group':'group_num';		
		
		var	scroll = $('.hl['+group_target+'="'+group+'"]:eq(0)');
		$('#'+_GLOBALS.active_panel+'-container').scrollTo(scroll,300,{offset:-50});	//scroll到定位
		
		//與上/下筆做連動
		$('#jing-tools-searchTool #jing-tools-searchTool-nowCount').text(group);
		$('.hl_bk').removeClass('hl_bk');
		$('.hl['+group_target+'="'+group+'"]').addClass('hl_bk');
		
		$('#tree_area_kwic_table .textsearch_table_hl').removeClass('textsearch_table_hl');
		$(this).addClass('textsearch_table_hl');
	
	});
	
	//kwic tab table click scroll(由新版搜尋介面連過來，from url highlight，並呼叫ray 的 kwic 版)
	$(document).on('click','#tree_area_kwic_ray_table > tbody tr',function()	{
		var punctuations = ['\\(','\\)','，','。','！','…','、','「','」','\\.','\\,','》','《','：','）','（','；','？','　','─','︰','〉','〈','』','『','．','－','～','＼','｡','\\?'];	
		var is_near = new URLSearchParams(location.search).get('q') && new URLSearchParams(location.search).get('q').indexOf('~') !== -1 ? true:false;
		var	scroll = $('.t[l="'+$(this).data('lb')+'"]:eq(0)');
		var lb = $(this).data('lb');
		var kwic_texts = $.trim($(this).find('td:eq(2)').html());	//從藏起的td取真正要計算的kwic，此kwic是「去掉夾注」的，但由於後端api回傳是有夾注，故由我這邊動手去掉，細節實做在functions.js的process_url_highlight()中
		$('#'+_GLOBALS.active_panel+'-container').scrollTo(scroll,300,{offset:-50});	//scroll到定位					
		
		$('.hl_bk').removeClass('hl_bk');
		
		
		//以下程式找出 起始 <mark> 到 最後一個 </mark> 結束中間所有的 lb，最終結果在 occurs_lbs[] +
		var now_lb_position = $('.lb').index($('.lb[id *= "'+lb+'"]'));
		var lb_find_limit = 6;
		var lb_find_count = 0;	//最多找6行就好
		var lb_find_text = '';
		var occurs_lbs = [];
		
		
		$('<div/>').attr('id','inline_note_temp_fuck').css('display','none').html($('#'+_GLOBALS.active_panel+'-container').clone()).appendTo('body');	//因應夾註需跳過，預備製作一份沒有夾註的html來用
		
		if(_GLOBALS.skipInlineNoteHL)	{	//需要跳過夾註的話，則做以下處理並將 #inline_note_temp_fuck small 移除
			$('#inline_note_temp_fuck small').each(function()	{		//因為夾註(<small>)下一行會被刪除，但夾註會包含lb，lb後面會用到，如果lb被刪掉取號會有誤，因此在這邊將「夾註裡面的最後一個lb」，取出放在small後面（只取最後一個是因為夾註結尾可能在同一行(lb)結束，後面接著經文，而經文要取lb時夾註卻被刪掉而拿不到，所以拿最後一個lb出來即可）
				var e = $(this).find('.lb:last');
				$(this).after(e);
			});
			$('#inline_note_temp_fuck small').remove();	//假 html 裡面移除夾註
		}
		
		//以下程式有用到 #inline_note_temp_fuck 就是要從假的、沒有夾註的 html 去找行號
		now_lb_position = $('#inline_note_temp_fuck .lb').index($('#inline_note_temp_fuck .lb[id *= "'+lb+'"]'));	
		while(lb_find_count < lb_find_limit)	{
			var finded_lb = $('#inline_note_temp_fuck .lb:eq('+now_lb_position+')').attr('id').split('p')[1];
			var finded_text_temp = $('#inline_note_temp_fuck .t[l="'+finded_lb+'"]').text().replace(new RegExp('['+punctuations.join('')+']','g'),'');	//把標點符號都去掉（內文）
			var kwic_mark_range_text = /(<mark>.+<\/mark>)/g.exec(kwic_texts)[1].replace(/(<mark>|<\/mark>)/g,'').replace(new RegExp('['+punctuations.join('')+']','g'),'');	//把標點符號都去掉（左邊kwic）
			
			lb_find_text += finded_text_temp;
			occurs_lbs.push(finded_lb);
			
			if(lb_find_text.match(new RegExp(kwic_mark_range_text,'g')))	break;	//如果mark範圍內的文字都找到了（含跨行），就終止
			//console.log(lb_find_text+'  '+kwic_mark_range_text)
			//console.log( $('.lb:eq('+now_lb_position+')').attr('id'));
			now_lb_position++;
			lb_find_count++;
		}
		//console.log(occurs_lbs)	
		//以下程式找出 起始 <mark> 到 最後一個 </mark> 結束中間所有的 lb，最終結果在 occurs_lbs -
		
		
		
		for(const lb of occurs_lbs)	{
			$('.t[l="'+lb+'"] .hl').addClass('hl_bk');	//注意：這邊是處理「真正」的html
		}
		
		$('#tree_area_kwic_ray_table .textsearch_table_hl').removeClass('textsearch_table_hl');
		$(this).addClass('textsearch_table_hl');

		if(is_near)	{
			//return;
			
			var text_group_nums = [];
			var kws = new URLSearchParams(location.search).get('q').split('~');
			var kwic_text = kwic_texts;
			var lb_text = '';
			
			for(const lb of occurs_lbs)
				lb_text += $('#inline_note_temp_fuck .t[l="'+lb+'"]').text().replace(new RegExp('['+punctuations.join('')+']','g'),'');	//取得文本該行的文字並去除標點，以方便跟沒標點的kwic比對
			
			kwic_text = kwic_text.replace(new RegExp('['+punctuations.join('')+']','g'),'');	//kwic也要去除標點，以方便跟已去標點的內文比對
			
			//console.log(lb_text)
			//console.log(kwic_text)
			var cut_regex = '('+lb_text.split('').map(s => '(<mark>|<\\/mark>)*?'+s+'(<mark>|<\\/mark>)*?').join('')+')';	//
			kwic_text= new RegExp(cut_regex,'g').exec(kwic_text)[1];	//因為kwic文字可能很長，我們需要擷取出與右邊文本該行相同的字串，否則kwic太多的話，可能會取代到多餘的不相干關鍵字
			
			kwic_text = kwic_text.replace(new RegExp('('+kws.join('|')+')',"g"),"<b>$1</b>");	//將kwic裡面的全部關鍵字都加上一個b
			
			//取得右邊文本該行的所有.hl關鍵字的group_num，等等會用到
			for(const lb of occurs_lbs)	{
				$('.t[l="'+lb+'"] .hl').each(function()	{		
					if($.inArray($(this).attr('group_num'),text_group_nums) === -1)	text_group_nums.push($(this).attr('group_num'));
				});
			}
			
			//console.log(kwic_text)
			//console.log(text_group_nums)
			
			//開始檢查是誰需要被移除 hl_bk，根據與kwic文字裡面，b的父親是否有mark來判斷
			$('<div/>').html(kwic_text).find('b').each(function(i,v)	{
				//console.log($(this).parent().get(0).tagName)
				if($(this).parent().get(0).tagName !== 'MARK')	{
					$('.hl[group_num="'+text_group_nums[i]+'"]').removeClass('hl_bk');
				}
			});
		}
		$('#inline_note_temp_fuck').remove();	//最後將這個假的html移除

	});	
	
	/*
	//上面的備份
	$(document).on('click','#tree_area_kwic_ray_table > tbody tr',function()	{
		var is_near = new URLSearchParams(location.search).get('q') && new URLSearchParams(location.search).get('q').indexOf('~') !== -1 ? true:false;
		var	scroll = $('.t[l="'+$(this).data('lb')+'"]:eq(0)');
		var lb = $(this).data('lb');
		var kwic_texts = $.trim($(this).find('td:eq(2)').html());
		$('#'+_GLOBALS.active_panel+'-container').scrollTo(scroll,300,{offset:-50});	//scroll到定位					
		
		$('.hl_bk').removeClass('hl_bk');
		$('.t[l="'+$(this).data('lb')+'"] .hl').addClass('hl_bk');
		$('#tree_area_kwic_ray_table .textsearch_table_hl').removeClass('textsearch_table_hl');
		$(this).addClass('textsearch_table_hl');

		if(is_near)	{
			//return;
			var punctuations = ['\\(','\\)','，','。','！','…','、','「','」','\\.','\\,','》','《','：','）','（','；','？','　','─','︰','〉','〈','』','『','．','－','～','＼','｡','\\?'];	
			var text_group_nums = [];
			var kws = new URLSearchParams(location.search).get('q').split('~');
			var kwic_text = $(this).find('td:eq(2)').html();
			var lb_text = $('.t[l="'+$(this).data('lb')+'"]').text().replace(new RegExp('['+punctuations.join('')+']','g'),'');	//取得文本該行的文字並去除標點，以方便跟沒標點的kwic比對
			var cut_regex = '('+lb_text.split('').map(s => '(<mark>|<\\/mark>)*?'+s+'(<mark>|<\\/mark>)*?').join('')+')';	//
			kwic_text= new RegExp(cut_regex,'g').exec(kwic_text)[1];	//因為kwic文字可能很長，我們需要擷取出與右邊文本該行相同的字串，否則kwic太多的話，可能會取代到多餘的不相干關鍵字
			//console.log(kwic_text)
			
			kwic_text = kwic_text.replace(new RegExp('('+kws.join('|')+')',"g"),"<b>$1</b>");	//將kwic裡面的全部關鍵字都加上一個b
			
			//取得右邊文本該行的所有.hl關鍵字的group_num，等等會用到
			$('.t[l="'+lb+'"] .hl').each(function()	{		
				if($.inArray($(this).attr('group_num'),text_group_nums) === -1)	text_group_nums.push($(this).attr('group_num'));
			});
			
			//console.log(kwic_text)
			//console.log(text_group_nums)
			
			//開始檢查是誰需要被移除 hl_bk，根據與kwic文字裡面，b的父親是否有mark來判斷
			$('<div/>').html(kwic_text).find('b').each(function(i,v)	{
				//console.log($(this).parent().get(0).tagName)
				if($(this).parent().get(0).tagName !== 'MARK')	{
					$('.t[l="'+lb+'"] .hl[group_num="'+text_group_nums[i]+'"]').removeClass('hl_bk');
				}
			});
		}
		
	});	
	*/
	
	//全文檢索 上一筆/下一筆 功能(**注意：目前是用active_panel判斷要對應哪個container，真的到雙框模式可能有問題還要修)
	$(document).on('click','.jing-tools-searchTool-btn',function()	{
		var nowIndex = parseInt($('#jing-tools-searchTool #jing-tools-searchTool-nowCount').text(),10);
		var endIndex = parseInt($('#jing-tools-searchTool #jing-tools-searchTool-endCount').text(),10);
		if($(this).attr('id') == 'jing-tools-searchTool-prev')	{
			nowIndex = nowIndex - 1 <= 0 ? endIndex:nowIndex-1;
		}
		else if($(this).attr('id') == 'jing-tools-searchTool-next')	{
			nowIndex = nowIndex + 1 > endIndex ? 1:nowIndex+1;
		}
		
		$('#'+_GLOBALS.active_panel+'-container').scrollTo($('span.hl[group_num="'+nowIndex+'"]:eq(0)'),{offset:-60});
		//$('#'+_GLOBALS.active_panel+'-container #body span.hl').removeClass('hl_bk').end().find('span.hl[group_num="'+nowIndex+'"]').addClass('hl_bk');
		
		$('#jing-tools-searchTool #jing-tools-searchTool-nowCount').text(nowIndex);
	});
	

	//進階(extended)全文檢索 上一筆/下一筆 功能
	$(document).on('click','.jing-tools-searchTool-extended-btn',function()	{
		var nowIndex = parseInt($('.hl_bk').size() <= 0 ? 0:$('.hl_bk:eq(0)').attr('extended-search-group'),10);
		var endIndex = parseInt(Math.max.apply(null, $.makeArray($('.hl[extended-search-group]').map(function()	{	return $(this).attr('extended-search-group');}))),10);

		if($(this).attr('id') == 'jing-tools-searchTool-extended-prev')	{
			nowIndex = nowIndex - 1 <= 0 ? endIndex:nowIndex-1;
		}
		else if($(this).attr('id') == 'jing-tools-searchTool-extended-next')	{
			nowIndex = (nowIndex + 1) > endIndex ? 1:nowIndex+1;
		}
		
		$('#'+_GLOBALS.active_panel+'-container').scrollTo($('span.hl[extended-search-group="'+nowIndex+'"]:eq(0)'),{offset:-40});
		//$('#'+_GLOBALS.active_panel+'-container #body span.hl').removeClass('hl_bk').end().find('span.hl[extended-search-group="'+nowIndex+'"]').addClass('hl_bk');
		
		//$('#jing-tools-searchTool #jing-tools-searchTool-nowCount').text(nowIndex);
	});	
	
	//全文檢索之infinite scroll顯示更多結果
	$('#side_func_textsearch').scroll(function()	{
		//console.log($(this).scrollTop()+ ' '+$(this).innerHeight() +' '+ this.scrollHeight)
		if($(this).scrollTop() + $(this).innerHeight()+1 >= this.scrollHeight) {	//scroll到底時
		
			if($('#side_func_textsearch_table').attr('full_loaded') === 'done')	return false;	//有full_loaded attr表示全部資料已讀取完畢，不再做此scroll事件了
		
			if($('#infinite_search_msg').size() <= 0)	$('#side_func_textsearch_table').after('<h2 id="infinite_search_msg" style="text-align:center">Loading...</h2>');
			
			var search_range = $('#textsearch_range_value').val() == '' ? '':$('#textsearch_range_value').val();
			var kw = $('#side_func_search_text_hidden').text();
			
			_GLOBALS.searchOffset += _GLOBALS.searchScrollLimit;
			
			$.ajax({
				url:'getData.php',
				data:'type=context_search&range='+search_range+'&offset='+_GLOBALS.searchOffset+'&qry='+encodeURIComponent(kw)+(_GLOBALS.searchSortby ? '&sortby='+_GLOBALS.searchSortby:''),	//_GLOBALS.searchSortby 於表格表頭欄位排序事件裡面設定
				type:'POST',
				success:function(json)	{
					$('#infinite_search_msg').remove();
					var json = $.parseJSON(json);
					if(!json || json.num_found <= 0)	{
						return;
					}
					var html = '';
					for(var index in json['results'])	{
						var time_str = (json['results'][index]['time_from'] ? json['results'][index]['time_from']:'') + (json['results'][index]['time_from'] && json['results'][index]['time_to'] ? '~':'') +(json['results'][index]['time_to'] ? json['results'][index]['time_to']:'');
						html += '<tr xml_id="'+json['results'][index]['file']+'" juan="'+json['results'][index]['juan']+'" jing_title="'+json['results'][index]['title']+'">'+(_GLOBALS.isExtendedSearch ? '':'<td>'+json['results'][index]['term_hits']+'</td>')+'<td>'+json['results'][index]['file']+'</td><td style="display:none">'+json['results'][index]['category']+'</td><td>'+json['results'][index]['title']+'</td><td>'+json['results'][index]['juan']+'</td><td>'+(json['results'][index]['creators'] ? json['results'][index]['creators']:'')+'</td><td>'+time_str+'</td></tr>';
					}
					$('#side_func_textsearch_table tbody').append(html);	//將結果append至現有的tbody裡面
					
					
					if(parseInt($('#side_func_textsearch_table tbody tr').size(),10) >= parseInt(json.num_found,10))	{	//如果已讀取的列數>=總筆數(json.num_found)表示已全部讀取，將table標上full_loaded attr讓下次不在執行此事件了
						$('#side_func_textsearch_table').attr('full_loaded','done');
					}
					
				}
			});
		
		}
		
		//if($(this).scrollTop() <= 0)	alert('top!')	//scroll到頭
	});
	
	//關閉全文檢索結果 上/下一筆、un-highlight
	$(document).on('click','.jing-tools-searchTool-close',function()	{
		//$(this).parent().empty().hide().parents('.content-block').find('.content span.hl,.content span.hl_bk').removeClass('hl').removeClass('hl_bk');
		
		
		if($('#tree_area .nav-tabs a[href="#tree_area_kwic"]').parent().hasClass('active'))	{	//如果kwic tab是active，就打開第一個tab（因為以下面即將移除kwic tab）
			$('#tree_area ul.nav-tabs a:first').tab('show');			
		}
		
		//移除kwic tab + tab content
		$('#tree_area .tab-content #tree_area_kwic').remove();
		$('#tree_area .nav-tabs a[href="#tree_area_kwic"]').parent().remove();
		
	});
	
	//問題回報-送出按鈕
	$('#feedback_modal_submit').click(function()	{
		var subject = $('#feedback_modal_subject').val();
		var mail = $('#feedback_modal_mail').val();
		var text = $('#feedback_modal_text').val();
		if($.trim(text) == '')	{
			alert(_GLOBALS[_GLOBALS.lang]['21_13']);
			return false;
		}
		
		//google reptcha V3 驗證
		grecaptcha.ready(function() {
			grecaptcha.execute('6LfZEOopAAAAAGM2W898DHet1f2f1aHUdflbZBtV', {action: 'submit'}).then(function(token) {
				//用email副本一份給林杯
				$.ajax({
					type: "POST",
					url: "getData.php",
					data: "type=sendmail&info="+$('#feedback_modal_info').text()+"&text="+encodeURIComponent(text)+"&mail="+encodeURIComponent(mail)+'&g-recaptcha-response='+token,
					success: function(res)	{
						/*
						if(res === 'ok')	{
							$('#feedback_modal_form').hide().parent().find('#feedback_modal_success').show();
						}
						else
							$('#feedback_modal_err').html(_GLOBALS[_GLOBALS.lang]['21_14']+res).show();
						*/
						$('#feedback_modal_form').hide().parent().find('#feedback_modal_success').show();
					}
				});			
				
				//bugify post issue
				var bugify_subject = $.trim(subject) != '' ? $.trim(subject):('bip 問題回報 '+(mail ? ' by '+mail:''));
				var bugify_desc = ($('#feedback_modal_info_tr').is(':visible') ? "問題位置：<br/>"+$('#feedback_modal_info').text():"")+"<br/><br/> 問題內容：<br/>"+text;	
				$.ajax({
					type: "POST",
					url: "getData.php",
					data: "type=bugify_report&subject="+encodeURIComponent(bugify_subject)+"&description="+encodeURIComponent(bugify_desc)+'&g-recaptcha-response='+token,
					success: function(res)	{
						if(res === 'ok')	{
							var bugify_report_successful = true;
						}
					}
				});			
			});
		});		
		
		
	});
	
	//經選擇器-經目查詢自動完成 +	//2015-01-07:老闆指示暫時停用
	/*
	$("#selector-mulu-title").autocomplete({
		source: 'getData.php?type=selector_mulu_autocomplete&sub_type=title'
	});
	
	$("#selector-mulu-author").autocomplete({
		source: 'getData.php?type=selector_mulu_autocomplete&sub_type=author'
	});
	*/
	//經選擇器-經目查詢自動完成 -	
	
	//全文檢索範圍選單
	$(document).on('click','#textsearch_range_menu li',function()	{
		var role = $.trim($(this).attr('role'));
		if(role == 'all')	{
			$('#textsearch_range_value').val('');
			
		}
		else if(role == 'custom')	{
			ContextSearchRange.show();
		}
		else if(role == 'single')	{
			$('#textsearch_range_value').val('works:'+_GLOBALS.open_container[_GLOBALS.active_panel+'-container']['reading_xmlID'].toRay());	
		}
		
		//如果是全部或單經的話，選完也直接啟動搜尋，但注意的是關鍵字是拿side_func_search_text_hidden去做，這才是原本搜尋關鍵字，因為user可能手動改掉框框的字，拿框框的字可能會錯
		//然後如果選的跟lastSearchRange一樣就不啟動，反之才啟動
		if((role == 'all' || role == 'single') && role != $('#textsearch_range_menu li > a:contains("'+_GLOBALS.lastSearchRange+'")').parent().attr('role') && $('#fake_search_range_holder').size() <= 0)	{	//其中#fake_search_range_holder 用來判斷是否是首頁一打開的範圍選單，是的話不做搜尋
			$('#side_func_search_text').val($('#side_func_search_text_hidden').text());
			$('#side_func_search_btn').trigger('click');
		}
		
		_GLOBALS.lastSearchRange = $(this).text();
		$(this).parent().prevAll('#search_range_select_btn').find('span[role="title"]').text($(this).text())
	});
	
	//全文檢索-表格欄位排序
	$(document).on('click','#side_func_textsearch_table > thead th[sortby]',function()	{
		var text = $.trim($('#side_func_search_text_hidden').text());
		var sortBy = $(this).attr('sortby');
		var sortOrder = $('#func_textsearch_result_panel').attr(sortBy+'-order') ? $('#func_textsearch_result_panel').attr(sortBy+'-order'):'desc';
		if(text && sortBy)	{
			do_textsearch('#side_func ',text,sortBy+':'+sortOrder);
			(sortOrder == 'desc' ? $('#func_textsearch_result_panel').attr(sortBy+'-order','asc') : $('#func_textsearch_result_panel').attr(sortBy+'-order','desc'));
			_GLOBALS.searchSortby = sortBy+':'+sortOrder;	//只有這邊才會動到_GLOBALS.searchSortby，為了後面infinite scroll資料的正確性，需於此紀錄現在的排序於infinite scroll使用
		}
	});
	
	//dlmbs more button :按下時顯示modal，直接把元素move過去
	$(document).on('click','.side_func_more_btn',function()	{
		var type = $(this).attr('clue');
		var title = $('#'+type).find('.side_func_titles').text();
		var original_dom = $('#'+type+' .more_wrapper');
		$('#'+type+' #dotdotdot').remove();	//移除label那邊的「.....」
		$('#'+type+' .side_func_result_labels > .label').show();	//more顯示時將所有隱藏的切換label顯示
		original_dom.attr({'original_max_height':parseInt(original_dom.css('max-height'),10),'original_dom_selector_str':'#'+type}).css({'max-height':'none'}).find('.side_func_more_btn').hide().end().appendTo('#side_func_more_modal .modal-body');
		$('#moreModalLabel').text(title);
		$('#side_func_more_modal').modal();

	});	
	// dlmbs more button :modal 關閉時，將搬過來的.panel-body搬回原本整合功能的地方
	$('#side_func_more_modal').on('hidden.bs.modal', function (e) {
		var original_dom = $('#side_func_more_modal .modal-body > .more_wrapper');
		var original_position = original_dom.attr('original_dom_selector_str');
		original_dom.css({'max-height':original_dom.attr('original_max_height')+'px'}).find('.side_func_more_btn').show().end().appendTo(original_position);
		
		//關閉時根據label數量再次隱藏label
		if($(original_position+' .side_func_result_labels > .label').size() > 5)	$(original_position+' .side_func_result_labels > .label').slice(5).hide().parent().append('<span id="dotdotdot" style="margin-left:7px">.....<a class="side_func_more_btn" clue="side_func_dlmbs">more</a></span>');	
		
		
		if(original_position == '#side_func_dlmbs')	{	//如果是dlmbs，可能user已在modal中切換到被藏起來的頁數，因此關閉modal後content塞回去隱藏那一頁會跑出來，故一律trigger回第一頁
			$(original_position).find('#footer_func_dlmbs_toolbar_labels > .label:eq(0)').trigger('click');
		}
		
	});	
	
	//關閉整合功能panel的按鈕
	$('#side_func_close_btn').click(function(e)	{
		side_func_display(false);
	});

	//顯示整合功能panel(side_func)按鈕
	$('#show_side_func_btn').click(function()	{
		side_func_display(true);
	});
	
	//引用複製設定 +
	//經名於前or後
	$('#refAndCopy_config input[type="radio"][name="rac_conf_jing_title_position"]').click(function()	{
		refAndCopy_config_adjust('title');
		$.cookie('rac_conf_jing_title_position',$('#refAndCopy_config input[type="radio"][name="rac_conf_jing_title_position"]:checked').val(),{path:'/'});
	});
	//是否顯示校勘
	$('#rac_conf_isNote').click(function()	{
		refAndCopy_config_adjust('note');
		$.cookie('rac_conf_isNote',$(this).prop('checked') ? '1':'0',{path:'/'});
	});
	//是否顯示品名
	$('#rac_conf_isPin').click(function()	{
		refAndCopy_config_adjust('pin');
		$.cookie('rac_conf_isPin',$(this).prop('checked') ? '1':'0',{path:'/'});
	});
	//是否顯示標點
	$('#rac_conf_isPunc').click(function()	{
		refAndCopy_config_adjust('punc');
		$.cookie('rac_conf_isPunc',$(this).prop('checked') ? '1':'0',{path:'/'});
	});	
	//是否顯示段落
	$('#rac_conf_isFormatted').click(function()	{
		refAndCopy_config_adjust('formatted');
		$.cookie('rac_conf_isFormatted',$(this).prop('checked') ? '1':'0',{path:'/'});
	});	
	//經文位置
	$('#refAndCopy_config input[type="radio"][name="rac_conf_line_format"]').click(function()	{
		refAndCopy_config_adjust('line');
		$.cookie('rac_conf_line_format',$('#refAndCopy_config input[type="radio"][name="rac_conf_line_format"]:checked').val(),{path:'/'});
	});	
	//校勘排列
	$('#refAndCopy_config input[type="radio"][name="rac_conf_note_position"]').click(function()	{
		refAndCopy_config_adjust('note_position');
		$.cookie('rac_conf_note_position',$('#refAndCopy_config input[type="radio"][name="rac_conf_note_position"]:checked').val(),{path:'/'});
	});		
	//引用複製設定 -
	
	
	//內文圖片click可以另開視窗放大
	$(document).on('click','.graphic img',function()	{
		window.open($(this).attr('src'))
	});
	
	//內文上下卷切換選單動作
	$(document).on('click','.inline_juan_menu.enabled',function()	{
		var xml_id = _GLOBALS.open_container[_GLOBALS.active_panel+'-container']['reading_xmlID'];
		var juan = $(this).attr('juan');
		
		$.getJSON(_GLOBALS.catalogAPI+'works?work='+xml_id.toRay()+'&callback=?',function(res)	{	//此ajax用經號去取經名
			res = res.results[0];
			var jing_title = res.title;
			$('#'+_GLOBALS.active_panel+'-container').html('<h1>Reading...</h1>');	//取得經名時的ajax做個Reading讓user知道在讀取，預防沒反應
			push_historyState(xml_id,juan,res.work+' '+jing_title);
			updateTitle_openJing(xml_id,res.work+' '+jing_title,juan);		
		});
	});
	
	//「顯示版權資訊」開關按鈕
	$(document).on('click','.display_copyright_btn',function()	{
		var that = $(this);
		$('#cbeta-copyright').slideToggle(100,function()	{
			if($('#cbeta-copyright').is(':hidden'))	
				that.html('<span class="glyphicon glyphicon-plus" style="margin-right:0.5em"></span>顯示版權資訊');
			else
				that.html('<span class="glyphicon glyphicon-minus" style="margin-right:0.5em"></span>隱藏版權資訊');			
		});
	});	
	
	//作譯者經錄資訊按鈕
	$(document).on('click','#author_info_btn',function()	{
		var this_btn = $(this);
		this_btn.find('span.author_info_btn_icon').hide().end().find('img:eq(0)').show();

		$.ajax({url:'getData.php',data:'type=authority&sub_type=catalog&id='+$(this).attr('xml_id'),type:'POST',success:function(json)	{
			var json = JSON.parse(json);
			var html = [];
			
			if(json.length <= 0)	{
				alert('There is no data');
				this_btn.find('span.author_info_btn_icon').show().end().find('img:eq(0)').hide();
				return false;
			}
			
			json = json[0];	//經資訊api只有一筆，取第一筆即可
			if(json.jing_title)		html.push('<div class="author_info_item"><span data-lang-id="1_14">'+_GLOBALS[_GLOBALS.lang]['1_14']+'：</span><span>'+json.jing_no+' '+json.jing_title+'</span><span style="margin-left:0.8em">( <a target="_blank" href="'+_GLOBALS.authorityROOT+'/catalog/?fromInner='+json.authority_id+'">Authority</a> )</span></div>');
			if(json.vol)			html.push('<div class="author_info_item"><span data-lang-id="26_87">'+_GLOBALS[_GLOBALS.lang]['26_87']+'：</span><span>'+json.vol+'</span></div>');	
			if(json.category)		html.push('<div class="author_info_item"><span data-lang-id="2_14">'+_GLOBALS[_GLOBALS.lang]['2_14']+'：</span><span>'+json.category+'</span></div>');			
			//if(json.juan)			html.push('<div class="author_info_item"><span>卷數：</span><span>'+json.juan+'</span></div>');
			if(json.editors)	{
				var editors_html = [];
				for(var editor in json.editors)	{
					for(var ei in json.editors[editor])	{
						editors_html.push(json.editors[editor][ei][1]+'<span class="author_info_links">(<a class="selector-author-items-person" pid="'+json.editors[editor][ei][0]+'" pname="'+json.editors[editor][ei][1]+'" data-lang-id="1_15">'+_GLOBALS[_GLOBALS.lang]['1_15']+'</a>、<a target="_blank" href="'+_GLOBALS.authorityROOT+'person/?fromInner='+json.editors[editor][ei][0]+'" data-lang-id="1_16">'+_GLOBALS[_GLOBALS.lang]['1_16']+'</a>)</span>');
					}
				}
				html.push('<div class="author_info_item"><span data-lang-id="1_19">'+_GLOBALS[_GLOBALS.lang]['1_19']+'：</span><span>'+editors_html.join(',')+'</span></div>');
			}
			if(json.dates)	{
				//多種日期版本擇一寫法
				var dates_html = [];
				var dateObj = false;
				for(var di in json.dates)	{	//日期有多種來源，選擇的順序為大正藏、cbeta、高麗藏
					if(json.dates[di].source === '大正藏')	{
						dateObj = json.dates[di];
						break;
					}
					else if(json.dates[di].source === 'CBETA')	{
						dateObj = json.dates[di];
						break;
					}
					else if(json.dates[di].source === '高麗藏')	{
						dateObj = json.dates[di];
						break;
					}					
				}
				if(dateObj.from)	dates_html.push('From:'+dateObj.from);
				if(dateObj.to)		dates_html.push('To:'+dateObj.to);
				if(dateObj.t)		dates_html.push(dateObj.t);	
				
				if(dates_html.length > 0)	html.push('<div class="author_info_item"><span data-lang-id="1_17">'+_GLOBALS[_GLOBALS.lang]['1_17']+'：</span><span>'+dates_html.join('，')+'</span></div>');

				//多種日期版本全列出來的寫法
				/*
				var dates_html = [];
				var dateObj = false;
				for(var di in json.dates)	{	//日期有多種來源，大正藏、cbeta、高麗藏		
					var dateItem = [];
					
					dateObj = json.dates[di];
					if(dateObj.from)	dateItem.push('From:'+dateObj.from);
					if(dateObj.to)		dateItem.push('To:'+dateObj.to);
					if(dateObj.t)		dateItem.push(dateObj.t);
					
					dates_html.push(dateItem.join('，')+' ('+dateObj.source+')');
				}
	
				if(dates_html.length > 0)	html.push('<div class="author_info_item"><span data-lang-id="1_17">'+_GLOBALS[_GLOBALS.lang]['1_17']+'：</span><span>'+dates_html.join('<br/>')+'</span></div>');
				*/
			}
			if(json.char_count && parseInt(json.char_count,10) > 0)		html.push('<div class="author_info_item"><span data-lang-id="26_77">'+_GLOBALS[_GLOBALS.lang]['26_77']+'：</span><span>'+number_format(parseInt(json.char_count,10))+'</span></div>');
			if(json.wit)			html.push('<div class="author_info_item"><span data-lang-id="22_10">'+_GLOBALS[_GLOBALS.lang]['22_10']+'：</span><span>'+json.wit+'</span><span style="cursor:pointer;margin-left:.8em" onclick="window.open(\'doc/zh/02-07_abbr_ver.php\')"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></span></div>');
			if(json.transdesc)		html.push('<div class="author_info_item"><span data-lang-id="1_18">'+_GLOBALS[_GLOBALS.lang]['1_18']+'：</span><span>'+json.transdesc+'</span></div>');
			if(json.cbc_links)		html.push('<div class="author_info_item"><span data-lang-id="26_79">'+_GLOBALS[_GLOBALS.lang]['26_79']+'：</span><span>'+json.cbc_links.map(l => '<a target="blank" href="'+l+'">'+l+'</a>').join('、')+'</span></div>');
			if(json.related_jings)	{
				var rj_html = [];
				for(var rj in json.related_jings)	{
					rj_html.push('<li><a target="_blank" href="'+json.related_jings[rj][1]+'">'+json.related_jings[rj][0]+'</a></li>');
				}
				html.push('<div class="author_info_item"><span data-lang-id="26_84">'+_GLOBALS[_GLOBALS.lang]['26_84']+'</span><span>'+'<ol style="padding-left:1.5em">'+rj_html.join('')+'</ol>'+'</span></div>');
			}			
			
			
			$("#author_info_box").find('div').html(html.join('')).end().dialog({ modal: true,resizable: true,width:'auto',position:{my: "top",at:"bottom",of:this_btn},close:function()	{ $('#author_info_box').parent().removeAttr('winxd-flag-anchor');}});
			$('#author_info_box').prev('.ui-dialog-titlebar').find('button').remove().end().append('<span class="ui-dialog-titlebar-close" onclick="$(\'#author_info_box\').dialog(\'close\')">X</span>').parent('.ui-dialog').css('max-width','650px');
			$('#author_info_box').parent('[aria-describedby="author_info_box"]').attr('winxd-flag-anchor','yes');
			this_btn.find('span.author_info_btn_icon').show().end().find('img:eq(0)').hide();
		}}).fail(function()	{
			alert('Error with get jing info.');
			this_btn.find('span.author_info_btn_icon').show().end().find('img:eq(0)').hide();
		});
	
	});
	
	//開啟作譯者經錄資訊jq dialog並點擊作品跑出modal時，jquery會有bug造成out of memory，解決方式是modal開啟時把dialog關掉，modal關掉時dialog在顯示，此段處理這個 +
	$('#jing_selector').on('hide.bs.modal', function (e) {
		var jq_dialog = $('#author_info_box').parent('[aria-describedby="author_info_box"]');
		if(jq_dialog.attr('winxd-flag-anchor') == 'yes' && !jq_dialog.is(':visible'))	{
			jq_dialog.show();
			
		}
	});
	$('#jing_selector').on('show.bs.modal', function (e) {
		var jq_dialog = $('#author_info_box').parent('[aria-describedby="author_info_box"]');
		if(jq_dialog.attr('winxd-flag-anchor') == 'yes' && jq_dialog.is(':visible'))	{
			jq_dialog.hide();
		}
	});
	//jq dialog 處理 -	
	
	//開啟/關閉目錄
	$('#toc_open_btn').click(function()	{
		var tree_min_width;
		if(parseInt($('#tree_area').css('width'),10) > 10)	{
			tree_min_width = 10;
			$('#tree_area > ul[role="tablist"]').hide();
			$(this).find('.glyphicon').next().text(_GLOBALS[_GLOBALS.lang]['1_2_1']).parent().attr('data-hint',_GLOBALS[_GLOBALS.lang]['1_2_1']);
		}
		else	{
			tree_min_width = 300;
			$('#tree_area > ul[role="tablist"]').show();
			$(this).find('.glyphicon').next().text(_GLOBALS[_GLOBALS.lang]['1_2_2']).parent().attr('data-hint',_GLOBALS[_GLOBALS.lang]['1_2_2']);
		}
		$('#tree_area').css({'width':tree_min_width+'px'});
		$('#reading-area-1').css({'width':$(window).width()-15-$('#tree_area').get(0).offsetWidth-$('#side_func').get(0).offsetWidth-$('#bookAndTree_adjuster').get(0).offsetWidth+'px'});		
	});
	
	
	//整合功能另開放大modal處理 +
	//整合功能-modal放大(移動內容)
	$('.side_func_enlarge_btn').click(function()	{
		var type = $(this).parent().parent().attr('id');
		var title = $(this).parent().text();
		var original_dom = $(this).parent().next();
		
		if(type == 'side_func_textsearch')	{	//全文檢索有綁定scroll事件，這邊的放大功能就cp一份不做mv了...
			original_dom.clone().find('tr:hidden').show().end().find('th[sortby]').removeAttr('sortby').end().appendTo('#side_func_enlarge_modal .modal-body > #enlarge_real_modal_body');
			$('#side_func_enlarge_modal .modal-body > #enlarge_real_modal_body #search_range_select_btn').remove();	//放大版移除搜尋範圍按鈕
		}
		else if(type == 'side_func_ngram')	{	//ngram iframe 複製會有非同步問題，在這邊直接弄一個假的比較簡單
			$('#side_func_ngram_iframe_enalrge_fake').show().get(0).contentWindow.postMessage(_GLOBALS.sideFuncLastKw,'https://syda.dila.edu.tw/');			
		}
		else	{	
			original_dom.attr({'original_dom_id':type}).appendTo('#side_func_enlarge_modal .modal-body > #enlarge_real_modal_body');
		}
		
		$('#enlargeModalLabel').text(title);
		$('#side_func_enlarge_modal').modal();		
	});
	
	//整合功能-modal放大(還原內容)
	$('#side_func_enlarge_modal').on('hidden.bs.modal', function (e) {
		var type = $('#side_func_enlarge_modal .modal-body > #enlarge_real_modal_body >.more_wrapper').attr('original_dom_id');
		if(type == 'side_func_authority' || type == 'side_func_dic')	$('#side_func_enlarge_modal .modal-body > #enlarge_real_modal_body > .more_wrapper').appendTo('#'+type);
		$('#side_func_enlarge_modal .modal-body > #enlarge_real_modal_body').html('');
		$('#side_func_ngram_iframe_enalrge_fake').hide();
	});		
	//整合功能另開放大modal處理 -
	
	
	//全域問題回報
	$('#global_bugReport').click(function()	{
		$('#feedback_modal').find('#feedback_modal_info_tr,#feedback_modal_err,#feedback_modal_success').hide().end().find('#feedback_modal_mail,#feedback_modal_text,#feedback_modal_subject').val('').end().find('#feedback_modal_form').show().end().modal();	//將「問題位置」隱藏再秀出		
	});	
	
	
	//複製到剪貼簿按鈕按下時(非flash，clipboard.js)
	$('#cp2clipboard_btn').click(function(e)	{
		
		//var s = $('#rac_modal .modal-body').contents().filter(function() { return $(this).is(':visible');  }).find(':hidden').remove().end().text();	//子元素中可能還有hidden，要移除 (此行為原本clipboard.js的備份)
		const formatted_text =  $('#rac_modal .modal-body').contents().filter(function() { return $(this).is(':visible');  }).find(':hidden').remove().end().html();	//子元素中可能還有hidden，要移除
		const pure_text = $('#rac_modal .modal-body').contents().filter(function() { return $(this).is(':visible');  }).find(':hidden').remove().end().find('p').after("\n").end().text();	//子元素中可能還有hidden，要移除，純文字需將p後面加入\n換行
		
		/*
		//校長新測試，未完成!!!
		var formatted_text_new = $('#rac_modal .modal-body').contents().filter(function() { return $(this).is(':visible');  }).find(':hidden').remove().end().find('.lg-cell .t').unwrap().end().html().replace(/<p[^>]+>/g,'[winxd-p-s]').replace(/<\/p>/g,'[winxd-p-e]').replace(/<div[^>]+>/g,'[winxd-div-s]').replace(/<\/div>/g,'[winxd-div-e]');
		formatted_text_new = $('<div/>').html(formatted_text_new).text().replaceAll('[winxd-p-s]','<p>').replaceAll('[winxd-p-e]','</p>').replaceAll('[winxd-div-s]','<div>').replaceAll('[winxd-div-e]','</div>');
		console.log(formatted_text_new)
		*/
		
		_GLOBALS.racTexts.pure = pure_text;
		_GLOBALS.racTexts.formatted = formatted_text;
		
		$('#rac_modal').modal('hide');
		
		//$('#cp2clipboard_textarea').val(s);	//clipboard.js的備份
		
		_GLOBALS.copyEventFromBtnFlag = true;
		document.execCommand('copy');	//2020-04-01:改用clipboard API，與下方document.addEventListener('copy') 搭配之
		
	});

	
	//clipboard API	copy事件
	document.addEventListener('copy', function(e){
		//console.log(e);
		//if(e.explicitOriginalTarget.id === 'cp2clipboard_btn')	{	//只有引用複製才需要做，不做此防備使用者按ctrl+c也會執行此段,2020-04-28:因各瀏覽器傳回的 e.explicitOriginalTarget 不同，故放棄此方法，改用下面全域變數 _GLOBALS.copyEventFromBtnFlag 判斷
		if(_GLOBALS.copyEventFromBtnFlag)	{
			e.clipboardData.setData('text/html', _GLOBALS.racTexts.formatted);
			e.clipboardData.setData('text/plain', _GLOBALS.racTexts.pure);
			
			_GLOBALS.copyEventFromBtnFlag = false;

			e.preventDefault(); // default behaviour is to copy any selected text
		}
	});	
	

	/* 2016-10-18:功能先拿掉
	//行號點擊時，開啟sat圖片()
	$(document).on('click','.lb',function()	{	//先拿掉監聽 .lineInfo，暫不做
		var class_name = $(this).attr('class');
		var lineData;
		if(class_name === 'lineInfo')	lineData = $('.lb[id *= "'+$(this).attr('line')+'"]').attr('id');
		else	lineData = $(this).attr('id');
		
		if(lineData && lineData.substr(0,1) == 'T')	{	//只有大正藏才有sat圖片
			var matches = /T([0-9]{2})n([0-9]{4})_p([0-9]{4}[a-c][0-9]{2})/.exec(lineData);
			window.open('http://21dzk.l.u-tokyo.ac.jp/imgs/limg.php?useid='+matches[2]+'_.'+matches[1]+'.'+matches[3], 'Sat', "width=500,height=400");
		}
	});
	*/
	
	//extend search(進階搜尋) 符號按鈕按下
	$('#side_func_search_extend_symbol > li').click(function()	{
		if($.trim($('#side_func_search_text').val()) === '')	$('#side_func_search_text').val(_GLOBALS[_GLOBALS.lang]['22_6_1']+' '+$(this).attr('symbol')+' '+_GLOBALS[_GLOBALS.lang]['22_6_2']);
		else	$('#side_func_search_text').val($('#side_func_search_text').val()+' '+$(this).attr('symbol')+' ');
	});
	
	//部類、冊別回上一層按鈕
	$('#selector-levels-back-btn').click(function()	{
		$('#selector-breadcrumb > li:nth-last-child(2)').trigger('click');
	});
	
	//歡迎頁面「確定」按鈕
	$('#welcome-holder-btn').click(function()	{
		var option = $('input[name="welcome-holder-option"]:checked').val();
		if(option == 'selector')	{		//開啟經選擇器
			jing_selector_show(true);
		}
		else if(option == 'go')	{	//直接開啟閱讀器，如果有welcome-holder-continue cookie就繼續上次紀錄的閱讀
			//console.log($.cookie('welcome-holder-continue'));
			if($.cookie('welcome-holder-continue'))	{
				var xml_id = $.cookie('welcome-holder-continue').split(';')[0];
				var juan = $.cookie('welcome-holder-continue').split(';')[1];
				var title = $.cookie('welcome-holder-continue').split(';')[2];
			
				update_tree(xml_id);
				updateTitle_openJing(xml_id,title,parseInt(juan,10),false,false);
				push_historyState(xml_id,parseInt(juan,10),title,false);
			}
		}
		else if(option == 'search')	{	//開啟整合搜尋
			$('#show_side_func_btn').trigger('click');
		}
		else if(option == 'random')	{		//每日一經
			console.log('random')
		}
		
		$.cookie('welcome-holder-option',option, { path:'/' , expires: 360 });	//將每次設定的都存下來
		
		if($('#welcome-holder-option-dismiss').prop('checked'))	{	//設定「下次不要顯示」
			$.cookie('welcome-holder-display','hide', { path:'/' , expires: 360 });
		}
			
		$('#welcome-holder').hide();
	});
	
	//註解popup裡面的經號要連到該經號位置
	$(document).on('click','.note_cf',function()	{
		window.open('http://'+_GLOBALS.prodHost+'/'+$(this).text());
	});
	
	
	//開啟經文匯出modal按鈕
	$('#jing_export_modal_btn').click(function()	{
		var juan = $('#tree_area #dynatree_juan ul.dynatree-container li').size() <= 0 ? '':'('+$('#tree_area #dynatree_juan ul.dynatree-container li').size()+'卷)';
		$('#jing_export_modal span[role="juan_now"]').text($('.jing-title .breadcrumb li:last').text().replace('卷/篇章',''));
		$('#jing_export_modal span[role="jing_all"]').text($('.jing-title .breadcrumb li:eq(0)').text()+juan);
		$('#jing_export_modal .modal-title').text(_GLOBALS[_GLOBALS.lang]['2_9']+' '+$('.jing-title .breadcrumb li:eq(0)').text());
		$('#jing_export_modal').modal();
	});
	
	//經文匯出格式選擇
	$('input[name="jing_export_type"]').click(function()	{
		var type = $('input[name="jing_export_type"]:checked').val();
		if(type === 'pdf' || type === 'epub' || type === 'mobi')	{
			$('input[name="jing_export_range"][value="all"]').prop('checked',true);
			$('input[name="jing_export_range"][value="now"]').parent().parent().hide();
			$('#jing_export_is_note_div').hide();
		}
		else if(type === 'txt')	{
			$('input[name="jing_export_range"]').parent().parent().show();
			$('#jing_export_is_note_div').show();
		}
		else	{
			$('input[name="jing_export_range"]').parent().parent().show();
			$('#jing_export_is_note_div').hide();
		}
	});
	
	//經文匯出執行按鈕
	$('#jing_export_btn').click(function()	{
		var type = $('input[name="jing_export_type"]:checked').val();
		var is_note_folder = type === 'txt' && $('input[name="jing_export_is_note"]:checked').val() === 'no' ? 'text':'text-with-notes';
		
		
		if($('input[name="jing_export_range"]:checked').val() == 'now')	{
			if(type == 'html')
				window.open(_GLOBALS.catalogAPI+'download/html/'+_GLOBALS.open_container['reading-area-1-container'].reading_xmlID.toRay()+'_'+_GLOBALS.juanNow+'.html');
			else if(type == 'txt')
				location.href = _GLOBALS.catalogAPI+'download/'+is_note_folder+'/'+_GLOBALS.open_container['reading-area-1-container'].reading_xmlID.toRay()+'_'+_GLOBALS.juanNow+'.txt.zip';
		}
		else if($('input[name="jing_export_range"]:checked').val() == 'all')	{
			if(type == 'html')
				location.href = _GLOBALS.catalogAPI+'download/html/'+_GLOBALS.open_container['reading-area-1-container'].reading_xmlID.toRay()+'.html.zip';
			else if(type == 'txt')
				location.href = _GLOBALS.catalogAPI+'download/'+is_note_folder+'/'+_GLOBALS.open_container['reading-area-1-container'].reading_xmlID.toRay()+'.txt.zip';
			else if(type == 'pdf' || type == 'mobi' || type == 'epub')	
				location.href = location.protocol+'//'+location.host+'/getData.php?type=ebooks_export&export_type='+$('input[name="jing_export_type"]:checked').val()+'&work='+_GLOBALS.open_container['reading-area-1-container'].reading_xmlID.toRay();
			
		}
		
	});
	
	//開啟經文傳送modal按鈕
	$('#jing_send_modal_btn').click(function()	{
		var juan = $('#tree_area #dynatree_juan ul.dynatree-container li').size() <= 0 ? '':'('+$('#tree_area #dynatree_juan ul.dynatree-container li').size()+'卷)';
		$('#jing_send_modal span[role="juan_now"]').text($('.jing-title .breadcrumb li:last').text().replace('卷/篇章',''));
		$('#jing_send_modal span[role="jing_all"]').text($('.jing-title .breadcrumb li:eq(0)').text()+juan);
		$('#jing_send_modal span[role="jing"]').text($('.jing-title .breadcrumb li:eq(0)').text());
		$('#jing_send_databaseName').val($('input[name="jing_send_range"]:checked').val() == 'all' ? _GLOBALS.open_container['reading-area-1-container'].reading_xmlID.toRay():_GLOBALS.open_container['reading-area-1-container'].reading_xmlID.toRay()+'_'+_GLOBALS.juanNow);
		$('#jing_send_form').show();
		$('#jing_send_result').hide();
		$('#jing_send_modal').modal();
	});	
	
	//經文傳送執行按鈕
	$('#jing_send_btn').click(function()	{
		docuskyUploader.upload();	
	});
	
	//經文傳送選擇範圍時，資料庫名稱的值會跟著改
	$('input[name="jing_send_range"]').click(function()	{
		$('#jing_send_databaseName').val($('input[name="jing_send_range"]:checked').val() == 'all' ? _GLOBALS.open_container['reading-area-1-container'].reading_xmlID.toRay():_GLOBALS.open_container['reading-area-1-container'].reading_xmlID.toRay()+'_'+_GLOBALS.juanNow);
	});
	
	
	//經文傳送(MARKUS)執行按鈕
	$('#jing_send_markus_btn').click(function()	{
		var uuid,url_param;
		if($('input[name="jing_send_markus_range"]:checked').val() === 'all')	{
			uuid = _GLOBALS.MARKUS_JING_UUID;		//選擇經的範圍
			url_param = 'resource_uuid';
		}
		else if($('input[name="jing_send_markus_range"]:checked').val() === 'now')	{
			uuid = _GLOBALS.MARKUS_JUAN_UUID;	//選擇卷的範圍
			url_param = 'section_uuids';
		}
		
		if(uuid)
			window.open(_GLOBALS.MarkusURL+'&'+url_param+'='+uuid);
	});
	
	//lb點擊開啟該頁圖片
	$(document).on('click','.scan-image-display-btn',function()	{
		var is_dongguk = $(this).data('dongguk') && $.trim($(this).data('dongguk')) === 'yes' ? true:false;
		var dongguk_params;
		
		if(is_dongguk)	{	//如果是韓國東國大學要另外連出去
			dongguk_params = $(this).attr('data-url-param').split('.')
			window.open('https://kabc.dongguk.edu/viewer/view?dataId=ABC_IT_'+dongguk_params[0]+'_T_'+_GLOBALS.juanNow+'&imgId='+dongguk_params[1].padStart(3,'0')+'_'+dongguk_params[2]+'_'+dongguk_params[3]);
		}
		else	{
			window.open(_GLOBALS.scannigImageURL+''+$(this).attr('data-url-param'));
		}
	});
	
	//外掛設定儲存按鈕
	$('#switch_plugin_save').click(function()	{
		$('.switch_plugin_toggle').each(function()	{
			var target = ($(this).data('related-id') === 'scan-image-display-btn' ? '.':'#')+$(this).data('related-id');
			$.cookie($(this).data('related-id'),$(this).prop('checked') ? 'on':'off',{path:'/'});	//每次設定都存下來
			if($(this).prop('checked'))	{
				$(target).show();
			}
			else	{
				$(target).hide();
			}
		});
		$('#switch_plugin_modal').modal('hide');
	});
	
	//搜尋結果匯出按鈕
	$(document).on('click','.search-export-menu-item',function()	{
		//看看是csv或excel
		if($(this).attr('id') === 'search-export-menu-item-excel')	$('form[name="search-export-post-form"] input[name="export"]').val('excel');
		else if($(this).attr('id') === 'search-export-menu-item-csv')	$('form[name="search-export-post-form"] input[name="export"]').val('csv');
		
		$('form[name="search-export-post-form"]').get(0).submit();
	});
	
	
	//首頁常用功能
	$(document).on('click','#funcarea .home-panel-functions-item-option',function(e)	{
		var action = $(this).data('action');
		if(action === 'open-jing-selector')	{
			jing_selector_show(true);
			e.stopPropagation();
		}
		else if(action === 'read-last-content')	{
			if($.cookie('last-reading-book'))	{
				var xml_id = $.cookie('last-reading-book').split(';')[0];
				var juan = $.cookie('last-reading-book').split(';')[1];
				var title = $.cookie('last-reading-book').split(';')[2];
				var scroll = $.cookie('last-reading-book-position') ? $.cookie('last-reading-book-position'):false;	//用cookie
				//alert(scroll)
			
				if(isOpenNewWindow(xml_id,parseInt(juan,10),title,false,(scroll?scroll:false)))	{	//如果將經文開在新視窗，下面也不用跑了，直接return false
					return false;	
				}
			
				update_tree(xml_id);
				updateTitle_openJing(xml_id,title,parseInt(juan,10),false,scroll);
				push_historyState(xml_id,parseInt(juan,10),title,false,(scroll?scroll:false));
			}			
		}
		else if(action === 'manual')	{
			if(_GLOBALS.lang === 'en')
				window.open('doc/en/01-content.php');
			else
				window.open('doc/zh/01-content.php');
		}
		else if(action === 'system-manual')	{
			if(_GLOBALS.lang === 'en')
				window.open('doc/en/03-00_system_oprator.php');
			else
				window.open('doc/zh/03-00_system_oprator.php');
		}
		else if(action === 'install-font')	{
			window.open('doc/zh/03-10_font_install.php');
		}
		else if(action === 'data-versions')	{
			window.open('doc/zh/version_latest.php');
		}
		else if(action === 'bug-report')	{
			$('#feedback_modal').find('#feedback_modal_info_tr,#feedback_modal_err,#feedback_modal_success').hide().end().find('#feedback_modal_mail,#feedback_modal_text,#feedback_modal_subject').val('').end().find('#feedback_modal_form').show().end().modal();	//將「問題位置」隱藏再秀出
		}
		else if(action === 'about-us')	{
			$('#aboutus_modal').modal('show');
		}
		else if(action === 'copyright')	{
			window.open('http://cbeta.org/copyright.php');
		}
	});
	
	//使用者最近操作
	$(document).on('click','.user-actions',function(e)	{
		if(e.target.tagName !== 'A' && e.target.tagName !== 'I')	return;		
		
		if($(this).data('type') === 'read')	{	//讀經
			if($(this).data('xml-id') && $(this).data('title') && $(this).data('juan'))	{
				
				if(isOpenNewWindow($(this).data('xml-id'),$(this).data('juan'),$(this).data('title'),$(this).data('xml-id').toRay()+'_'+PadDigits($(this).data('juan'),3)))	{	//如果將經文開在新視窗，下面也不用跑了，直接return false
					return false;	
				}				
				
				update_tree($(this).data('xml-id'));	//更新樹狀目錄
				updateTitle_openJing($(this).data('xml-id'),$(this).data('title'),$(this).data('juan'),false,false);
				push_historyState($(this).data('xml-id'),$(this).data('juan'),$(this).data('title'),$(this).data('xml-id').toRay()+'_'+PadDigits($(this).data('juan'),3));
			}			
		}
		else if($(this).data('type') === 'search')	{	//參考搜尋
			if($(this).data('kw'))	{
				side_func_display(true);
				$('#side_func_search_text').val($(this).data('kw'));
				$('#side_func_search_btn').trigger('click');
			}
		}
		else if($(this).data('type') === 'fulltext-search')	{	//全文檢索
			if($(this).data('kw') && $.trim($(this).data('near-words')) === "")	{
				do_fulltext_search($(this).data('kw'));
			}
			else if($(this).data('kw') && $.trim($(this).data('near-words')) !== "")	{	//near搜尋
				window.open(location.protocol+'//'+location.host+'/search/?q='+encodeURIComponent($(this).data('kw'))+'&near_words='+$.trim($(this).data('near-words'))+'&lang='+_GLOBALS.lang,'');
			}
		}
	});
	
	//刪除使用者最近操作
	$(document).on('click','.user-actions-del-btn',function()	{
		var thisIdx = parseInt($(this).parent().data('idx'),10);
		if(thisIdx >= 0 && _GLOBALS.userActions[thisIdx])	{
			_GLOBALS.userActions.splice(thisIdx,1);
			
			//回存 localstorage
			try	{
				localStorage.setItem("cbetaonline-user-actions",JSON.stringify(_GLOBALS.userActions));	//將actions存到localStorage
			}
			catch(err)	{
				var e = err;
			}
			
			loadLastActions();	//首頁重新整理
		}
		return false;
	});
	
	//首頁搜尋框按enter可啟動
	$(document).on('keyup','.home-panel-search-text',function(e)	{
		var code = e.keyCode || e.which; 
		if (code  == 13) {
			$('.home-panel-search-btn').trigger('click');
		}
	});
	
	//閱讀區scroll，記錄閱讀到的位置，使用debounce避免效能問題
	$('#reading-area-1-container').on('scroll',debounce(function()	{
		var pos = searchTopLB();
		pos = $('.lb[id *= "'+pos+'"]:eq(0)').attr('id');
		//console.log(pos);
		$.cookie('last-reading-book-position',pos,{path:'/'});	//紀錄上次閱讀的經典位置(span.t)
	},300));	
	
	
	//套疊的人名地名click
	$(document).on('click','.person_hl , .place_hl',function()	{
		var key = $(this).attr('adb-key');
		var data_type = false;
		var this_btn = $(this);
		
		if(key.match(/A[0-9]{6}/g))		data_type = 'type=person';
		else if(key.match(/PL[0-9]+/g))	data_type = 'type=place';
		
		
		if(data_type)	{
			$.getJSON(_GLOBALS.authorityROOT+'webwidget/getAuthorityData.php?'+data_type+'&id='+key+'&jsoncallback=?',function(json)	{
				//console.log(json);
				var title = json['data1'].name+' ('+key+')';
				var desc = json['data1'].note ? json['data1'].note:'';
				var adb_link = _GLOBALS.authorityROOT+data_type.replace('type=','')+'/?fromInner='+key;
				var lod_link = 'https://lod.dila.edu.tw/resource.php?id='+key;
				var html = desc+'<div style="text-align:right;margin:1em .5em 0 0"><a target="_blank" href="'+adb_link+'" style="color:#006699">DILA Authority</a> &nbsp;&nbsp;<a target="_blank" href="'+lod_link+'" style="color:#006699">DILA LOD</a></div>';
				
				$("#authority_info_box").find('div').html(html).end().dialog({ modal: false,title:title,resizable: true,width:'650px',position:{my: "top",at:"bottom",of:this_btn}});
				$('#authority_info_box').prev('.ui-dialog-titlebar').find('button').remove().end().append('<span class="ui-dialog-titlebar-close" onclick="$(\'#authority_info_box\').dialog(\'close\')">X</span>');
			})
		}
	});
	
	//online 內互相連結的功能
	$(document).on('click','.cbeta-link',function()	{
		var linehead = $(this).data('linehead');
		if(linehead)	{
			location.href = location.protocol+'//'+location.host+'/'+linehead;
			//window.open(location.protocol+'//'+location.host+'/'+linehead);
		}
	});
	
	//首頁最近操作換頁功能
	$(document).on('change','.user-actions-page-menu',function()	{
		var page = parseInt($(this).val(),10);
		$('.home-panel-last-actions:eq(1) li').hide();
		$('.home-panel-last-actions:eq(1) li').slice(((page-1)*10),((page-1)*10)+10).show().css('display','list-item');
		$('.home-panel-last-actions:eq(1)').attr('start',((page-1)*10)+1);
	});
	
	
	
	//手機版選取文字跳出功能選單功能 +
	//監聽觸控事件
	document.addEventListener('touchstart', function() {
		_GLOBALS.isTouchSelection = true;  // 表示是觸控選取
	});	
	
	//手機版選取文字跳出功能選單
    document.addEventListener('selectionchange', function(e) {
        // 獲取目前的選取範圍
        var selection = window.getSelection();
        var selectedText = selection.toString(); // 取得選取的文字
        
        if (selectedText && selection.rangeCount > 0 && _GLOBALS.isTouchSelection) {	//_GLOBALS.isTouchSelection = true 是觸控選取才觸發下面這段，否則會和原 mousedown 事件打架
			
			//console.log(e)
            var range = selection.getRangeAt(0); // 取得第一個選取範圍
            var rect = range.getBoundingClientRect(); // 獲取該範圍的座標
            var startNode = range.startContainer; // 選取範圍的起始節點
            var endNode = range.endContainer;     // 選取範圍的結束節點
			var startElement = startNode.nodeType === Node.TEXT_NODE ? startNode.parentElement : startNode;
            var endElement = endNode.nodeType === Node.TEXT_NODE ? endNode.parentElement : endNode;
			
			_GLOBALS.isTouchRange = range;	//先將此 selectionchange 得到的 range 存下來，要給蘋果系列專用的 hack

            if (!selection.isCollapsed) {
				var pass = true;
				/*
                var selectedText = selection.toString(); // 取得選取的文字
                console.log('選取文字:', selectedText);
                console.log('文字座標:', rect);
				*/
            }			
			
			
			if($(startElement).parents('#reading-area-1-container').length > 0)	{	//用 startElement 來測試是不是在閱讀區（#reading-area-1-container）裡面，是的話才顯示選單
				var top = rect.y + 70;
				var left = rect.x + 70;
				if(top + $('.ddbc-selectedMenu-dropdown-menu').height() > $(window).height())	top -= (top + $('.ddbc-selectedMenu-dropdown-menu').height())-$(window).height()+30;	//如果選單超過視窗可視範圍(太下面)，則調整top值
				if(left + $('.ddbc-selectedMenu-dropdown-menu').width() > $(window).width())	left -= (left + $('.ddbc-selectedMenu-dropdown-menu').width())-$(window).width()+30;	//如果選單超過視窗可視範圍(太右邊)，則調整left值
						
				$('.ddbc-selectedMenu-dropdown-menu').css({'left':left+'px','top':top+'px'}).show();
			}
			
        }
    });		
	//手機版選取文字跳出功能選單功能 -
	
	//page visibility api，當網頁分頁被顯示時，更新首頁最近操作
	document.addEventListener('visibilitychange', function()	{
		if(!document.hidden)	{
			loadLastActions();
		}
	}, false);
	
	//偵測瀏覽器是否被放大縮小(zoom)，有的話重新執行reize	+
	$($('#detect_zoom_frame').get(0).contentWindow).resize(function() {
		$(window).trigger('zoom');
	});
	$(window).on('zoom', function() {
		$(window).trigger('resize');
		//console.log('zoom', window.devicePixelRatio);
	});
	//偵測瀏覽器是否被放大縮小(zoom)，有的話重新執行reize	-
	
	//popstate 事件，實做pjax，啟動時切換經
	$(window).on('popstate',function()	{
		if(history.state && history.state != '')	{
			var state = history.state;
			
			if(state.xml_id === 'home')	{	//首頁處理
				showHome(true);
			}
			else	{	//不是首頁就是一般開啟經		
				update_tree(state.xml_id);	//更新樹狀目錄
				updateTitle_openJing(state.xml_id,state.title,state.juan,false,state.scroll);
			}
		}
	});
	
	
	//首頁loading page 結束時的處理
	Pace.on('done',function()	{
		hide_loading_page_holder();
	});
	
	
	//參考http://stackoverflow.com/questions/8504673/how-to-detect-on-page-404-errors-using-javascript 處理page js error顯示訊息，第三個參數一定要為true
	
	/*
	window.addEventListener('error', function(e) {		//全域錯誤處理，例如500、404都可於此處理
		var cbeta_api_error = new RegExp(_GLOBALS.catalogAPI,'g');
		if(cbeta_api_error.test(e.target.src))	//如果錯誤的來源是cbeta api，就顯示適當訊息
			alert('[無法開啟] 資料尚未收錄至本平台或伺服器網路有誤。');
	}, true);
	*/


}