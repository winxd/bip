function load_book(config)	{
	var ajax_cb = null;
	if(config.cb)	{
		ajax_cb = config.cb;
	}
	else	{
		ajax_cb = function(res)	{
			$('#reading-container-1').html(res);
			after_load();	//讀取完內容後調整content
		};
	}
	
	$.ajax({
		type: config.type,
		url: config.url,
		data: config.data,
		success:ajax_cb
	});	
}

function after_load(target,xml_id)	{
	target = (target ? target+' ':'');

	$(target+'.lb').append('：').not(':eq(0)').before('<br class="lb_br" />');
	
	//處理cbeta的自訂標題、手動加上換行，但.juan下面的.cbeta不一定是真的標題，有可能是修訂字而已，因此不能說.juan下面的.cbeta就一定是自訂標題
	//還要加上判斷，所以如果.juan.text() == .cbeta.text() 兩者文字完全相同，那應該就是自訂標題了，故用此判斷
	$(target+'.juan .cbeta').filter(function()	{	return $(this).parents('.juan:eq(0)').contents().text() === $(this).contents().text();	}).after('<br class="lb_br" />');
	
	
	//書本開啟後apply介面換行、顯示行號等選項
	interface_adjust_breakLine($.map(_GLOBALS.open_container,function(index,val)	{ return '#'+val+' #replaced';}).join(','));
	
	
	//上面科判的部份會動到content，先讓上面做完再插入noteAnchor
	$(target+'.noteAnchor').each(function()	{	//註解目前僅顯示.cb
		if($(this).hasClass('add'))	return;	// = continue，排除.add的部份;
		var star = $(this).attr('class').indexOf('star') == -1 ? false:true;
		var star_no = $(this).data('star-no') ? ' data-star-no="'+$.trim($(this).data('star-no'))+'"':'';
		var label_content = $(this).attr('href').split('-').length > 0 ? $(this).attr('href').split('-')[0]:$(this).attr('href');	//校勘裡面如有「-」則「-」後面部份不要，只取前面的部份顯示label（id不變）
		var label = star ? '＊':label_content.replace('#','').substr(-3).replace(/^0+([^0]+)/g,"$1").replace(/0+/g,'0');	//1.取非0的部份顯示 2.超過一次的0變成一次就好
		var key = $(this).data('key') ? ' data-key="'+$.trim($(this).data('key'))+'"':'';
		label = $(this).data('label') && $(this).data('label') !== '' ? $(this).data('label'):label;	//有data-label 一律已data-label顯示
		$(this).after('<span class="note-link cb" clue="'+$(this).attr('href').replace('#','')+'"'+star_no+key+'>['+label+']</span>'); //
	});	
	
	//cbeta修訂註解
	$(target+'.noteAnchor.add').each(function()	{
		var label = $(this).attr('href').replace('#','').split('_')[2].replace(/0+([^0]+)/g,"$1").replace(/0+/g,'0');	//1.取非0的部份顯示 2.超過一次的0變成一次就好
		var key = $(this).data('key') ? ' data-key="'+$.trim($(this).data('key'))+'"':'';
		$(this).after('<span class="note-link-cbeta" clue="'+$(this).attr('href').replace('#','')+'" '+key+'>[A'+label+']</span>');
	});		
	
	/*	//2020-08-24:圖片連結改用ray的api處理，前端就不處理圖片了
	//內建graphic處理
	$('.graphic').each(function()	{
		$(this).append('<img src="images/figures/'+$(this).attr('imgsrc').substr(0,1)+'/'+$(this).attr('imgsrc')+'" />');
	});
	*/
	
	//悉曇字、蘭札體處理：附加上圖片
	$('.siddam,.ranja').each(function()	{
		var className = $(this).hasClass('siddam') ? 'sd-gif':'rj-gif';
		$(this).append('<img src="images/'+className+'/'+$(this).attr('code').split('-')[1].substr(0,2)+'/'+$(this).attr('code')+'.gif" />');
		
		if($(this).hasClass('siddam') && $.trim($(this).attr('roman')) != '')	{	//為悉曇字加上英文，像cbeta那樣
			$(this).append('('+$(this).attr('roman')+')');
		}

	});
	
	//推測字處理
	$('.unclear').each(function()	{
		var cert = $(this).data('cert') === 'medium' ? '中':($(this).data('cert') === 'low' ? '低':'');
		var data_hint = cert !== '' ? '本字為推測字，信心程度：'+cert:'此處文字無法辨識';
		
		$(this).attr('data-hint',data_hint);
	});
	
	//南傳PTS 頁碼
	$('.hint').each(function()	{
		$(this).attr('title',$(this).data('text'));
	});
	
	//lg引用複製特殊處理
	$('.lg-cell:last-child').append("\t");						//對每個lg-cell的最後一個塞入特殊字元，for引用複製可支援換行，於引用複製時取代掉
	$('.lg-cell:not(:last-child)').append('&nbsp;&nbsp;');		//對不是該行最後一個的lg-cell塞入空格，以方便引用複製時每個cell中間能有間隔
	
	//依照head層級 data-head-level 調整head縮排間距
	$('[data-head-level]').each(function()	{
		$(this).css('margin-left',(parseInt($(this).attr('data-head-level'),10)+1)+'em');
	});
	
	//雙行小字註解前後的小括號需要能反白，改用此法並捨棄原本的css before/after作法(pseudo class無法反白)
	$('.doube-line-note').prepend('(').append(')');
	
	//「行間」註解加上小括號
	$('.interlinear-note').prepend('(').append(')');
	
	//「.lg.note1、.lg.note2」加上小括號
	$('.lg.note1 .lg-cell:first .t:first, .lg.note2 .lg-cell:first .t:first').prepend('(');
	$('.lg.note1 .lg-cell:last .t:last, .lg.note2 .lg-cell:last .t:last').append(')')
		
	//增加頁尾「顯示版權資訊」開關按鈕
	$('#cbeta-copyright').before('<br/><a class="display_copyright_btn" href="javascript:void(null)"> <span class="glyphicon glyphicon-plus" style="margin-right:1em"></span>顯示版權資訊</a>');
	
	//如有開啟作譯者經錄視窗，重新開經時一律關閉
	if($('.ui-dialog #author_info_box').size() >= 1)	$('#author_info_box').dialog('close');
	
	/*
	//為lb每個新頁碼前插入圖片瀏覽按鈕(2018-12-03:此為我做的依照頁碼前端產生之版本，因ray已於後端做了，故暫時移除，改用下面區塊的程式處理)
	var acceptCanon = ['T','GA','GB'];	//僅T、GA、GB有圖檔
	var canon_check = /([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB)([0-9]{3,4}[A-Za-z]?)/i.exec(_GLOBALS.open_container[_GLOBALS.active_panel+'-container']['reading_xmlID'].toRay());
	if($.inArray(canon_check[1],acceptCanon) !== -1)	{
		var lb_page_check = false;
		$('.lb').each(function()	{
			var tmp = /([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB)([0-9]{2,3})n([ABab]?[0-9]{3,4}[A-Za-z]?)_?p([a-z]?)([0-9]{3,4})([a-z])([0-9]{2})/i.exec($(this).attr('id'));
			var page = 'p'+tmp[4]+parseInt(tmp[5],10);
			var page_str = 'p'+tmp[4]+tmp[5];
			var canon = tmp[1];
			var vol = 'v'+tmp[2];
			
			if(page !== lb_page_check)	{
				$(this).after('<span class="label label-success scan-image-display-btn" data-page="'+page+'" data-url-param="'+canon+vol+page_str+'"><span class="glyphicon glyphicon-picture" aria-hidden="true" style="display:inline;vertical-align:text-top"></span></span>')
				//console.log(canon+vol+page);
			}
			lb_page_check = page;
		});
	}
	*/
	
	//預處理圖片瀏覽的格式與按鈕(後端tag由ray產生，但前端這邊還是得製作為跟之前一樣的格式才能使用) +
	//2024-07-24：此為一般圖片連結，加上 data-s 不為 dongguk 才可以照原本邏輯處理
	$('.facsimile[data-s != "dongguk"]').each(function()	{
		var short_code = !$(this).data('canon') || $.trim($(this).data('canon')) === '' ? '':_GLOBALS.CBETAShortCode[$.trim($(this).data('canon'))];
		var ref = $(this).data('ref');
		var page = /[^p]+p([abcxyz]?)(.+)/g.exec($(this).data('ref'));
		page = short_code+'p'+(page[1] ? page[1]:'')+page[2];
		$(this).replaceWith('<span class="label label-success scan-image-display-btn" data-page="'+page+'" data-url-param="'+ref+'"><span class="glyphicon glyphicon-picture" aria-hidden="true" style="display:none;vertical-align:text-top"></span></span>');
	});
	
	//data-s = "dongguk" 代表為要連到韓國東國大學之高麗藏圖檔，於此處理
	$('.facsimile[data-s = "dongguk"]').each(function()	{
		var params = $(this).data('ref').split('.');
		var label = '【麗】'+'p'+params[2]+params[3];
		//var label = 'K. '+'p'+params[2]+params[3];
		$(this).replaceWith('<span class="label label-success scan-image-display-btn" data-page="'+label+'" data-url-param="'+$(this).data('ref')+'" data-dongguk="yes"><span class="glyphicon glyphicon-picture" aria-hidden="true" style="display:none;vertical-align:text-top"></span></span>');
	});
	
	
	//預處理圖片瀏覽的格式與按鈕(後端tag由ray產生，但前端這邊還是得製作為跟之前一樣的格式才能使用) -
	
	
	
	
	//.line_space填入空格
	$('.line_space').each(function()	{
		$(this).css('width',$(this).data('size')+'em');
	});
	
	//table 表格特殊處理，如果.bip-table html有"style=margin-left" 屬性，則將屬性值填入其底下的.bip-table-row，讓其在原書換行(display:inline)時「每行」也有margin-left效果
	$('#body .bip-table').each(function()	{
		var marginLeft = getComputedStyle($(this).get(0))['margin-left'];
		if(marginLeft)	{
			$(this).find('.bip-table-row:not(:eq(0))').css('margin-left',marginLeft);	//因為第一個.bip-table-row在原書換行(display:inline)時會受到父元素.bip-table的margin-left影響，故第一個.bip-table-row不須套用
		}
	});
	
	
	//2021-07-22:後端有問題，會造成無窮迴圈，要查，先關掉此功能(2021-11-2已修復)
	//加入人名地名套疊
	if($('[class="person_start"]').length > 0 || $('[class="place_start"]').length > 0)	{	//有人名或地名才做
		$('#reading-area-1-container').textNodes().wrap('<span/>');	//將所有textnodes套上span，因為是anchor需做跨tag，必須保證每個元素都有span包起
		
		/*
		//製作唯一的id，因為人名地名可能在文本中重複
		$('[class="person_start"]').each(function(i,v)	{	$(this).attr('id','person_start_'+$(this).data('key')+'_'+i);});
		$('[class="person_end"]').each(function(i,v)	{	$(this).attr('id','person_end_'+$(this).data('key')+'_'+i);});
		$('[class="place_start"]').each(function(i,v)	{	$(this).attr('id','place_start_'+$(this).data('key')+'_'+i);});
		$('[class="place_end"]').each(function(i,v)	{	$(this).attr('id','place_end_'+$(this).data('key')+'_'+i);});	
		*/
		//人名執行跨tag anchor式 hl
		
		$('[class="person_start"]').each(function()	{	
			var key = $(this).data('key');
			var s = $(this).attr('id');
			var e = 'person_end_'+$(this).attr('id').split('_')[2];
			//console.log(s+' '+e)
			_GLOBALS.highlight_adb_person_obj.go(s,e);
		});
		
		//地名執行跨tag anchor式 hl
		$('[class="place_start"]').each(function()	{	
			var key = $(this).data('key');
			var s = $(this).attr('id');
			var e = 'place_end_'+$(this).attr('id').split('_')[2];
			_GLOBALS.highlight_adb_place_obj.go(s,e);
		});	
		
		//表格裡面會有 textnode wrap 的空 span 會造成表格顯示問題，故須移除此類 span 空標
		$('#reading-area-1-container table.bip-table span').each(function()	{
			if($.trim($(this).text()) === '')	{
				$(this).remove();
			}
		});
	}
	
	//$('#reading-area-1-container table.bip-table span:empty').remove();
	
	//如果本卷是ai標點，則加入訊息顯示
	display_AI_punc_msg();

	
	//從cookie讀回設定字型(如果有的話)
	if($.cookie('font-size'))	{
		$('#config_fontsize_text').val($.cookie('font-size'));	//使用直接指定字型的方式
		setFontSize('config_fontsize_text');
	}
	
	//從cookie讀回設定行距(如果有的話)
	if($.cookie('line-height'))	{
		$('#config_linespacing_text').val($.cookie('line-height'));	//使用直接指定行距的方式
		setLinespacingSize('config_linespacing_text');
	}
	
	//從cookie讀回設定字距(如果有的話)
	if($.cookie('letter-spacing'))	{
		$('#config_letterspacing_text').val($.cookie('letter-spacing'));	//使用直接指定字距的方式
		setLetterspacingSize('config_letterspacing_text');
	}
	
	
	
	
	
	//更換語系
	change_lang();
	
	//由於取回外掛設定需等待after_load完成(例如掃描圖片是動態放上的)，故放在本after_load函式後面執行之
	restorePluginSetting();
	
	//處理從url給q和l的hightlight情況、以及從url給note_clue校勘popup要跳出
	process_url_highlight(xml_id.toRay(),parseInt(_GLOBALS.juanNow,10));
	
	//加入與更新內文上下卷切換選單
	//**！注意：此function有ajax call，效果在ajax cb裡面完成，為避免不同步衝突問題此行宜放在after_load最後面執行
	update_inline_juan_menu(xml_id);	
	
	//編輯模式測試
	//$(target).textNodes().wrap('<span class="winxd_text" />');
	//$('.winxd_text').prop('contenteditable',true);
	//$('.t').prop('contenteditable',true);
	
	//grande.bind(document.querySelectorAll(".t"));	//需要編輯開此行即可，真的可用，且可對編輯的內文加上粗體、斜體等效果
	
}

//部類、冊別查詢 2020-03-19:增加支援 promise 
var bulei_vol_resHandle = function(text,desc,param)	{	//部類、冊別查詢結果處理函式
	//if(desc == 'vol')	return;//function closed 
	var deferred = $.Deferred();
	$('#selector-levels-ajax-loader > .ajax-loader-type-1').css('visibility','visible');
	$.getJSON(_GLOBALS.catalogAPI+'/catalog_entry?q='+param+'&callback=?',function(res)	{
		var json = res.results;
		var html = '<div style="float:left;width:410px">';
		var counter = 0;
		$('#selector-breadcrumb').html('<li class="selector-top-menu selector-side-menu" desc="'+desc+'"><a>'+text+'</a></li>');
		
		for(var i in json)	{
			if(counter++ == Math.round(Object.size(json) / 2))	html += '</div><div style="float:left;width:410px">';
			
			if(json[i]['file'])		//有file的話表示為最後一層，點了要準備開啟經
				html += '<button type="button" level="1" class="btn btn-default btn-sm selector-btn-final" n="'+json[i].n+'" desc="'+desc+'" label="'+json[i]['label']+'" xml_id="'+json[i]['file']+'"  '+(json[i]['node_type'] == 'html' ? 'data-open-new="yes"':'')+'  juan_start="'+(json[i]['juan_start'] ? json[i]['juan_start']:1)+'" '+(json[i]['alt_title'] ? 'alt_title="'+json[i]['alt_title']+'" alt_word="'+json[i]['alt_work']+'"':'')+'  '+(json[i]['lb'] ? 'lb="'+json[i]['lb']+'"':'')+'><span class="glyphicon glyphicon-file"></span>'+json[i]['label']+'</button>';
			else
				html += '<button type="button" level="1" class="btn btn-default btn-sm selector-multi-level-btn" n="'+json[i].n+'" desc="'+desc+'" label="'+json[i]['label']+'"><span class="glyphicon glyphicon-folder-open"></span>'+json[i]['label']+'</button>';
		}
		html += '</div>';
		$('#selector-levels').html(html);
		$('#selector-levels-ajax-loader > .ajax-loader-type-1').css('visibility','hidden');
		deferred.resolve();
		
	});
	return deferred.promise();
};
	
//顯示ai標點訊息
function display_AI_punc_msg()	{
	$('#ai-prompt-msg').remove();
	if($('#body').is('[data-punc="AI"]') && $('#config_showPunctuation').prop('checked'))	{	//該卷需為ai標點、且設定中「顯示標點」需打勾才需要顯示訊息
		if($('#config_showAIPunctuation').prop('checked'))
			$('#body[data-punc="AI"]').prepend('<div id="ai-prompt-msg" class="alert alert-success"><button type="button" class="close" data-dismiss="alert" style="margin:4px 0 0 15px">×</button><i class="fa fa-lightbulb-o" aria-hidden="true"></i>&nbsp;'+_GLOBALS[_GLOBALS.lang]['24_19']+'<a onclick="$(\'#config_showAIPunctuation\').trigger(\'click\');display_AI_punc_msg();" style="cursor:pointer">'+_GLOBALS[_GLOBALS.lang]['24_20']+'</a></div>');
		else
			$('#body[data-punc="AI"]').prepend('<div id="ai-prompt-msg" class="alert alert-success"><button type="button" class="close" data-dismiss="alert" style="margin:4px 0 0 15px">×</button><i class="fa fa-lightbulb-o" aria-hidden="true"></i>&nbsp;'+_GLOBALS[_GLOBALS.lang]['24_19']+'<a onclick="$(\'#config_showAIPunctuation\').trigger(\'click\');display_AI_punc_msg();" style="cursor:pointer">'+_GLOBALS[_GLOBALS.lang]['24_21']+'</a></div>');
	}	
}

//字形設定function
function setFontSize(func)	{
	var containers = $.map(_GLOBALS.open_container,function(index,val)	{ return '#'+val+' #replaced';}).join(',');
	containers = containers.replace(/#replaced/g,'.lb');
	var nowFontSize = parseInt($(containers.replace(/ \.lb/g,'')).eq(0).css('font-size'),10);
			
	if(func == 'config_fontsize_plus')	nowFontSize += 1;
	else if(func == 'config_fontsize_minus')	nowFontSize -= 1;
	else if(func == 'config_fontsize_text')	nowFontSize = parseInt($('#config_fontsize_text').val(),10);
			
	if(nowFontSize > 50 || nowFontSize < 12)	{	//字形最大不超過50、最小不低於12
		if(func == 'config_fontsize_text')	{
			alert(_GLOBALS[_GLOBALS.lang]['21_7']);
			$('#config_fontsize_text').val(parseInt($(containers.replace(/ \.lb/g,'')).eq(0).css('font-size'),10));
		}
		return false;
	}
		
	$(containers+','+containers.replace(/ \.lb/g,'')).css({'font-size':nowFontSize+'px'});
	$(containers.replace(/ \.lb/g,' .head')+','+containers.replace(/ \.lb/g,' .juan')).css({'font-size':(nowFontSize+4)+'px'});	//.head,.juan有另設字型大小故須額外處理，其與內文字體差7px		
	$('#config_fontsize_text').val(nowFontSize);
	$.cookie('font-size',nowFontSize, { path:'/' , expires: 360 });
}

//行距設定function
function setLinespacingSize(func)	{
	var containers = $.map(_GLOBALS.open_container,function(index,val)	{ return '#'+val+' #replaced';}).join(',');
	containers = containers.replace(/#replaced/g,'.lb');
	var nowLinespacingSize = parseFloat($(containers.replace(/ \.lb/g,'')).eq(0).css('line-height'),10);
	//console.log(nowLinespacingSize)

			
	if(func == 'config_linespacing_plus')	nowLinespacingSize += 0.8;
	else if(func == 'config_linespacing_minus')	nowLinespacingSize -= 0.8;
	else if(func == 'config_linespacing_text')	nowLinespacingSize = parseFloat($('#config_linespacing_text').val(),10);
	
	nowLinespacingSize = nowLinespacingSize.toFixed(1);	//取到小數點後一位即可
			
	if(nowLinespacingSize > 45.0 || nowLinespacingSize < 25.0)	{	//間距最大不超過40、最小不低於25
		if(func == 'config_linespacing_text')	{
			alert(_GLOBALS[_GLOBALS.lang]['27_08']);
			$('#config_linespacing_text').val(parseFloat($(containers.replace(/ \.lb/g,'')).eq(0).css('line-height'),10));
		}
		return false;
	}
		
	$(containers+','+containers.replace(/ \.lb/g,'')).css({'line-height':nowLinespacingSize+'px'});
	//$(containers.replace(/ \.lb/g,' .head')+','+containers.replace(/ \.lb/g,' .juan')).css({'font-size':(nowLinespacingSize+4)+'px'});	//.head,.juan有另設字型大小故須額外處理，其與內文字體差7px		
	$('#config_linespacing_text').val(nowLinespacingSize);
	$.cookie('line-height',nowLinespacingSize, { path:'/' , expires: 360 });
}

//字距設定function
function setLetterspacingSize(func)	{
	//此程式有很多+5是因為與實際設定值差5
	var containers = $.map(_GLOBALS.open_container,function(index,val)	{ return '#'+val+' #replaced';}).join(',');
	containers = containers.replace(/#replaced/g,'.lb');
	var nowLetterspacingSize = parseInt($(containers.replace(/ \.lb/g,'')).eq(0).css('letter-spacing'),10)+5;
	//console.log(nowLetterspacingSize)

	
	if(func == 'config_letterspacing_plus')	nowLetterspacingSize += 1;
	else if(func == 'config_letterspacing_minus')	nowLetterspacingSize -= 1;
	else if(func == 'config_letterspacing_text')	nowLetterspacingSize = parseInt($('#config_letterspacing_text').val(),10);
	
	
	if(nowLetterspacingSize > 13 || nowLetterspacingSize < 1)	{	//間距最大不超過9、最小不低於1
		if(func == 'config_letterspacing_text')	{
			alert(_GLOBALS[_GLOBALS.lang]['27_09']);
			$('#config_letterspacing_text').val(parseInt($(containers.replace(/ \.lb/g,'')).eq(0).css('letter-spacing'),10)+5);
		}
		return false;
	}
		
	$(containers+','+containers.replace(/ \.lb/g,'')).css({'letter-spacing':(nowLetterspacingSize-5)+'px'});
	//$(containers.replace(/ \.lb/g,' .head')+','+containers.replace(/ \.lb/g,' .juan')).css({'letter-spacing':(nowLetterspacingSize+4)+'px'});	//.head,.juan有另設字型大小故須額外處理，其與內文字體差7px		
	$('#config_letterspacing_text').val(nowLetterspacingSize);
	$.cookie('letter-spacing',nowLetterspacingSize, { path:'/' , expires: 360 });
}
	
function restorePluginSetting()	{	//從cookie讀回上次的外掛設定

	$('.switch_plugin_toggle').each(function()	{
		$(this).prop('checked',($.cookie($(this).data('related-id')) == 'on' ? true:false)).change();
	});			
	$('#switch_plugin_save').trigger('click');	
}

function panel_switch(target)	{
	if(target == 'kepan')	{
		$('#integrated-area').hide();
		$('#reading-area,#kepan-book-area').css({'width':'50%'}).show();
		//$('#kepan-tree-area').css({'width':'20%'}).show();
		
		
	}
	else if(target == 'normal')	{	
		$('#integrated-area').show();
		$('#reading-area').css({'width':'50%'}).show();
		$('#kepan-tree-area,#kepan-book-area').hide();
		
	}
	_GLOBALS.panelMode = target;
}

function adjustPage()	{	//微調整體頁面
	
	//調整#side_func 裡面的tab-pane內容的高度符合頁面 +
	var tempoary_show = false;
	if($('#side_func').is(':hidden'))	{	//調整#side_func高度時，如果display:none是取不到高度值的，故先暫時讓他顯示出來，等等再藏回去
		$('#side_func').css({'display':''});
		tempoary_show = true;
	}
	
	$('#side_func .tab-pane').css({'height':$(window).height()-60+'px'});
	
	//如果原本#side_func是不顯示，那要藏回去
	if(tempoary_show)
		$('#side_func').css({'display':'none'});
		
	//調整#side_func 裡面的accordions內容的高度符合頁面 -
	
	
}

function selectedTextSearchCB(clickedMenu,selText)	{	//文本閱讀區反白選單click後的行為

	if($('#side_func').is(':hidden') && clickedMenu.attr('url') == 'query')	{	//如果#side_func被關掉且選擇的是「參考資料查詢」的話，就讓#side_func重見天日
		side_func_display(true);
	}

	//全文檢索(2020-09-17:新增全文檢索，連到新介面)
	if(clickedMenu.attr('url') == 'fulltextSearch')	{		
		do_fulltext_search(_GLOBALS.racObj.get_pure_text(),_GLOBALS.open_container[_GLOBALS.active_panel+'-container']['reading_xmlID'].toRay());
	}
	
	//全文檢索-本經(2020-09-17:新增全文檢索，連到新介面)
	if(clickedMenu.attr('url') == 'fulltextSearchThisJing')	{
		do_fulltext_search(_GLOBALS.racObj.get_pure_text(),_GLOBALS.open_container[_GLOBALS.active_panel+'-container']['reading_xmlID'].toRay(),true);
	}

	//引用複製
	if(clickedMenu.attr('url') == 'refAndCopy')	{	//如果是引用複製就特別處理，不做整合功能選單，在這return掉
		/* //原bookTitle是抓文本第一個.juan裡面的純text，由於格式將與cbeta相同，因此現在棄用了
		var bookInnerContainer = $('span.lb[id="'+_GLOBALS.racObj.get_line().startLine+'"]').parents('div#body');
		var bookTitle = (bookInnerContainer.find('.juan:eq(0)').text() != '' ? bookInnerContainer.find('.juan:eq(0)').clone().children('.note-link').remove().end().text():'');	//必須要clone，否則會真的remove到頁面的children node
		*/
		var formatted_cite_text = _GLOBALS.racObj.get_content($('#reading-area-1-container'));
		
		//下面這行non_formatted_cite_text 比較複雜：僅有.refAndCopy_main_text的內容需要取text()，除此之外的html標記都需保留（因為有行號之前之後等功能需要），又因為取了text()之後，原本lib標上的span.refAndCopy_note與span.refAndCopy_punctuations會消失，故還要在這邊加回去，否則設定裡面的顯示校勘和顯示標點功能會因缺span tag而無法使用
		var non_formatted_cite_text = $('<div/>').html(_GLOBALS.racObj.get_content($('#reading-area-1-container'))).find('.refAndCopy_main_text').html(_GLOBALS.racObj.wrap_punctuations($('<div/>').html(formatted_cite_text).find('.refAndCopy_main_text').text()).replace(/(\[[A0-9＊]+\])/g,"<span class='refAndCopy_note'>$1</span>").replace(/(\[[科標解][0-9]+\])/g,"<span class='refAndCopy_note'>$1</span>")).end().html();
		
		$('#rac_modal').find('.modal-body div[role="formatted"]').html($('<div/>').html(formatted_cite_text).find('.lineInfo').remove().end().css({'pointer-events':'none'}).get(0).outerHTML);	//有格式的
		$('#rac_modal').find('.modal-body div[role="non-formatted"]').html(non_formatted_cite_text);	//沒有格式的
		
		
		//判斷裝置是否為 iphone 或 ipad，是的話把引用複製 modal 中「複製到剪貼簿」按鈕藏起，因為這兩裝置按下去會有問題
		if(isIphoneOrIpad())	{
			$('#cp2clipboard_btn').hide();
		}
		else	{
			$('#cp2clipboard_btn').show();
		}
		
		$('#rac_modal').modal();
		refAndCopy_config_adjust('all');
		return false;
	}
	
	//問題回報
	if(clickedMenu.attr('url') == 'feedback')	{	//問題回報不做整合功能選單，在這return掉
		/* //原bookTitle是抓文本第一個.juan裡面的純text，由於格式將與cbeta相同，因此現在棄用了
		var bookInnerContainer = $('span.lb[id="'+_GLOBALS.racObj.get_line().startLine+'"]').parents('div#body');
		var bookTitle = (bookInnerContainer.find('.juan:eq(0)').text() != '' ? bookInnerContainer.find('.juan:eq(0)').clone().children('.note-link').remove().end().text():'');
		*/
		$('#feedback_modal').find('#feedback_modal_info').html(_GLOBALS.racObj.get_content($('#reading-area-1-container'))).end().find('#feedback_modal_mail,#feedback_modal_text,#feedback_modal_subject').val('').end().find('#feedback_modal_err,#feedback_modal_success').hide().end().find('#feedback_modal_form,#feedback_modal_info_tr').show();
		$('#feedback_modal').modal();	
		return false;
	}
	
	//一般複製
	if(clickedMenu.attr('url') == 'copy')	{	
		//console.log(selText)
		$('#normal_copy_elm').val(_GLOBALS.racObj.get_pure_text());
		return false;
	}
	
	//URL複製
	if(clickedMenu.attr('url') == 'urlCopy')	{	
		//console.log(_GLOBALS.racObj.get_line());
		var line = _GLOBALS.racObj.get_line().startLine;
		if(line)	{
			$('#url_copy_elm').val(location.protocol+'//'+location.host+'/'+_GLOBALS.lang+'/'+line);
			$('#url_copy_trigger_btn').trigger('click');
		}
		return false;
	}	
	
	//字數計算
	if(clickedMenu.attr('url') == 'wordcount')	{	
		//console.log(_GLOBALS.racObj.get_line_range()+_GLOBALS.racObj.get_text());
		var pure_text = _GLOBALS.racObj.get_pure_text();
		pure_text = pure_text.replace(/[ \s]+|(<br\/>)+/g,'');	//racObj會去除所有標點，但我們這邊還得去除空白（去空白不能於rac裡面實做，因為引用複製有時需要空白），還有於lg自動附上的"<br/>" 也要去除
		//console.log(pure_text)
		alert("反白的字數為(不含標點)："+[...Array.from(pure_text)].length);
		return false;
	}

	//自動標點
	if(clickedMenu.attr('url') == 'autoDoPunc')	{	
		var pure_text = _GLOBALS.racObj.get_pure_text();
		$('form[name="auto-do-punc-form"] textarea').val(pure_text);
		$('form[name="auto-do-punc-form"]').submit();
		return false;
	}	
	
	//自動分詞
	if(clickedMenu.attr('url') == 'autoDoWordseg')	{	
		var pure_text = _GLOBALS.racObj.get_pure_text();
		$('form[name="auto-do-wordseg-form"] textarea').val(pure_text);
		$('form[name="auto-do-wordseg-form"]').submit();
		return false;
	}
	
	//資料查詢-單欄or雙欄模式
	if(_GLOBALS.panelMode == 'SINGLE_COLUMN')	{
		$('#side_func_search_text').val(_GLOBALS.racObj.get_pure_text());
		$('#side_func_search_btn').trigger('click');
	}
	else if(_GLOBALS.panelMode == 'TWO_COLUMN')	{
		if($('#footer_content').is(':hidden'))	$('#go_footer').trigger('click');	//雙欄模式啟動footer
		$('#xxxx').val(selText);	//待補：雙框模式下方整合功能的搜尋文字框
		$('#xxxx_func_search_btn').trigger('click');
		$('#footer_tab a[href="#'+clickedMenu.attr('url')+'"]').tab('show');	//顯示選定的功能之tab
	}
}

function append_more_button(mainSelector)	{	//規範資料庫、字典、dlmbs需要「more」按鈕，由此產生
	$('#'+mainSelector+' .side_func_more_btn').remove();	//防止more button多次查詢時重複出現故須清空
	var moreBtn = '<button class="btn btn-default btn-xs dropdown-toggle pull-right side_func_more_btn" type="button" clue="'+mainSelector+'"><span class="glyphicon glyphicon-fullscreen"></span>&nbsp;&nbsp;More </button>';
	$('#'+mainSelector+' > .panel-body').prepend(moreBtn);	
	return;
}

function do_dictionary(parentID,selText)	{	//查字典功能
	$(parentID+'#footer_func_dic_content').html('<h3>Loading...</h3>');
	$(parentID+'span[role="dic_searchCounter"]').html('');
	
	var deferred = $.Deferred();
	var xhr = $.ajax({
		url:'getData.php',
		data:'type=dict&term='+encodeURIComponent(selText),
		type:'POST',	
		success:function(json)	{
			json = $.parseJSON(json);
			if(!json || json.length <= 0 || json.status == 500)	{
				$(parentID+'#footer_func_dic_content').html('「<span style="color:red">'+selText+'</span>」 '+_GLOBALS[_GLOBALS.lang]['21_1']);
				$(parentID+'#footer_func_dic_toolbar_labels').html('');
				$(parentID+'span[role="dic_searchCounter"]').html('0').next().hide();	//顯示搜尋筆數
				deferred.resolve();
				return false;
			}
			var html = [],
				label_html = [],
				grouped_html = {},
				grouped_label_html = {},
				grouped_dic_c = {},
				dic_c = 0;
			for(var index in json)	{
				
				var dic_label = (json[index]['dict_name_zh'] ? json[index]['dict_name_zh']:json[index]['dict_name_en_short']);
				if(dic_label == 'NTI' || dic_label == 'Soothill Hodous')	{	//處理群組型資料，將原json回傳的一筆筆資料「整合」成一個tab和內容，目前NTI辭典需要這樣做，以後如果還有直接加在if即可，這是萬用程式
					if(!grouped_label_html[dic_label])	{	//此部份只跑一次
						dic_c++;
						grouped_label_html[dic_label] = '<span items_link="dic_item_'+dic_c+'" class="label label-default footer_func_dic_items_labels">'+dic_label+'</span>';
						grouped_html[dic_label] = [];
						grouped_dic_c[dic_label] = dic_c;
					}
					grouped_html[dic_label].push('<span class="footer_func_dic_term">'+json[index].term+'</span><br/><span>'+json[index].desc+'</span>');
				}
				else	{
					dic_c++;
					label_html.push('<span items_link="dic_item_'+dic_c+'" class="label label-default footer_func_dic_items_labels">'+dic_label+'</span>');
					html.push('<div items_link="dic_item_'+dic_c+'" class="footer_func_dic_items"><span class="footer_func_dic_term">'+json[index].term+'</span><span class="footer_func_dic_short_name">'+(json[index]['dict_name_zh'] ? json[index]['dict_name_zh']:json[index]['dict_name_en'])+'</span><br/><span>'+json[index].desc+'</span></div>');
				}
				
			}
			
			//製作more button
			//append_more_button('side_func_dic');
					
			//群組型資料轉成字串
			var grouped_label_html_str = Object.keys(grouped_label_html).map(function(k)	{	return grouped_label_html[k];	}).join('');
			var grouped_html_str = Object.keys(grouped_html).map(function(k)	{	return '<div items_link="dic_item_'+grouped_dic_c[k]+'" class="footer_func_dic_items">'+grouped_html[k].join('<hr/>')+'</div>';	}).join('');
					
			$(parentID+'#footer_func_dic_toolbar_labels').html(label_html.join('')+grouped_label_html_str);
			$(parentID+'#footer_func_dic_content').html(html.join('')+grouped_html_str).find('.footer_func_dic_items').eq(0).show();
			$(parentID+'.footer_func_dic_items_labels').eq(0).removeClass('label-default').addClass('label-success');
			
			$(parentID+'span[role="dic_searchCounter"]').html(dic_c > 999 ? '999+':dic_c).next().hide();	//顯示搜尋筆數
			deferred.resolve();
		}
	});		
	
	xhr.contentID = '#footer_func_dic_content,#footer_func_dic_toolbar_labels';
	_GLOBALS.xhrObjs.push(xhr);
	return deferred.promise();
}

function do_ngram(iframeID,selText)	{	//ngram viewer
	
	var search_range = $('#textsearch_range_value').val() == '' ? '':$('#textsearch_range_value').val();
	var data = {
		selText:selText,
		range:search_range
	}
	document.getElementById(iframeID).contentWindow.postMessage(JSON.stringify(data),'https://syda.dila.edu.tw');
}

function do_authority(parentID,selText)	{	//authority
		var handleJSON = function(json,type)	{
			json = $.parseJSON(json);
			if(!json)	{				
				return ['','',0];
			}
			
			var html = [],
				label_html = [];
			for(var index in json)	{
				var special_str = '';
				
				if(!json[index].name)	continue;	//連名字都沒有的，跳過(可能是已合併資料)
					
				if(type == 'person')	{
					if(json[index]['name'])				special_str += '<div><div>人名：</div><div><a target="_blank" href="http://authority.dila.edu.tw/'+type+'/?fromInner='+json[index]['authorityID']+'">'+json[index]['name']+'</a></div></div>';
					if(json[index]['names'])			special_str += '<div><div>別名：</div><div>'+json[index]['names']+'</div></div>';
					if(json[index]['bornDateBegin'] || json[index]['diedDateEnd'])	{
						if(json[index]['bornDateBegin'] && !json[index]['diedDateEnd'])	special_str += '<div><div>生年：</div><div>'+json[index]['bornDateBegin']+'</div></div>';
						if(!json[index]['bornDateBegin'] && json[index]['diedDateEnd'])	special_str += '<div><div>生年：</div><div>'+json[index]['diedDateEnd']+'</div></div>';
						if(json[index]['bornDateBegin'] && json[index]['diedDateEnd'])	special_str += '<div><div>生卒年：</div><div>'+json[index]['bornDateBegin']+' ~ '+json[index]['diedDateEnd']+'</div></div>';
					}
				}
				else if(type == 'place')	{
					if(json[index]['name'])				special_str += '<div><div>地名：</div><div><a target="_blank" href="http://authority.dila.edu.tw/'+type+'/?fromInner='+json[index]['authorityID']+'">'+json[index]['name']+'</a></div></div>';
					if(json[index]['names'])			special_str += '<div><div>別名：</div><div>'+json[index]['names']+'</div></div>';
					if(json[index]['dynasty'])			special_str += '<div><div>朝代：</div><div>'+json[index]['dynasty']+'</div></div>';
					if(json[index]['lat'] && json[index]['long'])	special_str += '<div><div>經緯度：</div><div>'+json[index]['long']+','+json[index]['lat']+'</div></div>';
					if(json[index]['districtModern'])	special_str += '<div><div>行政區：</div><div>'+json[index]['districtModern']+'</div></div>';
				}
				var authority_item_label = '<label link_clue="'+type+'_'+json[index]['authorityID']+'" class="label label-default func_authority_labels">'+(type == 'person' ? '<span class="glyphicon glyphicon-user"></span>':'<span class="glyphicon glyphicon-globe"></span>')+'&nbsp;'+json[index]['name']+'</label>';
				var authority_item_content = '<div id="'+type+'_'+json[index]['authorityID']+'" class="func_authority_items">'+special_str+(json[index]['note'] ? '<div><div>註解：</div><div>'+json[index]['note']+'</div></div>':'')+'</div>';
				
				html.push(authority_item_content);
				label_html.push(authority_item_label);
			}
			
			return [html,label_html,Object.size(json)];
			
			//'結果共 '+Object.size(json)+' 筆'
		};
		
		var deferred = $.Deferred();
		
		$(parentID+'span[role="authority_searchCounter"]').html('');
		
		//2014-05-10:改成先人名、後地名呈現，非原本的分開處理，所以地名需包在人名的callback裡面、地名處理完後再顯示最終結果
		var xhr_person = $.ajax({url:'getData.php',data:'type=authority&sub_type=person&id='+encodeURIComponent(selText),type:'POST',success:function(json)	{

			var person_res = handleJSON(json,'person');
			var xhr_place = $.ajax({url:'getData.php',data:'type=authority&sub_type=place&id='+encodeURIComponent(selText),type:'POST',success:function(json)	{

				var place_res = handleJSON(json,'place');
				
				//製作more button
				//append_more_button('side_func_authority');
				
				$(parentID+'#func_authority_title').html((person_res[1] ? person_res[1].join(''):'')+(place_res[1] ? place_res[1].join(''):''));
				$(parentID+'#func_authority_content').html((person_res[0] ? person_res[0].join(''):'')+(place_res[0] ? place_res[0].join(''):''));
			
				$(parentID+'#func_authority_title .func_authority_labels:eq(0)').trigger('click');	//啟動第一個label的click
				$(parentID+'span[role="authority_searchCounter"]').html((person_res[2]+place_res[2]) > 999 ? '999+':(person_res[2]+place_res[2])).next().hide();	//顯示搜尋筆數
				if(person_res[2]+place_res[2] <= 0)	$(parentID+'#func_authority_content').html('「<span style="color:red">'+selText+'</span>」'+_GLOBALS[_GLOBALS.lang]['21_1']);
				
				deferred.resolve();
			}});
			xhr_place.contentID = '#func_authority_content,#func_authority_title';
			_GLOBALS.xhrObjs.push(xhr_place);

		}});
		xhr_person.contentID = '#func_authority_content,#func_authority_title';
		_GLOBALS.xhrObjs.push(xhr_person);
		
		return deferred.promise();
}

function do_textsearch(parentID,selText,sortby)	{	//全文檢索
	_GLOBALS.searchOffset = 0;	//每次搜尋時重設offset
	_GLOBALS.searchSortby = '';	//每次搜尋時重設searchSortby(此變數只有在表格排序事件處理時用到，為了排序後能處理infinite scroll時資料的一致性(沒有一樣的sortby取出順序不同資料會不連續、有誤))
	
	var deferred = $.Deferred();
	
	$(parentID+' #func_textsearch_result_panel').html('<h3>Please wait...</h3>');
	$(parentID+'span[role="textsearch_searchCounter"]').html('');
	
	if(parseInt(selText.length,10) > 30)	{	//搜尋字數長度不得大於30
		$(parentID+' #func_textsearch_result_panel').html('<span style="color:red">'+_GLOBALS[_GLOBALS.lang]['21_2']+'</span>');
		$(parentID+'span[role="textsearch_searchCounter"]').next().hide();
		deferred.resolve();		
		return;
	}
	
	var search_range = $('#textsearch_range_value').val() == '' ? '':$('#textsearch_range_value').val();	//search_range預設是搜全部(大正藏、卍續藏、嘉興藏)，故參數給空
	
	addUserAction('search',{kw:selText,search_range:search_range});	//將搜尋加入上次操作
	
	//原get版本：$.getJSON('http://dev.dila.edu.tw:8080/LuceneSearch/Search01?format=json&ver=0.1&range='+search_range+'&qry='+encodeURIComponent(selText)+'&callback=?',function(json)	{ ...});
	
	var xhr = $.ajax({
		url:'getData.php',
		data:'type=context_search&range='+search_range+'&rows='+_GLOBALS.searchScrollLimit+'&offset='+_GLOBALS.searchOffset+'&qry='+encodeURIComponent(selText)+(sortby ? '&sortby='+sortby:''),
		type:'POST',
		success:function(json)	{
			var json = $.parseJSON(json);
			var use_tc_re_search = '';	//是否要用繁體中文重新搜尋之html
			
			if(!json || parseInt(json.num_found,10) <= 0)	{
				$(parentID+' #func_textsearch_result_panel').html('「<span style="color:red">'+selText+'</span>」 '+_GLOBALS[_GLOBALS.lang]['21_1']);
				deferred.resolve();
			}
			
			if(json.tc_counts && parseInt(json.total_term_hits,10) < parseInt(json.tc_counts.hits,10))	{	//如果繁體搜尋結果的筆數大於json.num_found（可能這邊對方輸入是簡體），則提示要不要改用繁體搜尋
				//use_tc_re_search = '<div class="alert alert-success" style="margin-bottom:15px;display:table;padding:.75rem 1.75rem;">'+_GLOBALS[_GLOBALS.lang]['24_13']+json.tc_counts.q+_GLOBALS[_GLOBALS.lang]['24_14']+json.tc_counts.hits+_GLOBALS[_GLOBALS.lang]['24_15']+'<a href="javascript:void(null)" q="'+json.tc_counts.q+'" onclick="$(\'#side_func_search_text\').val(this.getAttribute(\'q\'));$(\'#side_func_search_btn\').trigger(\'click\');">'+_GLOBALS[_GLOBALS.lang]['24_16']+'</a> '+_GLOBALS[_GLOBALS.lang]['24_17']+json.tc_counts.q+_GLOBALS[_GLOBALS.lang]['24_18']+'</div>';
				use_tc_re_search = '<div class="alert alert-success" style="margin-bottom:15px;display:table;padding:.75rem 1.75rem;">'+_GLOBALS[_GLOBALS.lang]['24_25']+'<a href="javascript:void(null)" q="'+json.tc_counts.q+'" onclick="$(\'#side_func_search_text\').val(this.getAttribute(\'q\'));$(\'#side_func_search_btn\').trigger(\'click\');">'+json.tc_counts.q+'</a></div>';
			}
			
			//四種格式匯出備份：var exportMenu = '<div class="dropdown pull-right" style="clear:both"><button class="btn btn-default btn-xs dropdown-toggle" type="button" id="" data-toggle="dropdown"><span class="glyphicon glyphicon-export"></span>&nbsp;&nbsp;匯出<span class="caret"></span></button><ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1"><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(null)" onclick="javascript:$(\'#side_func_textsearch_table\').tableExport({type:\'excel\',escape:\'false\'});">Excel</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(null)" onclick="javascript:$(\'#side_func_textsearch_table\').tableExport({type:\'xml\',escape:\'false\'});">XML</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(null)" onclick="javascript:$(\'#side_func_textsearch_table\').tableExport({type:\'json\',escape:\'false\'});">JSON</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(null)" onclick="javascript:$(\'#side_func_textsearch_table\').tableExport({type:\'csv\',escape:\'false\'});">CSV</a></li></ul></div>';			
			var searchRangeMenu = '<div class="dropdown pull-right" style="margin:-2px 5px 0 0"><button class="btn btn-default btn-xs dropdown-toggle" type="button" id="search_range_select_btn" data-toggle="dropdown">'+_GLOBALS[_GLOBALS.lang]['2_5']+'：<span role="title" style="color:red">'+(_GLOBALS.lastSearchRange ? _GLOBALS.lastSearchRange:_GLOBALS[_GLOBALS.lang]['2_6'])+'</span><span class="caret"></span></button><ul id="textsearch_range_menu" class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1"><li role="all"><a role="menuitem" tabindex="-1" href="javascript:void(null)">'+_GLOBALS[_GLOBALS.lang]['2_6']+'</a></li><li role="custom"><a role="menuitem" tabindex="-1" href="javascript:void(null)">'+_GLOBALS[_GLOBALS.lang]['2_7']+'</a></li><li role="single"><a role="menuitem" tabindex="-1" href="javascript:void(null)">'+_GLOBALS[_GLOBALS.lang]['2_8']+'</a></li></ul></div>';
			var html = '<div class="table-responsive"><table id="side_func_textsearch_table" class="table table-hover table-condensed"><thead><tr>'+(_GLOBALS.isExtendedSearch ? '':'<th sortBy="term_hits">'+_GLOBALS[_GLOBALS.lang]['2_10']+'</th>')+'<th sortBy="jing" title="依照經號排序">'+_GLOBALS[_GLOBALS.lang]['2_11']+'</th><th style="display:none">'+_GLOBALS[_GLOBALS.lang]['2_14']+'</th><th sortBy="title" title="依照經名排序">'+_GLOBALS[_GLOBALS.lang]['2_12']+'</th><th sortBy="juan">'+_GLOBALS[_GLOBALS.lang]['2_11_1']+'</th><th sortby="author" title="依照作譯者排序">'+_GLOBALS[_GLOBALS.lang]['2_13']+'</th><th sortBy="time">'+_GLOBALS[_GLOBALS.lang]['22_12']+'</th></tr></thead><tbody>';
			for(var index in json['results'])	{
				var time_str = (json['results'][index]['time_from'] ? json['results'][index]['time_from']:'') + (json['results'][index]['time_from'] && json['results'][index]['time_to'] ? '~':'') +(json['results'][index]['time_to'] ? json['results'][index]['time_to']:'');
				html += '<tr xml_id="'+json['results'][index]['file']+'" juan="'+json['results'][index]['juan']+'" jing_title="'+json['results'][index]['title']+'">'+(_GLOBALS.isExtendedSearch ? '':'<td>'+json['results'][index]['term_hits']+'</td>')+'<td>'+json['results'][index]['work']+'</td><td style="display:none">'+json['results'][index]['category']+'</td><td>'+json['results'][index]['title']+'</td><td>'+json['results'][index]['juan']+'</td><td>'+(json['results'][index]['byline'] ? json['results'][index]['byline']:'')+'</td><td>'+time_str+'</td></tr>';
			}
			html += '</tbody></table></div>';
			
			$(parentID+'span[role="textsearch_searchCounter"]').html(json.total_term_hits > 999 ? '999+':json.total_term_hits).next().hide();	//顯示搜尋筆數
			
			//舊的table2excel匯出備份 //var exportMenu = '<button class="btn btn-default btn-xs dropdown-toggle pull-right" type="button" id="" onclick="javascript:$(\'#side_func_textsearch_table\').tableExport({type:\'excel\',escape:\'false\',winxdFileName:\'CBETA_export_'+new Date().YMDHIS('mm-dd-hh-ii')+'\',defaultTitle:\''+(_GLOBALS[_GLOBALS.lang]['2_2']+' <span style=color:red>'+selText+'</span>：'+(json.num_found)+' '+_GLOBALS[_GLOBALS.lang]['2_3']+(_GLOBALS.isExtendedSearch ? '':'、'+json.total_term_hits+' '+_GLOBALS[_GLOBALS.lang]['2_4']))+'\'});"><span class="glyphicon glyphicon-export"></span>&nbsp;&nbsp;'+_GLOBALS[_GLOBALS.lang]['2_9']+' </button>';
			//原本的url get方式備份	//var exportMenu = '<div class="dropdown pull-right" style="margin-top:-2px"><button class="btn btn-default btn-xs dropdown-toggle" type="button" id="" data-toggle="dropdown"><span class="glyphicon glyphicon-export"></span>&nbsp;&nbsp;'+_GLOBALS[_GLOBALS.lang]['2_9']+'<span class="caret"></span></button><ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1"><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(null)" onclick="javascript:window.open(\'getData.php?export=excel&range='+($('#textsearch_range_value').val() == '' ? '':$('#textsearch_range_value').val())+(sortby ? '&sortby='+sortby:'')+'&rows=9999999&offset=0&qry='+encodeURIComponent(selText)+'\')">Excel</a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(null)" onclick="javascript:window.open(\'getData.php?export=csv&range='+($('#textsearch_range_value').val() == '' ? '':$('#textsearch_range_value').val())+(sortby ? '&sortby='+sortby:'')+'&rows=9999999&offset=0&qry='+encodeURIComponent(selText)+'\')">CSV</a></li></ul></div>';
			
			//匯出方式改用post，因為搜尋範圍參數可能太長，用原本的get會出包
			$('form[name="search-export-post-form"] input[name="range"]').val(($('#textsearch_range_value').val() == '' ? '':$('#textsearch_range_value').val())+(sortby ? '&sortby='+sortby:''));
			$('form[name="search-export-post-form"] input[name="qry"]').val(selText);
			var exportMenu = '<div class="dropdown pull-right" style="margin-top:-2px"><button class="btn btn-default btn-xs dropdown-toggle" type="button" id="" data-toggle="dropdown"><span class="glyphicon glyphicon-export"></span>&nbsp;&nbsp;'+_GLOBALS[_GLOBALS.lang]['2_9']+'<span class="caret"></span></button><ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1"><li role="presentation"><a id="search-export-menu-item-excel" class="search-export-menu-item role="menuitem" tabindex="-1" href="javascript:void(null)">Excel</a></li><li role="presentation"><a id="search-export-menu-item-csv" class="search-export-menu-item" role="menuitem" tabindex="-1" href="javascript:void(null)">CSV</a></li></ul></div>';			
			
			var search_res_info = '<span class="pull-left">「'+selText+'」 ( '+(json.num_found)+' '+_GLOBALS[_GLOBALS.lang]['2_3']+(_GLOBALS.isExtendedSearch ? ')</span>':','+json.total_term_hits+' '+_GLOBALS[_GLOBALS.lang]['2_4']+')</span>');
			$(parentID+' #func_textsearch_result_panel').html(use_tc_re_search+search_res_info+exportMenu+searchRangeMenu+html);
			
			if(sortby)	{	//全文檢索有按下表頭升降冪排序時加入上下箭頭指示功能
				var sortField = sortby.split(':')[0];
				var sortCaret = (sortby.split(':')[1] == 'desc' ? '<span class="caret"></span>':'<span class="dropup"><span class="caret"></span></span>');
				$('#side_func_textsearch_table').find('th[sortby="'+sortField+'"]').append(sortCaret);
			}
			
			deferred.resolve();
		}
	});	
	xhr.contentID = '#func_textsearch_result_panel';
	_GLOBALS.xhrObjs.push(xhr);
	
	if(location.hostname == _GLOBALS.prodHost)	{	//紀錄使用者搜了什麼關鍵字，送到ga(in prod)
		//GA3
		//ga('send', 'event', 'Full Text Search', 'start search', selText);
		
		//GA4
		gtag('event', 'Full Text Search', {'event_category': 'Search Keyword','event_label': selText,'value': selText});
	}
	
	return deferred.promise();
}

function do_dlmbs(parentID,selText)	{	//DLMBS
	var deferred = $.Deferred();
	$(parentID+'#footer_func_dlmbs_content').html('<h3>Loading...</h3>');	
	$(parentID+'span[role="dlmbs_searchCounter"]').html('');

	var xhr = $.ajax({url:'getData.php',data:'type=dlmbs&q='+encodeURIComponent(selText),type:'POST',success:function(json)	{
	//$.getJSON('http://buddhism.lib.ntu.edu.tw/DLMBS/jsondata?action=getDLMBS&sort=topic&pagesize=999999&pageno=1&q='+encodeURIComponent(selText)+'&callback=?',function(json)	{
		json = $.parseJSON(json);
		if(!json || parseInt(json.rows,10) <= 0)	{
			$(parentID+'#footer_func_dlmbs_content').html('「<span style="color:red">'+selText+'</span>」 '+_GLOBALS[_GLOBALS.lang]['21_1']);
			$(parentID+'#footer_func_dlmbs_toolbar_labels').html('');
			$(parentID+'span[role="dlmbs_searchCounter"]').html('0').next().hide();	//顯示搜尋筆數
			deferred.resolve();
			return false;
		}
		var html = [],
			label_html = [],
			dlmbs_c = 0;
		
		var total_page = (parseInt(json.rows,10) / _GLOBALS.DLMBS_num_of_page)+(parseInt(json.rows,10) % _GLOBALS.DLMBS_num_of_page == 0 ? 0:1);
		for(var page = 1; page <= total_page; page++)	{
			label_html.push('<span class="label label-default footer_func_dlmbs_pages" page_num="'+page+'">'+page+'</span>');
		}
		
		/*
		//製作more button
		append_more_button('side_func_dlmbs');
		*/
		
		$(parentID+'#footer_func_dlmbs_toolbar_labels').html(label_html.join(''));
		$(parentID+'#footer_func_dlmbs_content').html('<ol style="padding-left: 30px;">'+produce_dlmbs_content(json,selText)+'</ol>');
		$(parentID+'.footer_func_dlmbs_pages').eq(0).removeClass('label-default').addClass('label-success');
		
		//頁數大於5筆的話先藏起來...
		if($(parentID+'.footer_func_dlmbs_pages').size() > 5)
			$(parentID+'.footer_func_dlmbs_pages').slice(5).hide().parent().append('<span id="dotdotdot" style="margin-left:7px">..... <a class="side_func_more_btn" clue="side_func_dlmbs">more</a></span>');
		
		$(parentID+'span[role="dlmbs_searchCounter"]').html(parseInt(json.rows,10) > 999 ? '999+':parseInt(json.rows,10)).next().hide();	//顯示搜尋筆數
		
		deferred.resolve();
	}});
	xhr.contentID = '#footer_func_dlmbs_content,#footer_func_dlmbs_toolbar_labels';
	_GLOBALS.xhrObjs.push(xhr);
	return deferred.promise();
}

function produce_dlmbs_content(json,selText)	{
	var html = [];
		dlmbs_c = 0;
	for(var index in json['source'])	{
		dlmbs_c++;
		if(dlmbs_c > _GLOBALS.DLMBS_num_of_page)	break;
		var fields = [];
		var main_link = '<a href="https://buddhism.lib.ntu.edu.tw/DLMBS/search/search_detail.jsp?seq='+json['source'][index].seq+'&q='+selText+'" target="_blank">'+json['source'][index].topic+'</a>';
		var authors_id = json['source'][index].authorseq.split(';');
		var authors = $.map(json['source'][index].author.split(';'),function(e,i)	{	return '<a href="https://buddhism.lib.ntu.edu.tw/DLMBS/author/authorinfo.jsp?ID='+authors_id[i]+'" target="_blank">'+e+'</a>';	}).join(';');
		var source_topic = $.trim(json['source'][index].source_topic) != '' ? '《'+json['source'][index].source_topic+'》':'';
		var dateinfo = ($.trim(json['source'][index].archive) != '' ? json['source'][index].archive+'  ':'')+($.trim(json['source'][index].press_time) != '' ? '('+json['source'][index].press_time+')':'');
		var fulltext = json['source'][index].bfulltext=="1" ? '[<a href="'+json['source'][index].path +'" target="_blank">全文</a>]':'';
			
		fields.push(main_link,authors,source_topic,dateinfo,json['source'][index].page,fulltext);
		fields = $.grep(fields,function(elm,i){	return $.trim(elm) != ''	});	//去除空白元素
			
		html.push('<li items_link="dlmbs_item_'+dlmbs_c+'" class="footer_func_dlmbs_items">'+fields.join('&nbsp;,&nbsp;')+'</li>');
	}
	return html.join('');
}

function interface_adjust_breakLine(containers)	{	//介面調整：原書換行、行號四種排列組合、***目前內文僅有p為block，之後如有增加block元素就需要調整了...
		//下面這行當.lb_br數量太多時可能有點慢呢...太慢需想辦法解決
		$(containers.replace('#replaced','.lb_br')).css({'display':$('#config_breakLine').prop('checked') ? '':'none'});
		//$(containers.replace('#replaced','.abnormal_lg')).css({'display':$('#config_breakLine').prop('checked') ? 'none':''});
		$(containers.replace('#replaced','p.pre .lb_br')).css({'display':''});	//p.pre下的.lb_br一律顯示，強迫換行
		$(containers.replace('#replaced','.honorific')).prev('.lb_br').css('display','');	//.honorific的prev sibling lb_br也一律強迫換行
		
		
		//原書換行+顯示行號
		if($('#config_breakLine').prop('checked') && $('#config_showLineNo').prop('checked'))	{
			/*	//2020-02-11：chrome 80 問題已解
			if(_GLOBALS.isChrome)	{	//Chrome在原書換行有bug，顯示訊息提醒之，如果哪天bug沒了再把這段拿掉
				if(_GLOBALS.lang === 'zh')
					alert("您使用Chrome瀏覽器，因該瀏覽器在使用「原書換行」功能時可能會造成畫面卡頓，目前我們正在尋求解決方案，如果您也遇到相同問題，您可以嘗試：\n1. 改用 Firefox、Edge 瀏覽器\n2. 取消「原書換行」功能");
				else
					alert("You using the Chrome browser, which may cause a screen freeze when using the \"Arrange Text as in Printed Edition\" function. Currently we are looking for a solution. If you also encounter the same problem, you can try: \n1. Use Firefox or Edge instead. \n2. Cancel the \"Arrange Text as in Printed Edition\" function");
			}
			*/
			
			$(containers.replace('#replaced','.lg,.lg-row,.lg-cell,#body .bip-table,#body .bip-table-row,#body .bip-table-cell')).css({'display':'inline'});
			$(containers.replace('#replaced','p:not(#back .footnote p)')).css({'display':'inline'});	$(containers.replace('#replaced','#body div[class*="div-"]')).css({'display':'inline'});	$(containers.replace('#replaced','#body ul')).css({'display':'inline','padding-left':'0'});	$(containers.replace('#replaced','#body li')).css({'display':'inline'});
			$(containers.replace('#replaced','.lb')).show();
			$(containers.replace('#replaced','.lineInfo')).hide();
			
			$('#body .bip-table').removeClass('bip-table-bordered');	//表格框線會隨著是否inline而調整
			$('.byline').removeClass('byline-break-by-modern').addClass('byline-break-by-jing');	//byline之縮排調整
			$(containers.replace('#replaced','')).removeClass('break-line-modern break-line-jing').addClass('break-line-jing');	//不依原書換行時，p與lg需額外設定縮排
			div2table(true);
		}
		//不依原書換行+顯示行號(預設)
		else if(!$('#config_breakLine').prop('checked') && $('#config_showLineNo').prop('checked'))	{
			$(containers.replace('#replaced','.lg,#body .bip-table')).css({'display':'table'});	$(containers.replace('#replaced','.lg-row,#body .bip-table-row')).css({'display':'table-row'});	$(containers.replace('#replaced','.lg-cell,#body .bip-table-cell')).css({'display':'table-cell'});
			$(containers.replace('#replaced','p:not(#back .footnote p)')).css({'display':'block'});	$(containers.replace('#replaced','#body div[class*="div-"]')).css({'display':'block'});	$(containers.replace('#replaced','#body ul')).css({'display':'block','padding-left':'40px'});	$(containers.replace('#replaced','#body li')).css({'display':'list-item'});
			$(containers.replace('#replaced','.lb')).hide();
			$(containers.replace('#replaced','.lineInfo')).show();
			
			$('#body .bip-table').addClass('bip-table-bordered');	//表格框線會隨著是否inline而調整
			$('.byline').removeClass('byline-break-by-jing').addClass('byline-break-by-modern');	//byline之縮排調整
			$(containers.replace('#replaced','')).removeClass('break-line-modern break-line-jing').addClass('break-line-modern');	//不依原書換行時，p與lg需額外設定縮排
			div2table();
		}
		//原書換行+不顯示行號
		else if($('#config_breakLine').prop('checked') && !$('#config_showLineNo').prop('checked'))	{
			/* //2020-02-11：chrome 80 問題已解
			if(_GLOBALS.isChrome)	{	//Chrome在原書換行有bug，顯示訊息提醒之，如果哪天bug沒了再把這段拿掉
				if(_GLOBALS.lang === 'zh')
					alert("您使用Chrome瀏覽器，因該瀏覽器在使用「原書換行」功能時可能會造成畫面卡頓，目前我們正在尋求解決方案，如果您也遇到相同問題，您可以嘗試：\n1. 改用 Firefox、Edge 瀏覽器\n2. 取消「原書換行」功能");
				else
					alert("You using the Chrome browser, which may cause a screen freeze when using the \"Arrange Text as in Printed Edition\" function. Currently we are looking for a solution. If you also encounter the same problem, you can try: \n1. Use Firefox or Edge instead. \n2. Cancel the \"Arrange Text as in Printed Edition\" function");
			}
			*/
			
			$(containers.replace('#replaced','.lg,.lg-row,.lg-cell,#body .bip-table,#body .bip-table-row,#body .bip-table-cell')).css({'display':'inline'});
			$(containers.replace('#replaced','p:not(#back .footnote p)')).css({'display':'inline'});	$(containers.replace('#replaced','#body div[class*="div-"]')).css({'display':'inline'});	$(containers.replace('#replaced','#body ul')).css({'display':'inline','padding-left':'0'});	$(containers.replace('#replaced','#body li')).css({'display':'inline'});
			$(containers.replace('#replaced','.lb')).hide();
			$(containers.replace('#replaced','.lineInfo')).hide();
			
			$('#body .bip-table').removeClass('bip-table-bordered');	//表格框線會隨著是否inline而調整
			$('.byline').removeClass('byline-break-by-modern').addClass('byline-break-by-jing');	//byline之縮排調整
			$(containers.replace('#replaced','')).removeClass('break-line-modern break-line-jing').addClass('break-line-jing');	//不依原書換行時，p與lg需額外設定縮排
			div2table(true);
		}
		//不依原書換行+不顯示行號
		else if(!$('#config_breakLine').prop('checked') && !$('#config_showLineNo').prop('checked'))	{
			$(containers.replace('#replaced','.lg,#body .bip-table')).css({'display':'table'});	$(containers.replace('#replaced','.lg-row,#body .bip-table-row')).css({'display':'table-row'});	$(containers.replace('#replaced','.lg-cell,#body .bip-table-cell')).css({'display':'table-cell'});
			$(containers.replace('#replaced','p:not(#back .footnote p)')).css({'display':'block'});	$(containers.replace('#replaced','#body div[class*="div-"]')).css({'display':'block'});	$(containers.replace('#replaced','#body ul')).css({'display':'block','padding-left':'40px'});	$(containers.replace('#replaced','#body li')).css({'display':'list-item'});
			$(containers.replace('#replaced','.lb')).hide();
			$(containers.replace('#replaced','.lineInfo')).hide();
			
			$('#body .bip-table').addClass('bip-table-bordered');	//表格框線會隨著是否inline而調整
			$('.byline').removeClass('byline-break-by-jing').addClass('byline-break-by-modern');	//byline之縮排調整
			$(containers.replace('#replaced','')).removeClass('break-line-modern break-line-jing').addClass('break-line-modern');	//不依原書換行時，p與lg需額外設定縮排
			div2table();
		}	

		//是否顯示校勘
		if($('#config_showVarients').prop('checked'))	{
			$(containers.replace(/(#replaced|\s)/g,'')).removeClass('varients-hide').addClass('varients-show');
			//$('#rac_conf_isNote').prop('checked',true);	$('.refAndCopy_note').show();	//引用複製裡面的是否顯示校勘也跟著作動(2022-01-07:取消作動)
		}
		else	{
			$(containers.replace(/(#replaced|\s)/g,'')).removeClass('varients-show').addClass('varients-hide');
			//$('#rac_conf_isNote').prop('checked',false);	$('.refAndCopy_note').hide();	//引用複製裡面的是否顯示校勘也跟著作動(2022-01-07:取消作動)
		}
		
		//是否顯示標點
		if($('#config_showPunctuation').prop('checked'))	{
			$(containers.replace(/(#replaced|\s)/g,'')).removeClass('punctuations-hide').addClass('punctuations-show');
			$('#config_showAIPunctuation').prop({'disabled':false}).parents('li:eq(0)').removeClass('disabled');	//與ai標點連動
			$('#rac_conf_isPunc').prop('checked',true);	$('.refAndCopy_punctuation').show();	//引用複製裡面的是否顯示標點也跟著作動
		}
		else	{
			$(containers.replace(/(#replaced|\s)/g,'')).removeClass('punctuations-show').addClass('punctuations-hide');
			$('#config_showAIPunctuation').prop({'disabled':true}).parents('li:eq(0)').addClass('disabled');	//與ai標點選項連動
			$('#rac_conf_isPunc').prop('checked',false);	$('.refAndCopy_punctuation').hide();	//引用複製裡面的是否顯示標點也跟著作動
		}
		
		//是否顯示AI標點
		if($('#config_showAIPunctuation').prop('checked'))	{
			$(containers.replace(/(#replaced|\s)/g,'')).removeClass('ai-punctuations-hide').addClass('ai-punctuations-show');
		}
		else	{
			$(containers.replace(/(#replaced|\s)/g,'')).removeClass('ai-punctuations-show').addClass('ai-punctuations-hide');
		}
		
		//是否顯示功能選單
		_GLOBALS.showContextMenu = ($('#config_contextMenu').prop('checked') ? false:true);
		
		//護眼色
		if($('#config_eyeProtection').prop('checked'))	{
			$(containers.replace(/(#replaced|\s)/g,'')).removeClass('theme_3').addClass('theme_3');
		}
		else	{
			$(containers.replace(/(#replaced|\s)/g,'')).removeClass('theme_3');
		}
		
}
	
function updateTitle_openJing(xml_id,lastTitle,specify_juan,highlight,scroll)	{	//更換文本時重設標題，並產生相關的卷選單，如有指定第三參數specify_juan的話，則預設開啟該卷，否則從第一卷開始
		if(!_GLOBALS.active_panel)	return;
		if(_GLOBALS.prodHost === 'cbetaonline.cn' && xml_id.toRay().match(/^TX|Y/))	{	//中國版特別處理：TX 和 Y 於本開啟經程式判斷，符合條件則不可閱讀，屏蔽掉
			//alert('TX or Y 屏蔽中!!');
			location.href = './';
		}
		var deferred = $.Deferred();
		var newTitle = [];
		var start_juan = 1;				//起始的卷號(不一定從1開始喔)
		var start_juan_name = '卷/篇章 一';		
		var author_info_btn = true ? '<li id="author_info_btn" xml_id="'+xml_id.toRay()+'" title="經錄背景" data-lang-id="1_1" class="disable_select"><span class="author_info_btn_icon">&nbsp;</span><img src="images/ajax-loader.gif" style="width:20px;display:none"></li>':'';	//經錄資料庫目前只有T和K，因此經名是T開頭才有做譯者資訊
		
		newTitle.push('<li class="active" title="'+lastTitle+'">'+lastTitle+'</li>'+author_info_btn);
		
		newTitle.push('<li class="active">'+(specify_juan ? '卷/篇章 '+getChineseNumber(parseInt(specify_juan,10)):start_juan_name)+'</li>');
		$('#jing_info_area .breadcrumb').html(newTitle.join(''));	//重設標題
		
		addUserAction('read',{xml_id:xml_id,title:lastTitle,juan:specify_juan});	//將讀經加入上次操作
		gtag('event', 'Reading Book', {'event_category': 'Reading book name','event_label': xml_id.toRay()+' '+lastTitle+(specify_juan ? '卷'+getChineseNumber(parseInt(specify_juan,10)):''),'value': xml_id.toRay()+' '+lastTitle+(specify_juan ? '卷'+getChineseNumber(parseInt(specify_juan,10)):'')});	//將正在讀的經加入 ga 自訂事件送回去
		
		showHome(false);	//進入閱讀模式，關閉首頁模式
		
		if($('#jing_selector').is(':visible'))	jing_selector_show(false);	//關閉選擇器
		
		_GLOBALS.titleNow = lastTitle;	//紀錄目前的經名到全域變數，有很多用途
		document.title = lastTitle+' - 卷/篇章 '+parseInt(specify_juan,10)+' | '+_GLOBALS[_GLOBALS.lang]['23'];	//根據經名+卷號重設網頁標題
		
		//開啟經
		$('#'+_GLOBALS.active_panel+'-container').html('<h1>Reading...</h1>');
		
		_GLOBALS.juanNow = PadDigits(specify_juan ? specify_juan:start_juan,3);	//紀錄目前的卷到全域變數，內文上下卷切換、引用複製會用到
		
		//var url_param = 'html/'+RegExp.$1+'/'+RegExp.$1+RegExp.$2+'/'+xml_id+'_'+PadDigits(specify_juan ? specify_juan:start_juan,3)+'.htm';
		
		localStorage.removeItem('XML-'+xml_id+'_'+specify_juan);	//2019-05-20：暫時不用快取了，一有就砍掉！
		if(highlight || !localStorage.getItem('XML-'+xml_id+'_'+specify_juan))	{	//需要highlight的文本(hl需由php幫忙，無法cache)、或是storage取不到項目的文本，從server撈
			var url_param = _GLOBALS.catalogAPI+'juans?work='+xml_id.toRay()+'&juan='+specify_juan+'&edition=CBETA&work_info=1';
			url_param = highlight ? 'colorKeyword.php?book_url='+encodeURIComponent(url_param)+'&kw='+encodeURIComponent($('#side_func_search_text_hidden').text())+'&skip_inline_note='+(_GLOBALS.skipInlineNoteHL ? 'yes':'no') : url_param+'&callback=?';	//有傳highlight的話則使用kwic去highlight
			//load_book({type:'GET',url:url_param,data:null,cb:function(res)	{
			$.getJSON(url_param,function(res)	{
					if(res.num_found == 0)	{
						alert(_GLOBALS[_GLOBALS.lang]['21_3']);
						//location.href = '.';	//重新讀取頁面
						return;
					}
					_GLOBALS.MARKUS_JUAN_UUID = res.uuid;			//記錄每一卷的MARKUS UUID，以備後續傳送時使用
					_GLOBALS.MARKUS_JING_UUID = res.work_info.uuid;	//記錄每一經的MARKUS UUID，以備後續傳送時使用
					
					//2021-07-01：根據 api 的經名再次重設標題，因為本來經選擇器的最後一層是直接拿按鈕的經名當經名，這會讓兩邊不一致，統一用 api 的
					$('#jing_info_area .breadcrumb > li:eq(0)').html(res.work_info.work+' '+res.work_info.title).attr('title',res.work_info.work+' '+res.work_info.title);
					
					res = res.results[0];
					load_book_action(res,xml_id,highlight,scroll);
					deferred.resolve();
					/*
					if(!highlight)	{	//highlight的文本會有紅字，不做cache，其餘就cache起來
						localStorage.setItem('XML-'+xml_id+'_'+specify_juan,res);
					}
					*/
				}
			);	
		}
		else	{	//取出html cache
			var html = localStorage.getItem('XML-'+xml_id+'_'+specify_juan);
			load_book_action(html,xml_id,highlight,scroll);
			deferred.resolve();
		}
		return deferred.promise();
}

function load_book_action(res,xml_id,highlight,scroll)	{
	$('#'+_GLOBALS.active_panel+'-container').html(res);					
	_GLOBALS.open_container[_GLOBALS.active_panel+'-container']['reading_xmlID'] = xml_id;	//更新全域變數：閱讀中的xml id
	after_load('#'+_GLOBALS.active_panel+'-container',xml_id);	//讀取完內容後調整content
	$('#'+_GLOBALS.active_panel+'-container').trigger('scroll');//開啟後動一下scroll以記錄讀經位置
	if(_GLOBALS.lastSearchRange == '本經' || _GLOBALS.lastSearchRange == 'Current Text')	$('#textsearch_range_value').val('works:'+xml_id.toRay());		//如果上次搜尋範圍選擇「本經」，那麼經切換時「本經」的id當然要跟著換
						
	//有給highlight參數則於此進行 上一筆/下一筆 的介面調整，否則就把之前的資訊清除
	if(highlight)	{	
		if(_GLOBALS.isExtendedSearch)	{
			//下面的程式在文本內將進階搜尋的文字群組化(extended-search-group)，以利上下筆切換(一般搜尋時用的是後端php產生的group_num，但該邏輯不適用於進階，故於此處理進階group)
			var split_pattern = new RegExp('['+RegExp.quote(_GLOBALS.extended_search_pattern.join(''))+']',"g");
			var kws = $('#side_func_search_text_hidden').text().split(split_pattern).map(function(k)	{return $.trim(k);});
			//console.log(kws)
			var group_s = '',group_s_c = 1;
			$('#reading-area-1-container .hl').each(function()	{
				group_s += $(this).text();
				$(this).attr('extended-search-group',group_s_c);
				if(kws.indexOf(group_s) != -1)	{
					group_s_c++;
					group_s = '';
				}
			});
			
			//新增進階搜尋的上下筆
			$('#jing-tools-searchTool').html('<button style="float:left;margin-top: 3px;" type="button" class="close jing-tools-searchTool-close">×</button><button id="jing-tools-searchTool-extended-prev" class="btn btn-default btn-xs jing-tools-searchTool-extended-btn"><span class="glyphicon glyphicon-chevron-left"></span></button> '+_GLOBALS[_GLOBALS.lang]['1_10']+'「<span style="color:red">'+$('#side_func_search_text_hidden').text()+'</span>」 <button id="jing-tools-searchTool-extended-next" class="btn btn-default btn-xs jing-tools-searchTool-extended-btn"><span class="glyphicon glyphicon-chevron-right"></span></button>').show();
			$('#jing-tools-searchTool-extended-next').trigger('click');	//trigger 下一筆 按鈕(進階搜尋)
		}
		else	{
			//append一般搜尋的上下筆按鈕、次數
			$('#jing-tools-searchTool').html('<button class="close jing-tools-searchTool-close" type="button" style="float:left;margin-top: 3px;">×</button><span id="jing-tools-searchTool-text">'+_GLOBALS[_GLOBALS.lang]['1_10']+'「<span style="color:red">'+($('#side_func_search_text_hidden').text().length > 8 ? $('#side_func_search_text_hidden').text().substr(0,4)+' ... '+$('#side_func_search_text_hidden').text().substr(-4):$('#side_func_search_text_hidden').text())+'」</span></span> <button class="btn btn-default btn-xs jing-tools-searchTool-btn" id="jing-tools-searchTool-prev"><span class="glyphicon glyphicon-chevron-left"></span></button> <span id="jing-tools-searchTool-nowCount">0</span> / <span id="jing-tools-searchTool-endCount">'+$('#'+_GLOBALS.active_panel+'-container #body span.hl').last().attr('group_num')+'</span>&nbsp;'+_GLOBALS[_GLOBALS.lang]['2_15']+' <button class="btn btn-default btn-xs jing-tools-searchTool-btn" id="jing-tools-searchTool-next"><span class="glyphicon glyphicon-chevron-right"></span></button>').show();
			$('#jing-tools-searchTool-next').trigger('click');	//trigger 下一筆 按鈕
		}
	}
	else	{
		//$('#jing-tools-searchTool').html('');
		$('.jing-tools-searchTool-close').trigger('click');
	}
						
	//有給scroll參數則於此進行scroll
	if(scroll)	{
		var recoverFlag = false;
		
		/*
		if(scroll.match(/(T0[567]n0220)_(p[0-9]{4}[a-c][0-9]{2})/g))	{	//T05~T07大般若經個案處理，每個html經號如T07n0220b後面可能有[a-z]，會與我目前的檔案格式不符合，進來的參數是像「T07n0220_p0001a01」，沒有[a-z]因而無法scroll，故這邊要將T07n0220_p0001a04 轉成 T07n0220[a-z]_p0001a04，能接受[a-z]範圍
			//console.log(RegExp.$1+RegExp.$2)
			var special_line_handle_s = RegExp.$1,	//以進來的參數T07n0220_p0001a01拆成兩部份：T07n0220、p0001a01，後面要比對用的
				special_line_handle_e = RegExp.$2;

			scroll = $('.lb').filter(function() {	//用filter搭配拆掉的兩部份，找出[a-z]之經號補上[a-z]回傳
				var line_regexp = new RegExp(special_line_handle_s+"[a-z]_"+special_line_handle_e,"g");
				return this.id.match(line_regexp);
			}).attr('id');								
		}
		*/
		
		if(scroll.match(/(T0[567]n0220)([a-z])_(p[0-9]{4}[a-c][0-9]{2})/g))	{	//T05~T07大般若經個案處理，進來的scroll經號後面是含有[a-z],ex:T07n0220b，但因行號又沒有[a-z]因而無法scroll，故這邊要將T07n0220a_p0001a04 轉成 T07n0220_p0001a04
			scroll = RegExp.$1+'_'+RegExp.$3;
		}		
		
				
		if($('#'+scroll).size() <= 0)	{
			var likeScroll =  $('[id *= "'+scroll+'"]:eq(0)').attr('id');	//如果有scroll卻找不到物件，表示回來只有"頁碼"沒有含col和line，因此無法完整scroll到定位，所以這裡用一次 *= 去模糊搜尋頁碼，找到的第一筆即是頁的開頭，使用之	
			if(likeScroll)	{
				scroll = likeScroll;
			}
			else	{
				scroll = scroll.replace(/_/g,'');	//如果到這找不到scroll，可能是有些經號沒有底線，如：T02n0150Ap0875b05，在這邊去除底線在找一次
			}	
		}
		
		if($('span[id="'+scroll+'"]').is(':hidden'))	{	//因為.lb隱藏的話沒辦法scroll，所以如果介面設定已隱藏，先讓它show出來，scroll結束後在hide回去
			$('span[id="'+scroll+'"]').show();
			recoverFlag = true;
		}
							
		$('#'+_GLOBALS.active_panel+'-container').scrollTo($('span[id="'+scroll+'"]').addClass('scroll_hl'),{offset:-20});	//scroll到定位且增加scroll用的hl，讓user知道srcoll到哪
							
		if(recoverFlag)	$('span[id="'+scroll+'"]').hide();
	}

}

//更新樹狀目錄
function update_tree(xml_id)	{	
	var deferred = $.Deferred();
	$.getJSON(_GLOBALS.catalogAPI+'works/toc?work='+xml_id.toRay()+'&callback=?',function(res)	{
		var json = res.results[0];
		var tab_menu_tab = [],
			tab_menu_content = [];
				
		var produce_tree_tab_menu = function(json,tab_menu_tab,tab_menu_content)	{
			if(tab_menu_tab.length > 0)	{
				$('#tree_area').html('<div class="tab-content">'+tab_menu_content.join('')+'</div>'+'<ul class="nav nav-tabs nav-tabs-bottom" role="tablist">'+tab_menu_tab.join('')+'</ul>').find('ul > li:eq(0)').addClass('active').end().find('.tab-content > .tab-pane:eq(0)').addClass('active');
				if(json.mulu.length > 0)	{
					var copyright_info_node = {is_second:true,root_name:'dynatree_structure',title:_GLOBALS[_GLOBALS.lang]['27_10'],isFolder:false,expand:false};
					json.mulu.unshift(copyright_info_node);					
					var catelog_info_node = {is_root:true,root_name:'dynatree_structure',title:_GLOBALS[_GLOBALS.lang]['1_1'],isFolder:false,expand:false};
					json.mulu.unshift(catelog_info_node);				
					$("#dynatree_structure").dynatree({
						//children: {is_root:true,root_name:'dynatree_structure',title:_GLOBALS.titleNow,isFolder:false,expand:true,children:json.mulu},
						children:json.mulu,
						onActivate: function(node) {
							click_tree_action(xml_id,node);
							$('#dynatree_juan').dynatree('getActiveNode').deactivate();			//結構選單click時，就deactivate卷選單剛剛select的東西（+魚要求）
							//alert(node.data.title+' '+node.data.from+' '+node.data.to);
						}
					});				
				}
				if(json.juan.length > 0)	{
					var copyright_info_node = {is_second:true,root_name:'dynatree_juan',title:_GLOBALS[_GLOBALS.lang]['27_10'],isFolder:false,expand:false};
					json.juan.unshift(copyright_info_node);						
					var catelog_info_node = {is_root:true,root_name:'dynatree_juan',title:_GLOBALS[_GLOBALS.lang]['1_1'],isFolder:false,expand:false};
					json.juan.unshift(catelog_info_node);
					$('#dynatree_juan').dynatree({
						//children: {is_root:true,root_name:'dynatree_juan',title:_GLOBALS.titleNow,isFolder:false,expand:true,children:json.juan},
						children:json.juan,
						onActivate: function(node) {
							click_tree_action(xml_id,node);
							$('#dynatree_structure').dynatree('getActiveNode').deactivate();	//卷選單click時，就deactivate結構選單剛剛select的東西（+魚要求）
						}
					});
				}
				
				//樹狀目錄重新產生時，會讓下面的頁籤出現，如果樹狀區塊是縮到很小頁籤會爆出，因此在這設定當寬度小於一定值後將頁籤隱藏
				if(parseInt($('#tree_area').css('width'),10) <= 80)	$('#tree_area > ul[role="tablist"]').hide();
			}
			
			deferred.resolve();
		};
			
		if(!json.mulu)	json.mulu = [];
		if(!json.juan)	json.juan = [];				
				
		if(json.mulu.length <=0 && json.juan.length <= 0)	{	//都沒資料時表示其為單卷，做一個手動的卷tab(內容只有一卷) 給他
			$.getJSON(_GLOBALS.catalogAPI+'search/toc?q='+xml_id+'&callback=?',function(res)	{	//由於卷不一定從1開始，所以要再額外查詢起始卷是多少，不然會開不到檔案
				res = res.results;
				var juan_from = parseInt(res[0].juan_start,10);
				var start_juan = juan_from ? juan_from:1;
				json.juan = [{"title":"卷"+getChineseNumber(start_juan),"from":start_juan,"to":false}];
				if(json.juan.length > 0)	{
					tab_menu_tab.push('<li class="nav-tabs-bottom-tab"><a href="#tree_area_juan" role="tab" data-toggle="tab" data-lang-id="1_12">'+_GLOBALS[_GLOBALS.lang]['1_12']+'</a></li>');
					tab_menu_content.push('<div role="tabpanel" class="tab-pane" id="tree_area_juan"><div id="dynatree_juan"></div></div>');
					produce_tree_tab_menu(json,tab_menu_tab,tab_menu_content);
				}
			});
		}
		else	{
			if(json.mulu.length > 0)	{
				tab_menu_tab.push('<li class="nav-tabs-bottom-tab"><a href="#tree_area_structure" role="tab" data-toggle="tab" data-lang-id="1_11">'+_GLOBALS[_GLOBALS.lang]['1_11']+'</a></li>');
				tab_menu_content.push('<div role="tabpanel" class="tab-pane" id="tree_area_structure"><div id="dynatree_structure"></div></div>');
			}
				
			if(json.juan.length > 0)	{
				tab_menu_tab.push('<li class="nav-tabs-bottom-tab"><a href="#tree_area_juan" role="tab" data-toggle="tab" data-lang-id="1_12">'+_GLOBALS[_GLOBALS.lang]['1_12']+'</a></li>');
				tab_menu_content.push('<div role="tabpanel" class="tab-pane" id="tree_area_juan"><div id="dynatree_juan"></div></div>');
			}
				
			produce_tree_tab_menu(json,tab_menu_tab,tab_menu_content);
		}	
	});
	return deferred.promise();
}

function click_tree_action(xml_id,node)	{	//樹狀目錄，click樹狀目錄節點的動作
	if(node.data.is_root)	{	//如果是經名根節點，click 則跳出經錄資訊
		$('#author_info_btn').trigger('click');	//trigger 經錄資訊按鈕，即「i」box
		
		//因為一般目錄節點 click 事件是綁 onActive，點了同個節點只會執行一次，在目錄是合理點了同樣節點無須一直執行，但在這個經錄資訊就不行了，經錄需要可以一直點開，所以在經錄資訊節點click時跑下面兩行執行deactivate
		if(node.data.root_name === 'dynatree_structure')	{
			$('#dynatree_structure').dynatree('getActiveNode').deactivate();
		}
		else if(node.data.root_name === 'dynatree_juan')	{
			$('#dynatree_juan').dynatree('getActiveNode').deactivate();
		}
	}
	else if(node.data.is_second)	{	//版權說明節點
		if($('#cbeta-copyright').is(':hidden'))	{
			$('.display_copyright_btn').trigger('click');
		}
		$('#reading-area-1-container').scrollTo('max',500);
		
		//因為一般目錄節點 click 事件是綁 onActive，點了同個節點只會執行一次，在目錄是合理點了同樣節點無須一直執行，但在版權這功能就不行了，這需要一直可點開，所以click時跑下面兩行執行deactivate
		if(node.data.root_name === 'dynatree_structure')	{
			$('#dynatree_structure').dynatree('getActiveNode').deactivate();
		}
		else if(node.data.root_name === 'dynatree_juan')	{
			$('#dynatree_juan').dynatree('getActiveNode').deactivate();
		}		
	}
	else	{	//否則就是一般目錄節點 click 之行為
		$.getJSON(_GLOBALS.catalogAPI+'works?work='+xml_id.toRay()+'&callback=?',function(res)	{	//此ajax 用經號去取經名
			res = res.results[0];
			var jing_title = res.title;
			$('#'+_GLOBALS.active_panel+'-container').html('<h1>Reading...</h1>');	//取得經名時的ajax做個Reading讓user知道在讀取，預防沒反應
			push_historyState(xml_id,node.data.juan,res.work+' '+jing_title,node.data.file+'_p'+node.data.lb);
			updateTitle_openJing(xml_id,res.work+' '+jing_title,node.data.juan,false,node.data.file+'_p'+node.data.lb);
		});	
	}
}

function update_kwic_tab(kwic_obj)	{	//更新kwic tab
	var kwic_html = [];
	var count = 0;
	var group_num = 0;
	$('<div id="kwic_text_fake" style="display:none" />').html($('#reading-area-1-container').html()).appendTo('body');	//為了不影響真正的reading container內容，故在此做一個假的container放這些東西處理，用完即丟
	
	var group_target = $('#kwic_text_fake span.hl[extended-search-group]').size() > 0 ? 'extended-search-group':'group_num';	//有進階搜尋就以 extended-search-group為主
	var group_end_num = $('#kwic_text_fake span.hl['+group_target+']').last().attr(group_target);
	
	while(++group_num <= group_end_num)	{	//由於.hl是以每個字個別hl，這邊要將之wrap成關鍵字群組包上div.kwic_text_item以方便製作KWIC_TEXT的selector
		$('#kwic_text_fake span.hl['+group_target+'="'+(group_num)+'"]').first().before('[W]').end().last().after('[/W]');
		//$('#kwic_text_fake span.hl['+group_target+'="'+(group_num)+'"]').wrapAll('<div class="kwic_text_item" />');	//2016-12-21:用wrap效率太差，改直接處理成kwic_text吃的[W]...[/W]
	}	

	var kwic_texts = (KWIC_TEXT.get(false,false,10,false,$('#kwic_text_fake').find('.note-link,.note-link-cbeta,.lb,#back,#cbeta-copyright,.display_copyright_btn').remove().end().text().replace(/[\r\n]/g,'')));	//取出kwic文字，實做前先移除不必要元素
	$('#kwic_text_fake').remove();	//用完即丟
	
	var lb_prefix = /(.+p).+/.exec($('.lb:eq(0)').attr('id'))[1];

	for(var i in kwic_texts)	{
		count++;
		var lb = lb_prefix+$('#reading-area-1-container span.hl['+group_target+'="'+(count)+'"]').parent('.t').attr('l');
		//var lb='';
		var kwic_group = /([^<]*)<b>([^<]+)<\/b>([^<]*)/g.exec(kwic_texts[i]);
		kwic_html.push('<tr group="'+count+'"><td>'+(count)+'</td><td style="display:none">'+lb+'</td><td class="noShow_in_export">'+kwic_texts[i]+'</td><td style="display:none">'+kwic_group[1]+'</td><td style="display:none">'+kwic_group[2]+'</td><td style="display:none">'+kwic_group[3]+'</td></tr>');
	}
	$('#tree_area ul.nav-tabs').append('<li class="nav-tabs-bottom-tab"><a href="#tree_area_kwic" role="tab" data-toggle="tab" data-lang-id="1-10">'+_GLOBALS[_GLOBALS.lang]['1_10']+'</a></li>');
	$('#tree_area div.tab-content').append('<div role="tabpanel" class="tab-pane" id="tree_area_kwic"><div id="tree_area_kwic_resInfo" style="padding:5px 10px"><button id="" class="close" type="button" onclick="$(\'.jing-tools-searchTool-close\').trigger(\'click\');">×</button><b>'+_GLOBALS[_GLOBALS.lang]['22_13']+'<span>'+$('#side_func_search_text_hidden').text()+'</span></b></div><table id="tree_area_kwic_table" class="table table-hover"><thead><tr><th>#</th><th><div class="dropdown pull-right"><button class="btn btn-default btn-xs dropdown-toggle" type="button" id="" data-toggle="dropdown"><span class="glyphicon glyphicon-export"></span>&nbsp;&nbsp;'+_GLOBALS[_GLOBALS.lang]['2_9']+'<span class="caret"></span></button><ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1"><li role="presentation"><a id="kwic_export_btn_excel" class="kwic_export_btn" role="menuitem" tabindex="-1" href="javascript:void(null)">Excel</a></li><li role="presentation"><a id="kwic_export_btn_csv" class="kwic_export_btn" role="menuitem" tabindex="-1" href="javascript:void(null)" onclick="javascript:">CSV</a></li></ul></div></th><th style="display:none">行號</th></tr></thead><tbody>'+kwic_html.join('')+'</tbody></table></div>');
	
	
	//kwic匯出按鈕，每次需重綁
	$('.kwic_export_btn').click(function()	{
		var exportType = $(this).attr('id').replace('kwic_export_btn_','');
		var htmlContentValue = exportType === 'excel' ? 'true':'false';
		$('#tree_area_kwic_table').clone().find('td:nth-child(1) , thead , .noShow_in_export').remove().end().tableExport({type:exportType,htmlContent:htmlContentValue,escape:'false',winxdFileName:'KWIC_export_'+new Date().YMDHIS('mm-dd-hh-ii'),defaultTitle:_GLOBALS[_GLOBALS.lang]['22_13']+''+$('#tree_area_kwic_resInfo b > span').text()+' , '+kwic_html.length+_GLOBALS[_GLOBALS.lang]['12_1_2']+' <span style="color:red">('+$('#jing-toolbar .breadcrumb').text()+')</span>' });
	});
		
	$('#tree_area ul.nav-tabs a:last').tab('show');//預設開啟kwic tab		
	
}

/*
function update_inline_juan_menu(xml_id)	{	//製作與更新內文上下卷選單
	var juanList = [],
		juanArr = [],
		prev = false,
		next = false,
		showRange = 2,	//顯示卷數的範圍
		juanListOrigLen = 0
		
	$('.inline_juan_menu_div').html('');
	$.getJSON(_GLOBALS.catalogAPI+'works/toc?work='+xml_id.toRay()+'&callback=?',function(res)	{
		var json = res.results[0];
		if(json.juan && json.juan.length > 0)	{
			juanArr = uniqueObjects(json.juan,['juan']);	
			if(_GLOBALS.juanNow && juanArr)	{
				for(var i in juanArr)	{
					if(juanArr[i].juan == _GLOBALS.juanNow)	{
						var nowIndex = parseInt(i,10);
						//console.log('現在卷！'+juanArr[nowIndex].juan);
						prev = juanArr[nowIndex-1] ? '<span class="inline_juan_menu enabled" from="'+juanArr[nowIndex-1].juan+'" to="'+juanArr[nowIndex-1].file+'_p'+juanArr[nowIndex-1].lb+'"><</span>':false;
						next = juanArr[nowIndex+1] ? '<span class="inline_juan_menu enabled" from="'+juanArr[nowIndex+1].juan+'" to="'+juanArr[nowIndex+1].file+'_p'+juanArr[nowIndex+1].lb+'">></span>':false;
							
						juanList.push('<span class="inline_juan_menu">['+parseInt(juanArr[i].juan,10)+']</span>');
					}
					else	{
						juanList.push('<span class="inline_juan_menu enabled" from="'+juanArr[i].juan+'" to="'+juanArr[i].file+'_p'+juanArr[i].lb+'">'+parseInt(juanArr[i].juan,10)+'</span>');
					}
				}
					
				juanListOrigLen = juanList.length;	//由於長度下面會做slice變動到，先把原始長度暫存起來，後面計算需要
					
				if(juanList.length > showRange)	{	//超過顯示範圍為卷選單做slice
					if(nowIndex < showRange)	nowIndex = showRange;
					juanList = juanList.slice(nowIndex-showRange,nowIndex+showRange+1);
				}
				
				if(nowIndex > showRange)					juanList.splice(0,0,'<span class="inline_juan_menu enabled" from="'+juanArr[0].juan+'" to="'+juanArr[0].file+'_p'+juanArr[0].lb+'">'+parseInt(juanArr[0].juan,10)+'</span>&nbsp;&nbsp;...');	//第一卷
				if(nowIndex < juanListOrigLen-showRange-1)	juanList.push('&nbsp;&nbsp;...<span class="inline_juan_menu enabled" from="'+juanArr[juanArr.length-1].juan+'" to="'+juanArr[juanArr.length-1].file+'_p'+juanArr[juanArr.length-1].lb+'">'+parseInt(juanArr[juanArr.length-1].juan,10)+'</span>');	//最後一卷
				if(prev)	juanList.splice(0,0,prev);	//上一卷
				if(next)	juanList.push(next);		//下一卷
					
				$('.inline_juan_menu_div').html('<span data-lang-id="1_9">'+_GLOBALS[_GLOBALS.lang]['1_9']+'</span>：'+juanList.join(''));
			}		
		}
	});
}
*/

function update_inline_juan_menu(xml_id)	{	//製作與更新內文上下卷選單
	var juanList = [],
		prev = false,
		next = false,
		showRange = 2,	//顯示卷數的範圍
		juanListOrigLen = 0
		
	$('.inline_juan_menu_div').html('');
	$.getJSON(_GLOBALS.catalogAPI+'works?work='+xml_id.toRay()+'&callback=?',function(res)	{
		var json = res.results[0];
		var juan_list = json.juan_list.split(',');	//可能跳卷、故用ray回傳的list
		var nowIndex;
		
		if(juan_list && juan_list.length > 0)	{
			juan_list.forEach(function(elm,index)	{
				if(parseInt(elm,10) == parseInt(_GLOBALS.juanNow,10))	{
					nowIndex = parseInt(index,10);
					prev = juan_list[nowIndex-1] ? '<span class="inline_juan_menu enabled" xml_id="'+xml_id+'" juan="'+juan_list[nowIndex-1]+'"><</span>':false;
					next = juan_list[nowIndex+1] ? '<span class="inline_juan_menu enabled" xml_id="'+xml_id+'" juan="'+juan_list[nowIndex+1]+'">></span>':false;
							
					juanList.push('<span class="inline_juan_menu">['+parseInt(elm,10)+']</span>');
				}
				else	{
					juanList.push('<span class="inline_juan_menu enabled" xml_id="'+xml_id+'" juan="'+parseInt(elm,10)+'">'+parseInt(elm,10)+'</span>');
				}				
			});
			
			juanListOrigLen = juanList.length;	
			if(juanList.length > showRange)	{	//超過顯示範圍為卷選單做slice
				if(nowIndex < showRange)	nowIndex = showRange;
				juanList = juanList.slice(nowIndex-showRange,nowIndex+showRange+1);
			}
				
			if(nowIndex > showRange)					juanList.splice(0,0,'<span class="inline_juan_menu enabled" xml_id="'+xml_id+'" juan="'+juan_list[0]+'">'+parseInt(juan_list[0],10)+'</span>&nbsp;&nbsp;...');	//第一卷
			if(nowIndex < juanListOrigLen-showRange-1)	juanList.push('&nbsp;&nbsp;...<span class="inline_juan_menu enabled" xml_id="'+xml_id+'" juan="'+juan_list[juan_list.length-1]+'">'+parseInt(juan_list[juan_list.length-1],10)+'</span>');	//最後一卷
			if(prev)	juanList.splice(0,0,prev);	//上一卷
			if(next)	juanList.push(next);		//下一卷
					
			$('.inline_juan_menu_div').html('<span data-lang-id="1_9">'+_GLOBALS[_GLOBALS.lang]['1_9']+'</span>：'+juanList.join(''));
		}
	});
}

function side_func_display(display)	{	//side_func區塊是否關閉之處理
	if(display)	{	//三欄
		
		$('#side_func,#adjust-panel-line-old').show();
		
		if(_GLOBALS.lastMainSideWidth.main_area && _GLOBALS.lastMainSideWidth.side_func && _GLOBALS.lastMainSideWidth.window == $(window).width())	{	//如果有紀錄上一次的寬度就用之
			$('#main_area').css({'width':_GLOBALS.lastMainSideWidth.main_area+'px'});
			$('#side_func').css({'width':_GLOBALS.lastMainSideWidth.side_func+'px'});		
		}
		else	{
			$('#main_area').css({'width':'59.9%'});
			$('#side_func').css({'width':'39.9%'});
		}
		
		$('#reading-area-1').css({'width':$(window).width()-15-$('#tree_area').get(0).offsetWidth-$('#side_func').get(0).offsetWidth-$('#bookAndTree_adjuster').get(0).offsetWidth+'px'});
		$('#side_func_search_fake').css({'visibility':'hidden','width':'1px'}).hide();
		$('#show_side_func_btn,#dropdownMenu-about,#dropdownMenu-lang,#dropdownMenu-home,#da_links_btn,#interface_config_btn > span:eq(1),#toc_open_btn > span:eq(1),#jing_export_modal_btn > span:eq(1) , #jing_send_modal_btn > span:eq(1)').hide();
		$('.jing-title').css({'width':'95%'});	//為了經名text-overflow而設
		//$('#toc_open_btn').removeClass('no-data-hint');	$('#interface_config_btn').removeClass('no-data-hint');
	}
	else	{		//兩欄
		//紀錄本次main_area、side_func的寬度
		_GLOBALS.lastMainSideWidth.main_area = $('#main_area').get(0).offsetWidth;
		_GLOBALS.lastMainSideWidth.side_func = $('#side_func').get(0).offsetWidth;
		_GLOBALS.lastMainSideWidth.window = $(window).width();
		
		if(_GLOBALS.sideFuncFrameNeedMinus && _GLOBALS.lastMainSideWidth.side_func > 0)	{	//**重要：在1024大小時，或在調整大小後、或在resize後，整合功能panel有可能差1px會被擠下去（不知為何），要將寬度在減1才沒問題，由此變數判斷要不要做
			_GLOBALS.lastMainSideWidth.side_func--;
			_GLOBALS.sideFuncFrameNeedMinus = false;	
		}

		
		$('#side_func,#adjust-panel-line-old').hide();
		$('#main_area').css({'width':'100%'});
		$('#reading-area-1').css({'width':$(window).width()-15-$('#tree_area').get(0).offsetWidth-$('#bookAndTree_adjuster').get(0).offsetWidth+'px'});
		$('#side_func_search_fake,#show_side_func_btn,#dropdownMenu-about,#dropdownMenu-lang,#dropdownMenu-home,#da_links_btn,#interface_config_btn > span:eq(1),#toc_open_btn > span:eq(1),#jing_export_modal_btn > span:eq(1) , #jing_send_modal_btn > span:eq(1)').show();
		$('#side_func_search_fake').css({'visibility':'visible','width':'30%'});
		$('.jing-title').css({'width':'calc(100% - 30%)'});	//為了經名text-overflow而設
		//$('#toc_open_btn').addClass('no-data-hint');	$('#interface_config_btn').addClass('no-data-hint');
	}
}

function refAndCopy_config_adjust(type)	{	//引用複製設定函式
	if(type == 'title' || type == 'all')	{	//經名前後
		var front_elm = $('#rac_modal .refAndCopy_title_front'),
			back_elm = $('#rac_modal .refAndCopy_title_back');
		if($('input[name="rac_conf_jing_title_position"]:checked').val() == 'before')	{
			front_elm.show();
			back_elm.hide();
		}
		else if($('input[name="rac_conf_jing_title_position"]:checked').val() == 'after')	{
			front_elm.hide();
			back_elm.show();
		}		
	}
	
	if(type == 'formatted' || type == 'all')	{	//文字格式 $('#rac_modal').find('.modal-body div[role="non-formatted"]')
		if($('#rac_conf_isFormatted').prop('checked'))	{
			$('#rac_modal .modal-body div[role="formatted"]').show();
			$('#rac_modal .modal-body div[role="non-formatted"]').hide();
		}
		else	{
			$('#rac_modal .modal-body div[role="formatted"]').hide();
			$('#rac_modal .modal-body div[role="non-formatted"]').show();				
		}
	}
		
	if(type == 'note' || type == 'all')	{	//顯示校勘
		if($('#rac_conf_isNote').prop('checked'))	$('.refAndCopy_note').show();
		else	$('.refAndCopy_note').hide();		
	}
		
	if(type == 'pin' || type == 'all')	{	//顯示品名
		if($('#rac_conf_isPin').prop('checked'))	$('.refAndCopy_pin').show();
		else	$('.refAndCopy_pin').hide();		
	}

	if(type == 'punc' || type == 'all')	{	//顯示標點
		if($('#rac_conf_isPunc').prop('checked'))	$('.refAndCopy_punctuation').show();
		else	$('.refAndCopy_punctuation').hide();		
	}	

	if(type == 'line' || type == 'all')	{	//經文位置
		var front_elm = $('#rac_modal .refAndCopy_line_book'),
			back_elm = $('#rac_modal .refAndCopy_line_jing');
		if($('input[name="rac_conf_line_format"]:checked').val() == 'book')	{
			front_elm.show();
			back_elm.hide();
		}
		else if($('input[name="rac_conf_line_format"]:checked').val() == 'jing')	{
			front_elm.hide();
			back_elm.show();
		}		
	}
	
	if(type == 'note_position' || type == 'all')	{			//校勘排列
		if($('input[name="rac_conf_note_position"]:checked').val() == 'lines')	$('.refAndCopy_note br').show().next('span.note_linebreak_symbol').show();
		else if($('input[name="rac_conf_note_position"]:checked').val() == 'one')	$('.refAndCopy_note br').hide().next('span.note_linebreak_symbol').hide();
	}
}
	
function collate_window_close()	{	//關閉粉紅色小視窗
	$('#collate-window').hide();
	$('.active-note').removeClass('active-note');
}

function change_lang()	{	//更換語系
	$('[data-lang-id]').each(function()	{
		if($(this).attr('data-lang-id') == '1_1' || $(this).attr('data-lang-id') == '1_13' || $(this).attr('data-lang-id') == '22_9')	$(this).attr('title',_GLOBALS[_GLOBALS.lang][$(this).attr('data-lang-id')]);	//修改title	attr
		else if($(this).attr('data-lang-id') == '14_5' || $(this).attr('data-lang-id') == '8_9')	$(this).attr('placeholder',_GLOBALS[_GLOBALS.lang][$(this).attr('data-lang-id')]);	//修改placeholder attr
		else if($(this).attr('data-lang-id') == '1_3' || $(this).attr('data-lang-id') == '1_2_2' || $(this).attr('data-lang-id') == '2_9' || $(this).attr('data-lang-id') == '24_10_1')	$(this).attr('data-hint',_GLOBALS[_GLOBALS.lang][$(this).attr('data-lang-id')]);	//工具列的data-hint也要改 **必須「保證」這邊的語系變數僅有data-hint用到，否則下面tag內的text是不會執行到喔，必須確保這邊的變數是data-hint用到
		else	{
			if($(this).attr('data-lang-id') == '2_2' || $(this).attr('data-lang-id') == '4_1' || $(this).attr('data-lang-id') == '5_1' || $(this).attr('data-lang-id') == '6_1' || $(this).attr('data-lang-id') == '7_1')	$('[data-lang-id-clue="'+$(this).attr('data-lang-id')+'"]').attr('data-hint',_GLOBALS[_GLOBALS.lang][$(this).attr('data-lang-id')]);	//整合功能搜尋之data-hint
			
			//否則修改data-lang-id tag內之text
			$(this).text(_GLOBALS[_GLOBALS.lang][$(this).attr('data-lang-id')]);
			
		}
	});
	
	//特例：[data-lang-id="27_04"] , [data-lang-id="27_06"]取代內部換行
	$('[data-lang-id="27_04"] , [data-lang-id="27_06"]').each(function()	{
		var thisTxt = $(this).text().replace('<PTT>','<br>');
		$(this).html(thisTxt);
	});
}

function jing_selector_show(open)	{	//經選擇器 開/關
	if(open)	{
		if($('#selector_firstPage_holder').size() > 0)	{	//如果「請從左邊分類選擇經」區塊是存在的，表示第一次開啟選擇器，將此區塊改用部類目錄顯示之(2018-04-29:校長指示)
			$('#selector-menu-bar > li[desc="bulei"]').trigger('click');
		}
		
		$('#whole-reading-area').addClass('main_area_modal');	//將整塊閱讀區域加上純css之modal(使用filter:brightness(50%) + pointer-events:none)
		$("#jing_selector").show().animate({
			opacity: 1,
			left: "+=149"
		}, 300, function() {
			// Animation complete.
		});			
	}
	else	{
		$('#whole-reading-area').removeClass('main_area_modal');
		$("#jing_selector").animate({
			opacity: 0,
			left: "-=149"
		}, 300, function() {
			$("#jing_selector").css('left','-150px').hide();
		});	
	}
}

function hide_loading_page_holder()	{	//初次讀取頁面全部準備好後，淡出#loading-page-holder
	setTimeout(function()	{
		$('#loading-page-holder').fadeOut(400);
	},100);
	
	Pace.on('start',function()	{	//***！重要，在hide_loading_page_holder後，將Pace註冊為start時一律藏起來，否則之後的ajax call都會跑出來
		$('.pace').hide();
	});	
}

	
function updateLastReadingString()	{	//更新首頁「閱讀上次內容」的經名資訊
	if($.cookie('last-reading-book'))
		$('#funcarea [data-action="read-last-content"] > div').html('<span style="font-weight:normal;font-size:smaller">('+$.trim($.cookie('last-reading-book').split(';')[2])+')</span>');
}
	
function goHome()	{
	if(!$('#'+_GLOBALS.active_panel+'-container').hasClass('home-showing'))	push_historyState('home');	//已經是首頁就別再push state了
	showHome(true);
}
function showHome(show)	{
	if(show)	{
		
		var home_tree_irems = [
			{key:'home_tree_function',title:_GLOBALS[_GLOBALS.lang]['26_3'],content_anchor:'functions'},
			{key:'home_tree_news',title:_GLOBALS[_GLOBALS.lang]['26_41'],content_anchor:'news'},
			{key:'home_tree_content',title:_GLOBALS[_GLOBALS.lang]['26_26'],content_anchor:'content'},
			{key:'home_tree_about',title:_GLOBALS[_GLOBALS.lang]['26_24'],content_anchor:false},
			{key:'home_tree_donate',title:_GLOBALS[_GLOBALS.lang]['26_45'],content_anchor:false}
		];
		
		var home_tree = [
			{
			  "title": _GLOBALS[_GLOBALS.lang]['26_46'],
			  "isFolder":true,
			  "expand":true,
			  "children": home_tree_irems
			}
		];
		$('body').addClass('home-showing-body');	//body也加上首頁顯示中classname
		$('#'+_GLOBALS.active_panel+'-container').addClass('home-showing');
		$('#book-toolbar').css('display','none');
		$('#jing_info_area .breadcrumb').html('<li class="active" title="CBETAOnline">'+_GLOBALS[_GLOBALS.lang]['23']+'</li>');	//重設標題
		$('#'+_GLOBALS.active_panel+'-container').html($('#home-panel').html());
		
		//reload tree
		$('#tree_area').html('<div class="tab-content"><div role="tabpanel" class="tab-pane" id="tree_area_home"><div id="dynatree_home"></div></div></div>'+'<ul class="nav nav-tabs nav-tabs-bottom" role="tablist"><li class="nav-tabs-bottom-tab" style="visibility:hidden"><a href="#tree_area_home" role="tab" data-toggle="tab" data-lang-id="1_12">'+_GLOBALS[_GLOBALS.lang]['24_24']+'</a></li></ul>').find('ul > li:eq(0)').addClass('active').end().find('.tab-content > .tab-pane:eq(0)').addClass('active');
		
		
		$("#dynatree_home").dynatree({
			children: home_tree,
			onActivate: function(node) {
				//alert(node.data.title+' '+node.data.action);
				if(node.data.key == '_2')	{	//click root 「常用功能」(key=_2)時，不要收合，給它一直打開
					node.expand(true);
				}

				if(node.data.content_anchor)	{
					if(node.data.content_anchor === 'functions')
						$('#reading-area-1-container').scrollTo(0,300);
					else
						$('#reading-area-1-container').scrollTo($('[data-home-tree-anchor="'+node.data.content_anchor+'"]:eq(1)').get(0),300);
					
				}
				else if(node.data.key === 'home_tree_about')	{	//關於我們
					$('#aboutus_modal').modal('show');
				}
				else if(node.data.key === 'home_tree_donate')	{	//捐款給 CBETA
					window.open('https://www.cbeta.org/donation/index.php');
				}
				
				$('#dynatree_home').dynatree('getActiveNode').deactivate();	//click後，就deactivate
			}
		});				
		
		loadLastActions();	//讀取使用者上次操作
		side_func_display(false);	//強制變成兩欄
		
	}
	else	{
		$('#book-toolbar').css('display','flex');
		$('body').removeClass('home-showing-body')
		$('#'+_GLOBALS.active_panel+'-container').removeClass('home-showing');	
	}
	$(window).trigger('resize');	//顯示首頁時有牽涉到book-toolbar的隱藏與顯示，閱讀區高度可能會變小，用resize重新重整一下
}

function addUserAction(type,data)	{	//記錄使用者操作
	if(_GLOBALS.userActions.length >= 100)	{	//操作記錄100筆，超過的部份移除
		_GLOBALS.userActions.shift();
	}
	
	//先檢查有沒有重複，有重複的就移到最前面
	var ua_index = -1;
	for(var ua of _GLOBALS.userActions)	{
		ua_index++;
		if(ua.type === 'read' && $.trim(ua.data.title) === $.trim(data.title) && parseInt(ua.data.juan,10) === parseInt(data.juan,10))	{	_GLOBALS.userActions.splice(ua_index,1);	_GLOBALS.userActions.push({type:type,data:data});	localStorage.setItem("cbetaonline-user-actions",JSON.stringify(_GLOBALS.userActions));	return;	}
		else if(ua.type === 'fulltext-search' && ua.data.kw == data.kw || (data.near_words && ua.data.near_words && data.near_words.toString() == ua.data.near_words.toString()))	{	_GLOBALS.userActions.splice(ua_index,1);	_GLOBALS.userActions.push({type:type,data:data});	localStorage.setItem("cbetaonline-user-actions",JSON.stringify(_GLOBALS.userActions));	return;	}
		else if(ua.type === 'search' && ua.data.kw == data.kw)	{	_GLOBALS.userActions.splice(ua_index,1);	_GLOBALS.userActions.push({type:type,data:data});	localStorage.setItem("cbetaonline-user-actions",JSON.stringify(_GLOBALS.userActions));	return;	}
	}
	
	
	_GLOBALS.userActions.push({type:type,data:data});
	
	try	{
		localStorage.setItem("cbetaonline-user-actions",JSON.stringify(_GLOBALS.userActions));	//將actions存到localStorage
	}
	catch(err)	{
		var e = err;
	}
	loadLastActions();
}

function loadLastActions()	{
	_GLOBALS.userActions = (localStorage.getItem("cbetaonline-user-actions") ? JSON.parse(localStorage.getItem("cbetaonline-user-actions")):[]);	//讀取localstorage user操作記錄
	
	if(_GLOBALS.userActions.length <= 0)	{
		$('.home-panel-last-actions:eq(1)').html('無資料 / No Data');
		$('.user-actions-page-menu:eq(1)').parent().hide();
		return;
	}
	
	//2021-12-10:取消此檢查，現在起一律更新，反正這行也有bug
	//檢查 _GLOBALS.userActions 的最後一筆與網頁操作清單的第一筆是否相同，相同代表沒動過，就 return 不做下面的動作了，即不必更新
	//read去判斷title、全文檢索、參考查詢判斷kw，但要注意可能是read false時跑到這一個判斷式，既然是read那kw都會是空的，這樣反而變成true，故後面的全文檢索、參考查詢判斷還要再加一個type是否等於fulltext-search 或 search
	//if(($('.home-panel-last-actions:eq(1) li:eq(0)').data('type') === 'read' && _GLOBALS.userActions[_GLOBALS.userActions.length-1].type === 'read' && $('.home-panel-last-actions:eq(1) li:eq(0)').data('title') == _GLOBALS.userActions[_GLOBALS.userActions.length-1].data.title && parseInt($('.home-panel-last-actions:eq(1) li:eq(0)').data('juan'),10) == parseInt(_GLOBALS.userActions[_GLOBALS.userActions.length-1].data.juan,10)) || ($('.home-panel-last-actions:eq(1) li:eq(0)').data('type') === _GLOBALS.userActions[_GLOBALS.userActions.length-1].type && ($('.home-panel-last-actions:eq(1) li:eq(0)').data('type') === 'fulltext-search' || $('.home-panel-last-actions:eq(1) li:eq(0)').data('type') === 'search') && $('.home-panel-last-actions:eq(1) li:eq(0)').data('kw') === _GLOBALS.userActions[_GLOBALS.userActions.length-1].data.kw))	return;
	
	
	var html = [];
	var near_replace_count;
	for(var [ua_index,ua] of _GLOBALS.userActions.entries())	{
		if(ua.type === 'read')			html.push(`<li class="user-actions" data-idx="${ua_index}" data-type="read" data-xml-id="${ua.data.xml_id}" data-title="${ua.data.title}" data-juan="${ua.data.juan}"><a href="javascript:void(null)">${_GLOBALS[_GLOBALS.lang]['26_1']} ${ua.data.title} ${_GLOBALS[_GLOBALS.lang]['13_6']} ${getChineseNumber(parseInt(ua.data.juan,10))}</a><i class="fa fa-trash-o user-actions-del-btn" aria-hidden="true"></i></li>`);
		else if(ua.type === 'search')	html.push(`<li class="user-actions" data-idx="${ua_index}" data-type="search" data-kw="${ua.data.kw}" data-range="${ua.data.search_range}"><a href="javascript:void(null)">${_GLOBALS[_GLOBALS.lang]['26_2']} 「${ua.data.kw}」</a><i class="fa fa-trash-o user-actions-del-btn" aria-hidden="true"></i></li>`);
		else if(ua.type === 'fulltext-search')	{ near_replace_count = -1; html.push(`<li class="user-actions" data-idx="${ua_index}" data-type="fulltext-search" data-kw="${ua.data.kw}" data-near-words="${(ua.data.near_words ? ua.data.near_words:"")}"><a href="javascript:void(null)">${_GLOBALS[_GLOBALS.lang]['18_8']} 「${ua.data.kw.replace(new RegExp('(['+Object.keys(_GLOBALS.extended_search_pattern_map).join('')+']+)','g'),(match,p1) => ' '+_GLOBALS.extended_search_pattern_map[p1]+(ua.data.near_words ? ' '+ua.data.near_words[++near_replace_count]:"")+' ' )} 」</a><i class="fa fa-trash-o user-actions-del-btn" aria-hidden="true"></i></li>`);}
	}	
	$('.home-panel-last-actions:eq(1)').html(html.reverse().join('')).show();	
	
	//製作分頁下拉選單
	var page_options = [];
	var pages = $('.home-panel-last-actions:eq(1) li').length / 10 + ($('.home-panel-last-actions:eq(1) li').length % 10 === 0 ? 0:1);
	if($('.home-panel-last-actions:eq(1) li').length <= 0)	pages = 1;

	for(var pgi = 1; pgi <= pages; pgi++)	page_options.push('<option value="'+pgi+'">'+pgi+'</option>');
	$('.user-actions-page-menu:eq(1)').html(page_options.join(''));
	$('.user-actions-page-menu:eq(1)').trigger('change');	//啟動分頁整理一下
}

function process_kwic_exclude(q)	{
	var main_kw = $.trim(q.split('-')[0]);
	var all_kw = $.trim(q.split('-')[1]);
	var prefix_kw = all_kw.replace(main_kw,'');
	var main_pos = all_kw.indexOf(main_kw);
	var prefix_pos = all_kw.indexOf(prefix_kw);
	
	if (prefix_pos < main_pos) {
		return {q:main_kw,exclude_negative_param:'&negative_lookbehind='+encodeURIComponent(prefix_kw)};
	}
	else {
		return {q:main_kw,exclude_negative_param:'&negative_lookahead='+encodeURIComponent(prefix_kw)};
	}
}


function process_url_highlight(work,juan)	{
	var uobj = new URLSearchParams(location.search);
	if(uobj.get('q') && uobj.get('l') && uobj.get('q') !== '' && uobj.get('l') !== '')	{	//處理網址來的關鍵字hl
		//$('.hl').removeClass('hl');	//把預設的hl先藏起來
		
		var kws = uobj.get('q');
		var near_word = uobj.get('near_word') ? uobj.get('near_word'):'5';
		var around = uobj.get('kwic_around') ? uobj.get('kwic_around'):30;
		var is_near_search = false;
		var exclude_negative_param = false;
		
		//if(around <= 25)	around = 25;	//kwic前後文至少25個字，為了之後hl能順利運作
		near_word = near_word.split(',').reverse();
		
		
		if(kws.match(new RegExp('['+RegExp.quote(_GLOBALS.extended_search_pattern.join(''))+']','g')))	{	//有進階搜尋的處理
			var q;
			if(kws.indexOf('~') != -1)	{	//near 參數需特別處理成："老子" NEAR/7 "道"
				q = kws.split('~').map(w => '"'+w+'"'+(near_word.length > 0 ? ' NEAR/'+near_word.pop()+' ':'')).join('');
				is_near_search = true;
			}
			else if(kws.indexOf('^') != -1)	{	//not 參數也要由 "^" 替換成 "!"
				q = kws.replace(new RegExp('['+RegExp.quote('^')+']','g'),',');
			}
			else if(kws.indexOf('-') != -1)	{	//exclude 要將 q 額外處理並額外加上 exclude_negative_param 參數
				//q = kws.replace(new RegExp('['+RegExp.quote(_GLOBALS.extended_search_pattern.join(''))+']','g'),',');
				var temp = process_kwic_exclude(kws);
				q = temp.q;
				exclude_negative_param = temp.exclude_negative_param;
			}
			else	{	//其他 and、or 用「,」組合起來即可
				q = kws.replace(new RegExp('['+RegExp.quote(_GLOBALS.extended_search_pattern.join(''))+']','g'),',');
			}
			
			kwic_api = 'search/kwic?q='+encodeURIComponent(q)+'&work='+work+'&juan='+juan+'&around=40&sort=location&mark=1&note='+(_GLOBALS.skipInlineNoteHL ? '0':'1')+(exclude_negative_param ? exclude_negative_param:'')+'&callback=?';	//回傳的around一律長一點，暫定為40，為了後面hl比對用，kwic前後文必須長一點
		}
		else	{	//一般搜尋參數
			//kwic_api = 'search/kwic?q='+encodeURIComponent(uobj.get('q'))+'&work='+work+'&juan='+juan+'&around=40&rows=999999&sort=location&mark=1&callback=?';	//回傳的around一律長一點，暫定為40，為了後面hl比對用，kwic前後文必須長一點
			kwic_api = 'search/kwic?q='+encodeURIComponent(uobj.get('q'))+'&work='+work+'&juan='+juan+'&around=40&rows=999999&sort=location&mark=1&note='+(_GLOBALS.skipInlineNoteHL ? '0':'1')+'&callback=?';	//回傳的around一律長一點，暫定為40，為了後面hl比對用，kwic前後文必須長一點
		}
		
		$.getJSON(_GLOBALS.catalogAPI+kwic_api,function(json)	{
			//console.log(json);
			
			if(parseInt(json.num_found,10) > 0)	{
				var count = 0;
				var kwic_html = [];
				for(var o of json.results)	{
					count++;
					var lb = o.lb;
					var kwic_display =  new RegExp('(.{0,'+around+'}(<mark>.+<\\/mark>).{0,'+around+'})','g').exec(o.kwic)[1];	//給使用者看的kwic text，會根據網址傳入的kwic_around進行字串裁切，而原回傳的o.kwic則隱藏起來，這是要方便後面hl比對運算用的，原回傳的o.kwic並進行去掉夾注處理(2024-03-21:由於跨夾注選項化，增加變數_GLOBALS.skipInlineNoteHL判斷，如果設定不跨夾注則維持原本的o.kwic，有跨夾注才要另外做取代處理)，這份給user看的則保留夾注
					kwic_html.push('<tr group="'+count+'" data-lb="'+lb+'" '+(is_near_search ? 'data-is-near="yes"':'')+'><td>'+(count)+'</td><td style="display:none">'+lb+'</td><td style="display:none">'+(_GLOBALS.skipInlineNoteHL ? o.kwic.replace(new RegExp('\\([^\\)]+\\)','g'),'').replace(new RegExp('[^\\)]*\\)','g'),'').replace(new RegExp('\\([^\\(]*','g'),'') : o.kwic)+'</td><td class="">'+kwic_display+'</td></tr>');
				}
				$('#tree_area ul.nav-tabs').append('<li class="nav-tabs-bottom-tab"><a href="#tree_area_kwic" role="tab" data-toggle="tab" data-lang-id="1-10">'+_GLOBALS[_GLOBALS.lang]['1_10']+'</a></li>');
				$('#tree_area div.tab-content').append('<div role="tabpanel" class="tab-pane" id="tree_area_kwic"><div id="tree_area_kwic_resInfo" style="padding:5px 10px"><button id="" class="close" type="button" onclick="$(\'.jing-tools-searchTool-close\').trigger(\'click\');">×</button><b>'+_GLOBALS[_GLOBALS.lang]['22_13']+'<span>'+$('#side_func_search_text_hidden').text()+'</span></b></div><table id="tree_area_kwic_ray_table" class="table table-hover"><thead><tr><th>#</th><th><div class="dropdown pull-right"><button class="btn btn-default btn-xs dropdown-toggle" type="button" id="" data-toggle="dropdown"><span class="glyphicon glyphicon-export"></span>&nbsp;&nbsp;'+_GLOBALS[_GLOBALS.lang]['2_9']+'<span class="caret"></span></button><ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1"><li role="presentation"><a id="kwic_export_btn_excel" class="kwic_export_btn" role="menuitem" tabindex="-1" href="javascript:void(null)">Excel</a></li><li role="presentation"><a id="kwic_export_btn_csv" class="kwic_export_btn" role="menuitem" tabindex="-1" href="javascript:void(null)" onclick="javascript:">CSV</a></li></ul></div></th><th style="display:none">行號</th></tr></thead><tbody>'+kwic_html.join('')+'</tbody></table></div>');
				
				
				//kwic匯出按鈕，每次需重綁
				$('.kwic_export_btn').click(function()	{
					var exportType = $(this).attr('id').replace('kwic_export_btn_','');
					var htmlContentValue = exportType === 'excel' ? 'true':'false';
					$('#tree_area_kwic_ray_table').clone().find('td:nth-child(1) ,td:nth-child(2) , thead , .noShow_in_export').remove().end().tableExport({type:exportType,htmlContent:htmlContentValue,escape:'false',winxdFileName:'KWIC_export_'+new Date().YMDHIS('mm-dd-hh-ii'),defaultTitle:_GLOBALS[_GLOBALS.lang]['22_13']+''+$('#tree_area_kwic_resInfo b > span').text()+' , '+kwic_html.length+_GLOBALS[_GLOBALS.lang]['12_1_2']+' <span style="color:red">('+$('#jing-toolbar .breadcrumb').text()+')</span>' });
				});
					
				$('#tree_area ul.nav-tabs a:last').tab('show');//預設開啟kwic tab	
				
				$('#tree_area_kwic_ray_table tr[data-lb="'+uobj.get('l')+'"]').trigger('click');	//預設click啟動網址行號的kwic
				$('#tree_area_kwic').scrollTo($('#tree_area_kwic_ray_table tr[data-lb="'+uobj.get('l')+'"]').get(0));
			}
		});		
	}
	else if(uobj.get('q') && uobj.get('no_kwic') && uobj.get('q') !== '' && uobj.get('no_kwic') === 'yes')	{	//處理網址來的關鍵字hl，不呼叫左側 kwic 之版本，純做定位到行號和 hl(主要給相似句搜尋用)
		$('.hl').addClass('hl_bk');	//因為無左側 kwic，所以要自動加上 hl_bk 高亮關鍵字，但如果是相似句功能這邊會把該文本所有關鍵字都 hl 起來，所以還是需要後端提供「關鍵字所在之行號」才行！
	}
	else if(uobj.get('note_clue') && uobj.get('note_clue') !== '')	{	//處理網址來的校勘popup
		var clue = $.trim(uobj.get('note_clue'));
		var kw = $.trim(uobj.get('q'));
		$('span[clue="'+clue+'"]').trigger('click');
		var footnote_html = $('#collate-window div[role="collate-window-content"]').html();
		$('#collate-window div[role="collate-window-content"]').html(footnote_html.replace(new RegExp('('+kw.split(new RegExp('['+RegExp.quote(_GLOBALS.extended_search_pattern.join(''))+']','g')).join('|')+')','g'),"<span class=\"hl hl_bk\">$1</span>"));
	}
	else if(uobj.get('inline_note_l') && uobj.get('inline_note_l') !== '')	{	//夾註回來的hl
		var target_small = $('.t[l="'+uobj.get('inline_note_l')+'"]');
		
		$('.hl_bk').removeClass('hl_bk');
		target_small.each(function()	{	//同一行可能有多個 small，都 highlight 起來啦
			$(this).parents('small').eq(0).find('.hl').addClass('hl_bk');	//找到該lb所在的small並將該small下的.hl都加上hl_bk
		});
		
	}

}


function do_fulltext_search(kw,thisJing,limitRangeToThisJing,normalAdvancedSearch)	{	//執行新版全文檢索（參數解釋：thisJing：本經經號、limitRangeToThisJing：過去後啟動「進階表單裡面的本經搜尋」、normalAdvancedSearch：過去後啟用「一般進階搜尋」）
	kw = kw.escape();
	if($.trim(kw) !== '')	addUserAction('fulltext-search',{kw:kw});
	window.open(location.protocol+'//'+location.host+'/search/?q='+encodeURIComponent(kw)+(thisJing ? '&reading='+thisJing:'')+(limitRangeToThisJing ? '&limitRangeToThisJing=yes':'')+(normalAdvancedSearch ? normalAdvancedSearch:'')+'&lang='+_GLOBALS.lang,'');
}

function searchTopLB() {	//搜尋本頁最接近top的.t(因為.lb會隱藏，故改用內文中的span.t)
    var thisID = '';
    $('span.t').each(function () {	
        var thisID2 = $(this).attr('l');
        try {
			//console.log(thisID2+' '+$(this).position().top)
            if ($(this).position().top > -12) {
                thisID = thisID2;
                return false;
            }
        } catch (e) {
            var errMsg = " searchTopLB() can find any span.t!";
        }
    });
    return thisID;
}


function push_historyState(xml_id,juan,title,scroll,specify_url)	{	//執行history.pushState

	if(xml_id === 'home')	{	//如果是首頁就特殊處理
		history.pushState({ xml_id: 'home'}, "", '/'+_GLOBALS.lang+'/');	//於pushState第三個參數修改網址
		return;
	}

	var juanStr = juan ? '_'+PadDigits(juan,3):'';
	var url = specify_url ? _GLOBALS.lang+'/'+specify_url:_GLOBALS.lang+'/'+xml_id.toRay()+juanStr;	//有指定url就用指定的，否則預設是work+juan，ex：T0001_001
	
	history.pushState({ xml_id: xml_id,juan:juan ,title:title,scroll:scroll}, "", '/'+url);	//於pushState第三個參數修改網址
	$.cookie('last-reading-book',xml_id+';'+juan+';'+title,{path:'/'});	//紀錄上次閱讀的經典
	updateLastReadingString();	//更新首頁上次閱讀經典
	

	
	if(location.hostname == _GLOBALS.prodHost)	{	//換網址時加入ga pageview追蹤(in prod)
		//ga('send', 'pageview', "/"+_GLOBALS.lang+'/'+xml_id.toRay()+juanStr);
	}
}	

//是否要經文開啟至新新視窗
function isOpenNewWindow(xml_id,juan,title,scroll,specify_url)	{
	if($('#'+_GLOBALS.active_panel+'-container').hasClass('home-showing'))	{	//首頁顯示時才要開新視窗
		var juanStr = juan ? '_'+PadDigits(juan,3):'';
		var url = specify_url ? _GLOBALS.lang+'/'+specify_url:_GLOBALS.lang+'/'+(scroll ? scroll:xml_id.toRay()+juanStr);	//有指定url就用指定的，否則預設是work+juan，ex：T0001_001
		window.open(location.protocol+'//'+location.host+'/'+url);
		return true;
	}
	return false;
}
	
/*	//舊flash版複製到剪貼簿程式(flash callback)
function f1()	{	//複製到剪貼簿
	var s = $('#rac_modal .modal-body').contents().filter(function() { return $(this).is(':visible');  }).find(':hidden').remove().end().text();	//子元素中可能還有hidden，要移除
	$('#rac_modal').modal('hide');
	
    if (window.clipboardData)
        window.clipboardData.setData('text', s);
    else
        return (s);	
}	
*/

//判斷裝置是否為 iphone 或 ipad
function isIphoneOrIpad() {
    var ua = navigator.userAgent;
    var isIphone = ua.includes('iPhone');
    var isIpad = ua.includes('iPad') || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1);
    return isIphone || isIpad;
}

// CLEARABLE INPUT - 處理整合功能搜尋input text最右邊有個"x" 按下可清除裡面文字
function tog(v){return v?'addClass':'removeClass';} 
$(document).on('input', '.clearable', function(){
    $(this)[tog(this.value)]('x');
}).on('mousemove', '.x', function( e ){
    $(this)[tog(this.offsetWidth-18 < e.clientX-this.getBoundingClientRect().left)]('onX');   
}).on('touchstart click', '.onX', function( ev ){
    ev.preventDefault();
    $(this).removeClass('x onX').val('').change();
});
  
  
function PadDigits(n, totalDigits)	{ 	//eg.Response.Write(PadDigits(46,8)) ' returns 00000046
	n = n.toString(); 
	var pd = ''; 
	if (totalDigits > n.length) 
	{ 
		for (i=0; i < (totalDigits-n.length); i++) 
		{ 
			pd += '0'; 
		} 
	} 
	return pd + n.toString(); 
}	
function posX(obj) {
	var elmt = obj;
	var x = 0;

	for (var e = elmt ; e ; e = e.offsetParent) {
		x += e.offsetLeft;
	}
	for(e = elmt.parentNode; e && e != document.body; e = e.parentNode){
		if (e.scrollLeft) x -= e.scrollLeft;
	}
	return x;
}

function posY(obj) {
	var elmt = obj;
	var y = 0;

	for(var e = elmt ; e ; e = e.parentNode) {
		if(e.offsetTop)	y += e.offsetTop;
	}

	for (e = elmt.parentNode; e && e != document.body; e = e.parentNode){
		if (e.scrollTop) y -= e.scrollTop;
	}
	return y;
}
function findPos(obj) {
    var left, top;
    left = top = 0;
    if (obj.offsetParent) {
        do {
            left += obj.offsetLeft;
            top += obj.offsetTop;
        } while (obj = obj.offsetParent);
    }
    return {
        x : left,
        y : top,
    };
}

function getChineseNumber(number)	{	//將數字轉成中文顯示，如：getChineseNumber(200) => 兩百零一
	number += '';
	var ar = ["零", "一", "二", "三", "四", "五", "六", "七", "八", "九"];
	var cName = ["", "", "十", "百", "千", "萬", "拾", "佰", "仟", "億", "拾", "佰", "仟"];
	var conver = "";
	var cLast = "" ;
	var cZero = 0;
	var i = 0;
	
	for (var j = number.length ; j >=1 ; j--){  
      cNum = parseInt(number.substr(i, 1),10);
      cunit = cName[j]; //取出位數
      if (cNum == 0) { //判斷取出的數字是否為0,如果是0,則記錄共有幾0
         cZero++;
		 /*
         if (strpos(cunit,"萬億") >0 && (cLast == "")){ // '如果取出的是萬,億,則位數以萬億來補
          cLast = cunit ;
         } 
		*/
      }else {
        if (cZero > 0) {// '如果取出的數字0有n個,則以零代替所有的0
			/*
          if (strpos("萬億", substr(conver, strlen(conver)-2)) >0) {
             conver .= cLast; //'如果最後一位不是億,萬,則最後一位補上"億萬"
          }
		  */
          conver +=  "零" ;
          cZero = 0;
          cLast = "" ;
        }
		if(number >= 10 && number < 20 && j == 2)	//把原本11=一十一換成十一
			conver = conver+cunit; 
		else
			conver = conver+ar[cNum]+cunit; // '如果取出的數字沒有0,則是中文數字+單位          
      }
      i++;
    }  
	/*
	//判斷數字的最後一位是否為0,如果最後一位為0,則把萬億補上
     if (strpos("萬億", substr(conver, strlen(conver)-2)) >0) {
       conver .=cLast; // '如果最後一位不是億,萬,則最後一位補上"億萬"
    }
	*/
    return conver;
}

Object.size = function(obj) {	// Get the size of an object : Object.size(myArray);
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};


function div2table(reverse)	{
	
	if(!reverse)	{	//如果是div轉成table，需將行首資訊移到每個td內，這樣引用複製才會正常，且此段程式需於replaceWith開始前執行，否則轉為table後行首會因不允許在table的tr之間而被瀏覽器抽離出去
		$('#body .bip-table-row').each(function()	{
			var br = $(this).prev().prev();	//br與lb在每個.bip-table-row前面
			var lb = $(this).prev();
			if(br.hasClass('lb_br') && lb.hasClass('lb'))	{	//確定是要的 lb_br 和 lb才做，否則可能抓到別的元素變成無限遞迴
				$(this).find('.bip-table-cell').prepend(br).prepend(lb);
			}
		});
	}
	
	//轉換核心程式
	var td = reverse ? ['td','div']:['div','td'];
	var tr = reverse ? ['tr','div']:['div','tr'];
	var table = reverse ? ['table','div']:['div','table'];
	
	$('#body .bip-table-cell').replaceWith(function(){
		return this.outerHTML.replace("<"+td[0], "<"+td[1]).replace("</"+td[0], "</"+td[1]);
	});
	
	$('#body .bip-table-row').replaceWith(function(){
		return this.outerHTML.replace("<"+tr[0], "<"+tr[1]).replace("</"+tr[0], "</"+tr[1]);
	});	
	$('#body .bip-table').replaceWith(function(){
		return this.outerHTML.replace("<"+table[0], "<"+table[1]).replace("</"+table[0], "</"+table[1]);
	});	
	
	if(reverse)	{	//反之如果是轉回div，就把在td裡面的行首移回去bip-table-row之前(即原本位置)，但必須在轉換回div後再移，但因移完後行首會在tr之間，所以需先轉回div，否則會跟上面一樣的問題，故此段放在轉回div後執行
		$('#body .bip-table-row').each(function()	{
			var br = $(this).find('.bip-table-cell .lb_br:eq(0)');	//取第一個放回去即可
			var lb = $(this).find('.bip-table-cell .lb:eq(0)');
			$(this).before(br).before(lb);
			$(this).find('.bip-table-cell .lb_br').remove();	//在td內的lb、br都要移除
			$(this).find('.bip-table-cell .lb').remove();
		});		
	}
}

function selectNodeText(node)	{	//傳入DOM node，將該node內的文字全選
    if (document.body.createTextRange) {
        const range = document.body.createTextRange();
        range.moveToElementText(node);
        range.select();
    } else if (window.getSelection) {
        const selection = window.getSelection();
        const range = document.createRange();
        range.selectNodeContents(node);
        selection.removeAllRanges();
        selection.addRange(range);
    } else {
        console.warn("Could not select text in node: Unsupported browser.");
    }
}

function debounce(func, delay) {	//預防事件連續觸發cb用
  var timer = null;
  return function () {
    var context = this;
    var args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      func.apply(context, args)
    }, delay);
  }
}

function putBackRefAndCopyFromAutopunc()	{
	var puncs = ['，','。','[',']'];
	var ref_text = '<span class="refAndCopy_main_text"><span class="t" l="0001a13" w="15">阿含<span class="pc"><span class="refAndCopy_punctuation">，</span></span>秦</span><br class="lbbr" style="display: none;"><span class="lb" id="" style="display: none;"></span><span class="t" l="0001a14" w="1">言法歸<span class="pc"><span class="refAndCopy_punctuation">。</span></span>法歸者<span class="pc"><span class="refAndCopy_punctuation">，</span></span>蓋是萬善之淵府<span class="pc"><span class="refAndCopy_punctuation">，</span></span>總持之林</span><br class="lbbr" style="display: none;"><span class="lb" id="" style="display: none;"></span><span class="t" l="0001a15" w="1">苑<span class="pc"><span class="refAndCopy_punctuation">。</span></span>其為典也<span class="pc"><span class="refAndCopy_punctuation">，</span></span>淵博弘富<span class="pc"><span class="refAndCopy_punctuation">，</span></span></span><a class="noteAnchor" href="#n0001005"></a><span class="note-link cb" clue="n0001005"><span class="refAndCopy_note">[5]</span></span><span class="t" l="0001a15" w="10">韞</span><span class="t" l="0001a15" w="11">而彌廣<span class="pc"><span class="refAndCopy_punctuation">；</span></span>明宣禍</span><br class="lbbr" style="display: none;"><span class="lb" id="" style="display: none;"></span><span class="t" l="0001a16" w="1">福賢愚之迹<span class="pc"><span class="refAndCopy_punctuation">，</span></span>剖判真偽異</span><a class="noteAnchor" href="#n0001006"></a><span class="note-link cb" clue="n0001006"><span class="refAndCopy_note">[6]</span></span><span class="t" l="0001a16" w="11">齊</span><span class="t" l="0001a16" w="12">之原<span class="pc"><span class="refAndCopy_punctuation">，</span></span>歷記古</span><br class="lbbr" style="display: none;"><span class="lb" id="" style="display: none;"></span><span class="t" l="0001a17" w="1">今成敗之數<span class="pc"><span class="refAndCopy_punctuation">，</span></span>墟域二儀品物之倫<span class="pc"><span class="refAndCopy_punctuation">。</span></span>道無不由<span class="pc"><span class="refAndCopy_punctuation">，</span></span></span><br class="lbbr" style="display: none;"><span class="lb" id="" style="display: none;"></span><span class="t" l="0001a18" w="1">法無不在<span class="pc"><span class="refAndCopy_punctuation">，</span></span>譬彼巨海<span class="pc"><span class="refAndCopy_punctuation">，</span></span>百川所歸<span class="pc"><span class="refAndCopy_punctuation">，</span></span>故以法歸為</span><br class="lbbr" style="display: none;"><span class="lb" id="" style="display: none;"></span><span class="t" l="0001a19" w="1">名<span class="pc"><span class="refAndCopy_punctuation">。</span></span></span></span>';
	var auto_punc_text = '阿含，秦言法歸。法歸者，蓋是萬善之淵府，總持之林苑。其為典也，淵博弘富，韞而彌廣；明宣禍福賢愚之迹，剖判真偽異齊之原，歷記古今成敗之數，墟域二儀品物之倫。道無不由，法無不在，譬彼巨海，百川[所歸故以法]歸為名';
	
	ref_text = ref_text.replace(/<span class="refAndCopy_punctuation">[^<]+<\/span>/g,'');
	
	
	var last_word_index = -1;
	var res = [];
	for(var word of auto_punc_text.split(''))	{
		if(puncs.includes(word))	res.push([last_word_index+1,word])	//遇到標點記下位置跟標點
		last_word_index = ref_text.indexOf(word,last_word_index+1);		
		
	}
	console.log(res);
	
	res.forEach(function(obj,index)	{
		var [pos , new_punc] = obj;
		ref_text = ref_text.insertStr((pos+(index * new_punc.length)),0,new_punc);	//插入字時，需把上次已插入的字串長度算進去		
	});
	console.log(ref_text)
	
}

/*
   * 阿閒增加 winxd add
   * 將字元或字串插入字串裡面，使用 String.prototype 擴充
   * @param {number} start Index at which to start changing the string.
   * @param {number} delCount An integer indicating the number of old chars to remove.
   * @param {string} newSubStr The String that is spliced in.
   * @return {string} A new string with the spliced substring.
   * example: s.insertStr(4,0,'foo')
*/
String.prototype.insertStr = function(start, delCount, newSubStr) {
    return this.slice(0, start) + newSubStr + this.slice(start + Math.abs(delCount));
};


//測試功能：由內文行號打開樹狀目錄的lb
function openTreeByLB()	{
	var tree = $('#dynatree_structure').dynatree('getTree');
	var match = null;
	tree.visit(function(node){
		if(node.data.lb === "0082a06"){
			match = node;
			return false; // stop traversal (if we are only interested in first match)
		}
	});
	
	if(match)	{
		//console.log(match);	
		match.toggleExpand();
		match.activate(true);
	}
}

function number_format(number, decimals, decPoint, thousandsSep) { //行為模式跟php的number_format一樣
  //    input by: Kheang Hok Chin (https://www.distantia.ca/)
  //    input by: Jay Klehr
  //    input by: Amir Habibi (https://www.residence-mixte.com/)
  //    input by: Amirouche
  //   example 1: number_format(1234.56)
  //   returns 1: '1,235'
  //   example 2: number_format(1234.56, 2, ',', ' ')
  //   returns 2: '1 234,56'
  //   example 3: number_format(1234.5678, 2, '.', '')
  //   returns 3: '1234.57'
  //   example 4: number_format(67, 2, ',', '.')
  //   returns 4: '67,00'
  //   example 5: number_format(1000)
  //   returns 5: '1,000'
  //   example 6: number_format(67.311, 2)
  //   returns 6: '67.31'
  //   example 7: number_format(1000.55, 1)
  //   returns 7: '1,000.6'
  //   example 8: number_format(67000, 5, ',', '.')
  //   returns 8: '67.000,00000'
  //   example 9: number_format(0.9, 0)
  //   returns 9: '1'
  //  example 10: number_format('1.20', 2)
  //  returns 10: '1.20'
  //  example 11: number_format('1.20', 4)
  //  returns 11: '1.2000'
  //  example 12: number_format('1.2000', 3)
  //  returns 12: '1.200'
  //  example 13: number_format('1 000,50', 2, '.', ' ')
  //  returns 13: '100 050.00'
  //  example 14: number_format(1e-8, 8, '.', '')
  //  returns 14: '0.00000001'
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
  const n = !isFinite(+number) ? 0 : +number
  const prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
  const sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
  const dec = (typeof decPoint === 'undefined') ? '.' : decPoint
  let s = ''
  const toFixedFix = function (n, prec) {
    if (('' + n).indexOf('e') === -1) {
      return +(Math.round(n + 'e+' + prec) + 'e-' + prec)
    } else {
      const arr = ('' + n).split('e')
      let sig = ''
      if (+arr[1] + prec > 0) {
        sig = '+'
      }
      return (+(Math.round(+arr[0] + 'e' + sig + (+arr[1] + prec)) + 'e-' + prec)).toFixed(prec)
    }
  }
  // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec).toString() : '' + Math.round(n)).split('.')
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || ''
    s[1] += new Array(prec - s[1].length + 1).join('0')
  }
  return s.join(dec)
}