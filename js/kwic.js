/**
 * kwic_highlight function
 * 可跨tag highlight 關鍵字之pure-js 函式 
 *
 * 作者：李阿閒 <winxd@dila.edu.tw>
 * 版本：0.1
 * All right reserved. 轉載需經原作者同意
 *
 * 使用方法：KWIC.hl(關鍵字 , 目標html , highlight的class name , [optional]客製化的ignore regex);
 * 回傳值：將html裡面含有關鍵字的部份包上第三個參數之class name後回傳
 *
 */
 
var KWIC = {
	ignore_tags:['<[^>]+>','<\\/[^>]+>','<[^>]+\\/>','\\s'],	//ignore opening tag,self-close tag,close tag,all spaces
	ignore_punctuations: ['\\(','\\)','，','。','！','…','、','「','」','\\.','\\,','》','《','：','）','（','；','？','　','─','"',"'",'\\*','︰','\\]','\\[','〉','〈','』','『','．','－','～','＼','｡','\\?'],
	ignore_custom:[],
	hl:function(kws,target_html,hl_class,ignore_custom)	{
		if(!kws || !target_html || !hl_class)	{
			alert('關鍵字、目標html、highlight classname三個參數必須要有！');
			return false;
		}		
		var kwic_obj = this;
		var kw = kws.split('');
		kwic_obj.ignore_custom = ignore_custom || [];

		//關鍵字組合成regex，像下面這樣：
		//s.replace(/(思)((<[^>]+>|<\/[^>]+>|<[^>]+\/>|\s|，)*?)(量)((<[^>]+>|<\/[^>]+>|<[^>]+\/>|\s)*?)(及)((<[^>]+>|<\/[^>]+>|<[^>]+\/>|\s)*?)(了)((<[^>]+>|<\/[^>]+>|<[^>]+\/>|\s)*?)/gi,'replace');
		//！2017-04-06 更新，由於是純html的關係，故html的屬性不該被hl，故在關鍵字的前後需加上已保證不會hl到html的其他部份：(?!<.*?)(keyword)(?![^<>]*?>) 
		
		var regex = [];
		for(var k in kw)	{
			regex.push('(?!<.*?)('+this.quote(kw[k])+')(?![^<>]*?>)'+'(('+kwic_obj.ignore_tags.join('|')+'|'+kwic_obj.ignore_punctuations.join('|')+(kwic_obj.ignore_custom.length > 0 ? '|'+kwic_obj.ignore_custom.join('|'):'')+')*?)');
		}

		var r = new RegExp(regex.join(''),'gi');
		var group_counter = 0;
		var new_html = target_html.replace(r,function()	{
			var matches = [].slice.call(arguments, 1,-2);
			var cc = 0;
			var res = [];
			for(var i in matches)	{
				if(cc++ % 3 == 0)		res.push('<tspan class="'+hl_class+'" group="'+Math.ceil(++group_counter/kw.length)+'">'+matches[i]+'</tspan>');
				else if(cc % 3 != 0)	res.push(matches[i]);
			}
			return res.join('');			
		});
		
		return new_html;
	},
	quote:function(s)	{	//escape regex special char
		return (s+'').replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&");
	}
};