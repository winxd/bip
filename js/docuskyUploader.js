/*
 * author:阿閒 winxd@dila
 * 功能：封裝docusky widget上傳與登入機制implement
 *
*/
var docuskyUploader = {
	DocuSkyObj:false,
	DocuSky_SID:false,
	init:function()	{
		var ps = this;
		if(!this.DocuSkyObj)	{
			this.DocuSkyObj = docuskyManageDbListSimpleUI;
		}
		
		//登入按鈕
		$(document).on('click','#docuskyLoginSubmit',function()	{	
			var username = $("#docuskyLoginAcct").val();
			var password = $("#docuskyLoginPasswd").val();
			docuskyManageDbListSimpleUI.login(username, password, function(resJSON) {
				ps.DocuSky_SID = resJSON;
				//alert(ps.DocuSky_SID)
				docuskyUploader.upload();
			}, function(serverMsg) {
				alert("登入失敗\nFail to login\n")
			});			
		});
	},
	upload:function()	{
		var dbTitle	= $('#jing_send_databaseName').val();
		var login_html = '<div>請先登入<br/>Please login to DocuSky<br/><br/>\
							   <input id="docuskyLoginAcct" type="text" class="form-control" placeholder="your account"><br/>\
							   <input id="docuskyLoginPasswd" type="password" class="form-control" placeholder="your password"><br/>\
							   <button id="docuskyLoginSubmit" class="btn btn-default">Login DocuSky</button>\
						  </div>';
		var need_file_error_html = '<div>取得docusky xml 發生問題，請稍後再試。<br/>Error occurred with fetching Docusky XML.<br/> Please try again later.<br/><br/><div style="text-align:right"><button class="btn btn-default" onclick="$(\'#jing_send_modal\').modal(\'hide\')">Close</button></div>';
		var error_html = '<div>發生未知的錯誤，請稍後再試。<br/>Unknown error occurred , please try it again later.<br/><br/><div style="text-align:right"><button class="btn btn-default" onclick="$(\'#jing_send_modal\').modal(\'hide\')">Close</button></div>';
	
		var ps = this;
		var range = $('input[name="jing_send_range"]:checked').val() === 'now' ? _GLOBALS.catalogAPI+'download/docusky/'+_GLOBALS.open_container['reading-area-1-container'].reading_xmlID.toRay()+'_'+_GLOBALS.juanNow:_GLOBALS.catalogAPI+'download/docusky/'+_GLOBALS.open_container['reading-area-1-container'].reading_xmlID.toRay();
		
		if($.trim(dbTitle) === '')	{
			alert("請輸入資料庫名稱\nPlease fill in the Database name.");
			return false;
		}
		
		$.get('getData.php?type=docusky&url='+range+'.docusky.xml',function(xml)	{
			var data = {};
			data.my = { name:'dbTitleForImport', value:dbTitle};
			data.file = {name:'importedFiles[]', filename:_GLOBALS.open_container['reading-area-1-container'].reading_xmlID.toRay()+($('input[name="jing_send_range"]:checked').val() === 'all' ? '':'_'+_GLOBALS.juanNow)+'.xml', value:xml };

			$('#jing_send_form').hide();
			$('#jing_send_result').html('Sending ...').show();
			
			ps.DocuSkyObj.uploadMultipart('https://docusky.org.tw/docusky/webApi/uploadXmlFilesToBuildDbJson.php', data, function(serverMsg) { 
				//console.log(serverMsg);
				if(parseInt(serverMsg.code,10) == 0)	{	//上傳成功
					$('#jing_send_result').html("已成功上傳文件<br/>Send to DocuSky Successfully.<br/><br/><div style=''><button class='btn btn-default' onclick='$(\"#jing_send_modal\").modal(\"hide\")'>Close</button>&nbsp;<a target='_blank' class='btn btn-success' href='https://docusky.org.tw/DocuSky/docuTools/userMain/?"+ps.DocuSky_SID+"'>開啟DocuSky</a></div>");
					/*
					$('#jing_send_modal').modal('hide');
					$('#jing_send_form').show();
					$('#jing_send_result').hide();
					*/					
				}
				else if(parseInt(serverMsg.code,10) == 101)	{	//需要登入
					$('#jing_send_result').html(login_html);
				}
				else if(parseInt(serverMsg.code,10) == 292)	{	//上傳的檔案沒有內容
					$('#jing_send_result').html(need_file_error_html);
				}
				else	{
					$('#jing_send_result').html(error_html);
				}
			},function(serverMsg)	{	//error 時的 callback
				console.log(serverMsg)
				if(parseInt(serverMsg.code,10) == 101)	{	//需要登入
					$('#jing_send_result').html(login_html);
				}
				else if(parseInt(serverMsg.code,10) == 292)	{	//上傳的檔案沒有內容
					$('#jing_send_result').html(need_file_error_html);
				}
				else	{
					$('#jing_send_result').html(error_html);
				}
				
			});
		});
	}
}