		var refAndCopy	= function(tempContainerID)	{	//引用複製物件，需傳入一個暫時的「容器」之id，容器用div即可並且需要藏起來
			//標點符號不包含「'」、「"」、「[」、「]」、「*」，因為單雙引號會造成引用複製裡面的tag失效，[、]、* 則是校勘的符號，不做取代
			var punctuations = ['\\(','\\)','，','。','！','…','、','「','」','\\.','\\,','》','《','：','）','（','；','？','　','─','︰','〉','〈','』','『','．','－','～','＼','｡','\\?'];	
			var tempDiv = document.getElementById(tempContainerID) ? document.getElementById(tempContainerID) : null;
			var IE = (/msie/.test(navigator.userAgent.toLowerCase())) ? true : false;	//IE10(含)以下
			var IE11 = (/.net/.test(navigator.userAgent.toLowerCase()) ? true:false);	//IE11
			var edge = (/edge/.test(navigator.userAgent.toLowerCase()) ? true:false);	//edge
			var IEErrorMsg = '';
			var range;
			if(!tempDiv)	{
				showErr('container is null');
				return false;
			}
			
			function _init()	{
				if(IE)	{	//IE10(含)以下，使用ie唯一認得的createRange方法
					range = document.selection.createRange();
					if(range.text == '')	throw "請選擇文章內的文字後再使用引用複製功能。\n Please markup the text first.";
				}
				else	{	//firefox,chrome,IE11，則用此，因為ie11不支援上面的createRange，反而支援這邊...
					range = document.getSelection();
					if(range.toString() == '' && !_GLOBALS.isTouchSelection)	throw "請選擇文章內的文字後再使用引用複製功能。\n Please markup the text first.";
					//range = range.getRangeAt(0);
					
					if(_GLOBALS.isTouchSelection)	{	//如果是觸控版，就使用 _GLOBALS.isTouchRange 來當 range
						range = _GLOBALS.isTouchRange;
					}
					else	{
						range = range.getRangeAt(0);	//
					}
				}
				//range = (IE ? document.selection.createRange() : document.getSelection().getRangeAt(0));

			}
			
			function _getLineInfo()	{
				if(IE || IE11 || edge)	{	//IE11支援上面w3c標準的getSelection方法，卻又不支援comparePoint方法，只好放在此...
					var rangeHTML = (IE ? range.htmlText:range.cloneContents());
					$(tempDiv).html(rangeHTML);	
					var tempList = $('#'+tempContainerID).find('span.lb');					
					//IE正確的起始lb是tempList.first()的前一個才對，故做以下處理
					var lb_index = -1;
					$('span.lb').each(function()	{
						lb_index++;
						if($(this).attr('id') == tempList.first().attr('id'))
							return false;						
					});
					if(tempList.size() > 0)	{	//選擇的範圍如果有lb就可使用此法						
						IEErrorMsg = '';
						return [$('span.lb:eq('+(lb_index-1)+')').attr('id'),tempList.last().attr('id')];
					}
					else	{						//選擇的範圍沒有lb，就必須特別處理
						//_IE_insertNode();			//在選擇的範圍前插入一個暫時的node,#wanderer
						var IE_single_lb = _find_single_lb();
						return [IE_single_lb,IE_single_lb];
					}
				}
				else	{
					var FF_start,FF_end,range_lb_s = [],range_lb_e = [];
					$("span.lb").each(function (i) {
						var compareValue = range.comparePoint(this, 0);
						if (compareValue==-1) {
							FF_start = this.getAttribute('id');
							FF_end = this.getAttribute('id');
							range_lb_s.push(this.getAttribute('id'));
						} 
						else if (compareValue==0) {	
							FF_end = this.getAttribute('id');
							range_lb_e.push(this.getAttribute('id'));
						}
						else if (compareValue==1) return false;
					});				
					return [FF_start,FF_end,[range_lb_s[range_lb_s.length-1]].concat(range_lb_e)];
				}
				
			}			
			function _getLine()	{
				var tempLine = _getLineInfo();				
				var startLine = tempLine[0];
				var endLine = tempLine[1];
				return {'startLine':startLine,'endLine':endLine};
			}

			function _getText()	{
				var rangeHTML = (IE ? range.htmlText:range.cloneContents());
				//用jq的text()取得純文字、且對所有\n不論一個或一個以上皆取代成<br>\n，讓html和純文字都有換行效果
				//CBETA本身之搜尋搜不到數字[0-9]+英文[A-Za-z]，表示經文裡面應無數字or英文，因此數字英文皆取代掉，用以去除copy到.lb或.lineInfo之英數字，唯一例外是註解需要能被拷貝，如[12]、[A10]這種，因此加上 [^\[] 限定不為 "[" 開頭的才能去除
				//return $('<div/>').html(rangeHTML).text().replace(/\n|_|([TXJACFGKLMPSUDNI]|ZS|ZW)[0-9]{2}n[0-9]{4}_p[0-9]{4}[a-c][0-9]{2}：?|[^\[][a-zA-Z0-9]+[a-zA-Z0-9][a-zA-Z0-9]+：?/g,'').replace(/\t/g,"\n<br/>").replace(/(\[[A0-9＊]+\])/g,"<span class='refAndCopy_note'>$1</span>");	
				//2015-10-12:去除：「[^\[][a-zA-Z0-9]+[a-zA-Z0-9][a-zA-Z0-9]+：」這regex，會造成http://dev.dila.edu.tw/bugify/issues/231 的問題，原本是做.lineInfo，現在.lineInfo改用css ::before來做就不須此regex(複製不到)，但原始資料還是備份如上一行
				return $('<div/>').html(rangeHTML).html().replace(/\n|_|([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB|LC|TX|CC)[0-9]{2,3}n[ABab]?[0-9]{3,4}[A-Za-z]?_?p[a-z]?[0-9]{3,4}[a-c][0-9]{2}：?/g,'').replace(/\t/g,"\n<br/>").replace(/(\[[A0-9＊a-z]+\])/g,"<span class='refAndCopy_note'>$1</span>").replace(/(\[[科標解][0-9]+\])/g,"<span class='refAndCopy_note'>$1</span>");	
				
			}
			
			function _getPureText()	{	//與上方_getText()邏輯一模一樣，差別在於這裡取「text()」非「html()」
				var rangeHTML = (IE ? range.htmlText:range.cloneContents());
				return $('<div/>').html(rangeHTML).text().replace(/\n|_|([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB|LC|TX|CC)[0-9]{2,3}n[ABab]?[0-9]{3,4}[A-Za-z]?_?p[a-z]?[0-9]{3,4}[a-c][0-9]{2}：?/g,'').replace(/\t/g,"\n<br/>").replace(/(\[[A0-9＊a-z]+\])/g,"<span class='refAndCopy_note'>$1</span>").replace(/(\[[科標解][0-9]+\])/g,"<span class='refAndCopy_note'>$1</span>");	
				
			}
			
			function _getNote()	{	//如果選取文字範圍內有note，則需把note的解釋放入
				var rangeHTML = (IE ? range.htmlText:range.cloneContents());	
				return '<span class="refAndCopy_note">'+$('<span/>').html(rangeHTML).find('.note-link,.note-link-cbeta').map(function()	{	
					var htmlContent = '';
					var note_anchor_text = ($(this).data('star-no') && $.trim($(this).data('star-no')) !== '' ? $.trim($(this).text()).replace(/(])/g,$.trim($(this).data('star-no'))+"$1") : $.trim($(this).text()));
					if($(this).hasClass('note-link'))	{
						if($('#'+_GLOBALS.active_panel+'-container #back #'+$(this).attr('clue')+'.footnote').size() >= 1)	
							htmlContent = $('#'+_GLOBALS.active_panel+'-container #back #'+$(this).attr('clue')+'.footnote').text()+'　';
						else	
							htmlContent = $('#'+_GLOBALS.active_panel+'-container #back #'+$(this).attr('clue')+'.T').text()+'　';
					}
					else if($(this).hasClass('note-link-cbeta'))	{
						 htmlContent = $('#'+_GLOBALS.active_panel+'-container #back #'+$(this).attr('clue')+'[class="footnote add"]').text()+'　';
					}
					return "<br/><span class='note_linebreak_symbol'>\n</span>"+note_anchor_text+' '+htmlContent; }).get().join('')+'</span>';
				
			}
			
			function _getLineRange()	{
				return _getLineInfo()[2];
			}
			
			function _CBETA_parser(no)	{	//將cbeta經號轉換成：CBETA, T01, no. 1, p. 1, a1-p. 11, a1
				var result = /([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB|LC|TX|CC)([0-9]{2,3})n([ABab]?[0-9]{3,4}[A-Za-z]?)_?p([a-z]?)([0-9]{3,4})([a-z])([0-9]{2})/gi.exec(no);
				if(!result) return false;
				return {collection:result[1],vol:result[2],sutra:result[3],page_prefix:result[4],page:result[5],col:result[6],line:result[7]};
			}
			
			function _r2z_ln(line) {
				if(!line)	return '';
				var page=line.substr(0,4);
				page=parseInt(page,10);
				rest=page%2;
				page=Math.floor((page/2))+rest;
				page=page.toString();
				if (page!='') {
					if (page.length==1) page = "000" + page;
					if (page.length==2) page = "00" + page;
					if (page.length==3) page = "0" + page;
				}
				var column=line.substr(4,1);
				if (rest==0) {
					if (column=='a') column='c';
					else column='d';
				}
				line=page+'.'+column+line.substr(5);
				return line;
			}	
			
			function r2z(LineStartR,LineEndR)	{
				var temp = '';
				if(!LineStartR && !LineEndR)	{
					return {sZ:null,eZ:null};
				}
				LineStartR = LineStartR ? LineStartR.replace(/\.|R/g,''):'';
				LineEndR = LineEndR ? LineEndR.replace(/\.|R/g,''):'';

				var vol_r = LineStartR.substr(0,3);
				vol_r = parseInt(vol_r,10);
				LineStartR = LineStartR.substr(3);
			
				LineEndR = LineEndR.substr(3);
				var vol_z;
				if (vol_r <= 95) {
					temp = temp + "Z 1:" + vol_r + ".";
				} else if (vol_r <= "127") {
					vol_z = vol_r - 95;
					temp = temp + "Z 2:" + vol_z + ".";
				} else {
					vol_z = vol_r - 127;
					temp = temp + "Z 2B:" + vol_z + ".";
				}
				var LineStartZ=_r2z_ln(LineStartR);
				var LineEndZ=_r2z_ln(LineEndR);
				
				return {sZ:temp+LineStartZ,eZ:temp+LineEndZ};
			}
			
			function RZ_format(sR_obj,eR_obj)	{	//取得卍續藏r碼z碼
				var resObj = {bookLine:'',jingLine:''};
				var firstpart,pageline;
				
				if(sR_obj && eR_obj)	{
					firstpart = sR_obj[0];
					if(sR_obj[1] == eR_obj[1])	{
						/*
						console.log(sR_obj)
						console.log(eR_obj)
						*/
						if(sR_obj[2] == eR_obj[2])	pageline = ' p. '+parseInt(sR_obj[1],10)+''+(sR_obj[2] == eR_obj[2] ? sR_obj[2].substr(0,1)+parseInt(sR_obj[2].substr(-2),10):sR_obj[2].substr(0,1)+parseInt(sR_obj[2].substr(-2),10)+'-'+eR_obj[2].substr(0,1)+parseInt(eR_obj[2].substr(-2),10));
						else 						pageline = ' p. '+parseInt(sR_obj[1],10)+''+sR_obj[2].substr(0,1)+parseInt(sR_obj[2].substr(-2),10)+'-'+parseInt(eR_obj[2].substr(-2),10);
					}
					else	{
						pageline = ' pp. '+parseInt(sR_obj[1],10)+''+sR_obj[2]+'-'+parseInt(eR_obj[1],10)+''+eR_obj[2];
					}
				}
				else if(sR_obj || eR_obj)	{
					var tmp_seR_obj = sR_obj || eR_obj;	//可能只有一個，取有的那一個
					firstpart = tmp_seR_obj[0];
					pageline =  ' p. '+parseInt(tmp_seR_obj[1],10)+''+tmp_seR_obj[2];
				}
				
				if(firstpart && pageline)	{
					resObj.bookLine = ' // '+(firstpart.substr(0,1) == 'R' ? firstpart.substr(0,1)+parseInt(firstpart.substr(-3),10):firstpart)+','+pageline;	//書目格式
					resObj.jingLine = ' // '+(sR_obj && eR_obj && sR_obj.toString() != eR_obj.toString() ? sR_obj.toString().replace(/\,/,'_p').replace(/\,/g,'')+' ~ '+ eR_obj.toString().replace(/\,/,'_p').replace(/\,/g,''):sR_obj.toString().replace(/\,/,'_p').replace(/\,/g,'')); 	//行首格式
				}
				
				return resObj;
			}
			
			function getRZ_str(sLine,eLine)	{
				var z_temp = r2z($('#'+sLine).attr('data-lr'),$('#'+eLine).attr('data-lr'));
				var sR_obj = $('#'+sLine).attr('data-lr') ? $('#'+sLine).attr('data-lr').split('.'):null;	//起始r碼
				var eR_obj = $('#'+eLine).attr('data-lr') ? $('#'+eLine).attr('data-lr').split('.'):null;	//結束r碼
				var sZ_obj = z_temp.sZ ? z_temp.sZ.split('.'):null;	//起始z碼
				var eZ_obj = z_temp.eZ ? z_temp.eZ.split('.'):null;	//結束z碼
				var R = RZ_format(sR_obj,eR_obj);
				var Z = RZ_format(sZ_obj,eZ_obj);
				/*
				console.log(R)
				console.log(Z)				
				*/
				return {bookLine:R.bookLine+Z.bookLine,jingLine:R.jingLine+Z.jingLine};
			}
			
			function getLineStr()	{
				var resObj = {bookLine:null,jingLine:null};	//回傳行首格式+書目格式
				var lineTmp = _getLine();
				var sObj = _CBETA_parser(lineTmp.startLine);
				var eObj = _CBETA_parser(lineTmp.endLine);
				var RZObj = getRZ_str(lineTmp.startLine,lineTmp.endLine);	//取得r碼z碼 in 卍續藏
				
				if(sObj && eObj)	{
					var pageline;
					if(sObj.page_prefix+sObj.page == eObj.page_prefix+eObj.page)	{
						if(sObj.col == eObj.col)	pageline = ' p. '+sObj.page_prefix+parseInt(sObj.page,10)+''+sObj.col+(sObj.line == eObj.line ? parseInt(sObj.line,10):parseInt(sObj.line,10)+'-'+parseInt(eObj.line,10));
						else						pageline = ' p. '+sObj.page_prefix+parseInt(sObj.page,10)+''+sObj.col+parseInt(sObj.line,10)+'-'+eObj.col+parseInt(eObj.line,10);
					}
					else	{
						pageline = ' pp. '+sObj.page_prefix+parseInt(sObj.page,10)+''+sObj.col+sObj.line+'-'+eObj.page_prefix+parseInt(eObj.page,10)+''+eObj.col+parseInt(eObj.line,10);
					}
					resObj.bookLine = 'CBETA '+_GLOBALS.currentVersion+', '+sObj.collection+sObj.vol+', no. '+sObj.sutra.replace(/^0+([^0]+)/g,"$1")+','+pageline+RZObj.bookLine;	//書目格式
				}
				
				resObj.jingLine = (lineTmp.startLine == lineTmp.endLine ? lineTmp.startLine : lineTmp.startLine +' ~ '+lineTmp.endLine)+RZObj.jingLine;	//行首格式
				return resObj;
			}
			
			function getPin()	{	//取得品名
				var lineStart = _getLine().startLine;
				var pin;
				if($('#'+lineStart).prevAll('mulu').first().length > 0)	pin = $('#'+lineStart).prevAll('mulu').first().attr('s');
				else pin = $('#'+lineStart).parents('p,div').first().prevAll('mulu').first().attr('s'); 
				
				return pin ? '〈'+pin+'〉':'';
			}
			
			/*
			function _IE_insertNode()	{
				var wanderer = $("<span/>").attr("id","wanderer").get(0);						
				if (window.getSelection) {  // all browsers, except IE before version 9
					var selection = window.getSelection ();
					if (selection.rangeCount > 0) {
						var range1 = selection.getRangeAt(0);
						range1.insertNode(wanderer);
					}
				}
				else {  // Internet Explorer before version 9
					var textRange = document.selection.createRange();
					textRange.collapse(true);
					textRange.pasteHTML(wanderer.outerHTML);
					wanderer.parentNode.removeChild(wanderer);
				}			
			}
			function _find_single_lb()	{
				var stop_flag = $('#wanderer').prevAll('.lb').eq(0).attr('id');
				var parent_str = '#wanderer';
				while(!stop_flag)	{
					stop_flag = $(parent_str).parent().prevAll('.lb').eq(0).attr('id');
					parent_str = $(parent_str).parent();
				}
				$('#wanderer').remove();
				return stop_flag;				
			}
			*/
			
			function _find_single_lb()	{	//2016新招！使用ie支援的compareBoundaryPoints去找第一個.lb(此方法僅能找到s，e一定是.lb最後一個，故e可以不用)
				var s,e;
				$("span.lb").each(function (i) {
					var sourceRange = document.createRange();
					sourceRange.selectNode(this);
					compare = range.compareBoundaryPoints(Range.START_TO_END, sourceRange);
					if(compare == 1)	s = this;
					else if(compare == -1)	return false;
					else if(compare == 0)	return false;
				});	
				return s.getAttribute('id');
			}
			
			function wrap_punctuations(s)	{
				return s.replace(new RegExp('(['+punctuations.join('')+'])','gi'),"<span class='refAndCopy_punctuation'>$1</span>");
			}
			
			function showErr(msg)	{
				//alert(msg)
				console.log(msg)
				return;
			}
			
			return {
				get_content:function(bookContainer)	{
					try	{
						_init();
					}
					catch(e)	{
						showErr(e);
						return false;
					}
					try	{
						var bookTitle = bookContainer.find('title').first().text();
						//var juanInfo = '卷'+parseInt(bookContainer.find('meta[name="filename"]').attr('content').split('.')[0].split('_')[1],10)+' ';
						var juanInfo = '卷'+parseInt(_GLOBALS.juanNow,10);
						var pinName = '<span class="refAndCopy_pin">'+getPin()+'</span>';
						var lineInfo = getLineStr();
						
						if($('#dynatree_juan > ul > li').size() <= 1)	juanInfo = '';	//如果卷選單只有1卷，引用複製就不顯示「卷1」
						
						return '<span class="refAndCopy_title_front">《'+bookTitle+'》'+juanInfo+pinName+'：</span>「<span class="refAndCopy_main_text">'+wrap_punctuations(_getText())+'</span>」<span class="refAndCopy_title_back" style="display:none">《'+bookTitle+'》'+juanInfo+pinName+'</span>(<span class="refAndCopy_line_book">'+lineInfo.bookLine+'</span><span class="refAndCopy_line_jing" style="display:none">'+lineInfo.jingLine+'</span>)'+_getNote();
					}
					catch(err)	{
						//IEErrorMsg !== '' ? showErr(IEErrorMsg) : showErr('未知的錯誤');	
						showErr(err)
						return false;
					}
				},
				get_line:function()	{
					try	{
						_init();
					}
					catch(e)	{
						showErr(e);
						return false;
					}				
					return _getLine();
				},
				get_text:function()	{
					try	{
						_init();
					}
					catch(e)	{
						showErr(e);
						return false;
					}				
					return _getText();
				},
				get_pure_text:function()	{	//取得反白的純文字（無標點、無任何html tag）
					try	{
						_init();
					}
					catch(e)	{
						showErr(e);
						return false;
					}				
					return _getPureText().replace(/<span[^>]+>[^<]+<\/span>/gi,'').replace(new RegExp('(['+punctuations.join('')+'])','gi'),'');
				},
				get_line_range:function()	{
					try	{
						_init();
					}
					catch(e)	{
						showErr(e);
						return false;
					}
					return _getLineRange();
				},
				wrap_punctuations:function(s)	{
					return wrap_punctuations(s);
				}
			}
		}