var highlight = function(config)	{	//highlight object 有用到jquery!!必須先include jquery lib
	var hightlight_color = config.highlight_color ? config.highlight_color : null;
	var highlight_target = config.highlight_target ? config.highlight_target : null;
	var ignore_classnames = config.ignores ? config.ignores : null;	//不要做highlight的classnames array，ex：['lb','anchor']
	if(!hightlight_color || !highlight_target || (highlight_target != 'anchor'))	{
		alert('初始化有誤，建立物件失敗');
		return;
	}
	var highlightedObjs = [];
	var lbEndFlag = false;	//anchor專用，用來標示已找到結束anchor
	
	//anchor highlight專用之private func
	var traverseLbChild = function(elm,end_id)	{		
		if(lbEndFlag)	return;
		
		if(elm.children().size() > 0)	{
			elm.children().each(function()	{
				if(lbEndFlag)	return false;
				if($(this).children().size() > 0)	{
					if($(this).attr('id') == end_id || $(this).get(0).tagName.toLowerCase() == 'body')	lbEndFlag = true;
					traverseLbChild($(this),end_id);
				}
				else	{
					if($(this).attr('id') == end_id || $(this).get(0).tagName.toLowerCase() == 'body')	lbEndFlag = true;
					
					if(!haveIgnore($(this)))	{
						$(this).css({'background-color':hightlight_color});
						highlightedObjs.push($(this));
					}
				}
			});
		}
		else	{
			if(elm.attr('id') == end_id || elm.get(0).tagName.toLowerCase() == 'body')	lbEndFlag = true;
			if(!haveIgnore(elm))	{
				elm.css({'background-color':hightlight_color});
				highlightedObjs.push(elm);
			}
		}
	};
		
	var travaseNext = function(elm,end_id)	{
		elm.nextAll().each(function()	{
			if(lbEndFlag)	return false;

			traverseLbChild($(this),end_id);
		});
		return elm;
	};
	
	var haveIgnore = function(jqObj)	{
		return ignore_classnames.some(function(className, index)	{
			return jqObj.hasClass(className);
		});
	};
	
	return {		
		go:function(from,to,option)	{	
			console.log(from+','+to)
			
			if(!$('span.lb[id="'+from+'"]').get(0) || !$('span.lb[id="'+to+'"]').get(0))	{
				alert('起始或結束物件找不到，不做highlight');
				return false;
			}
			
			var ps = this;
			ps.reset();
			lbEndFlag = false;
			if(highlight_target === 'anchor')	{
				
				//如果from和to同一行，則end_id取from起算的的"下一個" anchor
				var end_id = (from == to ? $('span.lb').get($('span.lb').index($('span.lb[id="'+from+'"]'))+1).id : to);
				
				travaseNext($('span.lb[id="'+from+'"]'),end_id);
				
				//如果還是沒找到end，就往parent找，找到為止
				if(!lbEndFlag)	{
					var parent = $('span.lb[id="'+from+'"]').parent();
					while(!lbEndFlag)	{
						elm = travaseNext(parent,end_id);
						parent = elm.parent();
					}
				}				
			}
		},
		reset:function()	{				
			if(!highlightedObjs)	return;			
			for(var i = 0 ; i < highlightedObjs.length ; i++)	{
				highlightedObjs[i].css({'background-color':'inherit'});	//原本的背景色
			}
			highlightedObjs = [];
		}
	}
};