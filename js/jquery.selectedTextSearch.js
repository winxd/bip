//author:winxd@dila.edu.tw

(function($) {
    $.fn.selectedTextSearch = function(menu,cb,customIgnore) {		
		var selectedText = '',
			lastSelectedText = '';
			enabled = true;
		
		//create menu
		(function()	{
			var create_menu_html = [/*'<li class="ddbc-selectedMenu-dropdown-header">整合功能</li>'*/],
				menu_item_counter = 0;
				
			for(var menu_index in menu)	{
				create_menu_html.push('<li><a url="'+menu_index+'" href="javascript:void(null)" '+(menu_index === 'copy'? 'data-clipboard-target="#normal_copy_elm"':'')+'>'+(menu[menu_index].icon ? '<img src="'+menu[menu_index].icon+'" style="margin-right:5px" />':'')+(menu[menu_index].fa ? '<i class="fa '+menu[menu_index].fa+'"></i>':'')+menu[menu_index].title+'</a></li>'+((++menu_item_counter) == Object.size(menu) ? '':'<li class="divider"></li>'));
			}
			$('body').append('<ul class="ddbc-selectedMenu-dropdown-menu">'+create_menu_html.join('')+'</ul>');
		})();
		
			
		//get反白文字
		var getSelectedText = function()	{
			var IE = (/msie/.test(navigator.userAgent.toLowerCase())) ? true : false;
			var range;
			if(IE)	{
				range = document.selection.createRange();
				return $.trim(range.text);
			}
			else	{
				range = document.getSelection();
				return $.trim(range.toString());
			}			
		};
		
		
		//events
		var thisJQObj = this;	//指向juqery object，即$(xxx).selectedTextSearch 的 $(xxx)
		$(document).ready(function()	{
		
			thisJQObj.mouseup(function(e)	{
				if(!_GLOBALS.showContextMenu || _GLOBALS.panelFrameDrag || _GLOBALS.sideFrameDrag || $(e.target).parents('#reading-area-1-container').hasClass('home-showing') || $(e.target).parents('#cbeta-copyright').length > 0 || $(e.target).parents('.display_copyright_btn').length > 0)	{	//用全域的_GLOBALS.showContextMenu變數判斷是否顯示選單、以及如果還在拖曳調整panel中、或是首頁顯示中、或是版權資訊區塊，就不跳出選單，用全域變數不是很好，但先將就...
					return;
				}
				
				/*
				if(!enabled)	{	//如果是enable變數阻擋的話，表示其反白範圍有誤，要*特別*把反白取消掉，所以這個if要獨立處理
					window.getSelection().removeAllRanges();	//如果不做的話要把反白取消掉，否則下一次會拿這次的反白
					return;
				}
				*/

				selectedText = getSelectedText();
				if(selectedText)	{
					var top = (e.pageY+4);
					var left = (e.pageX+4);
					if(top + $('.ddbc-selectedMenu-dropdown-menu').height() > $(window).height())	top -= (top + $('.ddbc-selectedMenu-dropdown-menu').height())-$(window).height()+30;	//如果選單超過視窗可視範圍(太下面)，則調整top值
					if(left + $('.ddbc-selectedMenu-dropdown-menu').width() > $(window).width())	left -= (left + $('.ddbc-selectedMenu-dropdown-menu').width())-$(window).width()+30;	//如果選單超過視窗可視範圍(太右邊)，則調整left值
					
					$('.ddbc-selectedMenu-dropdown-menu').css({'left':left+'px','top':top+'px'}).show();
					
				}
				else	{
					$('.ddbc-selectedMenu-dropdown-menu').hide();
				}
				
				lastSelectedText = selectedText;
				selectedText = '';
				e.stopPropagation();
			});
			
			/*
			$(document).mousedown(function(e)	{	//判斷滑鼠按下時，是否在我們指定作用的框框裡面
				enabled = $(e.target).parents('#'+thisJQObj.attr('id')).size() >= 1 ? true:false;		
			});
			*/
			
			thisJQObj.mousedown(function(e)	{	//反白未消失時又按滑鼠click、則反白消失、選單跳出 << 防止這種bug情況，在作用範圍(thisObj)內，跳出的選單外面click的話，則反白要讓它消失(只有_GLOBALS.showContextMenu才需作動)
				if($(e.target).parents('.ddbc-selectedMenu-dropdown-menu').length <= 0 && _GLOBALS.showContextMenu)	{
					window.getSelection().removeAllRanges();
				}
				_GLOBALS.isTouchSelection = false;
			});
			
			$(document).mouseup(function(e)	{
				$('.ddbc-selectedMenu-dropdown-menu').hide();
				selectedText = '';
			});
			
			$(document).on('click','.ddbc-selectedMenu-dropdown-menu > li > a',function()	{				
				if(customIgnore)	{
					lastSelectedText = lastSelectedText.replace(new RegExp(customIgnore.join('|'),'g'),'');
				}
				cb($(this),lastSelectedText);
			});
			
		});
    };
})(jQuery);
