/*
bip專用之全文搜尋範圍限制模組，注意：須搭配bootstrap 3.0和dynatree.js方可使用
2016.08.10:增加i18n全域變數_GLOBALS，需注意！！
作者：winxd
*/
var ContextSearchRange = {
	ppp:this,
	active:false,
	lll:function(node,ppp)	{
		if(node.childList)	ppp.lll(node.childList,ppp);
		for(var n of node.childList)	{
			if(n.li && n.li.hidden)
				n.deactivate();
		}
	},
	custom_range_template:`<div class="row p-1">
					<div class="col-md-4 p-2" style="height:420px; overflow:auto; border-right: 1px solid #ddd;">
					<ol id="custom-set"></ol>
					</div>
					<div id="custom-range-right-container" class="col-md-8" style="height:420px;">
					  <div id="custom-range-add-jing-container" class="col-lg-12 mt-2 mb-2 small" style="display:none">
						<span data-lang-id="26_63">${_GLOBALS[_GLOBALS.lang]['26_63']}</span>
						<select class="form-control form-control-sm custom-range-add-jing-autoComplete" name="simple_select" placeholder="type to search..." data-url="getData.php?type=jing_typeahead" autocomplete="off"></select>
					  </div>
					  <table class="table" id="custom-jing">
						<thead style="display:none">
						  <tr>
							<th>經典</th>
							<th></th>
						  </tr>
						</thead>
						<tbody>
						  <tr colspan="3">
							<td>無資料 / No Data</td>
						  </tr>
						</tbody>
					  </table>
					</div>
				</div>			`,
	template:''+
		'<div class="modal fade" id="jing_selector_contextSearch_dynatree_modal" tabindex="-1" role="dialog" aria-labelledby="myJSCSDynatreeLabel" aria-hidden="true" data-backdrop="static">'+
			'<div class="modal-dialog" style="">'+
				'<div class="modal-content" style="width:800px">'+
					'<div class="modal-header">'+
						'<h4 class="modal-title" id="myJSCSDynatreeLabel" data-lang-id="3_1" style="display: inline-block;">'+_GLOBALS[_GLOBALS.lang]['3_1']+'</h4>'+
						'<button id="JSCS_close" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
						
					'</div>'+
					'<div class="modal-body" style="">'+
						'<ul id="JSCS_tabs_container" class="nav nav-tabs" role="tablist">'+
							'<li class="nav-item active"><a class="nav-link active" href="#JSCS_tab_bulei" role="tab" data-toggle="tab" style="padding:6px;" data-lang-id="3_2">'+_GLOBALS[_GLOBALS.lang]['3_2']+'</a></li>'+
							'<li class="nav-item"><a class="nav-link" href="#JSCS_tab_vol" role="tab" data-toggle="tab" style="padding:6px;" data-lang-id="3_3">'+_GLOBALS[_GLOBALS.lang]['3_3']+'</a></li>'+
							'<li class="nav-item"><a class="nav-link" href="#JSCS_tab_author" role="tab" data-toggle="tab" style="padding:6px;" data-lang-id="3_6">'+_GLOBALS[_GLOBALS.lang]['3_6']+'</a></li>'+
							'<li class="nav-item"><a class="nav-link" href="#JSCS_tab_dynasty" role="tab" data-toggle="tab" style="padding:6px;" data-lang-id="3_7">'+_GLOBALS[_GLOBALS.lang]['3_7']+'</a></li>'+
							'<li class="nav-item"><a class="nav-link" href="#JSCS_tab_custom" role="tab" data-toggle="tab" style="padding:6px;display:" data-lang-id="26_54">'+_GLOBALS[_GLOBALS.lang]['26_54']+'</a></li>'+
						'</ul>'+
						'<div class="tab-content">'+
							'<div class="tab-pane active" id="JSCS_tab_bulei"><input id="JSCS_tab_bulei_kw_filter" type="text" class="form-control input-sm" value="" placeholder="name to search..." style="width: 20em;margin: 1em;" /><div id="JSCS_tree_bulei" style="height:450px;overflow:auto"></div><button id="JSCS_tree_bulei_save" class="btn btn-success" style="display:block;margin:auto" data-lang-id="3_5">'+_GLOBALS[_GLOBALS.lang]['3_5']+'</button></div>'+
							'<div class="tab-pane" id="JSCS_tab_vol"><input id="JSCS_tab_vol_kw_filter" type="text" class="form-control input-sm" value="" placeholder="name to search..." style="width: 20em;margin: 1em;" /><div id="JSCS_tree_vol" style="height:450px;overflow:auto"></div><button id="JSCS_tree_vol_save" class="btn btn-success" style="display:block;margin:auto" data-lang-id="3_5">'+_GLOBALS[_GLOBALS.lang]['3_5']+'</button></div>'+
							'<div class="tab-pane" id="JSCS_tab_author"><input id="JSCS_tab_author_kw_filter" type="text" class="form-control input-sm" value="" placeholder="name to search..." style="width: 20em;margin: 1em;" /><div id="JSCS_tree_author" style="height:450px;overflow:auto"></div><button id="JSCS_tree_author_save" class="btn btn-success" style="display:block;margin:auto" data-lang-id="3_5">'+_GLOBALS[_GLOBALS.lang]['3_5']+'</button></div>'+
							'<div class="tab-pane" id="JSCS_tab_dynasty"><div id="JSCS_tree_dynasty" style="height:450px;overflow:auto"></div><button id="JSCS_tree_dynasty_save" class="btn btn-success" style="display:block;margin:auto" data-lang-id="3_5">'+_GLOBALS[_GLOBALS.lang]['3_5']+'</button></div>'+
							'<div class="tab-pane" id="JSCS_tab_custom"><div id="JSCS_tree_custom" style="height:450px;overflow:auto;overflow-x:hidden"></div><button id="JSCS_tree_custom_save" class="btn btn-success" style="display:block;margin:auto" data-lang-id="3_5">'+_GLOBALS[_GLOBALS.lang]['3_5']+'</button></div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>',
	
	bulei_cb:null,
	vol_cb:null,
	author_cb:null,
	dynasty_cb:null,
	custom_cb:null,
	close_cb:null,
	init:function(bulei_cb_p,vol_cb_p,author_cb_p,dynasty_cb_p,custom_cb_p,close_cb_p,source_path)	{
		if(!bulei_cb_p || !vol_cb_p || !author_cb_p || !dynasty_cb_p || !custom_cb_p || !close_cb_p)	{
			alert('[winxd:全文檢索搜尋範圍限制]必須給定五個callback參數。');
			return;
		}
		
		var ps = this;
		this.bulei_cb = bulei_cb_p;
		this.vol_cb = vol_cb_p;
		this.author_cb = author_cb_p;
		this.dynasty_cb = dynasty_cb_p;
		this.custom_cb = custom_cb_p;
		this.close_cb = close_cb_p;
		this.source_path = source_path ? source_path:'jing_selector_source';
		
		//apply tamplate to body's tail
		$('body').append(this.template);
		$('#JSCS_tree_custom').html(this.custom_range_template);	//加入自訂範圍的模板
		
		//append css to head
		var custom_styles = `
			#custom-set li::marker {
				color: #17a2b8;
				font-size: 0.8em;
				font-family: monospace;
			}
			#custom-set li{
				min-height: 32px;
				line-height: 32px;
				border-top: 1px solid #ddd;
				font-size: 0.8em;
				padding: 0 5px;
			}
			#custom-set li:hover{
				background-color: aliceblue;
				cursor: pointer;
			}
			#custom-set li:first-child{
				border-top: 0;
			}
			#custom-set .close{
				opacity: 1;
			}
			#custom-set .btn-group{
				margin-left: 5px;
			}
			#custom-set button{
				font-size: 0.5em;
			}

			#custom-jing td {
				padding: .25rem;
				vertical-align: top;
				border-top: 1px solid #dee2e6;
				font-size: 0.75em;
				color: #607D8B;
			}
			#custom-set li.active{
				background: #FFECB3;
				border: 3px solid #dddddd;
				border-bottom: 2px solid #ddd;
			}
			.custom-range-add-jing-autoComplete	{
				display:inline-block;
				width:80%;
			}
		`

		var custom_styleSheet = document.createElement("style");
		custom_styleSheet.type = "text/css";
		custom_styleSheet.innerText = custom_styles;
		document.head.appendChild(custom_styleSheet);	
		
		
		var trees = ['bulei','vol','author','dynasty'];
		for(var tree in trees)	{
			(function(tree,ppp)	{
				$("#JSCS_tree_"+trees[tree]).dynatree({
					checkbox: true,
					selectMode: 3,
					minExpandLevel:2,
					onClick: function(node, event) {
						// We should not toggle, if target was "checkbox", because this
						// would result in double-toggle (i.e. no toggle)
						
						//ppp.lll(node,ppp)
						
						if( node.getEventTargetType(event) == "title" )
							node.toggleSelect();
						
					},					
					cookieId: "dynatree-Cb3",
					idPrefix: "dynatree-Cb3-"
				});				
				$.ajax({
					type:"GET",
					url:ps.source_path+'/'+trees[tree]+'.html',
					success:function(returnStr)	{
						var child = $.trim(returnStr) == 'fail' ? []:eval('('+returnStr+')');
						$("#JSCS_tree_"+trees[tree]).dynatree("option", "children", child);							
						$("#JSCS_tree_"+trees[tree]).dynatree("getTree").reload();
					}
				});
			
			})(tree,this);
		}
		
		$('#JSCS_tree_bulei_save').click(function()	{
			var selKeys = [];
			selKeys = $.map($('#JSCS_tree_bulei').dynatree("getSelectedNodes"), function(node){
				if(node.data.key.match(/([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB|LC|TX|CC)[ABab]?[0-9]{3,4}[A-Za-z]?/g))
					return node.data.key;
			});
			$('#jing_selector_contextSearch_dynatree_modal').modal('hide');
			ps.bulei_cb(selKeys.join(','));
		});
		
		
		$('#JSCS_tree_vol_save').click(function()	{
			var selKeys = [];
			selKeys = $.map($('#JSCS_tree_vol').dynatree("getSelectedNodes"), function(node){
				if(node.data.key.match(/([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB|LC|TX|CC)[ABab]?[0-9]{3,4}[A-Za-z]?/g))
					return node.data.key;
			});
			$('#jing_selector_contextSearch_dynatree_modal').modal('hide');			
			ps.vol_cb(selKeys.join(','));		
		});
		/*
		//作譯者回傳人名id版本備份
		$('#JSCS_tree_author_save').click(function()	{
			var selKeys = [];
			selKeys = $.map($('#JSCS_tree_author').dynatree("getSelectedNodes"),function(node)	{	
				if(node.data.key.match(/A[0-9]{6}/g))
					return node.data.key;	
			});
			$('#jing_selector_contextSearch_dynatree_modal').modal('hide');			
			ps.author_cb(selKeys.join(','));		
		});
		*/
		
		$('#JSCS_tree_author_save').click(function()	{
			var selKeys = [];
			selKeys = $.map($('#JSCS_tree_author').dynatree("getSelectedNodes"),function(node)	{	
				//console.log(node)
				if(node.data.key.match(/([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB|LC|TX|CC)[ABab]?[0-9]{3,4}[A-Za-z]?/g) && !node.data.key.match(/A[0-9]{6}/g) && !node.li)	{	//一般沒有搜尋過濾的情況
					return node.data.key;	
				}
				else if(node.data.key.match(/([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB|LC|TX|CC)[ABab]?[0-9]{3,4}[A-Za-z]?/g) && !node.data.key.match(/A[0-9]{6}/g) && node.li && !node.li.hidden)	{	//有使用搜尋過濾，那麼有被搜尋隱藏的節點不能回傳
					return node.data.key;
				}
			});
			$('#jing_selector_contextSearch_dynatree_modal').modal('hide');			
			ps.author_cb(selKeys.join(','));		
		});

		$('#JSCS_tree_dynasty_save').click(function()	{
			var selKeys = [];
			selKeys = $.map($('#JSCS_tree_dynasty').dynatree("getSelectedNodes"),function(node)	{	
				if(node.data.key.match(/([TXJABCFGHKLMNPQRSUWZDIY]|ZS|ZW|GA|GB|LC|TX|CC)[ABab]?[0-9]{3,4}[A-Za-z]?/g))
					return node.data.key;	
			});
			$('#jing_selector_contextSearch_dynatree_modal').modal('hide');			
			ps.dynasty_cb(selKeys.join(','));	
		});		
		
		$('#JSCS_tree_custom_save').click(function()	{
			var range = $('#JSCS_tree_custom #custom-set li.custom-range-selected-this').data('real-range');
			var key = $('#JSCS_tree_custom #custom-set li.custom-range-selected-this').data('key');
			
			$('#jing_selector_contextSearch_dynatree_modal').modal('hide');			
			ps.custom_cb(range,key);	
		});	

		$('#JSCS_close').on('click', function() {
			ps.close_cb();
		});	
		
		
		//可依部類名字過濾搜尋tree
		$('#JSCS_tab_bulei_kw_filter').keyup(function()	{
			var kw = $.trim($(this).val());
			$("#JSCS_tree_bulei").dynatree("getRoot").search(kw);
			
			if($('#JSCS_tree_bulei li[hidden]').length > 0)	{	//用是否有節點被隱藏來判斷是否有關建字過濾tree
				$('#JSCS_tree_bulei .dynatree-node:eq(0) , #JSCS_tree_bulei .dynatree-has-children').addClass('disable_all_mouse_events');	//有過濾的話，把「選擇全部」節點disable(避免user混淆)
			}
			else	{
				$('#JSCS_tree_bulei .dynatree-node:eq(0) , #JSCS_tree_bulei .dynatree-has-children').removeClass('disable_all_mouse_events');//反之全部顯示無過濾時則允取「選擇全部」
			}
		});

		//可依冊別名字過濾搜尋tree
		$('#JSCS_tab_vol_kw_filter').keyup(function()	{
			var kw = $.trim($(this).val());
			$("#JSCS_tree_vol").dynatree("getRoot").search(kw);
			
			if($('#JSCS_tree_vol li[hidden]').length > 0)	{	//用是否有節點被隱藏來判斷是否有關建字過濾tree
				$('#JSCS_tree_vol .dynatree-node:eq(0) , #JSCS_tree_vol .dynatree-has-children').addClass('disable_all_mouse_events');	//有過濾的話，把「選擇全部」節點disable(避免user混淆)
			}
			else	{
				$('#JSCS_tree_vol .dynatree-node:eq(0) , #JSCS_tree_vol .dynatree-has-children').removeClass('disable_all_mouse_events');//反之全部顯示無過濾時則允取「選擇全部」
			}
		});
		
		//可依作譯者名字過濾搜尋tree
		$('#JSCS_tab_author_kw_filter').keyup(function()	{
			var kw = $.trim($(this).val());
			$("#JSCS_tree_author").dynatree("getRoot").search(kw);
			
			/* //2024-06-27:已成功撰寫有搜尋過濾隱藏時的節點不會被勾選，故下面這段已經不需要了
			if($('#JSCS_tree_author li[hidden]').length > 0)	{	//用是否有節點被隱藏來判斷是否有關建字過濾tree
				$('#JSCS_tree_author .dynatree-node:eq(0) , #JSCS_tree_author .dynatree-has-children').addClass('disable_all_mouse_events');	//有過濾的話，把「選擇全部」節點disable(避免user混淆)
				//$('#JSCS_tree_author .dynatree-node:eq(0) , #JSCS_tree_author .dynatree-has-children').addClass('disable_all_mouse_events').next('ul').filter((i,e) => $(e).find('ul').length <= 0).prev().removeClass('disable_all_mouse_events');	//第二版本，最後一層父節點的人名可以選擇，不要 disable，但還是會遇到一樣的問題，有些人名還是有隱藏問題，此題不能這樣解
			}
			else	{
				$('#JSCS_tree_author .dynatree-node:eq(0) , #JSCS_tree_author .dynatree-has-children').removeClass('disable_all_mouse_events');//反之全部顯示無過濾時則允取「選擇全部」
			}
			*/
		});
		
		this.active = true;
	},
	show:function()	{
		if(this.active)	{
			//有需要將樹重整成初始化沒有check的狀態可以用 reload(), ex:$("#JSCS_tree_bulei").dynatree("getTree").reload();
			$("#JSCS_tree_bulei").dynatree("getTree");
			$("#JSCS_tree_vol").dynatree("getTree");
			$("#JSCS_tree_author").dynatree("getTree");
			$("#JSCS_tree_dynasty").dynatree("getTree");
			$('#JSCS_tab_author_kw_filter').val('');
			$('#jing_selector_contextSearch_dynatree_modal').modal();
		}
	},
	active_tab:function(tab_name)	{
		$('#JSCS_tabs_container a[href="#'+tab_name+'"]').trigger('click');
	}
};